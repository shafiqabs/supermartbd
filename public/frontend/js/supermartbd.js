$(document).delegate(".add_cart_ajax",'click',function(e) {
    e.preventDefault();

    var product_id = jQuery(this).attr('product-id');
    var product_slug = jQuery(this).attr('product-slug');
    var product_image = jQuery(this).attr('product-image');
    var product_price = jQuery(this).attr('product-price');
    var product_quantity = jQuery(this).attr('product-quantity');
    var product_name = jQuery(this).attr('product-name');

    var url = jQuery(this).attr('data-href');
    // i=a.closest(".product, .product-popup");
    // Wolmart.Minipopup.open({
    //     productClass:" product-cart",
    //     name:i.find(".product-name, .product-title").text(),
    //     nameLink:i.find(".product-name > a, .product-title > a").attr("href"),
    //     imageSrc:i.find(".product-media img, .product-image:first-child img").attr("src"),
    //     imageLink:i.find(".product-name > a").attr("href"),
    //     message:"<p>has been added to cart:</p>",
    //     actionTemplate:'<a href="cart.html" class="btn btn-rounded btn-sm">View Cart</a><a href="checkout.html" class="btn btn-dark btn-rounded btn-sm">Checkout</a>'
    // });

        jQuery.ajax({
            url: url,
            method: "get",
            data: {product_name:product_name,product_id:product_id,product_slug:product_slug,product_image: product_image,product_price:product_price,product_quantity:product_quantity},
            dataType: "json",
            beforeSend: function (xhr) {

            }
        }).done(function( response ) {
            jQuery('.cart-count').text(response.total_item);
            jQuery('.price').text(response.cart_total);
            jQuery('.products').html(response.cart_body);


        }).fail(function( jqXHR, textStatus ) {

        });
        return false;

});


$(document).delegate("#remove-cart-product",'click',function(e) {
    e.preventDefault();
    var product_id = jQuery(this).attr('product_id');
    var url = jQuery(this).attr('data-href');
    var delivery_charge = jQuery('#delivery_charge').val();

    if (typeof delivery_charge === 'undefined'){
        var delivery_charge = 0;
    }
    var delivery_charge = parseInt(delivery_charge);


    // alert(delivery_charge);
    jQuery.ajax({
        url: url,
        method: "get",
        data: {product_id:product_id},
        dataType: "json",
        beforeSend: function (xhr) {

        }
    }).done(function( response ) {
        // alert(response.cart_total);
        var cart_total_price = parseInt(response.cart_total);
        var total_with_delivery = (Number(cart_total_price)+Number(delivery_charge));
        var total_with_delivery = total_with_delivery.toFixed(2)
        // alert(Number(cart_total_price));
        jQuery('.cart-count').text(response.total_item);
        jQuery('.price').text(response.cart_total);
        jQuery('.products').html(response.cart_body);
        jQuery('#remove_id_'+product_id).hide("slow");
        $('.subtotal_without_shipping').text(response.cart_total);
        $('.subtotal_with_shipping').text(total_with_delivery);
        $('#db_store_total_amount').val(total_with_delivery);

        if (response.total_item == 0){
            jQuery('#empty_message').show("slow");
        }

    }).fail(function( jqXHR, textStatus ) {

    });
    return false;
});


$(document).delegate(".quantity-plus ,.quantity-minus",'click',function(e) {
    e.preventDefault();
    var Quantity = jQuery('.quantity').val();

    document.getElementById("cart_quantity_set").setAttribute("product-quantity",Quantity);
    document.getElementById("qck-add-cart").setAttribute("product-quantity",Quantity);
});

$(document).delegate(".mycart-quantity-plus ,.mycart-quantity-minus",'click',function(e) {
    e.preventDefault();
    var product_id = jQuery(this).attr('product-id');
    var button_status = jQuery(this).attr('button-status');

    var product_quantity = jQuery('#quantity_value_'+product_id).val();
    var product_quantity = parseInt(product_quantity);

    if (button_status == 'plus'){
        var product_quantity = product_quantity+1;
    }

    if (button_status == 'minus'){
        var product_quantity = product_quantity-1;
        if (product_quantity == 0){
            var product_quantity = 1;
        }
    }

    var url = jQuery('#cart_update').attr('data-href');

    jQuery.ajax({
        url: url,
        method: "get",
        data: {product_id:product_id,product_quantity:product_quantity},
        dataType: "json",
        beforeSend: function (xhr) {

        }
    }).done(function( response ) {
        jQuery('.cart-count').text(response.total_item);
        jQuery('.price').text(response.cart_total);
        jQuery('.products').html(response.cart_body);
        $('#quantity_value_'+product_id).val(product_quantity);

        var product_price = jQuery('#product_price_'+product_id).val();
        var product_price = parseInt(product_price);
        var specific_sub_price = product_price*product_quantity;
        $('#amount_'+product_id).text(specific_sub_price);
        $('.subtotal_without_shipping').text(response.cart_total);
        $('.subtotal_with_shipping').text(response.cart_total);
    }).fail(function( jqXHR, textStatus ) {

    });
    return false;
});



$(document).delegate(".input_quantity",'keyup',function(e) {
    e.preventDefault();
    var product_id = jQuery(this).attr('product-id');
    var button_status = jQuery(this).attr('button-status');

    var product_quantity = jQuery(this).val();

    //
    if (product_quantity == 0  || product_quantity == '' || product_quantity == 'NaN' ){
        var product_quantity = 0;
    }
    var product_quantity = parseInt(product_quantity);
    // alert(product_quantity);

    var url = jQuery('#cart_update').attr('data-href');

    jQuery.ajax({
        url: url,
        method: "get",
        data: {product_id:product_id,product_quantity:product_quantity},
        dataType: "json",
        beforeSend: function (xhr) {

        }
    }).done(function( response ) {
        jQuery('.cart-count').text(response.total_item);
        jQuery('.price').text(response.cart_total);
        jQuery('.products').html(response.cart_body);
        $('#quantity_value_'+product_id).val(product_quantity);

        var product_price = jQuery('#product_price_'+product_id).val();
        var product_price = parseInt(product_price);
        var specific_sub_price = product_price*product_quantity;
        $('#amount_'+product_id).text(specific_sub_price);
        $('.subtotal_without_shipping').text(response.cart_total);
        $('.subtotal_with_shipping').text(response.cart_total);
    }).fail(function( jqXHR, textStatus ) {

    });
    return false;
});


$(document).delegate(".cart_clear",'click',function(e) {
    e.preventDefault();
    // confirm("Are You Sure");

    if (confirm('Are You Sure To Empty Cart?')) {

        var url = jQuery('#cart_clear_route').attr('data-href');

        jQuery.ajax({
            url: url,
            method: "get",
            dataType: "json",
            beforeSend: function (xhr) {

            }
        }).done(function (response) {
            if (response.cart_empty) {
                jQuery('.cart-count').text(0);
                jQuery('.price').text('0.00');
                jQuery('.products').html(response.cart_body);
                jQuery('.total_cart_hide').hide("slow");
                jQuery('#empty_message').show("slow");

                $('.subtotal_without_shipping').text('0.00');
                $('.subtotal_with_shipping').text('0.00');
            }

        }).fail(function (jqXHR, textStatus) {

        });
        return false;
    }
});

// Add customer address
$(document).delegate('#bill_country_id','change',function () {
    var sl_no = $(this).val();
    // alert(sl_no);
    if(sl_no == 19){
        $('.bill_district_id').show('slow');
        $('.bill_upazila_id').show('slow');
    }else{
        $('.bill_district_id').hide('slow');
        $('.bill_upazila_id').hide('slow');
    }
});

$(document).delegate('#ship_country_id','change',function () {
    var sl_no = $(this).val();
    // alert(sl_no);
    if(sl_no == 19){
        $('.ship_district_id').show('slow');
        $('.ship_upazila_id').show('slow');
    }else{
        $('.ship_district_id').hide('slow');
        $('.ship_upazila_id').hide('slow');
    }
});

$(document).delegate('#bill_district_id,#ship_district_id','change',function () {
    var id = $(this).val();
    var type = $(this).attr('type');


    var url = $('#findupazalaroute').attr('data-href');
    // alert(id+' '+type+' '+url);
    $.ajax({
        url: url,
        method: "GET",
        dataType: "json",
        data: {id: id},
        beforeSend: function( xhr ) {

        }
    }).done(function( response ) {
        if(response.message == 'success'){
            if(type == 'bill'){
                $('#bill_upazila_id')
                    .find('option')
                    .remove()
                    .end()
                    .append(response.content)
                ;
            }
            if(type == 'ship'){
                $('#ship_upazila_id')
                    .find('option')
                    .remove()
                    .end()
                    .append(response.content)
                ;
            }
        }else {
            alert(response.message);
        }
    }).fail(function( jqXHR, textStatus ) {

    });
    return false;
});


$(document).delegate("#CreateAccount",'click',function() {
    var name = jQuery('#name').val();

    if (name == ''){
        var validation = false;
        var message = 'Name must be filled';
    }else{
        var email = document.getElementById('email').value;
        if(email == ''){
            var validation = false;
            var message = 'Email Must be Filled';
        }else{
            function isValidEmailAddress(email) {
                var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
                return pattern.test(email);
            }
            if (!isValidEmailAddress(email)){
                var validation = false;
                var message = 'Email must be valid';
            }else{
                var mobile = document.getElementById('mobile').value;
                if(mobile == ''){
                    var validation = false;
                    var message = 'Mobile Must be filled';
                }else{
                    if (mobile%1 != 0){
                        var validation = false;
                        var message = 'Mobile Must be Number';
                    }else{
                        var mobilelenght = mobile.length;
                        if (mobilelenght != 11){
                            var validation = false;
                            var message = 'Mobile Length '+mobilelenght+' but Must be 11';
                        }else{
                            var address = document.getElementById('address').value;
                            if (address == ''){
                                var validation = false;
                                var message = 'Address Must be filled';
                            }else{
                                var image = document.getElementById('file').value;
                                if (image == '') {
                                    var message = 'Image Must Be Select';
                                    var validation = false;
                                }else{
                                    var image1 = document.getElementById("file").files[0];
                                    var t = image1.type.split('/').pop().toLowerCase();
                                    if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
                                        var message = 'Please select a valid image file';
                                        var validation = false;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if (validation == false){
        Swal.fire({
            title: message,
            showClass: {
                popup: 'animate__animated animate__fadeInDown'
            },
            hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
            },
            icon:'question',
            showCloseButton: true,
            showCancelButton: true,
            focusConfirm: false,
        })
        return false;
    }else{
        return true;
    }
});


// For filter size unit filter
$(document).delegate('.size-unit-filter','click',function () {
    var checkedValue = null;
    var checkedValue1 = '';
    var inputElements = document.getElementsByClassName('size-unit-filter');
    for(var i=0; inputElements[i]; ++i){
        if(inputElements[i].checked){
            checkedValue = inputElements[i].value;
            checkedValue1 = checkedValue1+','+checkedValue;
        }
    }
    document.getElementById("size-filter").value = checkedValue1;
    $( "#filter-submit" ).submit();
});

// For filter color filter
$(document).delegate('.color-filter','click',function () {
    var checkedValue = null;
    var checkedValue1 = '';
    var inputElements = document.getElementsByClassName('color-filter');
    for(var i=0; inputElements[i]; ++i){
        if(inputElements[i].checked){
            checkedValue = inputElements[i].value;
            checkedValue1 = checkedValue1+','+checkedValue;
        }
    }
    document.getElementById("color-filter").value = checkedValue1;
    $( "#filter-submit" ).submit();
});

// For filter price filter
$(document).delegate('.price-filter','click',function () {
    var checkedValue = null;
    var checkedValue1 = '';
    var inputElements = document.getElementsByClassName('price-filter');
    for(var i=0; inputElements[i]; ++i){
        if(inputElements[i].checked){
            checkedValue = inputElements[i].value;
            checkedValue1 = checkedValue1+','+checkedValue;
        }
    }
    document.getElementById("price-filter").value = checkedValue1;
    $( "#filter-submit" ).submit();
});

