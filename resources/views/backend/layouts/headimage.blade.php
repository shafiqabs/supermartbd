{{--<link rel="apple-touch-icon" href="{{ asset('backend/image/favicons/apple-touch-icon.png') }}" sizes="180x180">--}}
{{--<link rel="icon" href="{{ asset('backend/image/favicons/favicon-32x32.png') }}" sizes="32x32" type="image/png">--}}
{{--<link rel="icon" href="{{ asset('backend/image/favicons/favicon-16x16.png') }}" sizes="16x16" type="image/png">--}}
<link rel="icon" type="image/png" href="{{ asset('frontend/images/demos/demo3/logo/Logo.png') }}">

{{--<link rel="manifest" href="{{ asset('backend/image/favicons/manifest.json') }}">--}}
{{--<link rel="mask-icon" href="{{ asset('backend/image/favicons/safari-pinned-tab.svg') }}" color="#7952b3">--}}
{{--<link rel="icon" href="{{ asset('backend/image/favicons/favicon.ico') }}">--}}
<meta name="theme-color" content="#7952b3">
