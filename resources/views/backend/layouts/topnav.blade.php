<nav class="navbar navbar-expand navbar-light bg-light print-header-content">
    <ul class="navbar-nav me-auto">
        <li class="nav-item active">
            <a id="sidebar-toggle" class="sidebar-toggle nav-link" href="#">
                <i class="fas fa-bars"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">{{Auth::user()->name}}</a>
        </li>


        <li class="nav-item">
            <a class="nav-link" href="#">
                @if(Auth::user()->type == 'Vendor')
                {{Auth::user()->UserVendor->store_name}}
                @endif
            </a>
        </li>
    </ul>
    <ul class="navbar-nav ms-auto">
        <li class="nav-item">
            <a class="nav-link" href="#">Right</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">{{Auth::user()->type}}</a>
        </li>
    </ul>

    <div class="dropdown text-end">
        <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
            <img src="https://github.com/mdo.png" alt="mdo" class="rounded-circle" width="32" height="32">
        </a>
        <ul class="dropdown-menu text-small" aria-labelledby="dropdownUser1" style="">
            <li><a class="dropdown-item" href="#">New project...</a></li>
            <li><a class="dropdown-item" href="#">Settings</a></li>
            <li><a class="dropdown-item" href="#">Profile</a></li>
            <li><hr class="dropdown-divider"></li>
            <li>
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    {{ __('Sign out') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>
        </ul>
    </div>
</nav>
