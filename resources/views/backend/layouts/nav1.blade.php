<div class="sidebar">
    <div class="sidebar-header">
        <a href="#" class="sidebar-logo">
{{--            <img src="{{ asset('backend/image/logo/Supermartbd3.png') }}" width="60px" height="60px" alt="">--}}
            <img src="{{ asset('frontend/images/demos/demo3/logo/Logo.png') }}" alt="logo" width="100px" height="60px" />

        </a>
        <a href="#" class="sidebar-logo-text">MASABA<span> </span></a>
    </div><!-- sidebar-header -->
    <div class="sidebar-search">
        <div class="search-body">
            <i data-feather="home"></i>
            <a href="{{url('/home')}}" style="color: #000000b3">Dashboard</a>
            {{--
            <input type="text" class="form-control" placeholder="Search...">--}}
        </div><!-- search-body -->
    </div><!-- sidebar-search -->
    <div class="sidebar-body pt-20">

        {{--@if(Module::has('Quran'))
            @include('quran::layouts.nav')
        @endif--}}

        @if(Auth::user()->type == 'Admin')
            <div class="nav-group {{ Request::is('admin-banner-create') ? 'active show' : ''}}{{ Request::is('admin-banner-index') ? 'active show' : ''}}{{ Request::is('admin-banneritem-create') ? 'active show' : ''}}{{ Request::is('admin-banneritem-index') ? 'active show' : ''}}{{ Request::is('admin-banner-edit/*') ? 'active show' : ''}}{{ Request::is('admin-banneritem-edit/*') ? 'active show' : ''}}">
                <div class="nav-group-label">Manage Banner</div>
                <ul class="nav-sidebar">
                    <li class="nav-item">
                        <a href="{{route('admin.banner.index')}}" class="nav-link {{ Request::is('admin-banner-index') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Banner List</span></a>
                        <a href="{{route('admin.banneritem.index')}}" class="nav-link {{ Request::is('admin-banneritem-index') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Banner Item List</span></a>
                    </li>
                </ul>
            </div>


            <div class="nav-group {{ Request::is('admin-slider-index') ? 'active show' : ''}}{{ Request::is('admin-slider-create') ? 'active show' : ''}}{{ Request::is('admin-slider-edit/*') ? 'active show' : ''}}">
                <div class="nav-group-label">Manage Slider</div>
                <ul class="nav-sidebar">
                    <li class="nav-item">
                        <a href="{{route('admin.slider.create')}}" class="nav-link {{ Request::is('admin-slider-create') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>New Slider</span></a>
                        <a href="{{route('admin.slider.index')}}" class="nav-link {{ Request::is('admin-slider-index') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Slider List</span></a>
                    </li>
                </ul>
            </div>


            <div class="nav-group {{ Request::is('admin-order-index') ? 'active show' : ''}}{{ Request::is('admin-order-details/*') ? 'active show' : ''}}">
                <div class="nav-group-label">Manage Order</div>
                <ul class="nav-sidebar">
                    <li class="nav-item">
                        <a href="{{route('admin.order.index')}}" class="nav-link {{ Request::is('admin-order-index') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Order List</span></a>
                    </li>
                </ul>
            </div>


            <div class="nav-group {{ Request::is('admin-product-productcreate') ? 'active show' : ''}}{{ Request::is('admin-product-filecreate') ? 'active show' : ''}}{{ Request::is('admin-product-fileindex') ? 'active show' : ''}}{{ Request::is('admin-product-productedit/*') ? 'active show' : ''}}">
                <div class="nav-group-label">Manage Product</div>
                <ul class="nav-sidebar">
                    <li class="nav-item">
                        <a href="{{route('admin.product.fileindex')}}" class="nav-link {{ Request::is('admin-product-fileindex') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Product List</span></a>
                        <a href="{{route('admin.product.productcreate')}}" class="nav-link {{ Request::is('admin-product-productcreate') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>New Product</span></a>
                        <a href="{{route('admin.product.filecreate')}}" class="nav-link {{ Request::is('admin-product-filecreate') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>File Import</span></a>
                    </li>
                </ul>
            </div>

            <div class="nav-group {{ Request::is('admin-customer-index') ? 'active show' : ''}}{{ Request::is('admin-customer-create') ? 'active show' : ''}}{{ Request::is('admin-customer-edit/*') ? 'active show' : ''}}">
                <div class="nav-group-label">Manage Customer</div>
                <ul class="nav-sidebar">
                    <li class="nav-item">
                        <a href="{{route('admin.customer.create')}}" class="nav-link {{ Request::is('admin-customer-create') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>New Customer</span></a>
                        <a href="{{route('admin.customer.index')}}" class="nav-link {{ Request::is('admin-customer-index') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Customer List</span></a>
                    </li>
                </ul>
            </div>

            <div class="nav-group {{ Request::is('admin-vendor-index') ? 'active show' : ''}}{{ Request::is('admin-vendor-create') ? 'active show' : ''}}{{ Request::is('admin-vendor-edit/*') ? 'active show' : ''}}">
                <div class="nav-group-label">Manage Vendor</div>
                <ul class="nav-sidebar">
                    <li class="nav-item">
                        <a href="{{route('admin.vendor.create')}}" class="nav-link {{ Request::is('admin-vendor-create') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>New Vendor</span></a>
                        <a href="{{route('admin.vendor.index')}}" class="nav-link {{ Request::is('admin-vendor-index') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Vendor List</span></a>
                    </li>
                </ul>
            </div>

            <div class="nav-group {{ Request::is('admin-user-index') ? 'active show' : ''}}{{ Request::is('admin-user-create') ? 'active show' : ''}}{{ Request::is('admin-user-edit/*') ? 'active show' : ''}}">
                <div class="nav-group-label">Manage User</div>
                <ul class="nav-sidebar">
                    <li class="nav-item">
                        <a href="{{route('admin.user.create')}}" class="nav-link {{ Request::is('admin-user-create') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>New User</span></a>
                        <a href="{{route('admin.user.index')}}" class="nav-link {{ Request::is('admin-user-index') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>User List</span></a>
                    </li>
                </ul>
            </div>

            <div class="nav-group
{{ Request::is('admin-itemunit-create') ? 'active show' : ''}}{{ Request::is('admin-itemunit-edit/*') ? 'active show' : ''}}
{{ Request::is('admin-sizeunit-create') ? 'active show' : ''}}{{ Request::is('admin-sizeunit-edit/*') ? 'active show' : ''}}
{{ Request::is('admin-sizeweightdimension-create') ? 'active show' : ''}}{{ Request::is('admin-sizeweightdimension-edit/*') ? 'active show' : ''}}
{{ Request::is('admin-brand-create') ? 'active show' : ''}}{{ Request::is('admin-brand-edit/*') ? 'active show' : ''}}{{ Request::is('admin-brand-index') ? 'active show' : ''}}
{{ Request::is('admin-promotion-create') ? 'active show' : ''}}{{ Request::is('admin-promotion-edit/*') ? 'active show' : ''}}{{ Request::is('admin-promotion-index') ? 'active show' : ''}}
{{ Request::is('admin-country-create') ? 'active show' : ''}}{{ Request::is('admin-country-edit/*') ? 'active show' : ''}}
{{ Request::is('admin-category-create') ? 'active show' : ''}}{{ Request::is('admin-category-edit/*') ? 'active show' : ''}}{{ Request::is('admin-category-index') ? 'active show' : ''}}
{{ Request::is('admin-warningtype-create') ? 'active show' : ''}}{{ Request::is('admin-warningtype-edit/*') ? 'active show' : ''}}
{{ Request::is('admin-itemassurance-create') ? 'active show' : ''}}{{ Request::is('admin-itemassurance-edit/*') ? 'active show' : ''}}
{{ Request::is('admin-discount-create') ? 'active show' : ''}}{{ Request::is('admin-discount-edit/*') ? 'active show' : ''}}{{ Request::is('admin-discount-index') ? 'active show' : ''}}
{{ Request::is('admin-color-create') ? 'active show' : ''}}{{ Request::is('admin-color-edit/*') ? 'active show' : ''}}
{{ Request::is('admin-timeslot-create') ? 'active show' : ''}}{{ Request::is('admin-timeslot-edit/*') ? 'active show' : ''}}

                ">
                <div class="nav-group-label">Master Data</div>
                <ul class="nav-sidebar">
                    <li class="nav-item">
                        <a href="{{route('admin.itemunit.create')}}" class="nav-link {{ Request::is('admin-itemunit-create') ? 'active' : ''}}{{ Request::is('admin-itemunit-edit/*') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Item Unit</span></a>
                        <a href="{{route('admin.sizeunit.create')}}" class="nav-link {{ Request::is('admin-sizeunit-create') ? 'active' : ''}}{{ Request::is('admin-sizeunit-edit/*') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Size Unit</span></a>
                        <a href="{{route('admin.sizeweightdimension.create')}}" class="nav-link {{ Request::is('admin-sizeweightdimension-create') ? 'active' : ''}}{{ Request::is('admin-sizeweightdimension-edit/*') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Size/Weight/Dimension</span></a>
                        <a href="{{route('admin.brand.index')}}" class="nav-link {{ Request::is('admin-brand-index') ? 'active' : ''}}{{ Request::is('admin-brand-create') ? 'active' : ''}}{{ Request::is('admin-brand-edit/*') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Brand</span></a>
                        <a href="{{route('admin.promotion.index')}}" class="nav-link {{ Request::is('admin-promotion-index') ? 'active' : ''}}{{ Request::is('admin-promotion-create') ? 'active' : ''}}{{ Request::is('admin-promotion-edit/*') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Promotion</span></a>
                        <a href="{{route('admin.country.create')}}" class="nav-link {{ Request::is('admin-country-create') ? 'active' : ''}}{{ Request::is('admin-country-index') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Country</span></a>
                        <a href="{{route('admin.category.index')}}" class="nav-link {{ Request::is('admin-category-index') ? 'active' : ''}}{{ Request::is('admin-category-create') ? 'active' : ''}}{{ Request::is('admin-category-edit/*') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Category</span></a>
                        <a href="{{route('admin.warningtype.create')}}" class="nav-link {{ Request::is('admin-warningtype-create') ? 'active' : ''}}{{ Request::is('admin-warningtype-edit/*') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Warning Type</span></a>
                        <a href="{{route('admin.warningtype.create')}}" class="nav-link {{ Request::is('admin-warningtype-create') ? 'active' : ''}}{{ Request::is('admin-warningtype-edit/*') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Warning Type</span></a>
                        <a href="{{route('admin.itemassurance.create')}}" class="nav-link {{ Request::is('admin-itemassurance-create') ? 'active' : ''}}{{ Request::is('admin-itemassurance-edit/*') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Item Assurance</span></a>
                        <a href="{{route('admin.discount.index')}}" class="nav-link {{ Request::is('admin-discount-index') ? 'active' : ''}}{{ Request::is('admin-discount-create') ? 'active' : ''}}{{ Request::is('admin-discount-edit/*') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Discount</span></a>
                        <a href="{{route('admin.color.create')}}" class="nav-link {{ Request::is('admin-color-create') ? 'active' : ''}}{{ Request::is('admin-color-edit/*') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Color</span></a>
                        <a href="{{route('admin.timeslot.create')}}" class="nav-link {{ Request::is('admin-timeslot-create') ? 'active' : ''}}{{ Request::is('admin-timeslot-edit/*') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Delivery Time Slot</span></a>
                    </li>
                </ul>
            </div>


            <div class="nav-group {{ Request::is('admin-generalpage-index') ? 'active show' : ''}}{{ Request::is('admin-generalpage-create') ? 'active show' : ''}}{{ Request::is('admin-generalpage-edit/*') ? 'active show' : ''}}">
                <div class="nav-group-label">General Page</div>
                <ul class="nav-sidebar">
                    <li class="nav-item">
                        <a href="{{route('admin.generalpage.create')}}" class="nav-link {{ Request::is('admin-user-create') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>New Page</span></a>
                        <a href="{{route('admin.generalpage.index')}}" class="nav-link {{ Request::is('admin-user-index') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Page List</span></a>
                    </li>
                </ul>
            </div>

            <div class="nav-group {{ Request::is('admin-generalsetting-index') ? 'active show' : ''}}{{ Request::is('admin-generalsetting-create') ? 'active show' : ''}}{{ Request::is('admin-generalsetting-edit/*') ? 'active show' : ''}}{{ Request::is('admin-change-passwordform/*') ? 'active show' : ''}}">
                <div class="nav-group-label">General Setting</div>
                <ul class="nav-sidebar">
                    <li class="nav-item">
{{--                        <a href="{{route('admin.generalsetting.create')}}" class="nav-link {{ Request::is('admin-generalsetting-create') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>New Setting</span></a>--}}
                        <a href="{{route('admin.generalsetting.index')}}" class="nav-link {{ Request::is('admin-generalsetting-index') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Setting List</span></a>
                        <a href="{{route('admin.change.passwordform')}}" class="nav-link {{ Request::is('admin-change-passwordform') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Change Password</span></a>
                    </li>
                </ul>
            </div>
        @endif



        {{--@if(Auth::user()->type == 'Customer')

            <div class="nav-group {{ Request::is('admin-customer-index') ? 'active show' : ''}}{{ Request::is('admin-customer-create') ? 'active show' : ''}}{{ Request::is('admin-customer-edit/*') ? 'active show' : ''}}">
                <div class="nav-group-label">Customer</div>
                <ul class="nav-sidebar">
                    <li class="nav-item">
                        --}}{{--                        <a href="{{route('admin.generalsetting.create')}}" class="nav-link {{ Request::is('admin-generalsetting-create') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>New Setting</span></a>--}}{{--
                        <a href="{{route('admin.customer.index')}}" class="nav-link {{ Request::is('admin-customer-index') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Setting List</span></a>
                    </li>
                </ul>
            </div>
        @endif--}}


        @if(Auth::user()->type == 'Vendor')

            <div class="nav-group {{ Request::is('admin-vendor-index') ? 'active show' : ''}}{{ Request::is('admin-vendor-create') ? 'active show' : ''}}{{ Request::is('admin-vendor-edit/*') ? 'active show' : ''}}">
                <div class="nav-group-label">Vendor</div>
                <ul class="nav-sidebar">
                    <li class="nav-item">
                        <a href="{{route('admin.vendor.index')}}" class="nav-link {{ Request::is('admin-customer-index') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Vendor List</span></a>
                    </li>
                </ul>
            </div>

            <div class="nav-group {{ Request::is('admin-order-index') ? 'active show' : ''}}{{ Request::is('admin-order-create') ? 'active show' : ''}}{{ Request::is('admin-order-details/*') ? 'active show' : ''}}">
                <div class="nav-group-label">Order</div>
                <ul class="nav-sidebar">
                    <li class="nav-item">
                        <a href="{{route('admin.order.index')}}" class="nav-link {{ Request::is('admin-order-index') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Order</span></a>
                    </li>
                </ul>
            </div>

            <div class="nav-group {{ Request::is('admin-product-productcreate') ? 'active show' : ''}}{{ Request::is('admin-product-filecreate') ? 'active show' : ''}}{{ Request::is('admin-product-fileindex') ? 'active show' : ''}}{{ Request::is('admin-product-productedit/*') ? 'active show' : ''}}">
                <div class="nav-group-label">Manage Product</div>
                <ul class="nav-sidebar">
                    <li class="nav-item">
                        <a href="{{route('admin.product.fileindex')}}" class="nav-link {{ Request::is('admin-product-fileindex') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>Product List</span></a>
                        <a href="{{route('admin.product.productcreate')}}" class="nav-link {{ Request::is('admin-product-productcreate') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>New Product</span></a>
                        <a href="{{route('admin.product.filecreate')}}" class="nav-link {{ Request::is('admin-product-filecreate') ? 'active' : ''}}"><i data-feather="arrow-right"></i><span>File Import</span></a>
                    </li>
                </ul>
            </div>
        @endif




    </div><!-- sidebar-body -->


    <div class="sidebar-footer">
        <a href="" class="avatar online"><span class="avatar-initial" style="font-size: 15px;">SM</span></a>
        <div class="avatar-body">
            <div class="d-flex align-items-center justify-content-between">
                <h6>{{auth()->user()?auth()->user()->name:''}}</h6>
                <a href="" class="footer-menu"><i class="ri-settings-4-line"></i></a>
            </div>
            <span>
                {{--@php
                    $roles = auth()->user()->getRoleNames();
                    foreach ($roles as $role){
                        if (!empty($role)){
                            echo $role.' <br> ';
                        }
                    }
                @endphp--}}
            </span>
        </div><!-- avatar-body -->
    </div><!-- sidebar-footer -->
</div>

