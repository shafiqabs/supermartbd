<script src="{{ asset('backend/js/jquery-3.6.0.min.js') }}"></script>

<script src="{{ asset('backend/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('backend/js/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('backend/js/next-sidebar.js') }}"></script>

<script src="{{ asset('backend/js/feather.min.js') }}" ></script>
<script src="{{ asset('backend/js/dashboard.js') }}"></script>

<!-- for datatable js -->
<script src="{{ asset('backend/js/jquery.dataTables.js') }}"></script>
<!-- for select2 & multiple select -->
<script src="{{ asset('backend/js/select2.min.js') }}"></script>
<!-- for remodel popup -->
<script src="{{ asset('backend/js/zepto.js') }}"></script>
<script src="{{ asset('backend/js/remodal.js') }}"></script>
<!-- for auto complete -->
<script src="{{ asset('backend/js/auto-complete.js') }}"></script>
<!-- for kinzi print  -->
<script src="{{ asset('backend/js/kinzi.print.min.js') }}"></script>
<!-- for graph  -->
<script src="{{ asset('backend/js/raphael-min.js') }}"></script>
<script src="{{ asset('backend/js/morris.min.js') }}"></script>
<script src="{{ asset('backend/js/morris-main.js') }}"></script>
<!-- for material component js -->
<script src="{{ asset('backend/js/material-components-web.min.js') }}"></script>
<!-- for form validation -->
<script src="{{ asset('backend/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('backend/js/additional-methods.min.js') }}"></script>
<!-- for tooltip validation -->
<script src="{{ asset('backend/js/jquery.checkify.min.js') }}"></script>
<!-- for tooltip validation sms show -->
<script src="{{ asset('backend/js/tooltip-main.js') }}"></script>

<script src="{{ asset('backend/dashnav/lib/feathericons/feather.min.js') }}"></script>
<script src="{{ asset('backend/dashnav/lib/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('backend/dashnav/assets/js/script.js') }}"></script>

<!-- for plugin js default js  -->
<script src="{{ asset('backend/js/main-plugin.js') }}"></script>
{{--<script src="{{ asset('backend/js/editor.js') }}"></script>--}}
<script src="//cdn.ckeditor.com/4.16.2/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('editor');
    //for more attachment Product edit page
    $(document).delegate('#MoreAttachment','click',function () {
        var sl_no = $(this).attr('sl-no');
        var sl_no = parseInt(sl_no);
        var route = $('#moreattachroute').attr('data-href');
        // alert(sl_no+' '+route) ;
        sl_no += 1;
        $.ajax({
            url: route,
            method: "GET",
            dataType: "json",
            data: {sl_no: sl_no},
            beforeSend: function( xhr ) {

            }
        }).done(function( response ) {
            $('#add_more_attach_row').append(response.content);
            document.getElementById("MoreAttachment").setAttribute("sl-no",response.sl_no);

            // alert(response);
        }).fail(function( jqXHR, textStatus ) {

        });
        return false;
    });

    //delete more attach form

    $(document).delegate('#delete_click','click',function () {
        var sl_no = $(this).attr('sl_no');
        // alert(sl_no);
        document.getElementById("delete_row_"+sl_no).remove();
    });


    //for more attachment delete
    $(document).delegate('#DeleteMoreAttachment','click',function () {
        var sl_no = $(this).attr('sl-no');
        var route = $('#Deletemoreattachroute').attr('data-href');
        // alert(sl_no+' '+route) ;

        $.ajax({
            url: route,
            method: "GET",
            dataType: "json",
            data: {sl_no: sl_no},
            beforeSend: function( xhr ) {

            }
        }).done(function( response ) {
            $('#hide_'+sl_no).hide();
            // alert(response.ok);
        }).fail(function( jqXHR, textStatus ) {

        });
        return false;
    });


    // for more additional field
    $(document).delegate('#MoreField','click',function () {
        var sl_no = $(this).attr('sl-no');
        var sl_no = parseInt(sl_no);
        var route = $('#MoreFieldroute').attr('data-href');
        // alert(sl_no+' '+route) ;
        sl_no += 1;
        $.ajax({
            url: route,
            method: "GET",
            dataType: "json",
            data: {sl_no: sl_no},
            beforeSend: function( xhr ) {

            }
        }).done(function( response ) {
            $('#add_more_level_row').append(response.content);
            document.getElementById("MoreField").setAttribute("sl-no",response.sl_no);

            // alert(response);
        }).fail(function( jqXHR, textStatus ) {

        });
        return false;
    });

    //delete more addition field form
    $(document).delegate('#delete_field_click','click',function () {
        var sl_no = $(this).attr('sl_no');
        // alert(sl_no);
        document.getElementById("delete_field_row_"+sl_no).remove();
    });


    //for DeleteAdditional filed db
    $(document).delegate('#DeleteAdditional','click',function () {
        var sl_no = $(this).attr('sl-no');
        var route = $('#DeleteAdditionalroute').attr('data-href');
        // alert(sl_no+' '+route) ;

        $.ajax({
            url: route,
            method: "GET",
            dataType: "json",
            data: {sl_no: sl_no},
            beforeSend: function( xhr ) {

            }
        }).done(function( response ) {
            $('#hide_additional_field_'+sl_no).hide();
            // alert(response.ok);
        }).fail(function( jqXHR, textStatus ) {

        });
        return false;
    });

    // Add customer address
    $(document).delegate('#bill_country_id','change',function () {
        var sl_no = $(this).val();
        if(sl_no == 19){
            $('.bill_district_id').show('slow');
            $('.bill_upazila_id').show('slow');
        }else{
            $('.bill_district_id').hide('slow');
            $('.bill_upazila_id').hide('slow');
        }
    });

    $(document).delegate('#ship_country_id','change',function () {
        var sl_no = $(this).val();
        if(sl_no == 19){
            $('.ship_district_id').show('slow');
            $('.ship_upazila_id').show('slow');
        }else{
            $('.ship_district_id').hide('slow');
            $('.ship_upazila_id').hide('slow');
        }
    });

    $(document).delegate('#bill_district_id,#ship_district_id','change',function () {
        var id = $(this).val();
        var type = $(this).attr('type');

        var url = $('#findupazalaroute').attr('data-href');
        // alert(type);
        $.ajax({
            url: url,
            method: "GET",
            dataType: "json",
            data: {id: id},
            beforeSend: function( xhr ) {

            }
        }).done(function( response ) {
           if(response.message == 'success'){
               if(type == 'bill'){
                   $('#bill_upazila_id')
                       .find('option')
                       .remove()
                       .end()
                       .append(response.content)
                   ;
               }
               if(type == 'ship'){
                   $('#ship_upazila_id')
                       .find('option')
                       .remove()
                       .end()
                       .append(response.content)
                   ;
               }
           }else {
               alert(response.message);
           }
        }).fail(function( jqXHR, textStatus ) {

        });
        return false;
    });


    //for banner more items
    $(document).delegate('#BannerMoreItems','click',function () {
        var sl_no = $(this).attr('sl-no');
        var sl_no = parseInt(sl_no);
        var route = $('#banneritemroute').attr('data-href');
        // alert(sl_no) ;
        sl_no += 1;
        $.ajax({
            url: route,
            method: "GET",
            dataType: "json",
            data: {sl_no: sl_no},
            beforeSend: function( xhr ) {

            }
        }).done(function( response ) {
            $('#add_more_attach_row').append(response.content);
            document.getElementById("BannerMoreItems").setAttribute("sl-no",response.sl_no);

            // alert(response);
        }).fail(function( jqXHR, textStatus ) {

        });
        return false;
    });



    // For show & hide category & brand for banner items
    $(document).delegate('#banner_type','change',function () {
        var sl_no = $(this).attr('sl-no');
        var value = $(this).val();

        // alert(value+' '+sl_no);
        if(value == 'Category'){
            var ShowClass = '.category_show_'+sl_no;
            var HideClass = '.banner_show_'+sl_no;
        }

        if(value == 'Brand'){
            var ShowClass = '.banner_show_'+sl_no;
            var HideClass = '.category_show_'+sl_no;
        }
        $(ShowClass).show();
        $(HideClass).hide();
    });

</script>




