<ul class="sidebar-menu scrollable position-relative pt-3">

    <li class="nav-item dropdown">
        <a class="nav-link wave-effect" href="{{route('admin-dashboard')}}">
          <span class="icon-holder">
            <i class="fas fa-home"></i>
          </span>
            <span class="title">Dashboard</span>
        </a>
    </li>

    @if(Auth::user()->type == 'Admin')


        <li class="nav-item dropdown">
            <a class="nav-link wave-effect" href="{{route('admin.banner.index')}}">
          <span class="icon-holder">
            <i class="fas fa-folder-plus"></i>
          </span>
                <span class="title">Manage Banner</span>
            </a>
        </li>


        <li class="nav-item dropdown">
            <a class="nav-link wave-effect" href="{{route('admin.slider.index')}}">
          <span class="icon-holder">
            <i class="fas fa-folder-plus"></i>
          </span>
                <span class="title">Manage Slider</span>
            </a>
        </li>

        <li class="nav-item dropdown">
            <a class="nav-link wave-effect" href="{{route('admin.order.index')}}">
          <span class="icon-holder">
            <i class="fas fa-folder-plus"></i>
          </span>
                <span class="title">Manage Order</span>
            </a>
        </li>


        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#">
                    <span class="icon-holder">
                    <i class="fas fa-folder-plus"></i>
                    </span>
                <span class="title">Manage Product</span>
                <span class="arrow">
                    <i class="fas fa-angle-right"></i>
                    </span>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{route('admin.product.filecreate')}}">Product File Import</a>
                    <a href="{{route('admin.product.productcreate')}}">Add New Product</a>
                </li>
            </ul>
        </li>



        <li class="nav-item dropdown">
            <a class="nav-link wave-effect" href="{{route('admin.customer.index')}}">
          <span class="icon-holder">
            <i class="fas fa-folder-plus"></i>
          </span>
                <span class="title">Manage Customer</span>
            </a>
        </li>

        <li class="nav-item dropdown">
            <a class="nav-link wave-effect" href="{{route('admin.vendor.index')}}">
          <span class="icon-holder">
            <i class="fas fa-folder-plus"></i>
          </span>
                <span class="title">Manage Vendor</span>
            </a>
        </li>

        <li class="nav-item dropdown">
            <a class="nav-link wave-effect" href="{{route('admin.user.index')}}">
          <span class="icon-holder">
            <i class="fas fa-folder-plus"></i>
          </span>
                <span class="title">Manage User</span>
            </a>
        </li>

        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#">
          <span class="icon-holder">
            <i class="fas fa-folder-plus"></i>
          </span>
                <span class="title">Master Data</span>
                <span class="arrow">
            <i class="fas fa-angle-right"></i>
          </span>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{route('admin.itemunit.create')}}">Item Unit</a>
                </li>
                <li>
                    <a href="{{route('admin.sizeunit.create')}}">Size Unit</a>
                </li>
                <li>
                    <a href="{{route('admin.sizeweightdimension.create')}}">Size/Weight/Dimension</a>
                </li>
                <li>
                    <a href="{{route('admin.brand.create')}}">Brand</a>
                </li>
                <li>
                    <a href="{{route('admin.promotion.create')}}">Promotion</a>
                </li>
                <li>
                    <a href="{{route('admin.country.create')}}">Country</a>
                </li>
                <li>
                    <a href="{{route('admin.category.create')}}">Category</a>
                </li>

                <li>
                    <a href="{{route('admin.warningtype.create')}}">Warning Type</a>
                </li>

                <li>
                    <a href="{{route('admin.itemassurance.create')}}">Item Assurance</a>
                </li>

                <li>
                    <a href="{{route('admin.discount.create')}}">Discount</a>
                </li>

                <li>
                    <a href="{{route('admin.color.create')}}">Color</a>
                </li>

                <li>
                    <a href="{{route('admin.timeslot.create')}}">Delivery Time Slot</a>
                </li>
            </ul>
        </li>


        <li class="nav-item dropdown">
            <a class="nav-link wave-effect" href="{{route('admin.generalpage.index')}}">
          <span class="icon-holder">
            <i class="fab fa-pagelines"></i>
          </span>
                <span class="title">General Page</span>
            </a>
        </li>

        <li class="nav-item dropdown">
            <a class="nav-link wave-effect" href="{{route('admin.generalsetting.index')}}">
          <span class="icon-holder">
            <i class="fas fa-home"></i>
          </span>
                <span class="title">General Setting</span>
            </a>
        </li>


</ul>

@endif

@if(Auth::user()->type == 'Customer')
    <li class="nav-item dropdown">
        <a class="nav-link wave-effect" href="{{route('admin.customer.index')}}">
          <span class="icon-holder">
            <i class="fas fa-folder-plus"></i>
          </span>
            <span class="title">Customer</span>
        </a>
    </li>
@endif

@if(Auth::user()->type == 'Vendor')
    <li class="nav-item dropdown">
        <a class="nav-link wave-effect" href="{{route('admin.vendor.index')}}">
          <span class="icon-holder">
            <i class="fas fa-folder-plus"></i>
          </span>
            <span class="title">Vendor</span>
        </a>
    </li>

    <li class="nav-item dropdown">
        <a class="nav-link wave-effect" href="{{route('admin.order.index')}}">
          <span class="icon-holder">
            <i class="fas fa-folder-plus"></i>
          </span>
            <span class="title">Order</span>
        </a>
    </li>




<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#">
                    <span class="icon-holder">
                    <i class="fas fa-folder-plus"></i>
                    </span>
        <span class="title">Product</span>
        <span class="arrow">
                    <i class="fas fa-angle-right"></i>
                    </span>
    </a>
    <ul class="dropdown-menu">
        <li>
            <a href="{{route('admin.product.filecreate')}}">Product File Import</a>
            <a href="{{route('admin.product.productcreate')}}">Add New Product</a>
        </li>
    </ul>
</li>

</ul>
@endif

