
<!doctype html>
<html lang="en">
<head>

    <!-- meta item -->
    @include('backend.layouts.head')
    <!-- Favicons -->
    @include('backend.layouts.headimage')
    <!-- Bootstrap core CSS -->
    @include('backend.layouts.css')

</head>
<body class="app">



{{--<div class="sidebar">
    <div class="sidebar-inner">
        <!-- logo part -->
       @include('backend.layouts.logo')
        <!-- menu part -->
        @include('backend.layouts.nav')
    </div>
</div>--}}

@include('backend.layouts.nav1')



<div class="container-wide">
    <!-- top nav part -->
    @include('backend.layouts.topnav')

    <!-- body part -->
    @yield('body')

    <!-- footer part -->
    @include('backend.layouts.footer')
    <!-- js part -->
    @include('backend.layouts.js')

</div>
</body>
</html>
