@extends(/** @lang text */'backend.layouts.master')

@section('body')
    <div class="dashboard-area">

        <div id="carbon-block">

            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">Dashboard</h1>

                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group me-2">


                            <button type="button" id="printbutton" class="btn btn-sm btn-outline-secondary">print</button>
                            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
                            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
                        </div>
                        <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
                            <span data-feather="calendar"></span>
                            This week
                        </button>

                    </div>
                </div>
            </main>


            <div class="card">
                <div class="card-header">
                    Graph
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="card-title">Line Graph</h5>
                            <p class="card-text"></p>
                            <div id="mylinechart" style="height: 250px;"></div>
                        </div>

                        <div class="col-md-6">
                            <h5 class="card-title">Area charts</h5>
                            <p class="card-text"></p>
                            <div id="myareachart1" style="height: 250px;"></div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="card-title">Bar Graph</h5>
                            <p class="card-text"></p>
                            <div id="mybarchart" style="height: 250px;"></div>
                        </div>

                        <div class="col-md-6">
                            <h5 class="card-title">Bar Graph</h5>
                            <p class="card-text"></p>
                            <div id="mybarchart1" style="height: 250px;"></div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="card-title">Donut / Pie Graph</h5>
                            <p class="card-text"></p>
                            <div id="mydonutchart" style="height: 250px;"></div>
                        </div>

                        <div class="col-md-6">
                            <h5 class="card-title">Area Graph</h5>
                            <p class="card-text"></p>
                            <div id="myareachart" style="height: 250px;"></div>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="card-title">bar chart no exists</h5>
                            <p class="card-text"></p>
                            <div id="mybarchartnoexists" style="height: 250px;"></div>
                        </div>

                        <div class="col-md-6">
                            <h5 class="card-title">Donut Chart with %</h5>
                            <p class="card-text"></p>
                            <div id="mydonut%chart" style="height: 250px;"></div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="card-title">Stacked bars</h5>
                            <p class="card-text"></p>
                            <div id="Stacked_Bars" style="height: 250px;"></div>
                        </div>

                        <div class="col-md-6">
                            <h5 class="card-title">Donut Chart with %</h5>
                            <p class="card-text"></p>
                            <div id="mydonut%chart" style="height: 250px;"></div>
                        </div>
                    </div>

                </div>
            </div>




            <div class="card">
                <div class="card-header">
                    Featured
                </div>
                <div class="card-body">
                    <h5 class="card-title">table table-striped table-sm</h5>
                    <p class="card-text">table</p>
                    <div class="table-responsive">
                        <table class="table table-striped table-sm" id="table_id">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Header</th>
                                <th scope="col">Header</th>
                                <th scope="col">Header</th>
                                <th scope="col">Header</th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr>
                                <td>1,004</td>
                                <td>text</td>
                                <td>random</td>
                                <td>layout</td>
                                <td>dashboard</td>
                            </tr>
                            <tr>
                                <td>1,005</td>
                                <td>dashboard</td>
                                <td>irrelevant</td>
                                <td>text</td>
                                <td>placeholder</td>
                            </tr>
                            <tr>
                                <td>1,006</td>
                                <td>dashboard</td>
                                <td>illustrative</td>
                                <td>rich</td>
                                <td>data</td>
                            </tr>
                            <tr>
                                <td>1,007</td>
                                <td>placeholder</td>
                                <td>tabular</td>
                                <td>information</td>
                                <td>irrelevant</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>


                </div>
            </div>


            <div class="card">
                <div class="card-header">
                    Autocomplete
                </div>

                <div class="card-body">
                    <h5 class="card-title">Autocomplete text input</h5>
                    <p class="card-text"></p>


                    <div class="col-md-12">
                        <h2></h2>

                        <p>Start typing:</p>

                        <!--Make sure the form has the autocomplete function switched off:-->
                        <form autocomplete="off" action="/action_page.php">
                            <div class="autocomplete" style="width:300px;">
                                <input id="myInput" type="text" name="myCountry" placeholder="Country" class="autocomplete-input">
                            </div>
                        </form>

                    </div>
                </div>
            </div>



            <div class="card">
                <div class="card-header">
                    Select2
                </div>
                <div class="card-body">
                    <h5 class="card-title">Select2 select option</h5>
                    <p class="card-text"></p>

                    <select class="form-select js-example-basic-single" aria-label="Default select example">
                        <option selected>Open this select menu</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                    </select>

                </div>
            </div>



            <div class="card">
                <div class="card-header">
                    Multiple
                </div>

                <div class="card-body">
                    <h5 class="card-title">Multiple select option</h5>
                    <p class="card-text"></p>


                    <div class="col-md-12">
                        <select class="form-select js-example-basic-multiple" multiple="multiple" name="name[]" aria-label="Default select example">
                            <option value="">Open this select menu</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                </div>
            </div>


            <div class="card">
                <div class="card-header">
                    ReModel
                </div>
                <div class="card-body">
                    <h5 class="card-title">ReModel</h5>
                    <p class="card-text"></p>

                    <a href="#modal" class="btn btn-danger">Show Modal</a>

                    <!-- model popup show start -->
                    <div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
                        <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
                        <div>
                            <h2 id="modal1Title">Remodal</h2>
                            <p id="modal1Desc">
                                Responsive, lightweight, fast, synchronized with CSS animations, fully customizable modal window plugin
                                with declarative state notation and hash tracking.
                            </p>
                        </div>
                        <br>
                        <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
                        <button data-remodal-action="confirm" class="remodal-confirm">OK</button>
                    </div>
                    <!-- model popup show start -->
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    Multiple file upload
                </div>

                <div class="card-body">
                    <h5 class="card-title">Multiple file upload</h5>
                    <p class="card-text"></p>


                    <div class="col-md-12">
                        <h2></h2>
                        <p></p>


                        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">

                            <div class="row form-group">
                                <div class="col-12 col-md-12">
                                    <div class="control-group" id="fields">
                                        <label class="control-label" for="field1">
                                            Requests
                                        </label>
                                        <div class="controls">
                                            <div class="entry input-group upload-input-group">
                                                <input class="form-control multiple-upload-line-height" name="fields[]" type="file" >
                                                <button class="btn btn-upload btn-success btn-add" type="button">
                                                    <i class="fa fa-plus"></i>
                                                </button>
                                            </div>

                                        </div>
                                        <button class="btn btn-primary">Upload</button>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>



            <div class="card">
                <div class="card-header">
                    Validation Form
                </div>

                <div class="card-body">
                    <h5 class="card-title">Validation Form</h5>
                    <p class="card-text"></p>


                    <div class="col-md-12">
                        <h2></h2>
                        <p></p>


                        <form action="" method="post" enctype="multipart/form-data" class="form-horizontal" id="basic-form">

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="from-group">
                                        <label for="basic-url" class="form-label">User Name</label>
                                        <div class="input-group mb-3">
                                            <input type="text" name="name" class="form-control" placeholder="username" aria-label="username" aria-describedby="basic-addon2" data-checkify="minlen=2,required" >
                                            <!-- <span class="input-group-text" id="basic-addon2">@example.com</span> -->
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="from-group">
                                        <label for="basic-url" class="form-label">User email</label>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder="email" aria-label="email" aria-describedby="basic-addon2" name="email" required="true" data-checkify="email,required">
                                            <!-- <span class="input-group-text" id="basic-addon2">@example.com</span> -->
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="from-group">
                                        <label for="basic-url" class="form-label">Mobile</label>
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder="Mobile" aria-label="Mobile" aria-describedby="basic-addon2" name="mobile" required="true" data-checkify="minlen=6,required,number,maxlen=11">
                                            <!-- <span class="input-group-text" id="basic-addon2">@example.com</span> -->
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="row">
                                <div class="col-md-4 offset-md-4">
                                    <div class="from-group">
                                        <label for="basic-url" class="form-label">Gender</label>
                                        <div class="input-group mb-3">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="gender" id="inlineRadio1" value="Male" checked="checked"/>
                                                <label class="form-check-label" for="inlineRadio1">Male</label>
                                            </div>

                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="gender" id="inlineRadio2" value="Female"  />
                                                <label class="form-check-label" for="inlineRadio2">Female</label>
                                            </div>


                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="gender" id="inlineRadio3" value="Other"/>
                                                <label class="form-check-label" for="inlineRadio3">Other</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="from-group">
                                        <label for="basic-url" class="form-label">Department</label>
                                        <div class="input-group mb-3">
                                            <select class="form-select js-example-basic-single" name="department" aria-label="Default select example" id="required">
                                                <option value="">Select a department</option>
                                                <option value="1">Software</option>
                                                <option value="2">Account</option>
                                                <option value="3">Admin</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>



                            <div class="row">

                                <div class="col-md-8 offset-md-4">
                                    <div class="from-group">
                                        <label for="basic-url" class="form-label">Address</label>
                                        <div class="input-group mb-3">
                                            <textarea type="textarea" class="form-control" placeholder="address" aria-label="address" aria-describedby="basic-addon2" rows="2" required="true"> </textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>



                            <div class="row">

                                <div class="col-md-12" style="text-align: right;">
                                    <div class="from-group">
                                        <div class="">
                                            <button type="reset" class="btn submit-button">Reset</button>
                                            <button type="submit" class="btn submit-button">Submit</button>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </form>

                    </div>
                </div>
            </div>



            <div class="card">
                <div class="card-header">
                    Print Format
                </div>
                <div class="card-body">
                    <h5 class="card-title"></h5>
                    <p class="card-text"></p>

                    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

                    <div class="page-content container">
                        <div class="page-header text-blue-d2 row">
                            <div class="col-6">
                                <i class="fa fa-book fa-2x text-success-m2 mr-1"></i>
                                <span class="text-default-d3">Bootdey.com</span>

                                <h1 class="page-title text-secondary-d1">
                                    Company Name Inc
                                    <small class="page-info">
                                        Uttara , Dhaka
                                    </small>
                                </h1>
                            </div>

                            <div class="col-6">
                                <h1 class="page-title text-secondary-d1" style="text-align: right;">
                                    Invoice
                                    <small class="page-info">
                                        <i class="fa fa-angle-double-right text-80"></i>
                                        ID: #111-222
                                    </small>
                                </h1>
                            </div>

                        </div>

                        <div class="container px-0">
                            <div class="row mt-4">
                                <div class="col-12 col-lg-10 ">
                                    <!-- .row -->

                                    <hr class="row brc-default-l1 mx-n1 mb-4" />

                                    <div class="row">
                                        <div class="col-6 col-sm-6">
                                            <div>
                                                <span class="text-sm text-grey-m2 align-middle">To:</span>
                                                <span class="text-600 text-110 text-blue align-middle">Alex Doe</span>
                                            </div>
                                            <div class="text-grey-m2">
                                                <div class="my-1">
                                                    Street, City
                                                </div>
                                                <div class="my-1">
                                                    State, Country
                                                </div>
                                                <div class="my-1"><i class="fa fa-phone fa-flip-horizontal text-secondary"></i> <b class="text-600">111-111-111</b></div>
                                            </div>
                                        </div>
                                        <!-- /.col -->

                                        <div class="col-6 text-95 col-sm-6  justify-content-end">
                                            <hr class="d-sm-none" />
                                            <div class="text-grey-m2">
                                                <div class="mt-1 mb-2 text-secondary-m1 text-600 text-125">
                                                    Invoice
                                                </div>

                                                <div class="my-2"><i class="fa fa-circle text-blue-m2 text-xs mr-1"></i> <span class="text-600 text-90">ID:</span> #111-222</div>

                                                <div class="my-2"><i class="fa fa-circle text-blue-m2 text-xs mr-1"></i> <span class="text-600 text-90">Issue Date:</span> Oct 12, 2019</div>

                                                <div class="my-2"><i class="fa fa-circle text-blue-m2 text-xs mr-1"></i> <span class="text-600 text-90">Status:</span> <span class="badge badge-warning badge-pill px-25">Unpaid</span></div>
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>

                                    <div class="mt-4">
                                        <div class="row text-600 text-white bgc-default-tp1 py-25">
                                            <div class="d-none d-sm-block col-1">#</div>
                                            <div class="col-9 col-sm-5">Description</div>
                                            <div class="d-none d-sm-block col-4 col-sm-2">Qty</div>
                                            <div class="d-none d-sm-block col-sm-2">Unit Price</div>
                                            <div class="col-2">Amount</div>
                                        </div>

                                        <div class="text-95 text-secondary-d3">
                                            <div class="row mb-2 mb-sm-0 py-25">
                                                <div class="d-none d-sm-block col-1">1</div>
                                                <div class="col-9 col-sm-5">Domain registration</div>
                                                <div class="d-none d-sm-block col-2">2</div>
                                                <div class="d-none d-sm-block col-2 text-95">$10</div>
                                                <div class="col-2 text-secondary-d2">$20</div>
                                            </div>

                                            <div class="row mb-2 mb-sm-0 py-25 bgc-default-l4">
                                                <div class="d-none d-sm-block col-1">2</div>
                                                <div class="col-9 col-sm-5">Web hosting</div>
                                                <div class="d-none d-sm-block col-2">1</div>
                                                <div class="d-none d-sm-block col-2 text-95">$15</div>
                                                <div class="col-2 text-secondary-d2">$15</div>
                                            </div>

                                            <div class="row mb-2 mb-sm-0 py-25">
                                                <div class="d-none d-sm-block col-1">3</div>
                                                <div class="col-9 col-sm-5">Software development</div>
                                                <div class="d-none d-sm-block col-2">--</div>
                                                <div class="d-none d-sm-block col-2 text-95">$1,000</div>
                                                <div class="col-2 text-secondary-d2">$1,000</div>
                                            </div>

                                            <div class="row mb-2 mb-sm-0 py-25 bgc-default-l4">
                                                <div class="d-none d-sm-block col-1">4</div>
                                                <div class="col-9 col-sm-5">Consulting</div>
                                                <div class="d-none d-sm-block col-2">1 Year</div>
                                                <div class="d-none d-sm-block col-2 text-95">$500</div>
                                                <div class="col-2 text-secondary-d2">$500</div>
                                            </div>
                                        </div>

                                        <div class="row border-b-2 brc-default-l2"></div>



                                        <div class="row mt-3">
                                            <div class="col-12 col-sm-7 text-grey-d2 text-95 mt-2 mt-lg-0">
                                                Extra note such as company or payment information...
                                            </div>

                                            <div class="col-12 col-sm-5 text-grey text-90 order-first order-sm-last">
                                                <div class="row my-2">
                                                    <div class="col-7 text-right">
                                                        SubTotal
                                                    </div>
                                                    <div class="col-5">
                                                        <span class="text-120 text-secondary-d1">$2,250</span>
                                                    </div>
                                                </div>

                                                <div class="row my-2">
                                                    <div class="col-7 text-right">
                                                        Tax (10%)
                                                    </div>
                                                    <div class="col-5">
                                                        <span class="text-110 text-secondary-d1">$225</span>
                                                    </div>
                                                </div>

                                                <div class="row my-2 align-items-center bgc-primary-l3 p-2">
                                                    <div class="col-7 text-right">
                                                        Total Amount
                                                    </div>
                                                    <div class="col-5">
                                                        <span class="text-150 text-success-d3 opacity-2">$2,475</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <hr />

                                        <div>


                                            <!-- begin invoice-note -->
                                            <div class="invoice-note">
                                                * Make all cheques payable to [Your Company Name]<br>
                                                * Payment is due within 30 days<br>
                                                * If you have any questions concerning this invoice, contact  [Name, Phone Number, Email]
                                            </div>
                                            <!-- end invoice-note -->
                                            <!-- begin invoice-footer -->
                                            <div class="invoice-footer">
                                                <p class="text-center m-b-5 f-w-600">
                                                    THANK YOU FOR YOUR BUSINESS
                                                </p>
                                                <p class="text-center">
                                                    <span class="m-r-10"><i class="fa fa-fw fa-lg fa-globe"></i> matiasgallipoli.com</span>
                                                    <span class="m-r-10"><i class="fa fa-fw fa-lg fa-phone-volume"></i> T:016-18192302</span>
                                                    <span class="m-r-10"><i class="fa fa-fw fa-lg fa-envelope"></i> rtiemps@gmail.com</span>
                                                </p>
                                            </div>
                                            <!-- end invoice-footer -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



