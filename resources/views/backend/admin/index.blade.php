@extends(/** @lang text */'backend.layouts.master')

@section('body')
    <div class="dashboard-area">

        <div id="carbon-block">

            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">Dashboard</h1>

                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group me-2">


                            <button type="button" id="printbutton" class="btn btn-sm btn-outline-secondary">print</button>
                            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
                            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
                        </div>
                        <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
                            <span data-feather="calendar"></span>
                            This week
                        </button>

                    </div>
                </div>
            </main>


            <div class="card">
                <div class="card-header">
                    Graph
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="card-title">Line Graph</h5>
                            <p class="card-text"></p>
                            <div id="mylinechart" style="height: 250px;"></div>
                        </div>

                        <div class="col-md-6">
                            <h5 class="card-title">Area charts</h5>
                            <p class="card-text"></p>
                            <div id="myareachart1" style="height: 250px;"></div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection



