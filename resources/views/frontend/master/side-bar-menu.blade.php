@if(isset($response['WebMenu']))
<div class="dropdown-box">

{{--        <ul class="menu vertical-menu category-menu">--}}
{{--            @foreach($response['WebMenu'] as $Mainmanu)--}}
{{--            <li>--}}
{{--                <a href="{{route('category.wish.product',$Mainmanu['slug'])}}">--}}
{{--                    <i class="w-icon-tshirt2"></i>{{$Mainmanu['name']}}--}}
{{--                </a>--}}
{{--                <ul class="megamenu">--}}
{{--                    @if(isset($Mainmanu['sub_menu']) && count($Mainmanu['sub_menu']) > 0)--}}

{{--                        @foreach($Mainmanu['sub_menu'] as $SubManu)--}}
{{--                    <li>--}}
{{--                        <h4 class="menu-title">--}}
{{--                            <a href="{{route('category.wish.product',$SubManu['slug'])}}">--}}
{{--                                <i class="w-icon-tshirt2"></i>--}}
{{--                                {{$SubManu['name']}}--}}
{{--                            </a>--}}

{{--                        </h4>--}}
{{--                        <hr class="divider">--}}
{{--                        <ul>--}}
{{--                            @if(isset($SubManu['sub_menu']) && count($SubManu['sub_menu']) > 0)--}}
{{--                                @foreach($SubManu['sub_menu'] as $SubManuSub)--}}
{{--                                    <li>--}}
{{--                                        <a href="{{route('category.wish.product',$SubManuSub['slug'])}}">--}}
{{--                                            {{$SubManuSub['name']}}--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                @endforeach--}}
{{--                            @endif--}}
{{--                        </ul>--}}
{{--                    </li>--}}



{{--                        @endforeach--}}
{{--                    @endif--}}

{{--                    <li>--}}
{{--                        <div class="banner-fixed menu-banner menu-banner2">--}}
{{--                            <figure>--}}
{{--                                <img src="{{ asset('backend/image/ProCategoryImage').'/'.$Mainmanu['image_link']}}" alt="Menu Banner"--}}
{{--                                     width="235" height="347" />--}}
{{--                            </figure>--}}
{{--                            <div class="banner-content">--}}
{{--                                <div class="banner-price-info mb-1 ls-normal">Get up to--}}
{{--                                    <strong--}}
{{--                                        class="text-primary text-uppercase">20%Off</strong>--}}
{{--                                </div>--}}
{{--                                <h3 class="banner-title ls-normal">Hot Sales</h3>--}}
{{--                                <a href="shop-banner-sidebar.html"--}}
{{--                                   class="btn btn-dark btn-sm btn-link btn-slide-right btn-icon-right">--}}
{{--                                    Shop Now<i class="w-icon-long-arrow-right"></i>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </li>--}}
{{--                </ul>--}}
{{--            </li>--}}
{{--            @endforeach--}}

{{--        </ul>--}}



            @foreach($response['WebMenu'] as $Mainmanu)
        <ul class="menu vertical-menu category-menu">

            <li>
                <a href="{{route('category.wish.product',$Mainmanu['slug'])}}">
                    <i class="w-icon-tshirt2"></i>{{$Mainmanu['name']}}
                </a>
                @if(isset($Mainmanu['sub_menu']) && count($Mainmanu['sub_menu']) > 0)
                <ul class="megamenu">
                    <li>
                        <h4 class="menu-title">{{$Mainmanu['name']}}</h4>
                        <hr class="divider">
                        <ul>
                            @foreach($Mainmanu['sub_menu'] as $Submenu)
                            <li>
                                <a class="@if(isset($Submenu['sub_menu']) && count($Submenu['sub_menu'])){{'w-icon-angle-down'}}@else{{'w-icon-angle-right'}}@endif" href="{{route('category.wish.product',$Submenu['slug'])}}" style="padding: 10px 10px;">
                                    {{$Submenu['name']}}
                                </a>
                                @if(isset($Submenu['sub_menu']) && count($Submenu['sub_menu']) > 0)

                                    @foreach($Submenu['sub_menu'] as $submenusub)
                                        <br><a class="@if(isset($submenusub['sub_menu']) && count($submenusub['sub_menu'])){{'w-icon-angle-down'}}@else{{'w-icon-angle-right'}}@endif" href="{{route('category.wish.product',$submenusub['slug'])}}" style="

                                        margin-left: 30px;
                                        padding: 10px 10px;">
                                            {{$submenusub['name']}}
                                        </a>

                                        @if(isset($submenusub['sub_menu']) && count($submenusub['sub_menu']) > 0)

                                            @foreach($submenusub['sub_menu'] as $submenusub1)
                                                <br><a class="w-icon-angle-right" href="{{route('category.wish.product',$submenusub1['slug'])}}" style="

margin-left: 55px;
padding: 10px 10px;">
                                                    {{$submenusub1['name']}}
                                                </a>
                                            @endforeach
                                        @endif
                                    @endforeach
                                @endif
                            </li>
                            @endforeach
                        </ul>
                    </li>

                    <li>
                        <div class="banner-fixed menu-banner menu-banner2">
                            <figure>
                                <img src="{{ asset('backend/image/ProCategoryImage').'/'.$Mainmanu['image_link']}}" alt="Menu Banner"
                                     width="235" height="347" />
                            </figure>
                            <div class="banner-content" style="background: #23310317;
padding: 10px;">
                                <div class="banner-price-info mb-1 ls-normal">Get More
                                    <strong
                                        class="text-primary text-uppercase">Product</strong>
                                </div>
                                <h3 class="banner-title ls-normal">{{$Mainmanu['name']}}</h3>
                                <a href="{{route('category.wish.product',$Mainmanu['slug'])}}"
                                   class="btn btn-dark btn-sm btn-link btn-slide-right btn-icon-right">
                                    Shop Now<i class="w-icon-long-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                    </li>
                </ul>
                @endif
            </li>
            @endforeach

</div>
@endif
