<!-- Favicon -->
<link rel="icon" type="image/png" href="{{ asset('frontend/images/demos/demo3/logo/Logo.png') }}">

<!-- WebFont.js -->
<script>
    WebFontConfig = {
        google: { families: ['Poppins:400,600,500,700,800', 'Zeyada: 400'] }
    };
    (function (d) {
        var wf = d.createElement('script'), s = d.scripts[0];
        wf.src = 'frontend/js/webfont.js';
        wf.async = true;
        s.parentNode.insertBefore(wf, s);
    })(document);
</script>

<link rel="preload" href="{{ asset('frontend/vendor/fontawesome-free/webfonts/fa-regular-400.woff2') }}" as="font" type="font/woff2"
      crossorigin="anonymous">
<link rel="preload" href="{{ asset('frontend/vendor/fontawesome-free/webfonts/fa-solid-900.woff2') }}" as="font" type="font/woff2"
      crossorigin="anonymous">
<link rel="preload" href="{{ asset('frontend/vendor/fontawesome-free/webfonts/fa-brands-400.woff2') }}" as="font" type="font/woff2"
      crossorigin="anonymous">
<link rel="preload" href="{{ asset('frontend/fonts/wolmart.ttf?png09e') }}" as="font" type="font/ttf" crossorigin="anonymous">
<!-- for select2 & multile select -->
<link href="{{ asset('backend/css/select2.min.css') }}" rel="stylesheet">
<!-- Vendor CSS -->
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/vendor/fontawesome-free/css/all.min.css') }}">
<!-- Plugins CSS -->
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/vendor/owl-carousel/owl.carousel.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/vendor/animate/animate.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/vendor/magnific-popup/magnific-popup.min.css') }}">

<link rel="stylesheet" type="text/css" href="{{ asset('frontend/vendor/photoswipe/photoswipe.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/vendor/photoswipe/default-skin/default-skin.min.css') }}">
<!-- for validation tooltip -->
<link href="{{ asset('backend/css/jquery.checkify.min.css') }}" rel="stylesheet">

<!-- Default CSS -->
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/demo3.min.css') }}">
<?php
use Illuminate\Support\Facades\Route;
if (Route::currentRouteName() != ''){ ?>
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/style.min.css') }}">
<?php } ?>


<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/sweetalertcss.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/supermartbd.css') }}">
