<?php
$cart_items = [];
if(Session::has('cart')){
    $cart_items = Session::get('cart');
}

$cart_total = [];
if(Session::has('cart_total')){
    $cart_total = Session::get('cart_total');
}
?>
<div class="dropdown cart-dropdown cart-offcanvas mr-0 mr-lg-2">
    <div class="cart-overlay"></div>
    <a href="#" class="cart-toggle label-down link">
        <i class="w-icon-cart">
            <span class="cart-count">{{count($cart_items)}}</span>
        </i>
        <span class="cart-label">Cart</span>
    </a>

    <div class="dropdown-box">

        <div class="cart-header">
            <span>Shopping Cart</span>
            <a href="#" class="btn-close">Close<i class="w-icon-long-arrow-right"></i></a>
        </div>


        <div class="products" style="max-height: 100%;">

            @if(count($cart_items) > 0)
                @foreach($cart_items as $cart)
            <div class="product product-cart">
                <div class="product-detail">
                    <a href="{{route('single.product.details',$cart['product_slug'])}}" class="product-name">{{$cart['product_name']}}</a>
                    <div class="price-box">
                        <span class="product-quantity">{{$cart['product_quantity']}}</span>
                        <span class="product-price" style="margin-bottom: 0px;">{{$cart['product_price'].' = '}}</span>
                        <span class="product-price" style="margin-bottom: 0px;">{{$cart['product_price']*$cart['product_quantity']}}</span>
                    </div>
                </div>
                <figure class="product-media">
                    <a href="{{route('single.product.details',$cart['product_slug'])}}">
                        <img src="{{ asset('backend/image/ProProductImage').'/'.$cart['product_image']}}" alt="product" width="84"
                             height="94" />
                    </a>
                </figure>
                <button class="btn btn-link btn-close"  id="remove-cart-product" product_id="{{$cart['product_id']}}" data-href="{{route('web.cart.remove')}}">
                    <i class="fas fa-times"></i>
                </button>
            </div>
                @endforeach
            @endif
        </div>

        <div class="cart-total">
            <label>Subtotal:</label>
            <span class="price">
                @if(isset($cart_total['total']))
                {{$cart_total['total']}}
                @endif
            </span>
        </div>

        <div class="cart-action">
            <a href="{{route('web.cart.view')}}" class="btn btn-dark btn-outline btn-rounded">View Cart</a>
            <a href="{{route('web.checkout.store')}}" class="btn btn-primary  btn-rounded">Checkout</a>
        </div>
{{--        @else--}}
{{--            <div class="cart-action">--}}
{{--                Your shopping cart is empty <a href="{{ url('/') }}" class="">Start shopping now.</a>--}}
{{--            </div>--}}
{{--        @endif--}}
    </div>

    <!-- End of Dropdown Box -->
</div>
