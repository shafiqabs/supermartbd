<form method="get" action="{{route('web.product.search')}}" class="header-search hs-expanded hs-round d-none d-md-flex input-wrapper">
    <div class="select-box">
        <select id="category" name="cat" class="pb-0">
            <option value="">All Categories</option>

            @if(isset($response['Category']) && count($response['Category'])>0)
            @foreach($response['Category'] as $value)
                <option value="{{$value->slug}}"
                <?php
                    if (isset($CategorySlug) && !empty($CategorySlug)){
                        if ($CategorySlug == $value->slug){
                            echo 'selected';
                        }
                    }

                ?>
                >{{$value->name}}</option>
            @endforeach
            @endif

        </select>

    </div>
    <input type="text" class="form-control" name="key" id="search" value="@if(isset($KeyWord) && !empty($KeyWord)){{$KeyWord}}@endif"
           placeholder="Search in..."  />
    <button class="btn btn-search" type="submit"><i class="w-icon-search"></i>
    </button>
</form>
