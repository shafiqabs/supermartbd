<script src="{{ asset('frontend/vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('frontend/vendor/parallax/parallax.min.js') }}"></script>
<script src="{{ asset('frontend/vendor/jquery.plugin/jquery.plugin.min.js') }}"></script>
<script src="{{ asset('frontend/vendor/imagesloaded/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('frontend/vendor/zoom/jquery.zoom.js') }}"></script>
<script src="{{ asset('frontend/vendor/owl-carousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('frontend/vendor/jquery.countdown/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('frontend/vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>

{{--<script src="{{ asset('frontend/vendor/sticky/sticky.js') }}"></script>--}}
{{--<script src="{{ asset('frontend/vendor/imagesloaded/imagesloaded.pkgd.min.js') }}"></script>--}}
{{--<script src="{{ asset('frontend/vendor/photoswipe/photoswipe.js') }}"></script>--}}
{{--<script src="{{ asset('frontend/vendor/photoswipe/photoswipe-ui-default.js') }}"></script>--}}


<script>
    var supperMartBdCartadd='{{route('web.cart.add')}}';
    var supperMartBdCartUrl='{{route('web.cart.view')}}';
    var supperMartBdCheckoutUrl='{{route('web.checkout.store')}}';
    var supperMartBdCategoryWiseProduct='{{route('category.wish.product','')}}';
    var supperMartBdBrandWiseProduct='{{route('brand.wish.product','')}}';

</script>
<!-- for tooltip validation -->
<script src="{{ asset('backend/js/jquery.checkify.min.js') }}"></script>
<!-- for tooltip validation sms show -->
<script src="{{ asset('backend/js/tooltip-main.js') }}"></script>
<!-- for select2 & multiple select -->
<script src="{{ asset('backend/js/select2.min.js') }}"></script>
<script src="{{ asset('frontend/js/sweetalert.js') }}"></script>
<!-- Main Js -->
{{--<script src="{{ asset('frontend/js/doc.js') }}"></script>--}}
{{--<script src="{{ asset('frontend/js/main.js') }}"></script>--}}
<script src="{{ asset('frontend/js/main.js') }}"></script>
{{--<script src="{{ asset('frontend/js/webfont.js') }}"></script>--}}
{{--<script src="{{ asset('frontend/js/webfont.min.js') }}"></script>--}}
<script src="{{ asset('frontend/js/supermartbd.js') }}"></script>
