<div class="header-bottom sticky-content fix-top sticky-header has-dropdown">
    <div class="container">
        <div class="inner-wrap">
            <div class="header-left">
                <div class="{{$DefaultSideBarMenuclass}}" data-visible="true">
                    <a href="#" class="{{$SideBarMenuTextColor}} category-toggle" role="button" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="true" data-display="static"
                       title="Browse Categories">
                        <i class="w-icon-category"></i>
                        <span>Browse Categories</span>
                    </a>

{{--                    sidebar menu include--}}
                    @include('frontend.master.side-bar-menu')

                </div>
{{--                Main Nav menu include--}}
{{--                @include('frontend.master.main-nav')--}}
                @if($SticyMenuHome == 'yes')
                <nav class="main-nav">
                    <ul class="menu active-underline">
                        <li class="active">
                            <a href="{{url('')}}">Home</a>
                        </li>
                    </ul>
                </nav>
                @endif
            </div>
            <div class="header-right">
                <a href="#" class="d-xl-show"><i class="w-icon-map-marker mr-1"></i>Track Order</a>
{{--                <a href="#"><i class="w-icon-sale"></i>Daily Deals</a>--}}
            </div>
        </div>
    </div>
</div>
