<!DOCTYPE html>
<html lang="en">

<head>
    {{--Meta--}}
    @include('frontend.master.meta')
{{--    link & css--}}
    @include('frontend.master.link-css')

</head>
<?php
use Illuminate\Support\Facades\Route;
if (Route::currentRouteName() == ''){
    $DefaultRegisterPopupclass = 'home';
    $DefaultSideBarMenuclass = 'dropdown category-dropdown show-dropdown';
    $SideBarMenuTextColor = "text-white";
    $SticyMenuHome = 'no';
}else{
    $DefaultRegisterPopupclass = '';
    $DefaultSideBarMenuclass = 'dropdown category-dropdown has-border';
    $SideBarMenuTextColor = "";
    $SticyMenuHome = 'yes';

}
?>
<body class="{{$DefaultRegisterPopupclass}}">
<div class="page-wrapper">

    <!-- Start of Header -->
    <header class="header">


        <!-- start of Header Top -->
    @include('frontend.master.header-top')
        <!-- End of Header Top -->


        <!-- start of Header Middle -->
    @include('frontend.master.header-middle')
        <!-- End of Header Middle -->

{{--Header Bottom --}}
 @include('frontend.master.header-bottom')
    </header>
    <!-- End of Header -->

<!-- body part -->
@yield('body')




<!-- Plugin JS File -->
@include('frontend.master.js')
</body>

</html>
