<div class="header-middle" style="padding: 0px">
    <div class="container">
        <div class="header-left mr-md-4">
            <a href="#" class="mobile-menu-toggle  w-icon-hamburger">
            </a>
{{--logo--}}
@include('frontend.master.logo-header-middle')
            @include('frontend.master.search-box-header-middle')

        </div>
        <div class="header-right ml-4">
            <div class="header-call d-xs-show d-lg-flex align-items-center">
                <a href="tel:#" class="w-icon-call"></a>
                <div class="call-info d-lg-show">
                    <h4 class="chat font-weight-normal font-size-md text-normal ls-normal text-light mb-0">
                        <a href="mailto:#" class="text-capitalize">Call Us Now</a> :</h4>

                    <a href="tel:#" class="phone-number font-weight-bolder ls-50">{{$response['general_setting']->general_setting->mobile}}</a>
                </div>
            </div>
            <a class="wishlist label-down link d-xs-show" href="#">
                <i class="w-icon-exclamation-circle"></i>
                <span class="wishlist-label d-lg-show">FAQ</span>
            </a>

            @if(!isset(Auth::user()->id) && empty(Auth::user()->id))

            <a class="compare label-down link d-xs-show" href="{{route('web.register.account')}}">
                <i class="w-icon-user"></i>
                <span class="compare-label d-lg-show">Register</span>
            </a>
            @endif
  @include('frontend.master.cart-header-middle')
        </div>
    </div>
</div>
