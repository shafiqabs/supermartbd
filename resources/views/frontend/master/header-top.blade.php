<div class="header-top">
    <div class="container">
        <div class="header-left">
            <p class="welcome-msg">Welcome to MASABA Store message or remove it!</p>
        </div>
        <div class="header-right">
{{--            <div class="dropdown">--}}
{{--                <a href="#currency">USD</a>--}}
{{--                <div class="dropdown-box">--}}
{{--                    <a href="#USD">USD</a>--}}
{{--                    <a href="#EUR">EUR</a>--}}
{{--                </div>--}}
{{--            </div>--}}
            <!-- End of DropDown Menu -->

            {{--<div class="dropdown">
                <a href="#language"><img src="{{ asset('frontend/images/flags/eng.png') }}" alt="ENG Flag" width="14"
                                         height="8" class="dropdown-image" /> ENG</a>
                <div class="dropdown-box">
                    <a href="#ENG">
                        <img src="{{ asset('frontend/images/flags/eng.png') }}" alt="ENG Flag" width="14" height="8"
                             class="dropdown-image" />
                        ENG
                    </a>
                    <a href="#FRA">
                        <img src="{{ asset('frontend/images/flags/fra.png') }}" alt="FRA Flag" width="14" height="8"
                             class="dropdown-image" />
                        FRA
                    </a>
                </div>
            </div>--}}
            <!-- End of Dropdown Menu -->
{{--            <span class="divider d-lg-show"></span>--}}
            @if(isset($response['career']))
                <a href="{{route('view.general.page',$response['career']->slug)}}" class="d-lg-show">{{$response['career']->title}}</a>
            @endif

            @if(isset($response['about-us']))
                <a href="{{route('view.general.page',$response['about-us']->slug)}}" class="d-lg-show">{{$response['about-us']->title}}</a>
            @endif


            @if(isset($response['contact-us']))
            <a href="{{route('view.general.page',$response['contact-us']->slug)}}" class="d-lg-show">{{$response['contact-us']->title}}</a>
            @endif



            @if(Auth::user())
                <a href="{{route('web.my.account')}}" class="d-lg-show">

                    @if(isset(Auth::user()->UserCustomer->first_name))
                        {{Auth::user()->UserCustomer->first_name}} Account
                    @endif
                </a>

                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            @else
                <a href="{{route('web.customer.acclogin')}}" class="d-lg-show" class="w-icon-account">Login</a>

            @endif
       </div>
    </div>
</div>
