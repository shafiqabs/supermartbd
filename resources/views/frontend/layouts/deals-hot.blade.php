<div class="col-lg-9 mb-4">



    <div class="single-product h-100 br-sm">
        <h4 class="title-sm title-underline font-weight-bolder ls-normal">Deals Hot Of The Day</h4>
        <div class="owl-carousel owl-theme owl-nav-top owl-nav-lg row cols-1 gutter-no"
             data-owl-options="{
                                'nav': true,
                                'dots': false,
                                'margin': 20,
                                'items': 1
                            }">

            @if(isset($response['deals-hot']))
            @foreach($response['deals-hot'] as $value)

            <div class="product product-single row">
                <div class="col-md-6">
                    <div class="product-gallery product-gallery-vertical mb-0">
                        <div
                            class="product-single-carousel owl-carousel owl-theme owl-nav-inner row cols-1 gutter-no">
                            @foreach($value->product_image as $image)

                            <figure class="product-image">
                                <img src="{{ asset('backend/image/ProProductImage/More').'/'.$image->attach_link}}"
                                     data-zoom-image="{{ asset('backend/image/ProProductImage/More').'/'.$image->attach_link}}"
                                     alt="Product Image" width="800" height="900">
                            </figure>
                            @endforeach
                        </div>
                        <div class="product-thumbs-wrap">
                            <div class="product-thumbs">
                                <?php
                                    $i = 1;
                                ?>
                                @foreach($value->product_image as $image)
                                <div class="product-thumb @if($i==1){{'active'}}@endif">
                                    <img src="{{ asset('backend/image/ProProductImage/More').'/'.$image->attach_link}}"
                                         alt="Product thumb" width="60" height="68" />
                                </div>
                                    <?php $i++;?>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="product-details scrollable">
                        <h2 class="product-title mb-1"><a href="{{route('single.product.details',$value->slug)}}">{{$value->name}}</a></h2>
                        <h5><a href="{{route('category.wish.product',$value->product_category->slug)}}">
                                {{$value->product_category->name}}
                            </a>
                            ,
                            <a href="{{route('brand.wish.product',$value->product_brand->slug)}}">
                                {{$value->product_brand->name}}
                            </a>
                        </h5>

                        <hr class="product-divider">

                        <div class="product-price"><ins class="new-price ls-50">TK. {{$value->salesprice}}</ins></div>

{{--                        <div class="product-countdown-container flex-wrap">--}}
{{--                            <label class="mr-2 text-default">Offer Ends In:</label>--}}
{{--                            <div class="product-countdown countdown-compact"--}}
{{--                                 data-until="2022, 12, 31" data-compact="true">--}}
{{--                                629 days, 11: 59: 52</div>--}}
{{--                        </div>--}}

                        <div class="ratings-container">
                            <div class="ratings-full">
                                <span class="ratings" style="width: 80%;"></span>
                                <span class="tooltiptext tooltip-top"></span>
                            </div>
                            <a href="#" class="rating-reviews">(3 Reviews)</a>
                        </div>

                        <div class="product-form product-variation-form product-size-swatch mb-3">
                            <label class="mb-1">Size:</label>
                            <div class="flex-wrap d-flex align-items-center product-variations">
                                <a href="#" class="size">Extra Large</a>
                                <a href="#" class="size">Large</a>
                                <a href="#" class="size">Medium</a>
                                <a href="#" class="size">Small</a>
                            </div>
                            <a href="#" class="product-variation-clean">Clean All</a>
                        </div>

{{--                        <div class="product-variation-price">--}}
{{--                            <span></span>--}}
{{--                        </div>--}}

                        <div class="product-form pt-4">
                            <div class="product-qty-form mb-2 mr-2">
                                <div class="input-group">
                                    <input class="quantity form-control" type="number" min="1"
                                           max="10000000">
                                    <button class="quantity-plus w-icon-plus"></button>
                                    <button class="quantity-minus w-icon-minus"></button>
                                </div>
                            </div>



                            <button id="cart_quantity_set" class="btn btn-primary btn-cart add_cart_ajax" product-id="{{$value->id}}" product-name="{{$value->name}}" product-slug="{{$value->slug}}" product-image="{{$value->feature_image}}"  product-price="{{$value->salesprice}}" product-quantity="1" data-href="{{route('web.cart.add')}}">
                                <i class="w-icon-cart"></i>
                                <span>Add To Cart</span>
                            </button>

                        </div>

                        <div class="social-links-wrapper mt-1">
                            <div class="social-links">
                                <div class="social-icons social-no-color border-thin">
                                    @if($response['general_setting']->general_setting->facebook)
                                        <a href="{{$response['general_setting']->general_setting->facebook}}" class="social-icon social-facebook w-icon-facebook"></a>
                                    @endif

                                    @if($response['general_setting']->general_setting->twitter)
                                        <a href="{{$response['general_setting']->general_setting->twitter}}" class="social-icon social-twitter w-icon-twitter"></a>
                                    @endif

                                    @if($response['general_setting']->general_setting->instagram)
                                        <a href="{{$response['general_setting']->general_setting->instagram}}" class="social-icon social-instagram w-icon-instagram"></a>
                                    @endif

                                    @if($response['general_setting']->general_setting->youtube)
                                        <a href="{{$response['general_setting']->general_setting->youtube}}" class="social-icon social-youtube w-icon-youtube"></a>
                                    @endif
                                </div>
                            </div>
                            <span class="divider d-xs-show"></span>
{{--                            <div class="product-link-wrapper d-flex">--}}
{{--                                <a href="#"--}}
{{--                                   class="btn-product-icon btn-wishlist w-icon-heart"><span></span></a>--}}
{{--                                <a href="#"--}}
{{--                                   class="btn-product-icon btn-compare btn-icon-left w-icon-compare"><span></span></a>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
            @endif
            <!-- End of Product Single -->
        </div>
    </div>
</div>
