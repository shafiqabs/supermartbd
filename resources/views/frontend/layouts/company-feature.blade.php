<div class="owl-carousel owl-theme row cols-md-4 cols-sm-3 cols-1 icon-box-wrapper appear-animate br-sm mt-6 mb-10 appear-animate"
     data-owl-options="{
                    'nav': false,
                    'dots': false,
                    'loop': false,
                    'responsive': {
                        '0': {
                            'items': 1
                        },
                        '576': {
                            'items': 2
                        },
                        '768': {
                            'items': 3
                        },
                        '992': {
                            'items': 3
                        },
                        '1200': {
                            'items': 4
                        }
                    }
                }">

    @if(isset($response['GroupWisesupportPage']))
    <?php $i=1;?>
    @foreach($response['GroupWisesupportPage'] as $value)
        <div class="icon-box icon-box-side text-dark">
            <span class="icon-box-icon icon-shipping">
                @if($i == 1)
                <i class="w-icon-truck"></i>
                @endif
                @if($i == 2)
                    <i class="w-icon-bag"></i>
                @endif
                @if($i == 3)
                    <i class="w-icon-money"></i>
                @endif
                @if($i == 4)
                    <i class="w-icon-chat"></i>
                @endif
            </span>
            <div class="icon-box-content">
                <h4 class="icon-box-title mb-1 ls-normal">
                    {{$value->title}}
                </h4>
                <p class="text-default">
                <?php echo $value->content;?>
                </p>
            </div>
        </div>
        <?php $i++;?>
    @endforeach
    @endif

</div>
