<h2 class="title text-left pt-1 mb-5 appear-animate">Most Popular Brand</h2>
<div class="category-wrapper owl-carousel owl-theme row cols-xl-6 cols-lg-5 cols-md-4 cols-sm-3 cols-2 appear-animate"
     data-owl-options="{
                    'nav': false,
                    'dots': true,
                    'margin': 20,
                    'responsive': {
                        '0': {
                            'items': 2
                        },
                        '576': {
                            'items': 3
                        },
                        '768': {
                            'items': 4
                        },
                        '992': {
                            'items': 5
                        },
                        '1200': {
                            'items': 6
                        }
                    }
                }">



    @if(isset($response['all-brand']))
    @foreach($response['all-brand'] as $value)
        <div class="category category-ellipse">
            <figure class="category-media">
                <a href="{{route('brand.wish.product',$value->slug)}}">
                    <img src="{{ asset('backend/image/ProBrandImage').'/'.$value->brand_image;}}" alt="{{$value->name}}" width="190"
                         height="190" style="background-color: #5C92C0;" />
                </a>
            </figure>
            <div class="category-content">
                <h4 class="category-name">
                    <a href="{{route('brand.wish.product',$value->slug)}}">{{$value->name}}</a>
                </h4>
            </div>
        </div>
    @endforeach
    @endif
</div>
{{--    --}}
{{--    <div class="category category-ellipse">--}}
{{--        <figure class="category-media">--}}
{{--            <a href="shop-banner-sidebar.html">--}}
{{--                <img src="{{ asset('frontend/images/demos/demo3/categories/5.jpg') }}" alt="Categroy" width="190"--}}
{{--                     height="190" style="background-color: #9BC4CA;" />--}}
{{--            </a>--}}
{{--        </figure>--}}
{{--        <div class="category-content">--}}
{{--            <h4 class="category-name">--}}
{{--                <a href="shop-banner-sidebar.html">Nike</a>--}}
{{--            </h4>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="category category-ellipse">--}}
{{--        <figure class="category-media">--}}
{{--            <a href="shop-banner-sidebar.html">--}}
{{--                <img src="{{ asset('frontend/images/demos/demo3/categories/6.jpg') }}" alt="Categroy" width="190"--}}
{{--                     height="190" style="background-color: #48555B;" />--}}
{{--            </a>--}}
{{--        </figure>--}}
{{--        <div class="category-content">--}}
{{--            <h4 class="category-name">--}}
{{--                <a href="shop-banner-sidebar.html">Samsung</a>--}}
{{--            </h4>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="category category-ellipse">--}}
{{--        <figure class="category-media">--}}
{{--            <a href="shop-banner-sidebar.html">--}}
{{--                <img src="{{ asset('frontend/images/demos/demo3/categories/7.jpg') }}" alt="Categroy" width="190"--}}
{{--                     height="190" style="background-color: #D4E5EF;" />--}}
{{--            </a>--}}
{{--        </figure>--}}
{{--        <div class="category-content">--}}
{{--            <h4 class="category-name">--}}
{{--                <a href="shop-banner-sidebar.html">Xbox</a>--}}
{{--            </h4>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="category category-ellipse">--}}
{{--        <figure class="category-media">--}}
{{--            <a href="shop-banner-sidebar.html">--}}
{{--                <img src="{{ asset('frontend/images/demos/demo3/categories/8.jpg') }}" alt="Categroy" width="190"--}}
{{--                     height="190" style="background-color: #6A7881;" />--}}
{{--            </a>--}}
{{--        </figure>--}}
{{--        <div class="category-content">--}}
{{--            <h4 class="category-name">--}}
{{--                <a href="shop-banner-sidebar.html">Xiaomi</a>--}}
{{--            </h4>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="category category-ellipse">--}}
{{--        <figure class="category-media">--}}
{{--            <a href="shop-banner-sidebar.html">--}}
{{--                <img src="{{ asset('frontend/images/demos/demo3/categories/9.jpg') }}" alt="Categroy" width="190"--}}
{{--                     height="190" style="background-color: #E4E4E4;" />--}}
{{--            </a>--}}
{{--        </figure>--}}
{{--        <div class="category-content">--}}
{{--            <h4 class="category-name">--}}
{{--                <a href="shop-banner-sidebar.html">Apple</a>--}}
{{--            </h4>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
