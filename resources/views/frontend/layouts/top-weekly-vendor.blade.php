<h2 class="title text-left appear-animate mb-5">Top Weekly Vendors</h2>
<div class="owl-carousel owl-theme row cols-xl-4 cols-md-3 cols-sm-2 cols-1 vendor-wrapper appear-animate mb-10 pb-1"
     data-owl-options="{
                    'nav': false,
                    'dots': true,
                    'margin': 20,
                    'responsive': {
                        '0': {
                            'items': 1
                        },
                        '576': {
                            'items': 2
                        },
                        '768': {
                            'items': 3
                        },
                        '992': {
                            'items': 4
                        }
                    }
                }">


    @if(isset($response['vendor-limit']) && !empty($response['vendor-limit']))
        @foreach($response['vendor-limit'] as $value)

    <div class="vendor-widget">
        <div class="vendor-widget-banner">
            <figure class="vendor-banner">
                <a href="{{route('vendor.wish.product',$value->slug)}}">
                    <img src="{{ asset('backend/image/ProVendorImage').'/'.$value->store_image}}" alt="{{$value->store_name}}" width="1200"
                         height="390" style="background-color: #ECE7DF;" />
                </a>
            </figure>
            <div class="vendor-details">
                <figure class="vendor-logo">
                    <a href="{{route('vendor.wish.product',$value->slug)}}">
                        <img src="{{ asset('backend/image/ProVendorImage').'/'.$value->store_image}}" alt="{{$value->store_name}}"
                             width="90" height="90" />
                    </a>
                </figure>
                <div class="vendor-personal">
                    <h4 class="vendor-name">
                        <a href="{{route('vendor.wish.product',$value->slug)}}">{{$value->store_name}}</a>
                    </h4>
                    <span class="vendor-product-count">{{count($value->vendor_product)}} Products</span>
                </div>
            </div>
        </div>
        <!-- End of Vendor Widget Banner -->
    </div>

            @endforeach
        @endif
</div>
