<div class="footer-top">
    <div class="row">
        <div class="col-lg-4 col-sm-6">
            <div class="widget widget-about">
                <a href="{{ url('/') }}" class="logo-footer">
{{--                    <img src="{{ asset('frontend/images/demos/demo3/logo/Supermartbdlogo.png') }}" alt="logo" width="144" height="45" />--}}
                    <img src="{{ asset('frontend/images/demos/demo3/logo/Logo-black-bg.png') }}" alt="logo" width="144" height="45" />
                </a>
                <div class="widget-body">
                    <p class="widget-about-title">Got Question? Call us 24/7</p>
                    <a href="tel:18005707777" class="widget-about-call">{{$response['general_setting']->general_setting->mobile}}</a>
                    <p class="widget-about-desc">
                        {{$response['general_setting']->general_setting->description}}
                    </p>

                    <div class="social-icons social-icons-colored">
                        @if($response['general_setting']->general_setting->facebook)
                            <a href="{{$response['general_setting']->general_setting->facebook}}" class="social-icon social-facebook w-icon-facebook"></a>
                        @endif

                        @if($response['general_setting']->general_setting->twitter)
                            <a href="{{$response['general_setting']->general_setting->twitter}}" class="social-icon social-twitter w-icon-twitter"></a>
                        @endif

                        @if($response['general_setting']->general_setting->instagram)
                            <a href="{{$response['general_setting']->general_setting->instagram}}" class="social-icon social-instagram w-icon-instagram"></a>
                        @endif

                        @if($response['general_setting']->general_setting->youtube)
                            <a href="{{$response['general_setting']->general_setting->youtube}}" class="social-icon social-youtube w-icon-youtube"></a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
{{--        {{ dd($response['GroupWiseCompanyPage'])  }}--}}
        @if(isset($response['GroupWiseCompanyPage']))
        <div class="col-lg-3 col-sm-6">
            <div class="widget">
                <h3 class="widget-title">Company</h3>
                <ul class="widget-body">
                @foreach($response['GroupWiseCompanyPage'] as $value)
                        <li><a href="{{route('view.general.page',$value->slug)}}">{{$value->title}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif

        @if(isset($response['GroupWiseMyAccountPage']))
        <div class="col-lg-3 col-sm-6">
            <div class="widget">
                <h4 class="widget-title">My Account</h4>
                <ul class="widget-body">
                    <a href=""></a>
                @foreach($response['GroupWiseMyAccountPage'] as $value)
                    <li><a href="{{route('view.general.page',$value->slug)}}">{{$value->title}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif

        @if(isset($response['GroupWiseCustomerServicePage']))
        <div class="col-lg-3 col-sm-6">
            <div class="widget">
                <h4 class="widget-title">Customer Service</h4>
                <ul class="widget-body">
                @foreach($response['GroupWiseCustomerServicePage'] as $value)
                    <li><a href="{{route('view.general.page',$value->slug)}}">{{$value->title}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif
    </div>
</div>
