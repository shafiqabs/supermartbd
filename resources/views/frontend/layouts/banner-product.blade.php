<h2 class="title text-left appear-animate mb-5">
    @if(isset($response['category-product']))
    {{$response['category-product']->name}}
    @endif
</h2>

{{--{{dd($response['category-product'])}}--}}

<div class="row banner-product-wrapper appear-animate mb-10">
    <div class="col-lg-9 mb-4 mb-lg-0">
        <div class="banner banner-fixed br-sm appear-animate">
            <figure>
                @if(isset($response['category-product']))
                <img src="{{ asset('backend/image/ProCategoryImage').'/'.$response['category-product']->category_image}}" alt="banner" width="925" height="298"
                     style="background-color: #ECEDE8;" />
                @endif
            </figure>
            <div class="banner-content y-50">
                <h5 class="banner-subtitle text-primary font-weight-bold text-uppercase ls-25">{{$response['category-product']->banner_header}}</h5>
                <h3 class="banner-title font-weight-bold text-uppercase ls-25">
                    {{$response['category-product']->banner_title}}<br><strong class="text-capitalize">{{$response['category-product']->banner_sub_title}}</strong>
                </h3>
                <a href="{{route('category.wish.product',$response['category-product']->slug)}}" class="btn btn-dark btn-outline btn-rounded">Shop
                    Now</a>
            </div>
        </div>
        <div class="owl-carousel owl-theme row cols-md-4 cols-sm-3 cols-2 mt-4" data-owl-options="{
                            'nav': false,
                            'dots': false,
                            'margin': 20,
                            'responsive': {
                                '0': {
                                    'items': 2
                                },
                                '576': {
                                    'items': 3
                                },
                                '768': {
                                    'items': 4
                                }
                            }
                        }">
            @if(isset($response['category-product']) and $response['category-product']->category_product)
            @foreach($response['category-product']->category_product as $value)


            <div class="product-wrap">
                <div class="product product-simple text-center">
                    <figure class="product-media">
                        <a href="{{route('single.product.details',$value->slug)}}">
                            <img src="{{ asset('backend/image/ProProductImage').'/'.$value->feature_image}}" alt="Product"
                                 width="330" height="338" />
                        </a>
                        <div class="product-action-vertical">
{{--                            <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"--}}
{{--                               title="Add to wishlist"></a>--}}
{{--                            <a href="#" class="btn-product-icon btn-compare w-icon-compare"--}}
{{--                               title="Add to Compare"></a>--}}
                        </div>
                        <div class="product-action">
                            <a href="#" class="btn-product btn-quickview" product-id="{{$value->id}}" product-slug="{{$value->slug}}" data-href="{{route('web.quick.view')}}" title="Quick View">Quick
                                View</a>
                        </div>
                    </figure>
                    <div class="product-details">
                        <h4 class="product-name"><a href="{{route('single.product.details',$value->slug)}}">{{$value->name}}</a>
                        </h4>
                        <div class="ratings-container">
                            <div class="ratings-full">
                                <span class="ratings" style="width: 100%;"></span>
                                <span class="tooltiptext tooltip-top"></span>
                            </div>
                            <a href="" class="rating-reviews">(3 reviews)</a>
                        </div>
                        <div class="product-pa-wrapper">
                            <div class="product-price">
                                <ins class="new-price">Tk. {{$value->salesprice}}</ins>
                            </div>
                            <div class="product-action">
                                <a product-id="{{$value->id}}" product-name="{{$value->name}}" product-slug="{{$value->slug}}" product-image="{{$value->feature_image}}"  product-price="{{$value->salesprice}}" product-quantity="1" data-href="{{route('web.cart.add')}}" class="btn-cart btn-product btn btn-icon-right btn-link btn-underline add_cart_ajax">
                                    Add To Cart
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
            @endif
            <!-- End of Product Wrap -->
        </div>
    </div>
    <div class="col-lg-3 mb-4">
        <div class="widget widget-products widget-products-bordered h-100">
            <div class="widget-body br-sm h-100 pb-2">
                @php
                $count=0;
                    if (isset($response['recommend-product'])){
                     $count = count($response['recommend-product']);
                    }
                @endphp
                <h4 class="title-sm title-underline font-weight-bolder ls-normal mb-2">Top {{$count}} Recommend</h4>
                <div class="owl-carousel owl-theme owl-nav-top row cols-lg-1 cols-md-3"
                     data-owl-options="{
                                    'nav': true,
                                    'dots': false,
                                    'margin': 20,
                                    'responsive': {
                                        '0': {
                                            'items': 1
                                        },
                                        '576': {
                                            'items': 2
                                        },
                                        '992': {
                                            'items': 1
                                        }
                                    }
                                }">

                    @if(isset($response['recommend-product']))
                    @foreach(array_chunk($response['recommend-product'],4) as $chunk)
                        <div class="product-widget-wrap">

                            @foreach($chunk as $value)
                                <div class="product product-widget bb-no">
                                    <figure class="product-media">
                                        <a href="{{route('single.product.details',$value->slug)}}">
                                            <img src="{{ asset('backend/image/ProProductImage').'/'.$value->feature_image}}" alt="Product"
                                                 width="105" height="118" />
                                        </a>
                                    </figure>
                                    <div class="product-details">
                                        <h4 class="product-name">
                                            <a href="{{route('single.product.details',$value->slug)}}">{{$value->name}}</a>
                                        </h4>
                                        <div class="ratings-container">
                                            <div class="ratings-full">
                                                <span class="ratings" style="width: 80%;"></span>
                                                <span class="tooltiptext tooltip-top"></span>
                                            </div>
                                        </div>
                                        <div class="product-price">
                                            <ins class="new-price">{{$value->salesprice}}</ins>
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

