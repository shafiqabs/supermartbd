<div class="owl-carousel owl-theme intro-banner row cols-lg-3 cols-sm-2 cols-1 appear-animate"
     data-owl-options="{
                    'nav': false,
                    'dots': true,
                    'margin': 20,
                    'responsive': {
                        '0': {
                            'items': 1
                        },
                        '576': {
                            'items': 2
                        },
                        '992': {
                            'items': 3
                        }
                    }
                }">
<?php
//    echo '<pre>';
//    print_r($response['BannerTop']);
//    exit();
?>
@if(isset($response['BannerTop']) )
    @foreach($response['BannerTop']->top_banner_items as $value)

    <figure class="banner banner-fixed br-sm">
        <img src="{{ asset('backend/image/BannerImage').'/'.$value->attach_link}}" alt="Category Banner" width="400"
             height="200" style="background-color: #3C3C3C;" />
        <div class="banner-content y-50">
            <h5 class="banner-subtitle text-primary text-uppercase font-weight-bold ls-25">{{$value->title_top}}
            </h5>
            <h3 class="banner-title text-white font-weight-bold">{{$value->title}}</h3>
            <?php
                if ($value->banner_type == 'Category' && $value->category_id){
                    $category = \App\Modules\MasterData\Models\Category::find($value->category_id);
                    $route = route('category.wish.product',$category?$category->slug:'');
                }else{
                    $brand = \App\Modules\MasterData\Models\ProductBrand::find($value->brand_id);
                    $route = route('brand.wish.product',$brand?$brand->slug:'');
                }
            ?>
            <a href="{{$route}}"
               class="btn btn-white btn-link btn-underline btn-icon-right">
                Shop Now<i class="w-icon-long-arrow-right"></i>
            </a>
        </div>
    </figure>
        @endforeach
@endif
</div>
