<div class="cta-wrapper pt-2 pb-1 mb-10 appear-animate">
    <div class="banner banner-newsletter-bg br-sm"
         style="background-image: url({{ asset('frontend/images/demos/demo3/newsletter-bg.jpg') }}); background-color: #636363;">
        <div class="content-left mb-4 mb-lg-0">
            <h4 class="banner-title text-white ls-25 font-weight-bold text-uppercase">Subscribe To
                Our Newsletter</h4>
            <p class="text-white mb-0">Get all the latest information on Events, Sales and Offers.
            </p>
        </div>
        <div class="content-right">
            <form action="#" method="get"
                  class="input-wrapper input-wrapper-rounded ml-auto mr-auto mr-lg-0">
                <input type="email" class="form-control text-default" name="email" id="email"
                       placeholder="Your E-mail Address">
                <button class="btn btn-primary btn-rounded btn-icon-right" type="submit">SUBSCRIBE<i
                        class="w-icon-long-arrow-right"></i></button>
            </form>
        </div>
    </div>
</div>
