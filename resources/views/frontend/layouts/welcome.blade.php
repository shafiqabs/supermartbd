

@extends(/** @lang text */'frontend.master.master')

@section('body')


    <!-- Start of Main -->
    <div class="main">
        <div class="container">

{{--slider--}}
    @include('frontend.layouts.slider')
            <!-- End of Intro Wrapper -->
{{--banner--}}
@include('frontend.layouts.banner')
{{--company-feature--}}
    @include('frontend.layouts.company-feature')
            <!-- End of Iocn Box Wrapper -->
{{--most popular band--}}
@include('frontend.layouts.most-popular-brand')
            <!-- End of Category Wrapper -->

            <div class="row deals-wrapper appear-animate mb-7">
{{--deals hot--}}
@include('frontend.layouts.deals-hot')
{{--best seller--}}
@include('frontend.layouts.best-seller')
            </div>
            <!-- End of Deals Wrapper -->
{{--    top weekly vendor--}}
@include('frontend.layouts.top-weekly-vendor')
            <!-- End of Owl Carousel -->

{{--Banner product --}}
    @include('frontend.layouts.banner-product')
            <!-- Banner Product Wrapper -->
{{--banner 2--}}
@include('frontend.layouts.banner2')
            <!-- End of Category Accessories -->
{{--banner product 2--}}
    @include('frontend.layouts.banner-product2')
            <!-- Banner Product Wrapper -->

{{--banner product 3--}}
@include('frontend.layouts.banner-product3')
            <!-- Banner Product Wrapper -->
        </div>
        <!-- End of Container -->

        <section class="grey-section">
            <div class="container">
                <div class="title-link-wrapper mb-2 appear-animate">
                {{--our blog--}}
{{--                @include('frontend.layouts.our-blog')--}}
                <!-- End of Owl Carousel -->
{{--our clients--}}
{{--@include('frontend.layouts.our-clients')--}}
                <!-- End of Brands Wrapper -->

{{--our news letter--}}
{{--@include('frontend.layouts.our-newsetter')--}}
                <!--End of Cta Wrapper -->

{{--out instragram--}}
{{--@include('frontend.layouts.our-instragram')--}}
                </div>
            </div>
            <!-- End of Container -->
        </section>

    </div>
    <!-- End of Main -->

    <!-- Start of Footer -->
    <footer class="footer footer-dark appear-animate">
        <div class="container">
            @include('frontend.layouts.footer-top')
{{--            @include('frontend.layouts.footer-middle')--}}
            @include('frontend.layouts.footer-bottom')
        </div>
    </footer>
    <!-- End of Footer -->

<!-- End of Page-wrapper -->

<!-- Start of Sticky Footer -->
@include('frontend.layouts.sticky-footer')
<!-- End of Sticky Footer -->

<!-- Start of Scroll Top -->
<a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="fas fa-chevron-up"></i></a>
<!-- End of Scroll Top -->

<!-- Start of Mobile Menu -->
@include('frontend.layouts.mobile-menu')
<!-- End of Mobile Menu -->

<!-- Start of Quick View -->
@include('frontend.layouts.quick-view')
<!-- End of Quick view -->

@endsection
