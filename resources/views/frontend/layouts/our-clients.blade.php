<h2 class="title text-left mb-5 appear-animate">Our Clients</h2>
<div class="owl-carousel owl-theme brands-wrapper mb-10 row gutter-no cols-lg-5 cols-md-4 cols-sm-3 cols-2 bg-white appear-animate"
     data-owl-options="{
                        'nav': false,
                        'dots': false,
                        'margin': 0,
                        'responsive': {
                            '0': {
                                'items': 2
                            },
                            '576': {
                                'items': 3
                            },
                            '768': {
                                'items': 4
                            },
                            '992': {
                                'items': 5
                            }
                        }
                    }">
    <div class="brand-col">
        <figure class="brand-wrapper">
            <img src="{{ asset('frontend/images/demos/demo3/brand/1.png') }}" alt="Brand" width="247" height="94" />
        </figure>
        <figure class="brand-wrapper">
            <img src="{{ asset('frontend/images/demos/demo3/brand/2.png') }}" alt="Brand" width="247" height="94" />
        </figure>
    </div>
    <div class="brand-col">
        <figure class="brand-wrapper">
            <img src="{{ asset('frontend/images/demos/demo3/brand/3.png') }}" alt="Brand" width="247" height="94" />
        </figure>
        <figure class="brand-wrapper">
            <img src="{{ asset('frontend/images/demos/demo3/brand/4.png') }}" alt="Brand" width="247" height="94" />
        </figure>
    </div>
    <div class="brand-col">
        <figure class="brand-wrapper">
            <img src="{{ asset('frontend/images/demos/demo3/brand/5.png') }}" alt="Brand" width="247" height="94" />
        </figure>
        <figure class="brand-wrapper">
            <img src="{{ asset('frontend/images/demos/demo3/brand/6.png') }}" alt="Brand" width="247" height="94" />
        </figure>
    </div>
    <div class="brand-col">
        <figure class="brand-wrapper">
            <img src="{{ asset('frontend/images/demos/demo3/brand/7.png') }}" alt="Brand" width="247" height="94" />
        </figure>
        <figure class="brand-wrapper">
            <img src="{{ asset('frontend/images/demos/demo3/brand/8.png') }}" alt="Brand" width="247" height="94" />
        </figure>
    </div>
    <div class="brand-col">
        <figure class="brand-wrapper">
            <img src="{{ asset('frontend/images/demos/demo3/brand/9.png') }}" alt="Brand" width="247" height="94" />
        </figure>
        <figure class="brand-wrapper">
            <img src="{{ asset('frontend/images/demos/demo3/brand/10.png') }}" alt="Brand" width="247" height="94" />
        </figure>
    </div>
</div>
