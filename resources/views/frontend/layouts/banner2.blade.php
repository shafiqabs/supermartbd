<div class="row category-accessories appear-animate mb-7">
    @if(isset($response['BannerBottom']) )
        @foreach($response['BannerBottom']->bottom_banner_items as $value)
            <div class="col-md-6 mb-4">
                <figure class="banner banner-fixed br-sm">
                    <img src="{{ asset('backend/image/BannerImage').'/'.$value->attach_link}}" alt="Category Banner" width="610"
                         height="200" style="background-color: #52575B;" />
                    <div class="banner-content y-50">
                        <h5 class="banner-subtitle font-weight-bold text-primary text-uppercase ls-25">{{$value->title_top}}</h5>
                        <h3 class="banner-title text-white text-capitalize font-weight-bold">{{$value->title}}</h3>
                        <?php
                        if ($value->banner_type == 'Category' && $value->category_id){
                            $category = \App\Modules\MasterData\Models\Category::find($value->category_id);
                            $route = route('category.wish.product',$category?$category->slug:'');
                        }else{
                            $brand = \App\Modules\MasterData\Models\ProductBrand::find($value->brand_id);
                            $route = route('brand.wish.product',$brand?$brand->slug:'');
                        }
                        ?>
                        <a href="{{$route}}"
                           class="btn btn-white btn-link btn-underline btn-icon-right">
                            Shop Now<i class="w-icon-long-arrow-right"></i>
                        </a>
                    </div>
                </figure>
            </div>
        @endforeach
    @endif

</div>
