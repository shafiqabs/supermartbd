<div class="col-lg-3 mb-4">
    <div class="widget widget-products widget-products-bordered ">
        <div class="widget-body br-sm h-100">
            @php
            $count=0;
                if (isset($response['best-seller'])){
                    $count = count($response['best-seller']);
                }
            @endphp
            <h4 class="title-sm title-underline font-weight-bolder ls-normal mb-2">Top {{$count}} Best
                Seller</h4>
            <div class="owl-carousel owl-theme owl-nav-top row cols-lg-1 cols-md-3"
                 data-owl-options="{
                                    'nav': true,
                                    'dots': false,
                                    'margin': 20,
                                    'responsive': {
                                        '0': {
                                            'items': 1
                                        },
                                        '576': {
                                            'items': 2
                                        },
                                        '768': {
                                            'items': 3
                                        },
                                        '992': {
                                            'items': 1
                                        }
                                    }
                                }">


                @if(isset($response['best-seller']))
                            @foreach(array_chunk($response['best-seller'],3) as $chunk)
                        <div class="product-widget-wrap">

                                @foreach($chunk as $value)
                                <div class="product product-widget bb-no">
                                <figure class="product-media">
                                    <a href="{{route('single.product.details',$value->slug)}}">
                                        <img src="{{ asset('backend/image/ProProductImage').'/'.$value->feature_image}}" alt="Product"
                                             width="105" height="118" />
                                    </a>
                                </figure>
                                <div class="product-details">
                                    <h4 class="product-name">
                                        <a href="{{route('single.product.details',$value->slug)}}">{{$value->name}}</a>
                                    </h4>
                                    <div class="ratings-container">
                                        <div class="ratings-full">
                                            <span class="ratings" style="width: 80%;"></span>
                                            <span class="tooltiptext tooltip-top"></span>
                                        </div>
                                    </div>
                                    <div class="product-price">
                                        <ins class="new-price">{{$value->salesprice}}</ins>
                                    </div>
                                </div>
                                </div>
                        @endforeach

                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </div>
</div>
