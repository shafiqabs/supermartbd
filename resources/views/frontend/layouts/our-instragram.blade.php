<h2 class="title text-left mb-5 appear-animate">Our Instagram</h2>
<div class="owl-carousel owl-theme row cols-xl-6 cols-lg-5 cols-md-4 cols-sm-3 cols-2 appear-animate"
     data-owl-options="{
                        'items': 6,
                        'nav': false,
                        'dots': true,
                        'margin': 20,
                        'autoplay': false,
                        'responsive': {
                            '0': {
                                'items': 2
                            },
                            '576': {
                                'items': 3
                            },
                            '768': {
                                'items': 4
                            },
                            '992': {
                                'items': 5
                            },
                            '1200': {
                                'items': 6
                            }
                        }
                    }">
    <figure class="instagram br-sm">
        <a href="#">
            <img src="{{ asset('frontend/images/demos/demo3/instagram/1.jpg') }}" alt="Instagram" width="190"
                 height="190" style="background-color: #C3CAD0;" />
        </a>
    </figure>
    <!-- End of Instagram -->
    <figure class="instagram br-sm">
        <a href="#">
            <img src="{{ asset('frontend/images/demos/demo3/instagram/2.jpg') }}" alt="Instagram" width="190"
                 height="190" style="background-color: #C0C2BD;" />
        </a>
    </figure>
    <!-- End of Instagram -->
    <figure class="instagram br-sm">
        <a href="#">
            <img src="{{ asset('frontend/images/demos/demo3/instagram/3.jpg') }}" alt="Instagram" width="190"
                 height="190" style="background-color: #929CA5;" />
        </a>
    </figure>
    <!-- End of Instagram -->
    <figure class="instagram br-sm">
        <a href="#">
            <img src="{{ asset('frontend/images/demos/demo3/instagram/4.jpg') }}" alt="Instagram" width="190"
                 height="190" style="background-color: #CAD3DA;" />
        </a>
    </figure>
    <!-- End of Instagram -->
    <figure class="instagram br-sm">
        <a href="#">
            <img src="{{ asset('frontend/images/demos/demo3/instagram/5.jpg') }}" alt="Instagram" width="190"
                 height="190" style="background-color: #DBE1E1;" />
        </a>
    </figure>
    <!-- End of Instagram -->
    <figure class="instagram br-sm">
        <a href="#">
            <img src="{{ asset('frontend/images/demos/demo3/instagram/6.jpg') }}" alt="Instagram" width="190"
                 height="190" style="background-color: #B5CACB;" />
        </a>
    </figure>
    <!-- End of Instagram -->
</div>
