

@extends(/** @lang text */'frontend.master.master')

@section('body')


<?php
//    echo '<pre>';
//    print_r($response->vendor_product);
?>
    <!-- Start of Main -->
    <main class="main">
        <!-- Start of Breadcrumb -->
        <nav class="breadcrumb-nav">
            <div class="container">
                <ul class="breadcrumb bb-no">
                    <li><a href="demo1.html">Home</a></li>
                    <li><a href="#">Vendor</a></li>
                    <li><a href="#">{{$response['single-vendor-info']->store_name}}</a></li>
                    <li>Store</li>
                </ul>
            </div>
        </nav>
        <!-- End of Breadcrumb -->

        <!-- Start of Pgae Contetn -->
        <div class="page-content mb-8">
            <div class="container">
                <div class="row gutter-lg">
                    <aside class="sidebar left-sidebar vendor-sidebar sticky-sidebar-wrapper sidebar-fixed">
                        <!-- Start of Sidebar Overlay -->
                        <div class="sidebar-overlay"></div>
                        <a class="sidebar-close" href="#"><i class="close-icon"></i></a>
                        <a href="#" class="sidebar-toggle"><i class="fas fa-chevron-right"></i></a>
                        <div class="sidebar-content">
                            <div class="sticky-sidebar">
{{--                                <div class="widget widget-collapsible widget-categories">--}}
{{--                                    <h3 class="widget-title"><span>All Categories</span></h3>--}}
{{--                                    <ul class="widget-body filter-items search-ul">--}}
{{--                                        <li><a href="#">Clothing</a></li>--}}
{{--                                        <li><a href="#">Computers</a></li>--}}
{{--                                        <li><a href="#">Electronics</a></li>--}}
{{--                                        <li><a href="#">Fashion</a></li>--}}
{{--                                        <li><a href="#">Furniture</a></li>--}}
{{--                                        <li><a href="#">Games</a></li>--}}
{{--                                        <li><a href="#">Kitchen</a></li>--}}
{{--                                        <li><a href="#">Shoes</a></li>--}}
{{--                                        <li><a href="#">Sports</a></li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
<!-- Start of Collapsible widget -->
    <div class="widget widget-collapsible">
        <h3 class="widget-title"><label>All Categories</label></h3>
        <ul class="widget-body filter-items search-ul">
            @if(isset($response['Category']) && count($response['Category'])>0)
                @foreach($response['Category'] as $value)
                    <li><a href="{{route('category.wish.product',$value->slug)}}">{{$value->name}}</a></li>
                @endforeach
            @endif

        </ul>
    </div>
    <!-- End of Collapsible Widget -->
                                <!-- End of Widget -->
                                <div class="widget widget-collapsible widget-contact">
                                    <h3 class="widget-title"><span>Contact Vendor</span></h3>
                                    <div class="widget-body">
                                        <input type="text" class="form-control" name="name" id="name"
                                               placeholder="Your Name" />
                                        <input type="text" class="form-control" name="email" id="email_1"
                                               placeholder="you@example.com" />
                                        <textarea name="message" maxlength="1000" cols="25" rows="6"
                                                  placeholder="Type your messsage..." class="form-control"
                                                  required="required"></textarea>
                                        <a href="#" class="btn btn-dark btn-rounded">Send Message</a>
                                    </div>
                                </div>
                                <!-- End of Widget -->
{{--                                <div class="widget widget-collapsible widget-time">--}}
{{--                                    <h3 class="widget-title"><span>Store Time</span></h3>--}}
{{--                                    <ul class="widget-body">--}}
{{--                                        <li><label>Sunday</label></li>--}}
{{--                                        <li><label>Monday</label></li>--}}
{{--                                        <li><label>Tuesday</label></li>--}}
{{--                                        <li><label>Wednesday</label></li>--}}
{{--                                        <li><label>Thursday</label></li>--}}
{{--                                        <li><label>Friday</label></li>--}}
{{--                                        <li><label>Saturday</label></li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
                                <!-- End of Widget -->
                                @php
                                    $count = count($response['best-seller']);
                                @endphp


                                <div class="widget widget-collapsible widget-products">
                                    <h3 class="" style="margin: 10px;
padding: 1.4rem 2.5rem 1.4rem 0;
font-weight: 600;
font-size: 1.6rem;
color: #333;
border-bottom: 1px solid #eee;
text-transform: capitalize;"><span>{{$count}} Best Selling</span></h3>

                                    <div class="owl-carousel owl-theme owl-nav-top row cols-lg-1 cols-md-3"
                                         data-owl-options="{
                                    'nav': true,
                                    'dots': false,
                                    'margin': 20,
                                    'responsive': {
                                        '0': {
                                            'items': 1
                                        },
                                        '576': {
                                            'items': 2
                                        },
                                        '768': {
                                            'items': 3
                                        },
                                        '992': {
                                            'items': 1
                                        }
                                    }
                                }">
                                        @foreach(array_chunk($response['best-seller'],4) as $chunk)
                                            <div class="product-widget-wrap">

                                                @foreach($chunk as $value)
                                                    <div class="product product-widget">
                                                        <figure class="product-media">
                                                            <a href="{{route('single.product.details',$value->slug)}}">
                                                                <img src="{{ asset('backend/image/ProProductImage').'/'.$value->feature_image}}" alt="Product"
                                                                     width="105" height="118" />
                                                            </a>
                                                        </figure>
                                                        <div class="product-details">
                                                            <h4 class="product-name">
                                                                <a href="{{route('single.product.details',$value->slug)}}">{{$value->name}}</a>
                                                            </h4>
                                                            <div class="ratings-container">
                                                                <div class="ratings-full">
                                                                    <span class="ratings" style="width: 80%;"></span>
                                                                    <span class="tooltiptext tooltip-top"></span>
                                                                </div>
                                                            </div>
                                                            <div class="product-price">
                                                                <ins class="new-price">{{$value->salesprice}}</ins>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach

                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <!-- End of Widget -->
                                <?php
                                    $dealsHotcount = count($response['deals-hot']);
                                ?>
                                <div class="widget widget-collapsible widget-products">
                                    <h3 class="" style="margin: 10px;
padding: 1.4rem 2.5rem 1.4rem 0;
font-weight: 600;
font-size: 1.6rem;
color: #333;
border-bottom: 1px solid #eee;
text-transform: capitalize;"><span>Top {{$dealsHotcount}}   Deals Hot</span></h3>

                                    <div class="owl-carousel owl-theme owl-nav-top row cols-lg-1 cols-md-3"
                                         data-owl-options="{
                                    'nav': true,
                                    'dots': false,
                                    'margin': 20,
                                    'responsive': {
                                        '0': {
                                            'items': 1
                                        },
                                        '576': {
                                            'items': 2
                                        },
                                        '768': {
                                            'items': 3
                                        },
                                        '992': {
                                            'items': 1
                                        }
                                    }
                                }">
                                        @foreach(array_chunk($response['deals-hot'],4) as $chunk)
                                            <div class="product-widget-wrap">

                                                @foreach($chunk as $value)
                                                    <div class="product product-widget">
                                                        <figure class="product-media">
                                                            <a href="{{route('single.product.details',$value->slug)}}">
                                                                <img src="{{ asset('backend/image/ProProductImage').'/'.$value->feature_image}}" alt="Product"
                                                                     width="105" height="118" />
                                                            </a>
                                                        </figure>
                                                        <div class="product-details">
                                                            <h4 class="product-name">
                                                                <a href="{{route('single.product.details',$value->slug)}}">{{$value->name}}</a>
                                                            </h4>
                                                            <div class="ratings-container">
                                                                <div class="ratings-full">
                                                                    <span class="ratings" style="width: 80%;"></span>
                                                                    <span class="tooltiptext tooltip-top"></span>
                                                                </div>
                                                            </div>
                                                            <div class="product-price">
                                                                <ins class="new-price">{{$value->salesprice}}</ins>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach

                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </aside>
                    <!-- End of Sidebar -->

                    <div class="main-content">
                        @if(isset($response['single-vendor-info']->store_name) && !empty($response['single-vendor-info']->store_name))
                        <div class="store store-banner mb-4">
                            <figure class="store-media">
                                <img src="{{ asset('backend/image/ProVendorImage').'/'.$response['single-vendor-info']->store_image}}" alt="{{$response['single-vendor-info']->store_name}}" width="930" height="446"
                                     style="background-color: #414960;" />
                            </figure>
                            <div class="store-content">
                                <figure class="seller-brand">
                                    <img src="{{ asset('backend/image/ProVendorImage').'/'.$response['single-vendor-info']->store_image}}" alt="{{$response['single-vendor-info']->store_name}}" width="80"
                                         height="80" />
                                </figure>
                                <h4 class="store-title">{{$response['single-vendor-info']->store_name}}</h4>
                                <ul class="seller-info-list list-style-none mb-6">
                                    <li class="store-address">
                                        <i class="w-icon-map-marker"></i>
                                        {{$response['single-vendor-info']->store_address}}
                                    </li>
                                    <li class="store-phone">
                                        <a href="tel:{{$response['single-vendor-info']->mobile}}">
                                            <i class="w-icon-phone"></i>
                                            {{$response['single-vendor-info']->mobile}}
                                        </a>
                                    </li>
                                    <li class="store-rating">
                                        <i class="w-icon-star-full"></i>
                                        4.33 rating from 3 reviews
                                    </li>
                                    <li class="store-open">
                                        <i class="w-icon-cart"></i>
                                        Store Open
                                    </li>
                                </ul>
                                <div class="social-icons social-no-color border-thin">
                                    <a href="#" class="social-icon social-facebook w-icon-facebook"></a>
                                    <a href="#" class="social-icon social-google w-icon-google"></a>
                                    <a href="#" class="social-icon social-twitter w-icon-twitter"></a>
                                    <a href="#" class="social-icon social-pinterest w-icon-pinterest"></a>
                                    <a href="#" class="social-icon social-youtube w-icon-youtube"></a>
                                    <a href="#" class="social-icon social-instagram w-icon-instagram"></a>
                                </div>
                            </div>
                        </div>
                        @endif
                        <!-- End of Store Banner -->


                        <h2 class="title vendor-product-title mb-4"><a href="#">Products</a></h2>

                        <div class="product-wrapper row cols-md-3 cols-sm-2 cols-2">
                            @if(isset($response['vendor-product']->data))
                                @foreach($response['vendor-product']->data as $value)
                            <div class="product-wrap">
                                <div class="product text-center">
                                    <figure class="product-media">
                                        <a href="{{route('single.product.details',$value->slug)}}">
                                            <img src="{{ asset('backend/image/ProProductImage').'/'.$value->feature_image}}" alt="{{$value->name}}" width="300"
                                                 height="338" />
                                        </a>
                                        <div class="product-action-vertical">
                                            <a href="#" class="btn-product-icon btn-cart w-icon-cart add_cart_ajax"
                                               product-id="{{$value->id}}" product-name="{{$value->name}}" product-slug="{{$value->slug}}" product-image="{{$value->feature_image}}"  product-price="{{$value->salesprice}}" product-quantity="1" data-href="{{route('web.cart.add')}}"  title="Add to cart"></a>
{{--                                            <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"--}}
{{--                                               title="Wishlist"></a>--}}
{{--                                            <a href="#" class="btn-product-icon btn-compare w-icon-compare"--}}
{{--                                               title="Compare"></a>--}}
                                            <a href="#" class="btn-product-icon btn-quickview w-icon-search" product-id="{{$value->id}}" product-slug="{{$value->slug}}" data-href="{{route('web.quick.view')}}"
                                               title="Quick View"></a>
                                        </div>
                                    </figure>
                                    <div class="product-details">
                                        <h3 class="product-name">
                                            <a href="{{route('single.product.details',$value->slug)}}">{{$value->name}}</a>
                                        </h3>
                                        <div class="ratings-container">
                                            <div class="ratings-full">
                                                <span class="ratings" style="width: 100%;"></span>
                                                <span class="tooltiptext tooltip-top"></span>
                                            </div>
                                            <a href="{{route('single.product.details',$value->slug)}}" class="rating-reviews">(9 reviews)</a>
                                        </div>
                                        <div class="product-pa-wrapper">
                                            <div class="product-price">
                                                {{$value->salesprice}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                @endforeach
                            @endif
                                <div class=" justify-content-right">
{{--                                    {{ $response['vendor_product']->links('backend.layouts.pagination') }}--}}
                                </div>
                        </div>
                    </div>
                    <!-- End of Main Content -->
                </div>
            </div>
        </div>
        <!-- End of Page Content -->
    </main>
    <!-- End of Main -->



    <section class="grey-section">
        <div class="container">
            <div class="title-link-wrapper mb-2 appear-animate">
            {{--our blog--}}
            @include('frontend.layouts.our-blog')
            <!-- End of Owl Carousel -->
            {{--our clients--}}
            @include('frontend.layouts.our-clients')
            <!-- End of Brands Wrapper -->

            {{--our news letter--}}
            @include('frontend.layouts.our-newsetter')
            <!--End of Cta Wrapper -->

                {{--out instragram--}}
                {{--@include('frontend.layouts.our-instragram')--}}
            </div>
            <!-- End of Container -->
    </section>


    <!-- Start of Footer -->
    <footer class="footer footer-dark appear-animate">
        <div class="container">
            {{--footer top--}}
            @include('frontend.layouts.footer-top')
            @include('frontend.layouts.footer-middle')
            @include('frontend.layouts.footer-bottom')
        </div>
    </footer>
    <!-- End of Footer -->
    </div>
    <!-- End of Page-wrapper -->

    <!-- Start of Sticky Footer -->
    @include('frontend.layouts.sticky-footer')
    <!-- End of Sticky Footer -->

    <!-- Start of Scroll Top -->
    <a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="fas fa-chevron-up"></i></a>
    <!-- End of Scroll Top -->

    <!-- Start of Mobile Menu -->
    @include('frontend.layouts.mobile-menu')
    <!-- End of Mobile Menu -->

    <!-- Start of Quick View -->
    @include('frontend.layouts.quick-view')
    <!-- End of Quick view -->

@endsection
