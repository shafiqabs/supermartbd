

@extends(/** @lang text */'frontend.master.master')

@section('body')

    <?php
    $cart_items = [];
    if(Session::has('cart')){
        $cart_items = Session::get('cart');
    }

    $cart_total = [];
    if(Session::has('cart_total')){
        $cart_total = Session::get('cart_total');
    }
    ?>

    <!-- Start of Main -->
    <main class="main cart">
        <!-- Start of Breadcrumb -->
        <nav class="breadcrumb-nav">
            <div class="container">
                <ul class="breadcrumb shop-breadcrumb bb-no">
                    <li class="active"><a href="{{route('web.cart.view')}}">Shopping Cart</a></li>
                    <li><a href="{{route('web.checkout.store')}}">Checkout</a></li>
                    <li><a href="order.html">Order Complete</a></li>
                </ul>
            </div>
        </nav>
        <!-- End of Breadcrumb -->

        <!-- Start of PageContent -->
        <div class="page-content">
            <div class="container">
                <div class="row gutter-lg mb-10">
                    <div class="col-lg-8 pr-lg-4 mb-6">
                        <table class="shop-table cart-table">
                            <thead>
                            <tr>
                                <th class="product-name"><span>Product</span></th>
                                <th></th>
                                <th class="product-price"><span>Price</span></th>
                                <th class="product-quantity"><span>Quantity</span></th>
                                <th class="product-subtotal"><span>Subtotal</span></th>
                            </tr>
                            </thead>
                            <tbody>

                            @if(count($cart_items) > 0)
                                @foreach($cart_items as $cart)
                            <tr id="remove_id_{{$cart['product_id']}}" class="total_cart_hide">
                                <td class="product-thumbnail">
                                    <div class="p-relative">
                                        <a href="{{route('single.product.details',$cart['product_slug'])}}">
                                            <figure>
                                                <img src="{{ asset('backend/image/ProProductImage').'/'.$cart['product_image']}}" alt="{{$cart['product_name']}}"
                                                     width="300" height="338">
                                            </figure>
                                        </a>
                                        <button class="btn btn-link btn-close"   id="remove-cart-product" product_id="{{$cart['product_id']}}" data-href="{{route('web.cart.remove')}}">
                                            <i class="fas fa-times"></i>
                                        </button>
                                    </div>
                                </td>
                                <td class="product-name">
                                    <a href="{{route('single.product.details',$cart['product_slug'])}}">
                                        {{$cart['product_name']}}
                                    </a>
                                </td>
                                <td class="product-price">
                                    <span class="amount">{{$cart['product_price']}}</span>
                                    <input type="hidden" id="product_price_{{$cart['product_id']}}"  value="{{$cart['product_price']}}">
                                </td>
                                <td class="product-quantity">
                                    <div class="input-group">
                                        <a data-href="{{route('web.cart.update')}}" id="cart_update"></a>
                                        <input class="form-control input_quantity" product-id="{{$cart['product_id']}}" id="quantity_value_{{$cart['product_id']}}" type="number" min="1" max="100000" value="{{$cart['product_quantity']}}">
                                        <button class="mycart-quantity-plus w-icon-plus" button-status="plus" product-id="{{$cart['product_id']}}"></button>
                                        <button class="mycart-quantity-minus w-icon-minus" button-status="minus" product-id="{{$cart['product_id']}}"></button>
                                    </div>
                                </td>
                                <td class="product-subtotal">
                                    <span class="amount" id="amount_{{$cart['product_id']}}">{{$cart['product_price']*$cart['product_quantity']}}</span>
                                </td>
                            </tr>
                                @endforeach
                            @else
                                <tr align="center">
                                    <td colspan="4">
                                        Your Cart is empty
                                    </td>
                                </tr>
                            @endif

                            <tr align="center" id="empty_message" style="display: none">
                                <td colspan="4">
                                    Your Cart is empty
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <div class="cart-action mb-6">
                            <a data-href="{{route('web.cart.clear')}}" id="cart_clear_route"></a>
                            <a href="#" class="btn btn-dark btn-rounded btn-icon-left btn-shopping mr-auto"><i class="w-icon-long-arrow-left"></i>Continue Shopping</a>
                            <button type="submit" class="btn btn-rounded btn-default btn-clear cart_clear" name="clear_cart" value="Clear Cart">Clear Cart</button>
{{--                            <button type="submit" class="btn btn-rounded btn-update disabled" name="update_cart" value="Update Cart">Update Cart</button>--}}
                        </div>


                    </div>

                    <div class="col-lg-4 sticky-sidebar-wrapper">
                        <div class="sticky-sidebar">
                            <div class="cart-summary mb-4">
                                <h3 class="cart-title text-uppercase">Cart Totals</h3>
                                <div class="cart-subtotal d-flex align-items-center justify-content-between">
                                    <label class="ls-25">Subtotal</label>
                                    <span class="subtotal_without_shipping">
                                        @if(isset($cart_total['total']))
                                            {{$cart_total['total']}}
                                        @endif
                                    </span>
                                </div>

                                <hr class="divider">

                                <ul class="shipping-methods mb-2">
                                    <li>
                                        <label
                                            class="shipping-title text-dark font-weight-bold">Shipping</label>
                                    </li>
                                    <li>
                                        <div class="custom-radio">
{{--                                            <input type="radio" id="free-shipping" class="custom-control-input"--}}
{{--                                                   name="shipping">--}}
                                            <label for="free-shipping"
                                                   class="custom-control-label color-dark">Free
                                                Shipping</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="custom-radio">
{{--                                            <input type="radio" id="local-pickup" class="custom-control-input"--}}
{{--                                                   name="shipping">--}}
                                            <label for="local-pickup"
                                                   class="custom-control-label color-dark">Local
                                                Pickup</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="custom-radio">
{{--                                            <input type="radio" id="flat-rate" class="custom-control-input"--}}
{{--                                                   name="shipping">--}}
                                            <label for="flat-rate" class="custom-control-label color-dark">Flat
                                                rate:
                                                $5.00</label>
                                        </div>
                                    </li>
                                </ul>

{{--                                <div class="shipping-calculator">--}}
{{--                                    <p class="shipping-destination lh-1">Shipping to <strong>CA</strong>.</p>--}}

{{--                                    <form class="shipping-calculator-form">--}}
{{--                                        <div class="form-group">--}}
{{--                                            <div class="select-box">--}}
{{--                                                <select name="country" class="form-control form-control-md">--}}
{{--                                                    <option value="default" selected="selected">United States--}}
{{--                                                        (US)--}}
{{--                                                    </option>--}}
{{--                                                    <option value="us">United States</option>--}}
{{--                                                    <option value="uk">United Kingdom</option>--}}
{{--                                                    <option value="fr">France</option>--}}
{{--                                                    <option value="aus">Australia</option>--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <div class="select-box">--}}
{{--                                                <select name="state" class="form-control form-control-md">--}}
{{--                                                    <option value="default" selected="selected">California--}}
{{--                                                    </option>--}}
{{--                                                    <option value="ohaio">Ohaio</option>--}}
{{--                                                </select>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <input class="form-control form-control-md" type="text"--}}
{{--                                                   name="town-city" placeholder="Town / City">--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <input class="form-control form-control-md" type="text"--}}
{{--                                                   name="zipcode" placeholder="ZIP">--}}
{{--                                        </div>--}}
{{--                                        <button type="submit" class="btn btn-dark btn-outline btn-rounded">Update--}}
{{--                                            Totals</button>--}}
{{--                                    </form>--}}
{{--                                </div>--}}

                                <hr class="divider mb-6">
                                <div class="order-total d-flex justify-content-between align-items-center">
                                    <label>Total</label>
                                    <span class="ls-50 subtotal_with_shipping">
                                        @if(isset($cart_total['total']))
                                            {{$cart_total['total']}}
                                        @endif
                                    </span>
                                </div>
                                <a href="{{route('web.checkout.store')}}"
                                   class="btn btn-block btn-dark btn-icon-right btn-rounded  btn-checkout">
                                    Proceed to checkout<i class="w-icon-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of PageContent -->
    </main>
    <!-- End of Main -->







    <section class="grey-section">
        <div class="container">
            <div class="title-link-wrapper mb-2 appear-animate">
            {{--our blog--}}
{{--            @include('frontend.layouts.our-blog')--}}
            <!-- End of Owl Carousel -->
            {{--our clients--}}
{{--            @include('frontend.layouts.our-clients')--}}
            <!-- End of Brands Wrapper -->

            {{--our news letter--}}
{{--            @include('frontend.layouts.our-newsetter')--}}
            <!--End of Cta Wrapper -->

                {{--out instragram--}}
                {{--@include('frontend.layouts.our-instragram')--}}
            </div>
        </div>
            <!-- End of Container -->
    </section>


    <!-- Start of Footer -->
    <footer class="footer footer-dark appear-animate">
        <div class="container">
            {{--footer top--}}
            @include('frontend.layouts.footer-top')
            @include('frontend.layouts.footer-middle')
            @include('frontend.layouts.footer-bottom')
        </div>
    </footer>
    <!-- End of Footer -->

    <!-- End of Page-wrapper -->

    <!-- Start of Sticky Footer -->
    @include('frontend.layouts.sticky-footer')
    <!-- End of Sticky Footer -->

    <!-- Start of Scroll Top -->
    <a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="fas fa-chevron-up"></i></a>
    <!-- End of Scroll Top -->

    <!-- Start of Mobile Menu -->
    @include('frontend.layouts.mobile-menu')
    <!-- End of Mobile Menu -->

    <!-- Start of Quick View -->
    @include('frontend.layouts.quick-view')
    <!-- End of Quick view -->

@endsection
