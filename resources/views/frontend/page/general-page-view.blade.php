

@extends(/** @lang text */'frontend.master.master')

@section('body')




    <!-- Start of Main -->
    <main class="main">
        <!-- Start of Page Header -->
        <div class="page-header" style="height: 180px;">
            <div class="container">
                <h1 class="page-title mb-0">{{$response['PageContent']->title}}</h1>
            </div>
        </div>
        <!-- End of Page Header -->

        <!-- Start of Breadcrumb -->
        <nav class="breadcrumb-nav mb-10 pb-1">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="{{url('/')}}">Home</a></li>
                    <li>{{$response['PageContent']->title}}</li>
                </ul>
            </div>
        </nav>
        <!-- End of Breadcrumb -->

        <!-- Start of PageContent -->
        <div class="page-content faq">
            <div class="container">
{{--                <section class="content-title-section">--}}
{{--                    <h3 class="title title-simple justify-content-center bb-no pb-0">Frequent Asked--}}
{{--                        Questions--}}
{{--                    </h3>--}}
{{--                    <p class="description text-center">You can show the faqs with <b>Wolmart Elements</b> easily.</p>--}}
{{--                </section>--}}

                <section class="mb-14">
{{--                    <h4 class="title title-center mb-5">{{$response['PageContent']->title}} Information</h4>--}}
                    <div class="row" style="padding-bottom: 10px">
                        <div class="col-md-12 mb-16">
                            <div class="accordion accordion-bg accordion-gutter-md accordion-border">
                                <div class="card">
                                    <div class="card-header">
                                        <a href="#collapse1-1" class="collapse">{{$response['PageContent']->title}}?</a>
                                    </div>
                                    <div id="collapse1-1" class="card-body expanded">
                                        <p class="mb-0">
                                            <?php echo $response['PageContent']->content;?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>

            </div>
        </div>
        <!-- End of PageContent -->
    </main>
    <!-- End of Main -->









    <section class="grey-section">
        <div class="container">
            <div class="title-link-wrapper mb-2 appear-animate">
            {{--our blog--}}
            @include('frontend.layouts.our-blog')
            <!-- End of Owl Carousel -->
            {{--our clients--}}
            @include('frontend.layouts.our-clients')
            <!-- End of Brands Wrapper -->

            {{--our news letter--}}
            @include('frontend.layouts.our-newsetter')
            <!--End of Cta Wrapper -->

                {{--out instragram--}}
                {{--@include('frontend.layouts.our-instragram')--}}
            </div>
            <!-- End of Container -->
    </section>


    <!-- Start of Footer -->
    <footer class="footer footer-dark appear-animate">
        <div class="container">
            {{--footer top--}}
            @include('frontend.layouts.footer-top')
            @include('frontend.layouts.footer-middle')
            @include('frontend.layouts.footer-bottom')
        </div>
    </footer>
    <!-- End of Footer -->
    </div>
    <!-- End of Page-wrapper -->

    <!-- Start of Sticky Footer -->
    @include('frontend.layouts.sticky-footer')
    <!-- End of Sticky Footer -->

    <!-- Start of Scroll Top -->
    <a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="fas fa-chevron-up"></i></a>
    <!-- End of Scroll Top -->

    <!-- Start of Mobile Menu -->
    @include('frontend.layouts.mobile-menu')
    <!-- End of Mobile Menu -->

    <!-- Start of Quick View -->
    @include('frontend.layouts.quick-view')
    <!-- End of Quick view -->

@endsection
