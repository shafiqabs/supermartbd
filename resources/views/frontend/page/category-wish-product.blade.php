

@extends(/** @lang text */'frontend.master.master')

@section('body')


    <!-- Start of Main -->
    <main class="main">
        <!-- Start of Breadcrumb -->
        <nav class="breadcrumb-nav">
            <div class="container">
                <ul class="breadcrumb bb-no">
                    <li><a href="">Home</a></li>
                    <li>{{$TabHeader}}</li>
                    <li>{{$response['category-data']->name}}</li>
                </ul>
            </div>
        </nav>
        <!-- End of Breadcrumb -->

        <!-- Start of Page Content -->
        <div class="page-content">
            <div class="container">
                <!-- Start of Shop Banner -->
                <div class="shop-default-banner banner d-flex align-items-center mb-5 br-xs"
                     style="background-image: url(assets/images/shop/banner1.jpg); background-color: #FFC74E;">
                    <div class="banner-content">
                        <h4 class="banner-subtitle font-weight-bold">Total {{$response['category-wish-product']->total()}} products for {{$response['category-data']->name}}</h4>
                        <h3 class="banner-title text-white text-uppercase font-weight-bolder ls-normal">
                            {{$response['category-data']->name}} Collections
                        </h3>
                        <a href="" class="btn btn-dark btn-rounded btn-icon-right">Discover
                            Now<i class="w-icon-long-arrow-right"></i></a>
                    </div>
                </div>
                <!-- End of Shop Banner -->

                <div class="shop-default-brands mb-5">
                    <div class="row brands-carousel owl-carousel owl-theme cols-xl-7 cols-lg-6 cols-md-4 cols-sm-3 cols-2"
                         data-owl-options="{
                                'items': 7,
                                'nav': false,
                                'dots': true,
                                'margin': 20,
                                'loop': true,
                                'autoPlay': 'true',
                                'responsive': {
                                    '0': {
                                        'items': 2
                                    },
                                    '576': {
                                        'items': 3
                                    },
                                    '768': {
                                        'items': 4
                                    },
                                    '992': {
                                        'items': 6
                                    },
                                    '1200': {
                                        'items': 7
                                    }
                                }
                            }
                        ">
                        @foreach($response['all-brand'] as $value)
                        <figure style="text-align: center;font-weight: bold">
                            <a href="{{route('brand.wish.product',$value->slug)}}">
                            <img src="{{ asset('backend/image/ProBrandImage').'/'.$value->brand_image;}}" alt="{{$value->name}}" style="    width: 160px;
    height: 70px;"/>
                            <span style="color: #000">{{$value->name}}</span>
                            </a>
                        </figure>
                        @endforeach
                    </div>
                </div>
                <!-- End of Shop Brands-->

                <!-- Start of Shop Category -->
                <div class="shop-default-category category-ellipse-section mb-6">
                    <div class="owl-carousel owl-theme row gutter-lg cols-xl-8 cols-lg-7 cols-md-6 cols-sm-4 cols-xs-3 cols-2"
                         data-owl-options="{
                            'nav': false,
                            'dots': true,
                            'margin': 20,
                            'responsive': {
                                '0': {
                                    'items': 2
                                },
                                '480': {
                                    'items': 3
                                },
                                '576': {
                                    'items': 4
                                },
                                '768': {
                                    'items': 6
                                },
                                '992': {
                                    'items': 7
                                },
                                '1200': {
                                    'items': 8,
                                    'margin': 30
                                }
                            }
                        }">
                        @foreach($response['all-category'] as $value)
                        <div class="category-wrap">
                            <div class="category category-ellipse">
                                <figure class="category-media">
                                    <a href="{{route('category.wish.product',$value->slug)}}">
                                        <img src="{{ asset('backend/image/ProCategoryImage').'/'.$value->category_image;}}" alt="{{$value->name}}"
                                             width="190" height="190" style="background-color: #5C92C0;height: 130px;border-radius: 50%;" />
                                    </a>
                                </figure>
                                <div class="category-content">
                                    <h4 class="category-name">
                                        <a href="{{route('category.wish.product',$value->slug)}}">{{$value->name}}</a>
                                    </h4>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <!-- End of Shop Category -->

                <!-- Start of Shop Content -->
                <div class="shop-content row gutter-lg mb-10">
                    <!-- Start of Sidebar, Shop Sidebar -->
                    <aside class="sidebar shop-sidebar sticky-sidebar-wrapper sidebar-fixed">
                        <!-- Start of Sidebar Overlay -->
                        <div class="sidebar-overlay"></div>
                        <a class="sidebar-close" href="#"><i class="close-icon"></i></a>

                        <!-- Start of Sidebar Content -->
                        <div class="sidebar-content scrollable">
                            <!-- Start of Sticky Sidebar -->
                            <div class="sticky-sidebar">
                                <div class="filter-actions">
                                    <label>Filter :</label>
                                    <a href="#" class="btn btn-dark btn-link filter-clean">Clean All</a>
                                </div>
                                <!-- Start of Collapsible widget -->
                                <div class="widget widget-collapsible">
                                    <h3 class="widget-title"><label>All Categories</label></h3>
                                    <ul class="widget-body filter-items search-ul">
                                        @if(isset($response['Category']) && count($response['Category'])>0)
                                            @foreach($response['Category'] as $value)
                                                <li><a href="{{route('category.wish.product',$value->slug)}}">{{$value->name}}</a></li>
                                            @endforeach
                                        @endif

                                    </ul>
                                </div>
                                <!-- End of Collapsible Widget -->

                                <!-- Start of Collapsible widget -->
                                <div class="widget widget-collapsible">
                                    <h3 class="widget-title"><label>Brand</label></h3>
                                    <ul class="widget-body filter-items search-ul">
                                        @if(isset($response['all-brand']) && count($response['all-brand'])>0)
                                            @foreach($response['all-brand'] as $value)
                                                <li><a href="{{route('brand.wish.product',$value->slug)}}">{{$value->name}}</a></li>
                                            @endforeach
                                        @endif

                                    </ul>
                                </div>
                                <!-- End of Collapsible Widget -->



                                <!-- Start of Collapsible Widget -->
                                <div class="widget widget-collapsible">
                                    <h3 class="widget-title"><label>Price</label></h3>
                                    <div class="widget-body">
                                        <ul class="filter-items search-ul">
                                            <li><a href="#">$0.00 - $100.00</a></li>
                                            <li><a href="#">$100.00 - $200.00</a></li>
                                            <li><a href="#">$200.00 - $300.00</a></li>
                                            <li><a href="#">$300.00 - $500.00</a></li>
                                            <li><a href="#">$500.00+</a></li>
                                        </ul>
                                        <form class="price-range">
                                            <input type="number" name="min_price" class="min_price text-center"
                                                   placeholder="$min"><span class="delimiter">-</span><input
                                                type="number" name="max_price" class="max_price text-center"
                                                placeholder="$max"><a href="#"
                                                                      class="btn btn-primary btn-rounded">Go</a>
                                        </form>
                                    </div>
                                </div>
                                <!-- End of Collapsible Widget -->

                                <!-- Start of Collapsible Widget -->
                                <div class="widget widget-collapsible">
                                    <h3 class="widget-title"><label>Size</label></h3>
                                    <ul class="widget-body filter-items item-check mt-1">
                                        @if(isset($response['size-unit']))
                                            @foreach($response['size-unit'] as $value)
                                                <li><a href="#">{{$value->name}}</a></li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                                <!-- End of Collapsible Widget -->



                                <!-- Start of Collapsible Widget -->
                                <div class="widget widget-collapsible">
                                    <h3 class="widget-title"><label>Color</label></h3>
                                    <ul class="widget-body filter-items item-check mt-1">
                                        @if(isset($response['color']))
                                            @foreach($response['color'] as $value)
                                                <li><a href="#">{{$value->name}}</a></li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                                <!-- End of Collapsible Widget -->
                            </div>
                            <!-- End of Sidebar Content -->
                        </div>
                        <!-- End of Sidebar Content -->
                    </aside>
                    <!-- End of Shop Sidebar -->

                    <!-- Start of Shop Main Content -->
                    <div class="main-content">
                        <nav class="toolbox sticky-toolbox sticky-content fix-top">
                            <div class="toolbox-left">
                                <a href="#" class="btn btn-primary btn-outline btn-rounded left-sidebar-toggle
                                        btn-icon-left d-block d-lg-none"><i
                                        class="w-icon-category"></i><span>Filters</span></a>
                                <div class="toolbox-item toolbox-sort select-box text-dark">
                                    <label>Sort By :</label>
                                    <select name="orderby" class="form-control">
                                        <option value="default" selected="selected">Default sorting</option>
                                        <option value="popularity">Sort by popularity</option>
                                        <option value="rating">Sort by average rating</option>
                                        <option value="date">Sort by latest</option>
                                        <option value="price-low">Sort by pric: low to high</option>
                                        <option value="price-high">Sort by price: high to low</option>
                                    </select>
                                </div>
                            </div>
                            <div class="toolbox-right">
{{--                                <div class="toolbox-item toolbox-show select-box">--}}
{{--                                    <select name="count" class="form-control">--}}
{{--                                        <option value="9">Show 9</option>--}}
{{--                                        <option value="12" selected="selected">Show 12</option>--}}
{{--                                        <option value="24">Show 24</option>--}}
{{--                                        <option value="36">Show 36</option>--}}
{{--                                    </select>--}}
{{--                                </div>--}}
                                <div class="toolbox-item toolbox-layout">
                                    <a href="" class="icon-mode-grid btn-layout active">
                                        <i class="w-icon-grid"></i>
                                    </a>
                                    <a href="" class="icon-mode-list btn-layout">
                                        <i class="w-icon-list"></i>
                                    </a>
                                </div>
                            </div>
                        </nav>
                        @if(isset($response['category-wish-product']))

                        <div class="product-wrapper row cols-md-3 cols-sm-2 cols-2">
                            @foreach($response['category-wish-product'] as $value)
                            <div class="product-wrap">
                                <div class="product text-center">
                                    <figure class="product-media">
                                        <a href="{{route('single.product.details',$value->slug)}}">
                                            <img src="{{ asset('backend/image/ProProductImage').'/'.$value->feature_image}}" alt="Product" width="300"
                                                 height="338" />
                                        </a>
                                        <div class="product-action-horizontal">
                                            <a href="#" title="Add to cart" product-id="{{$value->id}}" product-name="{{$value->name}}" product-slug="{{$value->slug}}" product-image="{{$value->feature_image}}"  product-price="{{$value->salesprice}}" product-quantity="1" data-href="{{route('web.cart.add')}}" class="btn-product-icon btn-cart w-icon-cart add_cart_ajax">
                                            </a>
{{--                                            <a href="#" class="btn-product-icon btn-wishlist w-icon-heart"--}}
{{--                                               title="Wishlist"></a>--}}
{{--                                            <a href="#" class="btn-product-icon btn-compare w-icon-compare"--}}
{{--                                               title="Compare"></a>--}}
                                            <a href="#" class="btn-product-icon btn-quickview w-icon-visit" product-id="{{$value->id}}" product-slug="{{$value->slug}}" data-href="{{route('web.quick.view')}}"
                                               title="Quick View"></a>
                                        </div>
                                    </figure>
                                    <div class="product-details">
                                        <div class="product-cat">
                                            <a href="{{route('category.wish.product',$response['category-data']->slug)}}">{{$response['category-data']->name}}</a>
                                        </div>
                                        <h3 class="product-name">
                                            <a href="{{route('single.product.details',$value->slug)}}">{{$value->name}}</a>
                                        </h3>
                                        <div class="ratings-container">
                                            <div class="ratings-full">
                                                <span class="ratings" style="width: 100%;"></span>
                                                <span class="tooltiptext tooltip-top"></span>
                                            </div>
                                            <a href="#" class="rating-reviews">(3 reviews)</a>
                                        </div>
                                        <div class="product-pa-wrapper">
                                            <div class="product-price">
                                                {{$value->salesprice}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach







                        </div>

                        <div class="toolbox toolbox-pagination justify-content-between">
                            <p class="showing-info mb-2 mb-sm-0">
                                Showing<span>{{$response['category-wish-product']->firstItem()}}-{{$response['category-wish-product']->lastItem()}} of {{$response['category-wish-product']->total()}}</span>Products
                            </p>
                            <ul class="pagination">
                                {{ $response['category-wish-product']->links('backend.layouts.frontend-pagination') }}
                            </ul>

                        </div>

                        @endif


                    </div>
                    <!-- End of Shop Main Content -->
                </div>
                <!-- End of Shop Content -->
            </div>
        </div>
        <!-- End of Page Content -->
    </main>
    <!-- End of Main -->

    <section class="grey-section">
        <div class="container">
            <div class="title-link-wrapper mb-2 appear-animate">
            {{--our blog--}}
            @include('frontend.layouts.our-blog')
            <!-- End of Owl Carousel -->
            {{--our clients--}}
            @include('frontend.layouts.our-clients')
            <!-- End of Brands Wrapper -->

            {{--our news letter--}}
            @include('frontend.layouts.our-newsetter')
            <!--End of Cta Wrapper -->

                {{--out instragram--}}
                {{--@include('frontend.layouts.our-instragram')--}}
            </div>
            <!-- End of Container -->
    </section>


    <!-- Start of Footer -->
    <footer class="footer footer-dark appear-animate">
        <div class="container">
            {{--footer top--}}
            @include('frontend.layouts.footer-top')
            @include('frontend.layouts.footer-middle')
            @include('frontend.layouts.footer-bottom')
        </div>
    </footer>
    <!-- End of Footer -->
    </div>
    <!-- End of Page-wrapper -->

    <!-- Start of Sticky Footer -->
    @include('frontend.layouts.sticky-footer')
    <!-- End of Sticky Footer -->

    <!-- Start of Scroll Top -->
    <a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="fas fa-chevron-up"></i></a>
    <!-- End of Scroll Top -->

    <!-- Start of Mobile Menu -->
    @include('frontend.layouts.mobile-menu')
    <!-- End of Mobile Menu -->

    <!-- Start of Quick View -->
    @include('frontend.layouts.quick-view')
    <!-- End of Quick view -->

@endsection
