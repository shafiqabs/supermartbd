

@extends(/** @lang text */'frontend.master.master')

@section('body')




    <!-- Start of Main -->
    <main class="main order">
        <!-- Start of Breadcrumb -->
        <nav class="breadcrumb-nav">
            <div class="container">
                <ul class="breadcrumb shop-breadcrumb bb-no">
                    <li class="passed"><a href="{{route('web.cart.view')}}">Shopping Cart</a></li>
                    <li class="passed"><a href="{{route('web.checkout.store')}}">Checkout</a></li>
                    <li class="active"><a href="">Order Complete</a></li>
                </ul>
            </div>
        </nav>
        <!-- End of Breadcrumb -->


@if(isset($OrderData) && !empty($OrderData))
        <!-- Start of PageContent -->
        <div class="page-content mb-10 pb-2">
            <div class="container">
                <div class="order-success text-center font-weight-bolder text-dark">
                    <i class="fas fa-check"></i>
                    Thank you. Your order has been received.
                </div>
                <!-- End of Order Success -->

                <ul class="order-view list-style-none">
                    <li>
                        <label>Order number</label>
                        <strong>{{$OrderData->order_number}}</strong>
                    </li>
                    <li>
                        <label>Status</label>
                        <strong>
                            <?php
                                if ($OrderData->status == '1'){
                                    $status = 'Process';
                                }elseif ($OrderData->status == '2'){
                                    $status = 'Confirm';
                                }elseif ($OrderData->status == '3'){
                                    $status = 'Pending';
                                }elseif ($OrderData->status == '4'){
                                    $status = 'Approved';
                                }else{
                                    $status = 'Return';
                                }
                            ?>
                            {{$status}}</strong>
                    </li>
                    <li>
                        <label>Date</label>
                        <strong>

                            {{date('h:i:s a m/d/Y', strtotime($OrderData->created_at))}}
                        </strong>
                    </li>
                    <li>
                        <label>Total Amount</label>
                        <strong>{{$OrderData->total_amount}}</strong>
                    </li>
                    <li>
                        <label>Payment method</label>
                        <strong>{{$OrderData->payment_method}}</strong>
                    </li>
                </ul>
                <!-- End of Order View -->
<!--                --><?php
//                    echo '<pre>';
//                    print_r($OrderData->OrderProductItem);
//                ?>

                <div class="order-details-wrapper mb-5">
                    <h4 class="title text-uppercase ls-25 mb-5">Order Details</h4>
                    <table class="order-table">
                        <thead>
                        <tr>
                            <th class="text-dark">Product</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($OrderData->order_product_item as $value)
                        <tr>
                            <td>
                                <a target="new" href="{{route('single.product.details',$value->slug)}}">{{$value->name}}</a>&nbsp;<strong>( {{$value->quantity}} * {{$value->unit_price}})</strong><br>

                            </td>
                            <td>{{$value->subtotal}}</td>
                        </tr>
                        @endforeach

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Subtotal:</th>
                            <td>{{$OrderData->product_amount}}</td>
                        </tr>
                        <tr>
                            <th>Deleviry Charge:</th>
                            <td>{{$OrderData->delivery_charge}}</td>
                        </tr>
                        <tr>
                            <th>Payment method:</th>
                            <td>{{$OrderData->payment_method}}</td>
                        </tr>
                        <tr class="total">
                            <th class="border-no">Total:</th>
                            <td class="border-no">{{$OrderData->total_amount}}</td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- End of Order Details -->

                <?php
//                    echo '<pre>';
//                    print_r($OrderData->OrderBillingShipping)
                ?>

                <div id="account-addresses">
                    <div class="row">
                        <div class="page-content mb-10 pb-2 order-table">
                            <div class="ecommerce-address billing-address">
                                <h4 class="title title-underline ls-25 font-weight-bold">Billing/Shipping Address</h4>
                                <address class="mb-4">
                                    <table class="address-table">
                                        <tbody>
                                        @foreach($OrderData->order_billing_shipping as $value)
                                            <tr>
                                                <td>{{$value->type}}</td>
                                                <td>{{$value->firstname.' '.$value->lastname}}</td>
                                                <td>{{$value->country}}</td>
                                                <td>{{$value->district}}</td>
                                                <td>{{$value->address}}</td>
                                                <td>{{$value->phone}}</td>
                                                <td>{{$value->email}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </address>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- End of Account Address -->

                <a href="#" class="btn btn-dark btn-rounded btn-icon-left btn-back mt-6"><i class="w-icon-long-arrow-left"></i>Back To List</a>
            </div>
        </div>
        <!-- End of PageContent -->
    @else
        First Place Order
    @endif
    </main>
    <!-- End of Main -->









    <section class="grey-section">
        <div class="container">
            <div class="title-link-wrapper mb-2 appear-animate">
            {{--our blog--}}
            @include('frontend.layouts.our-blog')
            <!-- End of Owl Carousel -->
            {{--our clients--}}
            @include('frontend.layouts.our-clients')
            <!-- End of Brands Wrapper -->

            {{--our news letter--}}
            @include('frontend.layouts.our-newsetter')
            <!--End of Cta Wrapper -->

                {{--out instragram--}}
                {{--@include('frontend.layouts.our-instragram')--}}
            </div>
            <!-- End of Container -->
    </section>


    <!-- Start of Footer -->
    <footer class="footer footer-dark appear-animate">
        <div class="container">
            {{--footer top--}}
            @include('frontend.layouts.footer-top')
            @include('frontend.layouts.footer-middle')
            @include('frontend.layouts.footer-bottom')
        </div>
    </footer>
    <!-- End of Footer -->
    </div>
    <!-- End of Page-wrapper -->

    <!-- Start of Sticky Footer -->
    @include('frontend.layouts.sticky-footer')
    <!-- End of Sticky Footer -->

    <!-- Start of Scroll Top -->
    <a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="fas fa-chevron-up"></i></a>
    <!-- End of Scroll Top -->

    <!-- Start of Mobile Menu -->
    @include('frontend.layouts.mobile-menu')
    <!-- End of Mobile Menu -->

    <!-- Start of Quick View -->
    @include('frontend.layouts.quick-view')
    <!-- End of Quick view -->

@endsection
