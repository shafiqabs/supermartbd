

@extends(/** @lang text */'frontend.master.master')

@section('body')

    <?php
    $cart_items = [];
    if(Session::has('cart')){
        $cart_items = Session::get('cart');
    }

    $cart_total = [];
    if(Session::has('cart_total')){
        $cart_total = Session::get('cart_total');
    }

//    echo '<pre>';
//    print_r($cart_total);
    ?>




    <!-- Start of Main -->
    <main class="main checkout">
        <!-- Start of Breadcrumb -->
        <nav class="breadcrumb-nav">
            <div class="container">
                <ul class="breadcrumb shop-breadcrumb bb-no">
                    <li class="passed"><a href="{{route('web.cart.view')}}">Shopping Cart</a></li>
                    <li class="active"><a href="{{route('web.checkout.store')}}">Checkout</a></li>
                    <li>Order Complete</li>
                </ul>
            </div>
        </nav>
        <!-- End of Breadcrumb -->


        <!-- Start of PageContent -->
        <div class="page-content">
            <div class="container">
                <div class="login-toggle">
                    Returning customer? <a href="#"
                                           class="show-login font-weight-bold text-uppercase text-dark">Login</a>
                </div>

                <?php
//                $first_name = '';
                if (Session::has('UserLoginInfo')){
//                    echo '<pre>';
//                    print_r(Session::get('UserLoginInfo'));
                    $style = Session::get('UserLoginInfo')['msg'];

                    $first_name = Session::get('UserLoginInfo')['customerinfo']['first_name'];
                    $last_name = Session::get('UserLoginInfo')['customerinfo']['last_name'];
                    $bill_ship_street = Session::get('UserLoginInfo')['customerinfo']['bill_street_address'];
                    $email = Session::get('UserLoginInfo')['customerinfo']['email'];
                    $mobile = Session::get('UserLoginInfo')['customerinfo']['mobile'];

//                    echo $first_name;
//                    echo $first_name;

                }else{
                    $style = old('msg');
                    $first_name = old('bill_firstname');
                    $last_name = old('bill_lastname');
                    $bill_ship_street = old('bill_street_address');
                    $email = old('bill_email');
                    $mobile = old('bill_phone');
                }
                if (Session::has('error')){
                    $style = Session::get('error')['msg'];
                }
                ?>


                {!! Form::open(['route' => 'web.checkout.userlogin','enctype'=>'multipart/form-data',  'files'=> true, 'id'=>'basic-form', 'class' => 'login-content','style'=>$style]) !!}
                    <p>If you have shopped with us before, please enter your details below.
                        If you are a new customer, please proceed to the Billing section.</p>
                    <div class="row">
                        @if(isset(Session::get('error')['error']))
                            <h3 style="color: red">{{Session::get('error')['error']}}</h3>
                        @endif
                        <div class="col-xs-6">

                            <div class="form-group">
                                <label> Email & Mobile Number <span style="color: red">*</span></label>
                                <input type="text" class="form-control form-control-md" value="@if(old('emailormobile')){{old('emailormobile')}}@else{{$email}}@endif" name="emailormobile" style="margin-bottom: 0px !important;">
                                <span style="color: #ff0000">{!! $errors->first('emailormobile') !!}</span>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label>Password <span style="color: red">*</span></label>
                                <input type="password" class="form-control form-control-md" value="{{old('password')}}" name="password" style="margin-bottom: 0px !important;">
                                <span style="color: #ff0000">{!! $errors->first('password') !!}</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group checkbox">
                        <input type="checkbox" class="custom-checkbox" id="remember" name="remember">
                        <label for="remember" class="mb-0 lh-2">Remember me</label>
                        <a href="{{route('web.customer.forgetpassword')}}" class="ml-3">Forget Password</a>
                    </div>
                    <button class="btn btn-rounded btn-login">Login</button>
                {!! Form::close() !!}
                <div class="coupon-toggle">
                    Have a coupon? <a href="#"
                                      class="show-coupon font-weight-bold text-uppercase text-dark">Enter your
                        code</a>
                </div>
                <div class="coupon-content mb-4">
                    <p>If you have a coupon code, please apply it below.</p>
                    <div class="input-wrapper-inline">
                        <input type="text" name="coupon_code" class="form-control form-control-md mr-1 mb-2" placeholder="Coupon code" id="coupon_code">
                        <button type="submit" class="btn button btn-rounded btn-coupon mb-2" name="apply_coupon" value="Apply coupon">Apply Coupon</button>
                    </div>
                </div>




{{--                <form class="form checkout-form" action="" method="post">--}}
                {!! Form::open(['route' => 'web.checkout.orderplace','enctype'=>'multipart/form-data',  'files'=> true, 'id'=>'basic-form', 'class' => 'form checkout-form']) !!}
{{--                {!! Form::open(['route' => 'api.create-order','enctype'=>'multipart/form-data',  'files'=> true, 'id'=>'basic-form', 'class' => 'form checkout-form']) !!}--}}



                <div class="row mb-9">
                        <div class="col-lg-7 pr-lg-4 mb-4">
                            <h3 class="title billing-title text-uppercase ls-10 pt-1 pb-3 mb-0">
                                Billing Details

                            </h3>
                            <div class="row gutter-sm">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>First name <span style="color: red">*</span></label>
                                        @if (Session::has('UserLoginInfo'))
                                            <input type="hidden" name="customer_id" value="{{Session::get('UserLoginInfo')['customerinfo']->id}}">
                                        @endif
                                        <input type="text" class="form-control form-control-md" value="{{$first_name}}" name="bill_firstname" required>
                                    </div>
                                    <span style="color: #ff0000">{!! $errors->first('bill_firstname') !!}</span>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Last name </label>
                                        <input type="text" class="form-control form-control-md" value="{{$last_name}}" name="bill_lastname">
                                    </div>
                                    <span style="color: #ff0000">{!! $errors->first('bill_lastname') !!}</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Country</label>
                                <div class="select-box">
                                    <select name="bill_country" class="form-control form-control-md">
                                        <option value="Argentina" >Argentina</option>
                                        <option value="Australia">Australia</option>
                                        <option value="Bangladesh" selected="selected">Bangladesh</option>
                                        <option value="Brazil">Brazil</option>
                                        <option value="Canada">Canada</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>District</label>
                                <div class="select-box">
                                    <select name="bill_district" class="form-control form-control-md">
                                        <option value="Chittagong" >Chittagong</option>
                                        <option value="Rangpur">Rangpur</option>
                                        <option value="Rajshahi">Rajshahi</option>
                                        <option value="Dhaka" selected="selected">Dhaka</option>
                                        <option value="Jashore">Jashore</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Street address <span style="color: red">*</span></label>
                                <input type="text" placeholder="House number and street name"
                                       class="form-control form-control-md mb-2" value="{{$bill_ship_street}}" name="bill_street_address" required>
                           </div>
                            <span style="color: #ff0000">{!! $errors->first('bill_street_address') !!}</span>
                            <div class="row gutter-sm">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Phone <span style="color: red">*</span></label>
                                        <input type="text" class="form-control form-control-md" value="{{$mobile}}" name="bill_phone" required>
                                    </div>
                                    <span style="color: #ff0000">{!! $errors->first('bill_phone') !!}</span>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email address </label>
                                        <input type="email" class="form-control form-control-md" value="{{$email}}" name="bill_email">
                                    </div>
                                </div>
                            </div>

<div class="form-group checkbox-toggle pb-2">
    <input type="radio" id="shipping-toggle"
           class="custom-checkbox" name="another_ship" value=" Shipping" >

{{--    <input type="radio" class="custom-checkbox" id="shipping-toggle" name="another_shipping" value="another shipping">--}}
    <label for="shipping-toggle">Ship to a different address?</label>
</div>
@php
    $open_class = '';
    $style = '';
        if (!empty(old('bill_phone'))){
            if (old('ship_firstname') != '' || old('ship_lastname') != '' || old('ship_street_address') != '' || old('ship_phone') != '' ||old('ship_email') != ''){
                $open_class = 'open';
                $style = 'display: block';
            }else{
                 $open_class = '';
                 $style = '';
            }
        }
@endphp
                            <div class="checkbox-content {{$open_class}}" style="{{$style}}">
                                <div class="row gutter-sm">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label>First name </label>
                                            <input type="text" class="form-control form-control-md" value="{{old('ship_firstname')}}" name="ship_firstname">
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label>Last name </label>
                                            <input type="text" class="form-control form-control-md" value="{{old('ship_lastname')}}" name="ship_lastname">
                                        </div>
                                    </div>
                                </div>

<div class="form-group">
    <label>Country</label>
    <div class="select-box">
        <select name="ship_country" class="form-control form-control-md">
            <option value="Argentina" >Argentina</option>
            <option value="Australia">Australia</option>
            <option value="Bangladesh" selected="selected">Bangladesh</option>
            <option value="Brazil">Brazil</option>
            <option value="Canada">Canada</option>
        </select>
    </div>
</div>


<div class="form-group">
    <label>District</label>
    <div class="select-box">
        <select name="ship_district" class="form-control form-control-md">
            <option value="Chittagong" >Chittagong</option>
            <option value="Rangpur">Rangpur</option>
            <option value="Rajshahi">Rajshahi</option>
            <option value="Dhaka" selected="selected">Dhaka</option>
            <option value="Jashore">Jashore</option>
        </select>
    </div>
</div>
                                <div class="form-group">
                                    <label>Street address </label>
                                    <input type="text" placeholder="House number and street name" value="{{old('ship_street_address')}}"
                                           class="form-control form-control-md mb-2" name="ship_street_address">
                                </div>
                                <div class="row gutter-sm">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Phone </label>
                                            <input type="text" value="{{old('ship_phone')}}" class="form-control form-control-md" name="ship_phone">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email address </label>
                                            <input type="email" value="{{old('ship_email')}}" class="form-control form-control-md" name="ship_email">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group mt-3">
                                <label for="order-notes">Order notes (optional)</label>
                                <textarea class="form-control mb-0" id="order-notes" value="{{old('order_notes')}}" name="order_notes" cols="30"
                                          rows="4"
                                          placeholder="Notes about your order, e.g special notes for delivery"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-5 mb-4 sticky-sidebar-wrapper">
                            <div class="order-summary-wrapper sticky-sidebar">
                                <h3 class="title text-uppercase ls-10">Your Order</h3>
                                <div class="order-summary">
                                    <table class="order-table">
                                        <thead>
                                        <tr>
                                            <th colspan="2">
                                                <b>Product</b>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($cart_items) > 0)
                                            @foreach($cart_items as $cart)
                                        <tr class="bb-no" id="remove_id_{{$cart['product_id']}}">
                                            <td class="product-name">{{$cart['product_name']}} ({{$cart['product_price']}}<i
                                                    class="fas fa-times"></i> <span
                                                    class="product-quantity">{{$cart['product_quantity']}})</span></td>

                                            <input type="hidden" name="product_id[]" value="{{$cart['product_id']}}">
                                            <input type="hidden" name="product_quantity[]" value="{{$cart['product_quantity']}}">
                                            <input type="hidden" name="product_price[]" value="{{$cart['product_price']}}">

                                            <td class="product-total">
                                                {{number_format((float)$cart['product_price']*$cart['product_quantity'], 2, '.', '')}}
                                            </td>

                                        </tr>

                                            @endforeach
                                        @endif

                                        <tr class="order-total" style="border-top: 1px solid #eee;">

                                            <th>
                                                <b>Sub Total</b>
                                            </th>
                                            <td>
                                                <b class="subtotal_without_shipping">
                                                    @if(isset($cart_total['total']) && !empty($cart_total['total']))
                                                    {{number_format((float)$cart_total['total'], 2, '.', '')}}
                                                    @endif
                                                </b>
                                            </td>
                                        </tr>

                                        <tr class="order-total" style="border-top: 1px solid #eee;">

                                            <th>
                                                <b>Delivery Charge</b>
                                            </th>
                                            <td>
                                                <input type="hidden" name="delivery_charge" id="delivery_charge" value="60">
                                                <b class="">
                                                    60.00
                                                </b>
                                            </td>
                                        </tr>

                                        </tbody>

                                        <tfoot>

                                        <tr class="shipping-methods">
                                            <td colspan="2" class="text-left">
                                                <h4 class="title title-simple bb-no mb-1 pb-0 pt-3">Shipping Time Slot <span style="color: red">*</span>
                                                </h4>
                                                <ul id="shipping-method" class="mb-4">

                                                    @foreach($DeliveryTimeSlot as $value)
                                                        <li>

                                                            <div class="custom-radio">
                                                                <input type="radio" id="free-shipping{{$value->id}}"
                                                                       class="custom-control-input" name="shipping_time_slot" value="{{$value->time_slot}}" required>
                                                                <label for="free-shipping{{$value->id}}"
                                                                       class="custom-control-label color-dark">{{date('h:i:s a ', strtotime($value->start_time))}} - {{date('h:i:s a ', strtotime($value->end_time))}}</label>
                                                            </div>
                                                        </li>
                                                    @endforeach

                                                </ul>
                                            </td>
                                        </tr>



<tr class="shipping-methods">
<td colspan="2" class="text-left">
    <h4 class="title title-simple bb-no mb-1 pb-0 pt-3">Payment Method <span style="color: red">*</span>
    </h4>
    <ul id="Payment-method" class="mb-4">
        <li>
            <div class="custom-radio">
                <input type="radio" id="cash_on_delivery"
                       class="custom-control-input" name="payment_method" value="Cash on delivery" checked>
                <label for="cash_on_delivery"
                       class="custom-control-label color-dark">Cash on delivery</label>
            </div>
        </li>


        <li>
            <div class="custom-radio">
                <input type="radio" id="direct_bank_transfer"
                       class="custom-control-input" name="payment_method" value="Direct Bank Transfor">
                <label for="direct_bank_transfer"
                       class="custom-control-label color-dark">Direct Bank Transfor</label>
            </div>
        </li>


        <li>
            <div class="custom-radio">
                <input type="radio" id="check_payment"
                       class="custom-control-input" name="payment_method" value="Check Payments">
                <label for="check_payment"
                       class="custom-control-label color-dark">Check Payments</label>
            </div>
        </li>


    </ul>
</td>
</tr>


                                        <tr class="order-total">
                                        <th>
                                            <b>Total</b>
                                        </th>
                                        <td>
                                            <?php
                                            if (isset($cart_total['total']) && !empty($cart_total['total'])){
                                                $total_amount = $cart_total['total']+60;
                                            }else{
                                                $total_amount = 60;
                                            }

                                            ?>
                                            <input type="hidden" id="db_store_total_amount"  name="total_amount" value="{{number_format((float)$total_amount, 2, '.', '')}}">
                                            <b class="subtotal_with_shipping">

                                                {{number_format((float)$total_amount, 2, '.', '')}}</b>
                                        </td>
                                        </tr>
                                        </tfoot>
                                    </table>

                                    @if($total_amount>60)
                                    <div class="form-group place-order pt-6">
                                        <button type="submit" name="submit" class="btn btn-dark btn-block btn-rounded">Place Order</button>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
        <!-- End of PageContent -->
    </main>
    <!-- End of Main -->







    <section class="grey-section">
        <div class="container">
            <div class="title-link-wrapper mb-2 appear-animate">
            {{--our blog--}}
            @include('frontend.layouts.our-blog')
            <!-- End of Owl Carousel -->
            {{--our clients--}}
{{--            @include('frontend.layouts.our-clients')--}}
            <!-- End of Brands Wrapper -->

            {{--our news letter--}}
{{--            @include('frontend.layouts.our-newsetter')--}}
            <!--End of Cta Wrapper -->

                {{--out instragram--}}
                {{--@include('frontend.layouts.our-instragram')--}}
            </div>
            <!-- End of Container -->
    </section>


    <!-- Start of Footer -->
    <footer class="footer footer-dark appear-animate">
        <div class="container">
            {{--footer top--}}
            @include('frontend.layouts.footer-top')
            @include('frontend.layouts.footer-middle')
            @include('frontend.layouts.footer-bottom')
        </div>
    </footer>
    <!-- End of Footer -->
    </div>
    <!-- End of Page-wrapper -->

    <!-- Start of Sticky Footer -->
    @include('frontend.layouts.sticky-footer')
    <!-- End of Sticky Footer -->

    <!-- Start of Scroll Top -->
    <a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="fas fa-chevron-up"></i></a>
    <!-- End of Scroll Top -->

    <!-- Start of Mobile Menu -->
    @include('frontend.layouts.mobile-menu')
    <!-- End of Mobile Menu -->

    <!-- Start of Quick View -->
    @include('frontend.layouts.quick-view')
    <!-- End of Quick view -->

@endsection
