

@extends(/** @lang text */'frontend.master.master')

@section('body')

    <!-- Start of Main -->
    <main class="main checkout">
        <!-- Start of Breadcrumb -->
        <nav class="breadcrumb-nav">
            <div class="container">
                <ul class="breadcrumb shop-breadcrumb bb-no">
                    <li class="passed"><a href="{{ url('/') }}">Home</a></li>
                    <li class="active"><a href="">Create Account</a></li>
                </ul>
            </div>
        </nav>
        <!-- End of Breadcrumb -->


        <!-- Start of PageContent -->
        <div class="page-content">
            <div class="container">
                <div class="login-toggle">
                    New Customer? <a href="#" class="show-login font-weight-bold text-uppercase text-dark">Create account as a customer </a>
                </div>



                {!! Form::open(['route' => 'web.create.account','enctype'=>'multipart/form-data',  'files'=> true, 'id'=>'basic-form', 'class' => 'login-content','style'=>'display:block']) !!}
                <p>If you have shopped with us before, please enter your details below.
                    If you are a new customer, please proceed to the Billing section.</p>
                <div class="row">

                        <h3 style="color: red">{{Session::get('validate')}}</h3>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label> Name <span style="color: red">*</span></label>
                            <input type="text"  id="name" class="form-control form-control-md" value="{{old('name')}}" name="name" style="margin-bottom: 0px !important;">
                            <span style="color: #ff0000">{!! $errors->first('name') !!}</span>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Email <span style="color: red">*</span></label>
                            <input type="text" id="email" class="form-control form-control-md" value="{{old('email')}}" name="email" style="margin-bottom: 0px !important;">
                            <span style="color: #ff0000">{!! $errors->first('email') !!}</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label> Mobile <span style="color: red">*</span></label>
                            <input type="text" id="mobile" class="form-control form-control-md" value="{{old('mobile')}}" name="mobile" style="margin-bottom: 0px !important;">
                            <span style="color: #ff0000">{!! $errors->first('mobile') !!}</span>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Address <span style="color: red">*</span></label>
                            <textarea name="address" id="address"  rows="1" class="form-control form-control-md" style="height: 0px;margin-bottom: 0px !important;">{{old('address')}}</textarea>
                            <span style="color: #ff0000">{!! $errors->first('address') !!}</span>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label> Image <span style="color: red">*</span></label>
                            <input class="form-control" accept="image/*" name="image" type="file" id="file" onchange="loadFile(event)">
                            @if($errors->first('image'))
                                <span style="color: #ff0000">{!! $errors->first('image') !!}</span>
                            @else
                                <img id="image" width="150" height="120"/>
                            @endif
                        </div>
                        <script>
                            var loadFile = function(event) {
                                var image = document.getElementById('image');
                                image.src = URL.createObjectURL(event.target.files[0]);
                            };
                        </script>
                    </div>
                </div>



                <div class="form-group checkbox">

                </div>
                <button type="submit" class="btn btn-rounded btn-login" id="CreateAccount">Submit</button>
                {!! Form::close() !!}

            </div>
        </div>
        <!-- End of PageContent -->
    </main>
    <!-- End of Main -->







    <section class="grey-section">
        <div class="container">
            <div class="title-link-wrapper mb-2 appear-animate">
            {{--our blog--}}
            @include('frontend.layouts.our-blog')
            <!-- End of Owl Carousel -->
            {{--our clients--}}
            @include('frontend.layouts.our-clients')
            <!-- End of Brands Wrapper -->

            {{--our news letter--}}
            @include('frontend.layouts.our-newsetter')
            <!--End of Cta Wrapper -->

                {{--out instragram--}}
                {{--@include('frontend.layouts.our-instragram')--}}
            </div>
            <!-- End of Container -->
    </section>


    <!-- Start of Footer -->
    <footer class="footer footer-dark appear-animate">
        <div class="container">
            {{--footer top--}}
            @include('frontend.layouts.footer-top')
            @include('frontend.layouts.footer-middle')
            @include('frontend.layouts.footer-bottom')
        </div>
    </footer>
    <!-- End of Footer -->

    <!-- End of Page-wrapper -->

    <!-- Start of Sticky Footer -->
    @include('frontend.layouts.sticky-footer')
    <!-- End of Sticky Footer -->

    <!-- Start of Scroll Top -->
    <a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="fas fa-chevron-up"></i></a>
    <!-- End of Scroll Top -->

    <!-- Start of Mobile Menu -->
    @include('frontend.layouts.mobile-menu')
    <!-- End of Mobile Menu -->

    <!-- Start of Quick View -->
    @include('frontend.layouts.quick-view')
    <!-- End of Quick view -->


@endsection
