<?php

namespace App\Exports;

//use App\Modules\Fileimport\Models\Fileimport;
use App\Modules\Product\Models\ProductFile;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class FileExportExport implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return ProductFile::all();
    }

    public function headings(): array
    {
        return [
            'name',
            'brand',
            'category',
            'size',
            'unit',
            'quantity',
            'minquantity',
            'maxquantity',
            'salesprice',
            'purchaseprice',
        ];
    }

    public function map($fileimport): array
    {
        return [
            $fileimport->name,
            $fileimport->brand,
            $fileimport->category,
            $fileimport->size,
            $fileimport->unit,
            $fileimport->quantity,
            $fileimport->minquantity,
            $fileimport->maxquantity,
            $fileimport->salesprice,
            $fileimport->purchaseprice,
        ];
    }
}
