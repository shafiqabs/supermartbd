<?php

use Illuminate\Support\Facades\Route;

//Route::get('general-page', 'GeneralPageController@welcome');

Route::group(['module' => 'GeneralPage', 'middleware' => ['web','auth','adminmiddleware']], function() {
    include 'generalpage.php';
});
