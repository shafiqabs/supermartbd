<?php

Route::get('admin-generalpage-create', [
    'as' => 'admin.generalpage.create',
    'uses' => 'GeneralPageController@create'
]);

Route::get('admin-generalpage-index', [
    'as' => 'admin.generalpage.index',
    'uses' => 'GeneralPageController@index'
]);

Route::post('admin-generalpage-store',[
    'as' => 'admin.generalpage.store',
    'uses' => 'GeneralPageController@store'
]);

Route::get('admin-generalpage-edit/{id}',[
    'as' => 'admin.generalpage.edit',
    'uses' => 'GeneralPageController@edit'
]);

Route::PATCH('admin-generalpage-update/{id}',[
    'as' => 'admin.generalpage.update',
    'uses' => 'GeneralPageController@update'
]);

Route::get('admin-generalpage-delete/{id}',[
    'as' => 'admin.generalpage.delete',
    'uses' => 'GeneralPageController@delete'
]);
