<?php
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request as Input;

?>

    <div class="row">
        <div class="col-md-12">
            <div class="row">
            <div class="col-md-6">
            <div class="from-group">
                {!! Form::label('Select Group', 'Select Group', array('class' => 'form-label')) !!}
                <span style="color: red">*</span>
                <div class="input-group mb-3">
                    <?php
                        $GroupData = array();
                    $GroupData[''] = 'Select Group';
                    $GroupData['company'] = 'Company';
                    $GroupData['myaccount'] = 'My Account';
                    $GroupData['customerservice'] = 'Customer Service';
                    $GroupData['support'] = 'Support';
                    ?>
                    {!! Form::select('group',$GroupData,Input::old('group'),['id'=>'group','class' => 'form-control form-select js-example-basic-single','data-checkify'=>'required','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
                </div>
                <span style="color: #ff0000">{!! $errors->first('group') !!}</span>

            </div>
            </div>

            <div class="col-md-6">
            <div class="from-group">
                {!! Form::label('Page Title', 'Page Title', array('class' => 'form-label')) !!}
                <span style="color: red">*</span>
                <div class="input-group mb-3">
                    {!! Form::text('title',Input::old('title'),['id'=>'title','class' => 'form-control','data-checkify'=>'minlen=3,required','Placeholder' => 'Enter title','aria-label' =>'content','aria-describedby'=>'basic-addon2']) !!}
                </div>
                <span style="color: #ff0000">{!! $errors->first('title') !!}</span>

            </div>
            </div>
            </div>

            <div class="row">
                <div class="col-md-12">
            <div class="from-group">
                {!! Form::label('Content', 'Content', array('class' => 'form-label')) !!}
                <span style="color: red">*</span>
                <div class="input-group mb-3">
                    {!! Form::textarea('content',Input::old('content'),['id'=>'editor','class' => 'form-control','data-checkify'=>'minlen=3,required','aria-label' =>'content','aria-describedby'=>'basic-addon2','rows'=>2]) !!}
                    <span style="color: #ff0000">{!! $errors->first('content') !!}</span>
                </div>
            </div>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-4 offset-md-8">
            <div class="from-group">
                <div class="from-group">
                    {!! Form::label('Status', 'Status', array('class' => 'form-label')) !!}
                    <span style="color: red">*</span>
                    <div class="input-group mb-3">
                        {!! Form::select('status',array(1=>'Active',0=>'Inactive'),Input::old('status'),['id'=>'ordering','class' => 'form-control','data-checkify'=>'minlen=1,required,number','Placeholder' => 'Enter ordering number','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('ordering') !!}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="row">

        <div class="col-md-12" style="text-align: right;">
            <div class="from-group">
                <div class="">
                    <button type="reset" class="btn submit-button">Reset</button>
                    <button type="submit" class="btn submit-button">Submit</button>
                </div>
            </div>
        </div>

    </div>
