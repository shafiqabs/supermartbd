@extends(/** @lang text */'backend.layouts.master')

@section('body')
    <div class="dashboard-area">
        <div id="carbon-block" class="">
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">{{$ModuleTitle}}</h1>

                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group me-2">
                            <a style="color: #000;" href="{{route('admin.generalpage.index')}}" title="Slider List" class="module_button_header">
                                <button type="button" class="btn btn-sm btn-outline-secondary">
                                    <i class="fas fa-th-list"></i> Page List
                                </button>
                            </a>
                        </div>


                    </div>
                </div>
            </main>

        <div class="row">
            <div class="col-md-12">
                <div class="card">

                <div class="card-header">
                    {{$PageTitle}}
                </div>
                <div class="card-body">

                    @include('backend.layouts.message')

                    {!! Form::open(['route' => 'admin.generalpage.store','enctype'=>'multipart/form-data',  'files'=> true, 'id'=>'basic-form', 'class' => '']) !!}

                    @include('GeneralPage::generalpage._form')

                    {!! Form::close() !!}

                </div>
                </div>
            </div>
        </div>



        </div>
    </div>


@endsection
