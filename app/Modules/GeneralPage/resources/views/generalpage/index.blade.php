@extends(/** @lang text */'backend.layouts.master')

@section('body')
    <div class="dashboard-area">
        <div id="carbon-block" class="">
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">{{$ModuleTitle}}</h1>

                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group me-2">
                            <a style="color: #000;" href="{{route('admin.generalpage.create')}}" title="Add General Page" class="module_button_header">
                                <button type="button" class="btn btn-sm btn-outline-secondary">
                                    <i class="fas fa-plus-circle"></i> Create New
                                </button>
                            </a>
                        </div>
                    </div>
                </div>
            </main>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">
                            {{$PageTitle}}
                        </div>
                        <div class="card-body">

                            @include('backend.layouts.message')

                            @if(isset($GenerlPage) && !empty($GenerlPage))
                                <div class="table-responsive">
                                    <table  class="table table-striped table-bordered text-center" id="">
                                        <thead>
                                        <th>SL</th>
                                        <th>Group</th>
                                        <th>Title</th>
                                        <th>Slug</th>
                                        <th>Action</th>
                                        </thead>

                                        <tbody>
                                        <?php $i=1; ?>
                                        @foreach($GenerlPage as $value)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>
                                                    @if($value->group == 'customerservice')
                                                    {{'Customer Service'}}
                                                    @endif
                                                    @if($value->group == 'myaccount')
                                                        {{'My Account'}}
                                                    @endif
                                                    @if($value->group == 'support')
                                                        {{'Support'}}
                                                    @endif
                                                    @if($value->group == 'company')
                                                        {{'Company'}}
                                                    @endif
                                                </td>
                                                <td>{{$value->title}}</td>
                                                <td>{{$value->slug}}</td>

                                                <td>
                                                    <a href="{{route('admin.generalpage.edit',$value->id)}}" title="Edit General Page"><i class="fas fa-user-edit"></i></a> |
                                                    <a href="{{route('admin.generalpage.delete',$value->id)}}" title="Delete General Page" onclick="return confirm('Are you sure to Delete?')"><i class="fas fa-trash-alt"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class=" justify-content-right">
                                    {{ $GenerlPage->links('backend.layouts.pagination') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>


@endsection
