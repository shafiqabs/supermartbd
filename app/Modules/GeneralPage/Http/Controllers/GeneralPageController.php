<?php

namespace App\Modules\GeneralPage\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\GeneralPage\Models\GeneralPage;
use Illuminate\Http\Request;
use App\Modules\GeneralPage\Requests;

use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;
class GeneralPageController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $ModuleTitle = "Manage General Page ";
        $PageTitle = " General Page list";
        $TableTitle = "General Page list";

        $GenerlPage = GeneralPage::where('status',1)->orderby('id','desc')->paginate(10);

        return view("GeneralPage::generalpage.index", compact('ModuleTitle','PageTitle','TableTitle','GenerlPage'));
    }

    public function create(){
        $ModuleTitle = "Manage General Page ";
        $PageTitle = "Add General Page";
        $TableTitle = "General Page list";

        return view("GeneralPage::generalpage.create", compact('ModuleTitle','PageTitle','TableTitle'));
    }

    public function store(Requests\GeneralPageRequest $request){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['title']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = GeneralPage::where('slug',$slug)->count();

        if ($SlugExistsOrNot ==0) {
            $input['slug'] = $slug;

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // Store brand data
                if ($PageData = GeneralPage::create($input)) {
                    $PageData->save();

                }

                DB::commit();
                Session::flash('message', 'Information added Successfully!');
                return redirect()->route('admin.generalpage.index');
            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Page Title already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function edit($id){
        $ModuleTitle = "Manage General Page";
        $PageTitle = "Update General Page";

        $data = GeneralPage::where('status','1')->where('id',$id)->first();

        return view("GeneralPage::generalpage.edit", compact('data','ModuleTitle','PageTitle'));
    }

    public function update(Requests\GeneralPageRequest $request,$id){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['title']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);
        $input['slug'] = $slug;

        $SlugExistsOrNot = GeneralPage::where('slug',$slug)->count();
        $SlugToId = GeneralPage::where('slug',$slug)->first();
        $PageUpdateModel = GeneralPage::where('id',$id)->first();

        if ($SlugExistsOrNot == 0 || ($SlugExistsOrNot == 1 && $id == $SlugToId->id)){
            $input['slug'] = $slug;

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // update page data
                $result = $PageUpdateModel->update($input);
                $PageUpdateModel->save();

                DB::commit();

                Session::flash('message', 'Information Updated Successfully!');
                return redirect()->route('admin.generalpage.index');

            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }

        }else{
            Session::flash('validate', 'Page title already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function delete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {

            $PageDeleteModel = GeneralPage::where('id', $id)
                ->select('*')
                ->first();
            $PageDeleteModel->delete();
            Session::flash('delete', 'Delete Successfully !');

            DB::commit();
            return redirect('admin-generalpage-index');
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }
}
