<?php

namespace App\Modules\GeneralPage\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class GeneralPageRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'group'       => 'required',
            'title'       => 'required',
            'content'       => 'required',
        ];

    }

}
