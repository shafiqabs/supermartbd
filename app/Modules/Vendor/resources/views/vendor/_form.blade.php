<?php
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request as Input;
?>


    <div class="row">
        <div class="col-md-4">
            <div class="from-group">
                {!! Form::label('Store/Company Name', 'Store/Company Name', array('class' => 'form-label')) !!}
                <span style="color: red">*</span>
                <div class="input-group mb-3">
                    {!! Form::text('store_name',Input::old('store_name'),['id'=>'store_name','class' => 'form-control','data-checkify'=>'minlen=3,required','Placeholder' => 'Enter Store/Company name','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                    <span style="color: #ff0000">{!! $errors->first('store_name') !!}</span>
                </div>
            </div>

            <div class="from-group">
                {!! Form::label('Store/Company Address', 'Store/Company Address', array('class' => 'form-label')) !!}
                <div class="input-group mb-3">
                    {!! Form::textarea('store_address',Input::old('store_address'),['id'=>'store_address','class' => 'form-control','Placeholder' => 'Enter Store/Company address','aria-label' =>'store_address','aria-describedby'=>'basic-addon2','rows'=>2]) !!}
                    <span style="color: #ff0000">{!! $errors->first('store_address') !!}</span>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="from-group">
                {!! Form::label('Contact Person Name', 'Contact Person Name', array('class' => 'form-label')) !!}
                <span style="color: red">*</span>
                <div class="input-group mb-3">
                    {!! Form::text('contact_person_name',Input::old('contact_person_name'),['id'=>'contact_person_name','class' => 'form-control','data-checkify'=>'minlen=3,required','Placeholder' => 'Enter Contact Person name','aria-label' =>'contact_person_name','aria-describedby'=>'basic-addon2']) !!}
                    <span style="color: #ff0000">{!! $errors->first('contact_person_name') !!}</span>
                </div>
            </div>

            <div class="from-group">
                {!! Form::label('Mobile', 'Mobile', array('class' => 'form-label')) !!}
                <span style="color: red">*</span>
                <div class="input-group mb-3">
{{--                    @if(isset($data))--}}
{{--                        {!! Form::text('mobile',Input::old('mobile'),['readonly'=>'true','id'=>'mobile','class' => 'form-control','data-checkify'=>'minlen=6,required,maxlen=14,number','Placeholder' => 'Enter Contact Person mobile','aria-label' =>'mobile','aria-describedby'=>'basic-addon2']) !!}--}}
{{--                        <span style="color: #ff0000">{!! $errors->first('mobile') !!}</span>--}}
{{--                    @else--}}
                        {!! Form::text('mobile',Input::old('mobile'),['id'=>'mobile','class' => 'form-control','data-checkify'=>'minlen=6,required,maxlen=14,number','Placeholder' => 'Enter Contact Person mobile','aria-label' =>'mobile','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('mobile') !!}</span>
{{--                    @endif--}}

                </div>
            </div>

            <div class="from-group">
                {!! Form::label('Email', 'Email', array('class' => 'form-label')) !!}
{{--                <span style="color: red">*</span>--}}
                <div class="input-group mb-3">
                    {!! Form::text('email',Input::old('email'),['id'=>'email','class' => 'form-control','data-checkify'=>'minlen=6,maxlen=40,email','Placeholder' => 'Enter Contact Person email','aria-label' =>'email','aria-describedby'=>'basic-addon2']) !!}
                    <span style="color: #ff0000">{!! $errors->first('email') !!}</span>
                </div>
            </div>
        </div>



        <div class="col-md-4">
            <div class="from-group">
                {!! Form::label('Store/Company Image', 'Store/Company Image', array('class' => 'form-label','for'=>'formFile')) !!}
                @if(isset($data))
                @else
                    <span style="color: red">*</span>
                @endif

                <div class="mb-3">
                    @if(isset($data))
                        <input class="form-control" accept="image/*" name="store_image" type="file" id="file" onchange="loadFile(event)">
                    @else
                        <input required class="form-control" accept="image/*" name="store_image" type="file" id="file" onchange="loadFile(event)">

                    @endif

                    <div style="text-align: center;padding-top: 5px;">
                        @if($errors->first('store_image'))
                            <span style="color: #ff0000">{!! $errors->first('store_image') !!}</span>
                        @else
                            <img id="store_image" width="150" height="120"/>
                        @endif

                        @if(isset($data) && $data->store_image !='')
                            <img id="store_image" src="{{ asset('backend/image/ProVendorImage').'/'.$data->store_image}}" width="150" height="120"/>
                        @endif

                    </div>
                </div>
            </div>


            <script>
                var loadFile = function(event) {
                    var image = document.getElementById('store_image');
                    image.src = URL.createObjectURL(event.target.files[0]);
                };
            </script>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 offset-md-8">
            <div class="from-group">
                <div class="from-group">
                    {!! Form::label('Status', 'Status', array('class' => 'form-label')) !!}
                    <span style="color: red">*</span>
                    <div class="input-group mb-3">
                        {!! Form::select('status',array(1=>'Active',0=>'Inactive'),Input::old('status'),['id'=>'ordering','class' => 'form-control','data-checkify'=>'minlen=1,required,number','Placeholder' => 'Enter ordering number','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('ordering') !!}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="row">

        <div class="col-md-12" style="text-align: right;">
            <div class="from-group">
                <div class="">
                    <button type="reset" class="btn submit-button">Reset</button>
                    <button type="submit" class="btn submit-button">Submit</button>
                </div>
            </div>
        </div>

    </div>
