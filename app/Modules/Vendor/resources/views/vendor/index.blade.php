@extends(/** @lang text */'backend.layouts.master')

@section('body')
    <div class="dashboard-area">
        <div id="carbon-block" class="">
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">{{$ModuleTitle}}</h1>

                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group me-2">

{{--                            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>--}}
{{--                            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>--}}
                        </div>
                        @if(Auth::user()->type == 'Admin')
                            <a style="color: #000;" href="{{route('admin.vendor.create')}}" title="Add Vendor" class="module_button_header">
                                <button type="button" class="btn btn-sm btn-outline-secondary">
                                    <i class="fas fa-plus-circle"></i> Create New
                                </button>
                            </a>
                        @endif


                    </div>
                </div>
            </main>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">
                            {{$PageTitle}}
                        </div>
                        <div class="card-body">

                            @include('backend.layouts.message')

                            @if(isset($VendorData) && !empty($VendorData))
                                <div class="table-responsive">
                                    <table  class="table table-striped table-bordered text-center" id="" style="overflow-x: clip;overflow-y: clip;">
                                        <thead>
                                        <th>SL</th>
                                        <th>Store name</th>
                                        <th>Contact Person</th>
                                        <th>Mobile</th>
                                        <th>Email</th>
{{--                                        <th>Address</th>--}}
                                        <th>Image</th>
                                        <th>Action</th>
                                        </thead>

                                        <tbody>
                                        <?php $i=1; ?>
                                        @foreach($VendorData as $value)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$value->store_name}}</td>
                                                <td>{{$value->contact_person_name}}</td>
                                                <td>
                                                   {{$value->mobile}}
                                                </td>
                                                <td>{{$value->email}}</td>
{{--                                                <td>{{$value->store_address}}</td>--}}
                                                <td>
                                                    <div class="tooltipme">Image
                                                        <span class="tooltipmetext">
                                                                <img class="hover_image" src="{{ asset('backend/image/ProVendorImage').'/'.$value->store_image;}}" alt="">

                                                        </span>
                                                    </div>
                                                </td>
                                                <td>
                                                    @if(Auth::user()->type == 'Admin' || (Auth::user()->type == 'Vendor' && Auth::user()->id == $value->user_id))
                                                        <a href="{{route('admin.vendor.edit',$value->id)}}" title="Edit Vendor Info"><i class="fas fa-user-edit"></i></a>
                                                    @if(Auth::user()->type == 'Admin')
                                                            | <a href="{{route('admin.vendor.delete',$value->id)}}" title="Delete Vendor Info" onclick="return confirm('Are you sure to Delete?')"><i class="fas fa-trash-alt"></i></a>
                                                    @endif
                                                    @endif
</td>
</tr>
@endforeach
</tbody>
</table>
</div>
<div class=" justify-content-right">
{{ $VendorData->links('backend.layouts.pagination') }}
</div>
@endif
</div>
</div>
</div>
</div>



</div>
</div>


@endsection
