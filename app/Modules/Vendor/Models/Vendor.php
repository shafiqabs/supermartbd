<?php

namespace App\Modules\Vendor\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Auth;

class Vendor extends Model
{
    use HasFactory;
    protected $table = 'ems_vendor';
    protected $fillable = [
        'user_id',
        'store_name',
        'slug',
        'store_address',
        'contact_person_name',
        'mobile',
        'email',
        'store_image',
        'status',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];


    public function VendorProduct(){
        return $this->hasMany('App\Modules\Product\Models\Product','vendor_id','id');
    }

    // TODO :: boot
    // boot() function used to insert logged user_id at 'created_by' & 'updated_by'
    public static function boot(){
        parent::boot();
        static::creating(function($query){
            if(Auth::check()){
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function($query){
            if(Auth::check()){
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}
