<?php

namespace App\Modules\Vendor\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Modules\MasterData\Models\Color;
use App\Modules\User\Models\User;
use Illuminate\Http\Request;

use App\Modules\Vendor\Models\Vendor;

use App\Modules\Vendor\Requests;
use Illuminate\Support\Facades\Input;

use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;
class VendorController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */


    public function create(){
        $ModuleTitle = "Manage Vendor Information ";
        $PageTitle = "Add Vendor Information";
        $TableTitle = "Vendor Information list";

        return view("Vendor::vendor.create", compact('ModuleTitle','PageTitle','TableTitle'));

    }

    public function store(Requests\VendorRequest $request){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['store_name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = Vendor::where('slug',$slug)->count();
        if ($SlugExistsOrNot ==0) {
            $input['slug'] = $slug;

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // Store vendor data
                $UserMailExistsOrNot = App\Modules\User\Models\User::where('email',$input['email'])->count();
//                echo $UserMailExistsOrNot;
                if ($UserMailExistsOrNot == 0){
                    $MobileExistsOrNot = App\Modules\User\Models\User::where('mobile',$input['mobile'])->count();
                    if ($MobileExistsOrNot == 0){
                        $Password = rand(10,100000).''.$input['mobile'];
                        $UserInfo['password'] = password_hash($Password,PASSWORD_DEFAULT) ;
                        $UserInfo['name'] = $input['contact_person_name'];
                        $UserInfo['mobile'] = $input['mobile'];
                        $UserInfo['email'] = $input['email'];
                        $UserInfo['address'] = $input['store_address'];
                        $UserInfo['type'] = 'Vendor';
                        $UserInfo['user_image'] = 'defaultuser.png';
                        $UserInfo['status'] = $input['status'];
                        $UserCreate = App\Modules\User\Models\User::create($UserInfo);
                        $input['user_id'] = $UserCreate->id;

                        $details = [
                            'mailpage' => 'UserMail',
                            'title' => 'Supermartbd Login Information',
                            'name' => 'Hi ' . $input['contact_person_name'],
                            'Email' => $input['email'],
                            'Type' => 'Vendor',
                            'Password' => $Password
                        ];

                        \Mail::to($input['email'])->send(new \App\Mail\MailSend($details));
                    }else{
                        Session::flash('message', 'Mobile Already exists into user');
                        return redirect()->back()->withInput($input);
                    }
                }else{
                    Session::flash('message', 'Email Already exists into user');
                    return redirect()->back()->withInput($input);
                }

                if ($UserCreate){
                    if ($request->file('store_image') != '') {
                        $avatar = $request->file('store_image');
                        $file_title = $slug.time().'.'.$avatar->getClientOriginalExtension();
                        $input['store_image'] = $file_title;
                        $path = public_path("backend/image/ProVendorImage/");
                        $target_file =  $path.basename($file_title);
                        $file_path = $_FILES['store_image']['tmp_name'];
                        move_uploaded_file($file_path,$target_file);
                    }
                    if ($VendorData = Vendor::create($input)) {
                        $VendorData->save();
                    }
                }


                DB::commit();
                Session::flash('message', 'Information added Successfully!');
//                return redirect()->back();
                return redirect()->route('admin.vendor.index');
            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Store Name already exists');
            return redirect()->back()->withInput($input);
        }

    }



    public function index(){
        $ModuleTitle = "Manage Vendor Information ";
        $PageTitle = " Vendor Information list";
        $TableTitle = "Vendor Information list";

        $VendorData = Vendor::where('status',1)->orderby('id','desc')->paginate(10);

        return view("Vendor::vendor.index", compact('ModuleTitle','PageTitle','TableTitle','VendorData'));

    }

    public function edit($id){
        $ModuleTitle = "Manage Vendor Information";
        $PageTitle = "Update Vendor Information";

        $data = Vendor::where('status','1')->where('id',$id)->first();

        return view("Vendor::vendor.edit", compact('data','ModuleTitle','PageTitle'));

    }

    public function update(Requests\UpdateVendorRequest $request,$id){
        $input = $request->all();


        $slug = str_replace(' ', '-', $input['store_name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);
        $input['slug'] = $slug;


        $SlugExistsOrNot = Vendor::where('slug',$slug)->count();
        $SlugToId = Vendor::where('slug',$slug)->first();
        $VendorUpdateModel = Vendor::where('id',$id)->first();

        if ($SlugExistsOrNot == 0 || ($SlugExistsOrNot == 1 && $id == $SlugToId->id)){
            $input['slug'] = $slug;


//            $UserMailExistsOrNot = User::where('email',$input['email'])->count();
//            $UserData = User::where('id',$VendorUpdateModel->user_id)->first();
//            if ( $UserData->email == $EmailId->id){
//                $MobileExistsOrNot = User::where('mobile',$input['mobile'])->count();

//                if ($MobileExistsOrNot == 0 || ($MobileExistsOrNot == 1 && $VendorUpdateModel->user_id == $EmailId->id)){
                    if ($request->file('store_image') != '') {
                        File::delete(public_path().'/backend/image/ProVendorImage/'.$VendorUpdateModel->store_image);
                        $avatar = $request->file('store_image');
                        $file_title = $slug.time().'.'.$avatar->getClientOriginalExtension();
                        $input['store_image'] = $file_title;
                        $path = public_path("backend/image/ProVendorImage/");
                        $target_file =  $path.basename($file_title);
                        $file_path = $_FILES['store_image']['tmp_name'];
                        $result = move_uploaded_file($file_path,$target_file);
                    }else{
                        $input['store_image'] = $VendorUpdateModel['store_image'];
                    }

                    /* Transaction Start Here */
                    DB::beginTransaction();
                    try {
                        // update vendor data
                        $result = $VendorUpdateModel->update($input);
                        $VendorUpdateModel->save();

                        //user data update
//                        $UserInfo['email'] = $input['email'];
//                        $UserInfo['name'] = $input['contact_person_name'];
//                        $UserInfo['mobile'] = $input['mobile'];
//                        $UserInfo['address'] = $input['store_address'];
//                        $UserInfo['type'] = 'Vendor';
//
//                        $EmailId->update($UserInfo);
//                        $EmailId->save();

                        DB::commit();

                        Session::flash('message', 'Information Updated Successfully!');
                        return redirect()->route('admin.vendor.index');

                    } catch (\Exception $e) {
                        //If there are any exceptions, rollback the transaction`
                        DB::rollback();
                        print($e->getMessage());
                        exit();
                        Session::flash('danger', $e->getMessage());
                    }
//                }else{
//                    Session::flash('validate', 'Mobile number already exists');
//                    return redirect()->back()->withInput($input);
//                }
//            }else{
//                Session::flash('validate', 'Email already exists');
//                return redirect()->back()->withInput($input);
//            }
        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function delete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {
            $VendorExistsOrNot = Vendor::where('id',$id)->first();
//            $UserInfo = User::where('id',$VendorExistsOrNot->id)->first();
//            $UserType = $UserInfo->type;

//            if ($UserType == 'Vendor'){
                    $ProductVendor = App\Modules\Product\Models\Product::where('vendor_id',$id)->count();

                    if ($ProductVendor == 0){

                        $VendorModel = Vendor::where('id', $id)
                            ->select('*')
                            ->first();

                        if (isset($VendorModel->store_image) && !empty($VendorModel->store_image)) {
                            File::delete(public_path() . '/backend/image/ProVendorImage/' . $VendorModel->store_image);
                            $VendorModel->delete();
                        }else{
                            $VendorModel->delete();
                        }

                        $UserModel = User::where('id',$VendorExistsOrNot->user_id)->first();
                        if (isset($UserModel->user_image) && !empty($UserModel->user_image) && $UserModel->user_image != 'defaultuser.png') {
                            File::delete(public_path() . '/backend/image/UserImage/' . $UserModel->user_image);
                        }
                        $UserModel->delete();
                        DB::commit();

                        Session::flash('validate', 'User remove Successfully');
                        return redirect('admin-vendor-index');
                    }else{
                        Session::flash('validate', 'This vendor has total '.$ProductVendor.' products.');
                        return redirect('admin-vendor-index');
                    }
//            }
            DB::commit();
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }


    public function PasswordForm(){
        $ModuleTitle = Auth::user()->type." Password Change";
        $PageTitle = Auth::user()->name." Password Change";
        $TableTitle = "Password Change";

        return view("User::user.passwordchange", compact('ModuleTitle','PageTitle','TableTitle'));

    }

    public function PasswordChange(Request $request){
        $input = $request->all();

        $validated = $request->validate([
            'password' => 'required|min:6',
            'confirm' => 'required|min:6',
        ]);
//        |same:confirm
        if ($input['password'] == $input['confirm']){
            $password = password_hash($input['password'],PASSWORD_DEFAULT) ;

            $UpatePaaword = User::find(Auth::user()->id);
            $UpPass['password'] = $password;
            $UpatePaaword->update($UpPass);
            $UpatePaaword->save();
            Session::flash('message', 'Password change successfully');
            return redirect()->back();
        }else{
            Session::flash('validate', 'Password must be same');
            return redirect()->back()->withInput($input);
        }
    }


}
