<?php

Route::get('admin-vendor-create', [
    'as' => 'admin.vendor.create',
    'uses' => 'VendorController@create'
]);

Route::get('admin-vendor-index', [
    'as' => 'admin.vendor.index',
    'uses' => 'VendorController@index'
]);

Route::post('admin-vendor-store',[
    'as' => 'admin.vendor.store',
    'uses' => 'VendorController@store'
]);

Route::get('admin-vendor-edit/{id}',[
    'as' => 'admin.vendor.edit',
    'uses' => 'VendorController@edit'
]);

Route::PATCH('admin-vendor-update/{id}',[
    'as' => 'admin.vendor.update',
    'uses' => 'VendorController@update'
]);

Route::get('admin-vendor-delete/{id}',[
    'as' => 'admin.vendor.delete',
    'uses' => 'VendorController@delete'
]);

Route::get('admin-change-passwordform',[
    'as' => 'admin.change.passwordform',
    'uses' => 'VendorController@PasswordForm'
]);

Route::post('admin-change-passwordchange',[
    'as' => 'admin.change.passwordchange',
    'uses' => 'VendorController@PasswordChange'
]);


