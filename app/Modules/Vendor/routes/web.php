<?php

use Illuminate\Support\Facades\Route;

//Route::get('vendor', 'VendorController@welcome');

Route::group(['module' => 'Vendor', 'middleware' => ['web','auth','adminmiddleware']], function() {
    include 'vendor.php';
});

Route::group(['module' => 'Vendor', 'middleware' => ['web','auth','VendorMiddleware']], function() {
    include 'vendor.php';


});
