<?php

Route::get('admin-timeslot-create', [
    'as' => 'admin.timeslot.create',
    'uses' => 'DeliveryTimeSlotController@create'
]);

Route::get('admin-timeslot-index', [
    'as' => 'admin.timeslot.index',
    'uses' => 'DeliveryTimeSlotController@index'
]);

Route::post('admin-timeslot-store',[
    'as' => 'admin.timeslot.store',
    'uses' => 'DeliveryTimeSlotController@store'
]);

Route::get('admin-timeslot-edit/{id}',[
    'as' => 'admin.timeslot.edit',
    'uses' => 'DeliveryTimeSlotController@edit'
]);

Route::PATCH('admin-timeslot-update/{id}',[
    'as' => 'admin.timeslot.update',
    'uses' => 'DeliveryTimeSlotController@update'
]);

Route::get('admin-timeslot-delete/{id}',[
    'as' => 'admin.timeslot.delete',
    'uses' => 'DeliveryTimeSlotController@delete'
]);


