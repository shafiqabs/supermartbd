<?php

use Illuminate\Support\Facades\Route;

//Route::get('delivery-time-slot', 'DeliveryTimeSlotController@welcome');
Route::group(['module' => 'DeliveryTimeSlot', 'middleware' => ['web','auth','adminmiddleware']], function() {
    include 'timeslot.php';
});
