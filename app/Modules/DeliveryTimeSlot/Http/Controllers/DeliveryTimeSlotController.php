<?php

namespace App\Modules\DeliveryTimeSlot\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Modules\DeliveryTimeSlot\Requests;
use Illuminate\Support\Facades\Input;
USE App\Modules\DeliveryTimeSlot\Models\DeliveryTimeSlot;

use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class DeliveryTimeSlotController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $ModuleTitle = "Manage Delivery Time Slot";
        $PageTitle = "Add Delivery Time Slot";
        $TableTitle = "Delivery Time Slot list";

        $AllTimeSlot = DeliveryTimeSlot::where('status','1')->orderby('id','desc')->paginate(10);

        return view("DeliveryTimeSlot::timeslot.create", compact('ModuleTitle','PageTitle','TableTitle','AllTimeSlot'));

    }

    public function store(Requests\TimeSlotRequest $request){
        $input = $request->all();
        $input['time_slot'] = $input['start_time'].' - '.$input['end_time'];

        $SlugExistsOrNot = DeliveryTimeSlot::where('time_slot',$input['time_slot'])->count();

        if ($SlugExistsOrNot == 0) {

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // Store sizeunit data
                if ($TimeSlot = DeliveryTimeSlot::create($input)) {
                    $TimeSlot->save();

                }

                DB::commit();
                Session::flash('message', 'Information added Successfully!');
                return redirect()->back();

            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function edit($id){
        $ModuleTitle = "Manage Delivery Time Slot";
        $PageTitle = "Update Delivery Time Slot";
        $TableTitle = "Delivery Time Slot list";

        $data = DeliveryTimeSlot::where('status','1')->where('id',$id)->first();
        $AllTimeSlot = DeliveryTimeSlot::where('status','1')->orderby('id','desc')->paginate(10);

        return view("DeliveryTimeSlot::timeslot.edit", compact('data','ModuleTitle','PageTitle','TableTitle','AllTimeSlot'));

    }

    public function update(Requests\TimeSlotRequest $request,$id){
        $input = $request->all();
        $input['time_slot'] = $input['start_time'].' - '.$input['end_time'];

        $SlugExistsOrNot = DeliveryTimeSlot::where('time_slot',$input['time_slot'])->count();
        $SlugToId = DeliveryTimeSlot::where('time_slot',$input['time_slot'])->first();
        $TimeSlotModel = DeliveryTimeSlot::where('id',$id)->first();
        if ($SlugExistsOrNot == 0 || ($SlugExistsOrNot == 1 && $id == $SlugToId->id)){
            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // update time slot data
                $result = $TimeSlotModel->update($input);
                $TimeSlotModel->save();

                DB::commit();

                Session::flash('message', 'Information Updated Successfully!');

                if ($input['status'] == 1){
                    return redirect()->route('admin.timeslot.edit', ['id' => $id]);
                }else{
                    return redirect()->route('admin.timeslot.create');
                }



            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function delete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {
            $OrderTimeSlot = App\Models\OrderHead::where('time_slot_id',$id)->count();
            if ($OrderTimeSlot == 0) {
                $DeliverySlot = DeliveryTimeSlot::where('id', $id)
                    ->select('*')
                    ->first();
                $DeliverySlot->delete();

                DB::commit();
                Session::flash('delete', 'Delete Successfully !');
//            return Redirect::back()->withErrors(['message', 'Delete Successfully !']);
                return redirect('admin-timeslot-create');
            }else{
                Session::flash('validate', 'Already use in Product !');
                return redirect('admin-timeslot-create');
            }
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }
}
