<?php

namespace App\Modules\DeliveryTimeSlot\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class TimeSlotRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'start_time'       => 'required',
            'end_time'       => 'required',
            'ordering'=> 'integer',
        ];

    }

}
