<?php
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request as Input;

?>


    <div class="row">
        <div class="col-md-6">
            <div class="from-group">
                {!! Form::label('Start Time', 'Start Time', array('class' => 'form-label')) !!}
                <span style="color: red">*</span>
                <div class="input-group mb-3">
                    {!! Form::time('start_time',Input::old('start_time'),['id'=>'start_time','class' => 'form-control','data-checkify'=>'minlen=3,required','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                    <span style="color: #ff0000">{!! $errors->first('start_time') !!}</span>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="from-group">
                {!! Form::label('End Time', 'End Time', array('class' => 'form-label')) !!}
                <span style="color: red">*</span>
                <div class="input-group mb-3">
                    {!! Form::time('end_time',Input::old('end_time'),['id'=>'end_time','class' => 'form-control','data-checkify'=>'minlen=3,required','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                    <span style="color: #ff0000">{!! $errors->first('end_time') !!}</span>
                </div>
            </div>
        </div>


        <div class="col-md-6">
            <div class="from-group">
                <div class="from-group">
                    {!! Form::label('Ordering', 'Ordering', array('class' => 'form-label')) !!}
                    <span style="color: red">*</span>
                    <div class="input-group mb-3">
                        {!! Form::text('ordering',Input::old('ordering'),['id'=>'ordering','class' => 'form-control','data-checkify'=>'minlen=1,number,required','Placeholder' => 'Enter ordering number','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('ordering') !!}</span>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-6">
            <div class="from-group">
                <div class="from-group">
                    {!! Form::label('Status', 'Status', array('class' => 'form-label')) !!}
                    <span style="color: red">*</span>
                    <div class="input-group mb-3">
                        {!! Form::select('status',array(1=>'Active',0=>'Inactive'),Input::old('status'),['id'=>'ordering','class' => 'form-control','data-checkify'=>'minlen=1,required,number','Placeholder' => 'Enter ordering number','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('status') !!}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="row">

        <div class="col-md-12" style="text-align: right;">
            <div class="from-group">
                <div class="">
                    <button type="reset" class="btn submit-button">Reset</button>
                    <button type="submit" class="btn submit-button">Submit</button>
                </div>
            </div>
        </div>

    </div>
