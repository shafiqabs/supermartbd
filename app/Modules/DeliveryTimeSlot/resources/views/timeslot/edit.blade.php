@extends(/** @lang text */'backend.layouts.master')

@section('body')
    <div class="dashboard-area">
        <div id="carbon-block" class="">
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">{{$ModuleTitle}}</h1>

                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group me-2">

{{--                            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>--}}
{{--                            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>--}}

                            <a href="{{route('admin.timeslot.create')}}" title="Add Time Slot" class="module_button_header">
                                <button type="button" class="btn btn-sm btn-outline-secondary">
                                    <i class="fas fa-plus-circle"></i> Create New
                                </button>
                            </a>

                        </div>
                    </div>
                </div>
            </main>

            <div class="row">
                <div class="col-md-6">
                    <div class="card">

                        <div class="card-header">
                            {{$PageTitle}}
                        </div>
                        <div class="card-body">

                            @include('backend.layouts.message')

                            {!! Form::model($data, ['method' => 'PATCH', 'files'=> true, 'route'=> ['admin.timeslot.update', $data->id],"class"=>"", 'id' => 'basic-form']) !!}

                            @include('DeliveryTimeSlot::timeslot._form')

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">

                        <div class="card-header">
                            {{$TableTitle}}
                        </div>
                        <div class="card-body">
                            @if(isset($AllTimeSlot) && !empty($AllTimeSlot))
                                <div class="table-responsive">
                                    <table  class="table table-striped table-bordered text-center" id="">
                                        <thead>
                                        <th>SL</th>
                                        <th>Start Time</th>
                                        <th>End Time</th>
                                        <th>Time Slot</th>
                                        <th>Ordering</th>
                                        <th>Action</th>
                                        </thead>

                                        <tbody>
                                        <?php $i=1; ?>
                                        @foreach($AllTimeSlot as $value)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$value->start_time}}</td>
                                                <td>{{$value->end_time}}</td>
                                                <td>{{$value->time_slot}}</td>
                                                <td>{{$value->ordering}}</td>
                                                <td>
                                                    <a href="{{route('admin.timeslot.edit',$value->id)}}" title="Edit Time Slot"><i class="fas fa-user-edit"></i></a> |
                                                    <a href="{{route('admin.timeslot.delete',$value->id)}}" title="Delete Time Slot" onclick="return confirm('Are you sure to Delete?')"><i class="fas fa-trash-alt"></i></a>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class=" justify-content-right">
                                    {{ $AllTimeSlot->links('backend.layouts.pagination') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>


@endsection
