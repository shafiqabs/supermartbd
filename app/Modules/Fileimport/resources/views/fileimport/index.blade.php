@extends(/** @lang text */'backend.layouts.master')

@section('body')
    <div class="dashboard-area">

        <div id="carbon-block">

            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">Dashboard</h1>

                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group me-2">


                            <button type="button" id="printbutton" class="btn btn-sm btn-outline-secondary">print</button>
                            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
                            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
                        </div>
                        <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
                            <span data-feather="calendar"></span>
                            This week
                        </button>

                    </div>
                </div>
            </main>


            <div class="card">
                <div class="card-header">
                    {{$module_title}}


                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            @if($message = Session::get('success'))

                                <div class="alert alert-primary" role="alert">
                                    <strong>{{$message }}</strong>
                                </div>

                            @endif
                            {!! Session::forget('success') !!}
                            <h2 class="text-title">Import Export Excel/CSV</h2>
                            <a href="{{ route('admin.exportExcel', 'xls') }}"><button class="btn btn-secondary btn-sm">Download xls</button></a>
                            <a href="{{ route('admin.exportExcel', 'xlsx') }}"><button class="btn btn-secondary btn-sm">Download xlsx</button></a>
                            <a href="{{ route('admin.exportExcel', 'csv') }}"><button class="btn btn-secondary btn-sm">Download CSV</button></a>
                            <form style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{ route('admin.importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="file" name="import_file" class="import-class" required/>
                                <button class="btn btn-secondary">Import File</button>
                            </form>
                        </div>

                        <div class="col-md-6">
                            <h5 class="card-title">File Import List</h5>
                            <p class="card-text"></p>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered" id="table_id">
                                    <thead>
                                    <tr>
                                        <th>Sl</th>
                                        <th>Name</th>
                                        <th>Type</th>
                                        <th>Amount</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @if(count($import_data) > 0)
                                        <?php
                                        $total_rows = 1;
                                        ?>

                                        @foreach($import_data as $value)
                                            <tr>
                                                <td><?=$total_rows?></td>
                                                <td>{{$value->name}}</td>
                                                <td>{{$value->type}}</td>
                                                <td>{{$value->amount}}</td>
                                                <td>
                                                    <a href="{{route('admin.importExportdelete',$value->id)}}" onclick="return confirm('Are you sure to Delete?')"><i class="fas fa-trash-alt"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                            $total_rows++;
                                            ?>
                                        @endforeach

                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
