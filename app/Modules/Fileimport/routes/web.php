<?php

use Illuminate\Support\Facades\Route;

//Route::get('fileimport', 'FileimportController@welcome');
Route::group(['module' => 'Fileimport', 'middleware' => ['web','auth']], function() {
    include ('fileimport.php');
});
