<?php

Route::get('admin-importExportView', [
    'as' => 'admin.importExportView',
    'uses' => 'FileimportController@importExportView'
]);

Route::post('admin-importExcel', [
    'as' => 'admin.importExcel',
    'uses' => 'FileimportController@importExcel'
]);

Route::get('admin-exportExcel/{type}', [
    'as' => 'admin.exportExcel',
    'uses' => 'FileimportController@exportExcel'
]);


Route::get('admin-importExportdelete/{id}', [
    'as' => 'admin.importExportdelete',
    'uses' => 'FileimportController@importExportdelete'
]);
