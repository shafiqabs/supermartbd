<?php

namespace App\Modules\Fileimport\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Modules\Fileimport\Models\Fileimport;
//use App\Models\Fileimport;
use App\Exports\FileExportExport;
use App\Imports\FileImportImort;


use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class FileimportController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @return \Illuminate\Support\Collection
     */
    public function importExportView()
    {
        $module_title = "File import export system";
        $action_title = "File import export information";

        $module_title_table = "File import Data";
        $action_title_table = "File import information";

        $import_data = Fileimport::get();
//        echo "<pre>";
//        print_r($import_data);
//        exit();

        return view('Fileimport::fileimport.index',compact(
            'module_title',
            'action_title',
            'module_title_table',
            'action_title_table',
            'import_data'
        ));
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function exportExcel($type)
    {
        return \Excel::download(new FileExportExport, 'fileimport.'.$type);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function importExcel(Request $request)
    {
        \Excel::import(new FileImportImort,$request->import_file);

        \Session::put('success', 'Your file is imported successfully in database.');

        return back();
    }

    public function welcome()
    {
        return view("Fileimport::welcome");
    }


    public function importExportdelete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {
            $file_model = Fileimport::where('id', $id)
                ->select('*')
                ->first();
            $file_model->delete();

            DB::commit();
            Session::flash('success', 'Delete Successfully !');
//            return Redirect::back()->withErrors(['message', 'Delete Successfully !']);
            return redirect('admin-importExportView');
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }
}
