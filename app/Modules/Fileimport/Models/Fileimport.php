<?php

namespace App\Modules\Fileimport\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fileimport extends Model
{
    use HasFactory;

    protected $table = 'fileimport';
    protected $guarded = array();
}
