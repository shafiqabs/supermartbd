<?php
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request as Input;

?>


    <div class="row">
        <div class="col-md-8">
            <div class="row col-md-12">
                <div class="from-group">
                    {!! Form::label('Discount Name', 'Discount Name', array('class' => 'form-label')) !!}
                    <span style="color: red">*</span>
                    <div class="input-group mb-3">
                        {!! Form::text('name',Input::old('name'),['id'=>'name','class' => 'form-control','data-checkify'=>'minlen=3,required','Placeholder' => 'Enter Discount Name','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('name') !!}</span>
                    </div>
                </div>
            </div>

            <hr>

            <div class="row col-md-12">
                <div class="col-md-6">
                <div class="from-group">
                <div class="form-check">
                    @if(isset($data) && $data->feature !='')
                        {!! Form::checkbox('feature', 'yes',$data->feature,['id'=>'flexCheckDefault','class'=>'form-check-input']) !!}
                    @else
                        {!! Form::checkbox('feature', 'yes',false,['id'=>'flexCheckDefault','class'=>'form-check-input']) !!}
                    @endif
                    {!! Form::label('Is Feature', 'Is Feature', array('class' => 'form-check-label','for'=>'flexCheckDefault')) !!}
                </div>
                </div>
                </div>

                <div class="col-md-6">
                <div class="from-group">
                    <div class="form-check">
                        @if(isset($data) && $data->subitem !='')
                            {!! Form::checkbox('subitem', 'yes',$data->subitem,['id'=>'flexCheckDefault','class'=>'form-check-input']) !!}
                        @else
                            {!! Form::checkbox('subitem', 'yes',false,['id'=>'flexCheckDefault','class'=>'form-check-input']) !!}
                        @endif
                        {!! Form::label('Discount SubItem', 'Discount SubItem', array('class' => 'form-check-label','for'=>'flexCheckDefault')) !!}
                    </div>
                </div>
                </div>
            </div>
<hr>
            <div class="row col-md-12">
                <div class="col-md-6">
                    <div class="from-group">
                        {!! Form::label('Discount Type', 'Discount Type', array('class' => 'form-label')) !!}
                        <span style="color: red">*</span>
                        <div class="input-group mb-3">
                            {!! Form::select('type',array('Percentage'=>'Percentage','Flat'=>'Flat'),Input::old('type'),['id'=>'ordering','class' => 'form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                            <span style="color: #ff0000">{!! $errors->first('type') !!}</span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="from-group">
                        {!! Form::label('Discount Amount', 'Discount Amount', array('class' => 'form-label')) !!}
                        <span style="color: red">*</span>
                        <div class="input-group mb-3">
                            {!! Form::text('amount',Input::old('amount'),['id'=>'amount','class' => 'form-control','data-checkify'=>'minlen=1,required,number','Placeholder' => 'Enter Discount Amount','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                            <span style="color: #ff0000">{!! $errors->first('amount') !!}</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row col-md-12">
                <div class="from-group">
                    {!! Form::label(' Brand', ' Brand', array('class' => 'form-label')) !!}
{{--                    <span style="color: red">*</span>--}}
                    <div class="input-group mb-3">
                        @if(isset($data))
                            {!! Form::select('brand_id[]',$Brand,$DiscountBrand,['id'=>'brand_id','multiple'=>'multiple','class' => 'form-select js-example-basic-multiple form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                            <span style="color: #ff0000">{!! $errors->first('brand_id') !!}</span>
                        @else
                            {!! Form::select('brand_id[]',$Brand,Input::old('brand_id'),['id'=>'brand_id','multiple'=>'multiple','class' => 'form-select js-example-basic-multiple form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                            <span style="color: #ff0000">{!! $errors->first('brand_id') !!}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row col-md-12">
                <div class="from-group">
                    {!! Form::label(' Category', ' Category', array('class' => 'form-label')) !!}
{{--                    <span style="color: red">*</span>--}}
                    <div class="input-group mb-3">

                        @if(isset($data))
                        {!! Form::select('category_id[]',$Category,$DiscountCategory,['id'=>'category_id','multiple'=>'multiple','class' => 'form-select js-example-basic-multiple form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        @else
                            {!! Form::select('category_id[]',$Category,Input::old('brand_id'),['id'=>'category_id','multiple'=>'multiple','class' => 'form-select js-example-basic-multiple form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>



        <div class="col-md-4">
            <div class="from-group">
                {!! Form::label('Discount Image', 'Discount Image', array('class' => 'form-label','for'=>'formFile')) !!}
                @if(isset($data))
                @else
                    <span style="color: red">*</span>
                @endif

                <div class="mb-3">
                    @if(isset($data))
                        <input class="form-control" accept="image/*" name="discount_image" type="file" id="file" onchange="loadFile(event)">
                    @else
                        <input required class="form-control" accept="image/*" name="discount_image" type="file" id="file" onchange="loadFile(event)">

                    @endif

                    <div style="text-align: center;padding-top: 5px;">
                        @if($errors->first('discount_image'))
                            <span style="color: #ff0000">{!! $errors->first('discount_image') !!}</span>
                        @else
                            <img id="discount_image" width="150" height="120"/>
                        @endif

                        @if(isset($data) && $data->discount_image !='')
                            <img id="discount_image" src="{{ asset('backend/image/ProDiscountImage').'/'.$data->discount_image;}}" width="150" height="120"/>
                        @endif

                    </div>
                </div>
            </div>


            <script>
                var loadFile = function(event) {
                    var image = document.getElementById('discount_image');
                    image.src = URL.createObjectURL(event.target.files[0]);
                };
            </script>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 offset-md-8">
            <div class="from-group">
                <div class="from-group">
                    {!! Form::label('Status', 'Status', array('class' => 'form-label')) !!}
                    <span style="color: red">*</span>
                    <div class="input-group mb-3">
                        {!! Form::select('status',array(1=>'Active',0=>'Inactive'),Input::old('status'),['id'=>'ordering','class' => 'form-control','data-checkify'=>'minlen=1,required,number','Placeholder' => 'Enter ordering number','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('ordering') !!}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="row">

        <div class="col-md-12" style="text-align: right;">
            <div class="from-group">
                <div class="">
                    <button type="reset" class="btn submit-button">Reset</button>
                    <button type="submit" class="btn submit-button">Submit</button>
                </div>
            </div>
        </div>

    </div>
