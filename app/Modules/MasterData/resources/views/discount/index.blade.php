@extends(/** @lang text */'backend.layouts.master')

@section('body')
    <div class="dashboard-area">
        <div id="carbon-block" class="">
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">{{$ModuleTitle}}</h1>

                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group me-2">

{{--                            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>--}}
{{--                            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>--}}
                        </div>

                        <a style="color: #000;" href="{{route('admin.discount.create')}}" title="Add Discount" class="module_button_header">
                            <button type="button" class="btn btn-sm btn-outline-secondary">
                                <i class="fas fa-plus-circle"></i> Create New
                            </button>
                        </a>


                    </div>
                </div>
            </main>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">
                            {{$PageTitle}}
                        </div>
                        <div class="card-body">

                            @include('backend.layouts.message')

                            @if(isset($all_discount) && !empty($all_discount))
                                <div class="table-responsive table-xy-overflow">
                                    <table  class="table table-striped table-bordered" id="">
                                        <thead>
                                        <th>SL</th>
                                        <th> Name</th>
                                        <th> Type</th>
                                        <th> Amount</th>
{{--                                        <th>Slug</th>--}}

                                        <th>Feature</th>
                                        <th>Sub Item</th>
                                        <th>Brand</th>
                                        <th>Category</th>
                                        <th>Image</th>
                                        <th>Action</th>
                                        </thead>

                                        <tbody>
                                        <?php $i=1; ?>
                                        @foreach($all_discount as $value)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$value->name}}</td>
                                                <td>{{$value->type}}</td>
                                                <td>
                                                    <?php
                                                        if ($value->type == 'Percentage'){
                                                            $amount =  round($value->amount);
                                                            $amount =  $amount.'%';
                                                        }else{
                                                            $amount = $value->amount;
                                                        }
                                                    ?>
                                                    {{$amount}}
                                                </td>
{{--                                                <td>{{$value->slug}}</td>--}}

                                                <td>
                                                    @if(isset($value->feature) && !empty($value->feature))
                                                        <i class="fas fa-check"></i> {{$value->feature}}
                                                    @endif
                                                 </td>
                                                <td style="text-align: left;">
                                                    @if(isset($value->subitem) && !empty($value->subitem))
                                                        <i class="fas fa-check"></i> Yes
                                                    @endif
                                                </td>

                                                <td>
                                                    @if($value->DiscountBrand)
                                                        @foreach($value->DiscountBrand as $discount)
                                                            {{$discount->ProductBrand->name}},

                                                        @endforeach
                                                    @endif
                                                </td>

                                                <td>
                                                    @if($value->DiscountCategory)
                                                        @foreach($value->DiscountCategory as $category)
                                                            {{$category->ProductCategory->name}},
                                                        @endforeach
                                                    @endif
                                                </td>

                                                <td>

                                                    <div class="tooltipme">Image
                                                        <span class="tooltipmetext">
                                                                <img class="hover_image" src="{{ asset('backend/image/ProDiscountImage').'/'.$value->discount_image;}}" alt="">

                                                        </span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <a href="{{route('admin.discount.edit',$value->id)}}" title="Edit Discount Information"><i class="fas fa-user-edit"></i></a> |
                                                    <a href="{{route('admin.discount.delete',$value->id)}}" title="Delete Discount Information" onclick="return confirm('Are you sure to Delete?')"><i class="fas fa-trash-alt"></i></a>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class=" justify-content-right">
                                    {{ $all_discount->links('backend.layouts.pagination') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>


@endsection
