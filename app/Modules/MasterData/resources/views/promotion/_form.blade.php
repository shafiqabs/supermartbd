<?php
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request as Input;

?>


    <div class="row">
        <div class="col-md-8">
            <div class="from-group">
                {!! Form::label('Promotion Name', 'Promotion Name', array('class' => 'form-label')) !!}
                <span style="color: red">*</span>
                <div class="input-group mb-3">
                    {!! Form::text('name',Input::old('name'),['id'=>'name','class' => 'form-control','data-checkify'=>'minlen=3,required','Placeholder' => 'Enter promotion Name','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                    <span style="color: #ff0000">{!! $errors->first('name') !!}</span>
                </div>
            </div>

            <div class="col-md-4">
            <div class="from-group">
            {!! Form::label('Is Feature', 'Is Feature', array('class' => 'form-label')) !!}
            <div class="form-check">
                @if(isset($data) && $data->feature !='')
                    {!! Form::checkbox('feature', 'yes',$data->feature,['id'=>'flexCheckDefault','class'=>'form-check-input']) !!}
                @else
                    {!! Form::checkbox('feature', 'yes',false,['id'=>'flexCheckDefault','class'=>'form-check-input']) !!}
                @endif
                {!! Form::label('Feature', 'Feature', array('class' => 'form-check-label','for'=>'flexCheckDefault')) !!}
            </div>
            </div>
            </div>

            <div class="col-md-4">
            <div class="from-group">
            {!! Form::label('Type', 'Type', array('class' => 'form-label')) !!}
            <div class="form-check">
                @if(isset($data) && $data->tag !='')
                    {!! Form::checkbox('tag', 'yes',$data->tag,['id'=>'flexCheckDefault','class'=>'form-check-input']) !!}
                @else
                    {!! Form::checkbox('tag', 'yes',false,['id'=>'flexCheckDefault','class'=>'form-check-input']) !!}
                @endif
                    {!! Form::label('Tag', 'Tag', array('class' => 'form-check-label','for'=>'flexCheckDefault')) !!}
           </div>

            <div class="form-check">
                @if(isset($data) && $data->promotion !='')
                    {!! Form::checkbox('promotion', 'yes',$data->promotion,['id'=>'flexCheckDefault','class'=>'form-check-input']) !!}
                @else
                    {!! Form::checkbox('promotion', 'yes',false,['id'=>'flexCheckDefault','class'=>'form-check-input']) !!}
                @endif
                {!! Form::label('Promotion', 'Promotion', array('class' => 'form-check-label','for'=>'flexCheckDefault')) !!}
            </div>
            </div>
            </div>
        </div>



        <div class="col-md-4">
            <div class="from-group">
                {!! Form::label('Promotion Image', 'Promotion Image', array('class' => 'form-label','for'=>'formFile')) !!}
                @if(isset($data))
                @else
                    <span style="color: red">*</span>
                @endif

                <div class="mb-3">
                    @if(isset($data))
                        <input class="form-control" accept="image/*" name="promotion_image" type="file" id="file" onchange="loadFile(event)">
                    @else
                        <input required class="form-control" accept="image/*" name="promotion_image" type="file" id="file" onchange="loadFile(event)">

                    @endif

                    <div style="text-align: center;padding-top: 5px;">
                        @if($errors->first('promotion_image'))
                            <span style="color: #ff0000">{!! $errors->first('promotion_image') !!}</span>
                        @else
                            <img id="promotion_image" width="150" height="120"/>
                        @endif

                        @if(isset($data) && $data->promotion_image !='')
                            <img id="promotion_image" src="{{ asset('backend/image/ProPromotionImage').'/'.$data->promotion_image;}}" width="150" height="120"/>
                        @endif

                    </div>
                </div>
            </div>


            <script>
                var loadFile = function(event) {
                    var image = document.getElementById('promotion_image');
                    image.src = URL.createObjectURL(event.target.files[0]);
                };
            </script>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 offset-md-8">
            <div class="from-group">
                <div class="from-group">
                    {!! Form::label('Status', 'Status', array('class' => 'form-label')) !!}
                    <span style="color: red">*</span>
                    <div class="input-group mb-3">
                        {!! Form::select('status',array(1=>'Active',0=>'Inactive'),Input::old('status'),['id'=>'ordering','class' => 'form-control','data-checkify'=>'minlen=1,required,number','Placeholder' => 'Enter ordering number','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('ordering') !!}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="row">

        <div class="col-md-12" style="text-align: right;">
            <div class="from-group">
                <div class="">
                    <button type="reset" class="btn submit-button">Reset</button>
                    <button type="submit" class="btn submit-button">Submit</button>
                </div>
            </div>
        </div>

    </div>
