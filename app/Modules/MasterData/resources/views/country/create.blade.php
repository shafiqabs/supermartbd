@extends(/** @lang text */'backend.layouts.master')

@section('body')
    <div class="dashboard-area">
        <div id="carbon-block" class="">
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">{{$ModuleTitle}}</h1>

                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group me-2">

{{--                            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>--}}
{{--                            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>--}}
                        </div>
{{--                        <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">--}}
{{--                            <span data-feather="calendar"></span>--}}
{{--                            This week--}}
{{--                        </button>--}}

                    </div>
                </div>
            </main>

        <div class="row">
            <div class="col-md-6">
                <div class="card">

                <div class="card-header">
                    {{$PageTitle}}
                </div>
                <div class="card-body">

                    @include('backend.layouts.message')

                    {!! Form::open(['route' => 'admin.country.store','enctype'=>'multipart/form-data',  'files'=> true, 'id'=>'basic-form', 'class' => '']) !!}

                    @include('MasterData::country._form')

                    {!! Form::close() !!}

                </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">

                <div class="card-header">
                    {{$TableTitle}}
                </div>
                <div class="card-body">


                    @if(isset($all_country) && !empty($all_country))
                        <div class="table-responsive">
                        <table  class="table table-striped table-bordered text-center" id="">
                        <thead>
                            <th>SL</th>
                            <th>Name</th>
                            <th>Slug</th>
                            <th>Code</th>
                            <th>Ordering</th>
                            <th>Action</th>
                        </thead>

                        <tbody>
                        <?php $i=1; ?>
                        @foreach($all_country as $value)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$value->name}}</td>
                                <td>{{$value->slug}}</td>
                                <td>{{$value->code}}</td>
                                <td>{{$value->ordering}}</td>
                                <td>
                                    <a href="{{route('admin.country.edit',$value->id)}}" title="Edit Country"><i class="fas fa-user-edit"></i></a> |
                                    <a href="{{route('admin.country.delete',$value->id)}}" title="Delete Country" onclick="return confirm('Are you sure to Delete?')"><i class="fas fa-trash-alt"></i></a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                        </div>
                        <div class=" justify-content-right">
                            {{ $all_country->links('backend.layouts.pagination') }}
                        </div>
                    @endif
                </div>
            </div>
            </div>
        </div>



        </div>
    </div>


@endsection
