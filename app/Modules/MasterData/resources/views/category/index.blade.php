<?php
use App\Modules\MasterData\Models\Category;
use App\Modules\MasterData\Models\ProductBrand;

?>
@extends(/** @lang text */'backend.layouts.master')

@section('body')

    <div class="dashboard-area">
        <div id="carbon-block" class="">
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">{{$ModuleTitle}}</h1>

                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group me-2">
                            <a style="color: #000;" href="{{route('admin.category.create')}}" title="Add Size Unit" class="module_button_header">
                                <button type="button" class="btn btn-sm btn-outline-secondary">
                                    <i class="fas fa-plus-circle"></i> Create New
                                </button>
                            </a>
                        </div>

                    </div>
                </div>
            </main>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">
                            {{$PageTitle}}
                        </div>
                        <div class="card-body">

                            @include('backend.layouts.message')

                            @if(isset($all_category) && !empty($all_category))
                                <div class="table-responsive">
                                    <table  class="table table-striped table-bordered text-center" id="">
                                        <thead>
                                        <th>SL</th>
                                        <th> Name</th>
                                        <th> Parent</th>
                                        <th>Slug</th>
                                        <th>Feature</th>

                                        <th style="width: 158.047px;">Image</th>
                                        <th>Action</th>
                                        </thead>

                                        <tbody>
                                        <?php $i=1; ?>
                                        @foreach($all_category as $values)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$values->name}}</td>

                                                <td>
                                                        {{$values->parentName?$values->parentName->name:''}}

                                                </td>
                                                <td>{{$values->slug}}</td>
                                                <td>
                                                    @if(isset($values->feature) && !empty($values->feature))
                                                    <i class="fas fa-check"></i> {{$values->feature}}
                                                    @endif
                                                </td>

                                                <td>
                                                    <div class="tooltipme">image
                                                        <span class="tooltipmetext">
                                                                <img class="hover_image" src="{{ asset('backend/image/ProCategoryImage').'/'.$values->category_image}}" alt="">

                                                        </span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <a href="{{route('admin.category.edit',$values->id)}}" title="Edit Category Information"><i class="fas fa-user-edit"></i></a> |
                                                    <a href="{{route('admin.category.delete',$values->id)}}" title="Delete Category Information" onclick="return confirm('Are you sure to Delete?')"><i class="fas fa-trash-alt"></i></a>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class=" justify-content-right">
                                    {{ $all_category->links('backend.layouts.pagination') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>


@endsection
