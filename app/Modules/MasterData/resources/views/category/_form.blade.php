<?php
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request as Input;

?>


    <div class="row">
        <div class="col-md-8">
            <div class="from-group">
                {!! Form::label('Parent Category', 'Parent Category', array('class' => 'form-label')) !!}

                <div class="input-group mb-3">
                    {!! Form::select('category_parent_id',$ParentId,Input::old('category_parent_id'),['id'=>'category_parent_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                </div>
            </div>

            <div class="from-group">
                {!! Form::label('Category Name', 'Category Name', array('class' => 'form-label')) !!}
                <span style="color: red">*</span>
                <div class="input-group mb-3">
                    {!! Form::text('name',Input::old('name'),['id'=>'name','class' => 'form-control','data-checkify'=>'minlen=3,required','Placeholder' => 'Enter category Name','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                    <span style="color: #ff0000">{!! $errors->first('name') !!}</span>
                </div>
            </div>

            <div class="from-group">
                {!! Form::label('Content', 'Content', array('class' => 'form-label')) !!}
                <div class="input-group mb-3">
                    {!! Form::textarea('content',Input::old('content'),['id'=>'content','class' => 'form-control','Placeholder' => 'Enter Category Content','aria-label' =>'content','aria-describedby'=>'basic-addon2','rows'=>1]) !!}
                    <span style="color: #ff0000">{!! $errors->first('content') !!}</span>
                </div>
            </div>

            <div class="from-group">
                {!! Form::label('Banner Header', 'Banner Header', array('class' => 'form-label')) !!}
                <span style="color: red">*</span>
                <div class="input-group mb-3">
                    {!! Form::text('banner_header',Input::old('banner_header'),['id'=>'banner_header','class' => 'form-control','data-checkify'=>'maxlen=20,required']) !!}
                    <br><span style="color: #ff0000">{!! $errors->first('banner_header') !!}</span>
                </div>
            </div>

            <div class="from-group">
                {!! Form::label('Banner Title', 'Banner Title', array('class' => 'form-label')) !!}
                <span style="color: red">*</span>
                <div class="input-group mb-3">
                    {!! Form::text('banner_title',Input::old('banner_title'),['id'=>'banner_title','class' => 'form-control','data-checkify'=>'maxlen=25,required']) !!}
                    <br><span style="color: #ff0000">{!! $errors->first('banner_title') !!}</span>
                </div>
            </div>

            <div class="from-group">
                {!! Form::label('Banner Sub Title', 'Banner Sub Title', array('class' => 'form-label')) !!}
                <span style="color: red">*</span>
                <div class="input-group mb-3">
                    {!! Form::text('banner_sub_title',Input::old('banner_sub_title'),['id'=>'banner_sub_title','class' => 'form-control','data-checkify'=>'maxlen=16,required']) !!}
                    <br><span style="color: #ff0000">{!! $errors->first('banner_sub_title') !!}</span>
                </div>
            </div>

            <div class="row">
            <div class="col-md-4">
            <div class="from-group">
            {!! Form::label('Is Feature', 'Is Feature', array('class' => 'form-label')) !!}
            <div class="form-check">
                @if(isset($data) && $data->feature !='')
                    {!! Form::checkbox('feature', 'yes',$data->feature,['id'=>'flexCheckDefault','class'=>'form-check-input']) !!}
                @else
                    {!! Form::checkbox('feature', 'yes',false,['id'=>'flexCheckDefault','class'=>'form-check-input']) !!}
                @endif
                {!! Form::label('Feature', 'Feature', array('class' => 'form-check-label','for'=>'flexCheckDefault')) !!}
            </div>
            </div>
            </div>

            <div class="col-md-4 offset-4">
                <div class="from-group">
                    <div class="from-group">
                        {!! Form::label('Status', 'Status', array('class' => 'form-label')) !!}
                        <span style="color: red">*</span>
                        <div class="input-group mb-3">
                            {!! Form::select('status',array(1=>'Active',0=>'Inactive'),Input::old('status'),['id'=>'ordering','class' => 'form-control','data-checkify'=>'minlen=1,required,number','Placeholder' => 'Enter ordering number','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                            <span style="color: #ff0000">{!! $errors->first('ordering') !!}</span>
                        </div>
                    </div>
                </div>
            </div>
            </div>

        </div>



        <div class="col-md-4">
            <div class="from-group">
                {!! Form::label('Category Image', 'Category Image', array('class' => 'form-label','for'=>'formFile')) !!}
                @if(isset($data))
                @else
                    <span style="color: red">*</span>
                @endif

                <div class="mb-3">
                    @if(isset($data))
                        <input class="form-control" accept="image/*" name="category_image" type="file" id="file" onchange="loadFile(event)">
                    @else
                        <input required class="form-control" accept="image/*" name="category_image" type="file" id="file" onchange="loadFile(event)">

                    @endif

                    <div style="text-align: center;padding-top: 5px;">
                        @if($errors->first('category_image'))
                            <span style="color: #ff0000">{!! $errors->first('category_image') !!}</span>
                        @else
                            <img id="category_image" width="150" height="120"/>
                        @endif

                        @if(isset($data) && $data->category_image !='')
                            <img id="category_image" src="{{ asset('backend/image/ProCategoryImage').'/'.$data->category_image;}}" width="150" height="120"/>
                        @endif

                    </div>
                </div>
            </div>


            <script>
                var loadFile = function(event) {
                    var image = document.getElementById('category_image');
                    image.src = URL.createObjectURL(event.target.files[0]);
                };
            </script>
        </div>
    </div>




    <div class="row">

        <div class="col-md-12" style="text-align: right;">
            <div class="from-group">
                <div class="">
                    <button type="reset" class="btn submit-button">Reset</button>
                    <button type="submit" class="btn submit-button">Submit</button>
                </div>
            </div>
        </div>

    </div>
