<?php

namespace App\Modules\MasterData\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App;
use Auth;

class Category extends Model
{
    use HasFactory;

    protected $table = 'ems_category';
    protected $fillable = [
        'category_parent_id',
        'name',
        'slug',
        'content',
        'feature',
        'category_image',
        'status',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'banner_header',
        'banner_title',
        'banner_sub_title',
    ];


    public function parentName()
    {
        return $this->belongsTo('App\Modules\MasterData\Models\Category', 'category_parent_id', 'id');
    }

    public function CategoryProduct()
    {
        return $this->hasMany('App\Modules\Product\Models\Product', 'category_id', 'id');
    }

    // TODO :: boot
    // boot() function used to insert logged user_id at 'created_by' & 'updated_by'
    public static function boot()
    {
        parent::boot();
        static::creating(function ($query) {
            if (Auth::check()) {
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function ($query) {
            if (Auth::check()) {
                $query->updated_by = Auth::user()->id;
            }
        });
    }


//    get category getHierarchyCategory

    public static function getHierarchyCategory($except = '')
    {
        // Initialize data
        $response = [];
//        $response[''] = 'Select Category';

        // Find parent category
        $data = self::where('category_parent_id', NULL)
            ->select('*')
            ->get();

        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                if ($except != $value->id) {
                    $response[$value->id] = $value->name;

                    $dot = '   -- ';
                    // Find child category
                    $response = self::get_child_category($value->id, $response, $dot, $except);
                }
            }
        }

        // Return response
        return $response;
    }


    public static function get_child_category($parent, $response, $dot, $except)
    {

        if ($dot == '') {
            $dot = '--';
        }

        // Find child category
        $child_category = self::where('category_parent_id', $parent)
            ->select('*')
            ->get();

        if (!empty($child_category)) {
            foreach ($child_category as $key => $value) {

                if ($except != $value->id) {
                    if (isset($response[$value->id])) {
                        $response[$value->id] .= ', ' . $dot . $value->name;
                    } else {
                        $response[$value->id] = $dot . $value->name;
                    }

                    $dot1 = $dot . ' -- ';
                    // Find recursive child category
                    $response = self::get_child_category($value->id, $response, $dot1, $except);
                }
            }
        }

        // Return response
        return $response;
    }


    public static function getWebMenu(){

        $response = [];

        $data = self::where('category_parent_id',NULL);

        $data = $data->where('status','1');
        $data = $data->select('*')
            ->OrderBy('name')
//            ->limit(12)
            ->get()
            ->toArray();


        if(count($data) > 0){
            foreach ($data as $key => $value){

                $response[$key]['id'] = $value['id'];
                $response[$key]['name'] = $value['name'];
                $response[$key]['slug'] = $value['slug'];
                $response[$key]['image_link'] = $value['category_image'];

                $sub_menu_data = self::get_sub_category($value['id'], $response);

                if(count($sub_menu_data) > 0)
                {
                    $response[$key]['sub_menu'] = $sub_menu_data;
                }
            }
        }

        return $response;

    }


    public static function get_sub_category($parent_id, $response)
    {
        $response = [];

        $sub_category = self::where('category_parent_id',$parent_id);

        $sub_category = $sub_category->where('status',1);

        $sub_category = $sub_category->select('*')
            ->orderBy('name','asc')
            ->get()->toArray();

        if(!empty($sub_category)){
            foreach ($sub_category as $key => $value) {

                $response[$key]['id'] = $value['id'];
                $response[$key]['name'] = $value['name'];
                $response[$key]['slug'] = $value['slug'];

                $sub_menu_data = self::get_sub_category($value['id'], $response);

                if(count($sub_menu_data) > 0)
                {
                    $response[$key]['sub_menu'] = $sub_menu_data;
                }

            }
        }

        return $response;
    }

    public static function AllCategory(){
        $data = self::where('status','1');
        $data = $data->select('*')
            ->OrderBy('name','asc')
            ->inRandomOrder()
            ->limit(15)
            ->get();
        return $data;
    }

}
