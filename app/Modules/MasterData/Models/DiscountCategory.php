<?php

namespace App\Modules\MasterData\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App;
use Auth;

class DiscountCategory extends Model
{
    use HasFactory;

    protected $table = 'ems_discount_category';
    protected $fillable = [
        'discount_id', 'category_id','created_at','updated_at'
    ];

    public function ProductCategory(){
        return $this->belongsTo('App\Modules\MasterData\Models\Category','category_id','id');
    }

}
