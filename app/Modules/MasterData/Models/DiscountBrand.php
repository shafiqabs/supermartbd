<?php

namespace App\Modules\MasterData\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App;
use Auth;

class DiscountBrand extends Model
{
    use HasFactory;

    protected $table = 'ems_discount_brand';
    protected $fillable = [
        'discount_id', 'brand_id','created_at','updated_at'
    ];

    public function ProductBrand(){
        return $this->belongsTo('App\Modules\MasterData\Models\ProductBrand','brand_id','id');
    }
}
