<?php

namespace App\Modules\MasterData\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App;
use Auth;

class Discount extends Model
{
    use HasFactory;

    protected $table = 'ems_discount';
    protected $fillable = [
        'name',
        'slug',
        'feature',
        'subitem',
        'amount',
        'type',
        'discount_image',
        'status',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

//   Get Discount brand
    public function DiscountBrand(){
        return $this->hasMany('App\Modules\MasterData\Models\DiscountBrand','discount_id','id');
    }

    //    Get Discount category
    public function DiscountCategory(){
        return $this->hasMany('App\Modules\MasterData\Models\DiscountCategory','discount_id','id');
    }

    // TODO :: boot
    // boot() function used to insert logged user_id at 'created_by' & 'updated_by'
    public static function boot(){
        parent::boot();
        static::creating(function($query){
            if(Auth::check()){
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function($query){
            if(Auth::check()){
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}
