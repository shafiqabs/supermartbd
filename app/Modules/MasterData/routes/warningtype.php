<?php

Route::get('admin-warningtype-create', [
    'as' => 'admin.warningtype.create',
    'uses' => 'WarningtypeController@create'
]);

Route::post('admin-warningtype-store',[
    'as' => 'admin.warningtype.store',
    'uses' => 'WarningtypeController@store'
]);

Route::get('admin-warningtype-edit/{id}',[
    'as' => 'admin.warningtype.edit',
    'uses' => 'WarningtypeController@edit'
]);

Route::PATCH('admin-warningtype-update/{id}',[
    'as' => 'admin.warningtype.update',
    'uses' => 'WarningtypeController@update'
]);

Route::get('admin-warningtype-delete/{id}',[
    'as' => 'admin.warningtype.delete',
    'uses' => 'WarningtypeController@delete'
]);


