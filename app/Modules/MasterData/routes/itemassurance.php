<?php

Route::get('admin-itemassurance-create', [
    'as' => 'admin.itemassurance.create',
    'uses' => 'ItemAssuranceController@create'
]);

Route::post('admin-itemassurance-store',[
    'as' => 'admin.itemassurance.store',
    'uses' => 'ItemAssuranceController@store'
]);

Route::get('admin-itemassurance-edit/{id}',[
    'as' => 'admin.itemassurance.edit',
    'uses' => 'ItemAssuranceController@edit'
]);

Route::PATCH('admin-itemassurance-update/{id}',[
    'as' => 'admin.itemassurance.update',
    'uses' => 'ItemAssuranceController@update'
]);

Route::get('admin-itemassurance-delete/{id}',[
    'as' => 'admin.itemassurance.delete',
    'uses' => 'ItemAssuranceController@delete'
]);


