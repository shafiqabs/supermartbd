<?php

Route::get('admin-promotion-create', [
    'as' => 'admin.promotion.create',
    'uses' => 'PromotionController@create'
]);

Route::get('admin-promotion-index', [
    'as' => 'admin.promotion.index',
    'uses' => 'PromotionController@index'
]);

Route::post('admin-promotion-store',[
    'as' => 'admin.promotion.store',
    'uses' => 'PromotionController@store'
]);

Route::get('admin-promotion-edit/{id}',[
    'as' => 'admin.promotion.edit',
    'uses' => 'PromotionController@edit'
]);

Route::PATCH('admin-promotion-update/{id}',[
    'as' => 'admin.promotion.update',
    'uses' => 'PromotionController@update'
]);

Route::get('admin-promotion-delete/{id}',[
    'as' => 'admin.promotion.delete',
    'uses' => 'PromotionController@delete'
]);


