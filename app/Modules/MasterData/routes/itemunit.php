<?php

Route::get('admin-itemunit-create', [
    'as' => 'admin.itemunit.create',
    'uses' => 'ItemunitController@create'
]);

Route::post('admin-itemunit-store',[
    'as' => 'admin.itemunit.store',
    'uses' => 'ItemunitController@store'
]);

Route::get('admin-itemunit-edit/{id}',[
    'as' => 'admin.itemunit.edit',
    'uses' => 'ItemunitController@edit'
]);

Route::PATCH('admin-itemunit-update/{id}',[
    'as' => 'admin.itemunit.update',
    'uses' => 'ItemunitController@update'
]);

Route::get('admin-itemunit-delete/{id}',[
    'as' => 'admin.itemunit.delete',
    'uses' => 'ItemunitController@delete'
]);


