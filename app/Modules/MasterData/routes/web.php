<?php

use Illuminate\Support\Facades\Route;

//Route::get('master-data', 'MasterDataController@welcome');
Route::group(['module' => 'MasterData', 'middleware' => ['web','auth','adminmiddleware']], function() {
    include 'itemunit.php';
    include 'sizeunit.php';
    include 'sizeweightdimension.php';
    include 'brand.php';
    include 'promotion.php';
    include 'country.php';
    include 'category.php';
    include 'warningtype.php';
    include 'itemassurance.php';
    include 'discount.php';
    include 'color.php';
});
