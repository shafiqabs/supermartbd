<?php

Route::get('admin-sizeunit-create', [
    'as' => 'admin.sizeunit.create',
    'uses' => 'SizeunitController@create'
]);

Route::post('admin-sizeunit-store',[
    'as' => 'admin.sizeunit.store',
    'uses' => 'SizeunitController@store'
]);

Route::get('admin-sizeunit-edit/{id}',[
    'as' => 'admin.sizeunit.edit',
    'uses' => 'SizeunitController@edit'
]);

Route::PATCH('admin-sizeunit-update/{id}',[
    'as' => 'admin.sizeunit.update',
    'uses' => 'SizeunitController@update'
]);

Route::get('admin-sizeunit-delete/{id}',[
    'as' => 'admin.sizeunit.delete',
    'uses' => 'SizeunitController@delete'
]);


