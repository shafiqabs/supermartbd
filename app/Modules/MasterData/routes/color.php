<?php

Route::get('admin-color-create', [
    'as' => 'admin.color.create',
    'uses' => 'ColorController@create'
]);

Route::get('admin-color-index', [
    'as' => 'admin.color.index',
    'uses' => 'ColorController@index'
]);

Route::post('admin-color-store',[
    'as' => 'admin.color.store',
    'uses' => 'ColorController@store'
]);

Route::get('admin-color-edit/{id}',[
    'as' => 'admin.color.edit',
    'uses' => 'ColorController@edit'
]);

Route::PATCH('admin-color-update/{id}',[
    'as' => 'admin.color.update',
    'uses' => 'ColorController@update'
]);

Route::get('admin-color-delete/{id}',[
    'as' => 'admin.color.delete',
    'uses' => 'ColorController@delete'
]);


