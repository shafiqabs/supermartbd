<?php

Route::get('admin-brand-create', [
    'as' => 'admin.brand.create',
    'uses' => 'BrandController@create'
]);

Route::get('admin-brand-index', [
    'as' => 'admin.brand.index',
    'uses' => 'BrandController@index'
]);

Route::post('admin-brand-store',[
    'as' => 'admin.brand.store',
    'uses' => 'BrandController@store'
]);

Route::get('admin-brand-edit/{id}',[
    'as' => 'admin.brand.edit',
    'uses' => 'BrandController@edit'
]);

Route::PATCH('admin-brand-update/{id}',[
    'as' => 'admin.brand.update',
    'uses' => 'BrandController@update'
]);

Route::get('admin-brand-delete/{id}',[
    'as' => 'admin.brand.delete',
    'uses' => 'BrandController@delete'
]);


