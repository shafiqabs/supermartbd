<?php

Route::get('admin-discount-create', [
    'as' => 'admin.discount.create',
    'uses' => 'DiscountController@create'
]);

Route::get('admin-discount-index', [
    'as' => 'admin.discount.index',
    'uses' => 'DiscountController@index'
]);

Route::post('admin-discount-store',[
    'as' => 'admin.discount.store',
    'uses' => 'DiscountController@store'
]);

Route::get('admin-discount-edit/{id}',[
    'as' => 'admin.discount.edit',
    'uses' => 'DiscountController@edit'
]);

Route::PATCH('admin-discount-update/{id}',[
    'as' => 'admin.discount.update',
    'uses' => 'DiscountController@update'
]);

Route::get('admin-discount-delete/{id}',[
    'as' => 'admin.discount.delete',
    'uses' => 'DiscountController@delete'
]);


