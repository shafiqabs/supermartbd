<?php

Route::get('admin-sizeweightdimension-create', [
    'as' => 'admin.sizeweightdimension.create',
    'uses' => 'SizeWeightDimensionController@create'
]);

Route::post('admin-sizeweightdimension-store',[
    'as' => 'admin.sizeweightdimension.store',
    'uses' => 'SizeWeightDimensionController@store'
]);

Route::get('admin-sizeweightdimension-edit/{id}',[
    'as' => 'admin.sizeweightdimension.edit',
    'uses' => 'SizeWeightDimensionController@edit'
]);

Route::PATCH('admin-sizeweightdimension-update/{id}',[
    'as' => 'admin.sizeweightdimension.update',
    'uses' => 'SizeWeightDimensionController@update'
]);

Route::get('admin-sizeweightdimension-delete/{id}',[
    'as' => 'admin.sizeweightdimension.delete',
    'uses' => 'SizeWeightDimensionController@delete'
]);


