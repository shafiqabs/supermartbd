<?php

Route::get('admin-country-create', [
    'as' => 'admin.country.create',
    'uses' => 'CountryController@create'
]);

Route::get('admin-country-index', [
    'as' => 'admin.country.index',
    'uses' => 'CountryController@index'
]);

Route::post('admin-country-store',[
    'as' => 'admin.country.store',
    'uses' => 'CountryController@store'
]);

Route::get('admin-country-edit/{id}',[
    'as' => 'admin.country.edit',
    'uses' => 'CountryController@edit'
]);

Route::PATCH('admin-country-update/{id}',[
    'as' => 'admin.country.update',
    'uses' => 'CountryController@update'
]);

Route::get('admin-country-delete/{id}',[
    'as' => 'admin.country.delete',
    'uses' => 'CountryController@delete'
]);


