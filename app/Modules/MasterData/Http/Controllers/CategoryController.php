<?php

namespace App\Modules\MasterData\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Modules\MasterData\Requests;
use Illuminate\Support\Facades\Input;


use App\Modules\MasterData\Models\Category;
use App\Modules\MasterData\Models\DiscountCategory;




use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class CategoryController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */

    public function create(){
        $ModuleTitle = "Manage Category Information";
        $PageTitle = "Add Category Information";
        $TableTitle = "Category Information list";

        $ParentId = Category::where('status',1)
                                ->select('id', DB::raw("concat(name,' (',id,')') as name"))
                                #->where('parent_id',null)
                                ->pluck('name','id')
                                ->all();

        $ParentId[''] = 'Select Parent Category';
        ksort($ParentId);

        return view("MasterData::category.create", compact('ModuleTitle','PageTitle','TableTitle','ParentId'));

    }

    public function store(Requests\CategoryRequest $request){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = Category::where('slug',$slug)->count();

        if ($SlugExistsOrNot ==0) {
            if (!isset($input['feature'])){
                $input['feature'] = null;
            }

            if (isset($input['category_parent_id']) && $input['category_parent_id'] == '') {
                $input['category_parent_id'] = null;
            }

            $input['slug'] = $slug;

            if ($request->file('category_image') != '') {
                $avatar = $request->file('category_image');
                $file_title = $slug.'.'.$avatar->getClientOriginalExtension();
                $input['category_image'] = $file_title;
                $path = public_path("backend/image/ProCategoryImage/");
                $target_file =  $path.basename($file_title);
                $file_path = $_FILES['category_image']['tmp_name'];
                $result = move_uploaded_file($file_path,$target_file);
            }

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // Store sizeunit data
                if ($category_data = Category::create($input)) {
                    $category_data->save();
                }

                DB::commit();
                Session::flash('message', 'Information added Successfully!');
//                return redirect()->back();
                return redirect()->route('admin.category.index');
            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function index(){
        $ModuleTitle = "Manage Category Information";
        $PageTitle = " Category Information list";
        $TableTitle = "Category Information list";

        $all_category = Category::where('status',1)->orderby('id','desc')->paginate(10);

        return view("MasterData::category.index", compact('ModuleTitle','PageTitle','TableTitle','all_category'));

    }

    public function edit($id){
        $ModuleTitle = "Manage Category Information";
        $PageTitle = "Update Category Information";
        $TableTitle = "Category Information list";

        $ParentId = Category::where('status',1)
            ->select('id', DB::raw("concat(name,' (',id,')') as name"))
            #->where('parent_id',null)
            ->pluck('name','id')
            ->all();

        $ParentId[''] = 'Select Parent Category';
        ksort($ParentId);

        $data = Category::where('status','1')->where('id',$id)->first();

        return view("MasterData::category.edit", compact('data','ModuleTitle','PageTitle','TableTitle','ParentId'));

    }

    public function update(Requests\UpdateCategoryRequest $request,$id){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = Category::where('slug',$slug)->count();
        $SlugToId = Category::where('slug',$slug)->first();
        $CategoryModel = Category::where('id',$id)->first();

        if ($SlugExistsOrNot == 0 || ($SlugExistsOrNot == 1 && $id == $SlugToId->id)){

            if (!isset($input['feature'])){
                $input['feature'] = null;
            }

            if (isset($input['category_parent_id']) && $input['category_parent_id'] == '') {
                $input['category_parent_id'] = null;
            }

            $input['slug'] = $slug;

            if ($request->file('category_image') != '') {
                File::delete(public_path().'/backend/image/ProCategoryImage/'.$CategoryModel->category_image);
                $avatar = $request->file('category_image');
                $file_title = $slug.'.'.$avatar->getClientOriginalExtension();
                $input['category_image'] = $file_title;
                $path = public_path("backend/image/ProCategoryImage/");
                $target_file =  $path.basename($file_title);
                $file_path = $_FILES['category_image']['tmp_name'];
                $result = move_uploaded_file($file_path,$target_file);
            }else{
                $input['category_image'] = $CategoryModel['category_image'];
            }

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // update sizeunit data
                $result = $CategoryModel->update($input);
                $CategoryModel->save();

                DB::commit();

                Session::flash('message', 'Information Updated Successfully!');

//                if ($input['status'] == 1){
//                    return redirect()->route('admin.category.edit', ['id' => $id]);
//                }else{
//                    return redirect()->route('admin.category.create');
//                }
                return redirect()->route('admin.category.index');

            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function delete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {
            $CategoryModel = Category::where('id', $id)
                ->select('*')
                ->first();
            $ParentIdExistsOrNot = Category::where('category_parent_id',$id)->count();
            if ($ParentIdExistsOrNot != 0){
                Session::flash('validate', 'Category is assigned as parent category & can not delete ');
                return redirect('admin-category-index');
            }else{
                $CategoryBrandExists = DiscountCategory::where('category_id',$id)->count();
                if ($CategoryBrandExists == 0 ) {
                    $ProductBrand = App\Modules\Product\Models\Product::where('category_id',$id)->count();
                    if ($ProductBrand == 0) {
                        File::delete(public_path() . '/backend/image/ProCategoryImage/' . $CategoryModel->category_image);
                        $CategoryModel->delete();
                        Session::flash('delete', 'Delete Successfully !');
                    }else{
                        Session::flash('validate', 'Already use in Product !');
                        return redirect('admin-category-index');
                    }
                }else{
                    Session::flash('validate', 'Already use in discount !');
                }
            }

            DB::commit();

//            return Redirect::back()->withErrors(['message', 'Delete Successfully !']);
            return redirect('admin-category-index');
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }
}
