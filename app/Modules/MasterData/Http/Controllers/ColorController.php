<?php

namespace App\Modules\MasterData\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Modules\MasterData\Requests;
use Illuminate\Support\Facades\Input;


use App\Modules\MasterData\Models\Color;



use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class ColorController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */

    public function create(){
        $ModuleTitle = "Manage Color Information";
        $PageTitle = "Add Color Information";
        $TableTitle = "Color Information list";

        $all_color = Color::where('status','1')->orderby('id','desc')->paginate(10);

        return view("MasterData::color.create", compact('ModuleTitle','PageTitle','TableTitle','all_color'));

    }

    public function store(Requests\ColorRequest $request){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = Color::where('slug',$slug)->count();

        if ($SlugExistsOrNot ==0) {
            $input['slug'] = $slug;

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // Store sizeunit data
                if ($country_data = Color::create($input)) {
                    $country_data->save();

                }

                DB::commit();
                Session::flash('message', 'Information added Successfully!');
                return redirect()->back();

            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function edit($id){
        $ModuleTitle = "Manage Color Information";
        $PageTitle = "Update Color Information";
        $TableTitle = "Color Information list";

        $data = Color::where('status','1')->where('id',$id)->first();
        $all_color = Color::where('status','1')->orderby('id','desc')->paginate(10);

        return view("MasterData::color.edit", compact('data','ModuleTitle','PageTitle','TableTitle','all_color'));

    }

    public function update(Requests\ColorRequest $request,$id){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = Color::where('slug',$slug)->count();
        $SlugToId = Color::where('slug',$slug)->first();
        $ColorModel = Color::where('id',$id)->first();
        if ($SlugExistsOrNot == 0 || ($SlugExistsOrNot == 1 && $id == $SlugToId->id)){
            $input['slug'] = $slug;
            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // update sizeunit data
                $result = $ColorModel->update($input);
                $ColorModel->save();

                DB::commit();

                Session::flash('message', 'Information Updated Successfully!');

                if ($input['status'] == 1){
                    return redirect()->route('admin.color.edit', ['id' => $id]);
                }else{
                    return redirect()->route('admin.color.create');
                }



            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function delete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {
            $ProductBrand = App\Modules\Product\Models\ProductColor::where('color_id',$id)->count();
            if ($ProductBrand == 0) {
                $ColorModel = Color::where('id', $id)
                    ->select('*')
                    ->first();
                $ColorModel->delete();

                DB::commit();
                Session::flash('delete', 'Delete Successfully !');
    //            return Redirect::back()->withErrors(['message', 'Delete Successfully !']);
                return redirect('admin-color-create');
            }else{
                Session::flash('validate', 'Already use in Product !');
                return redirect('admin-color-create');
            }
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }
}
