<?php

namespace App\Modules\MasterData\Http\Controllers;

use App\Http\Controllers\Controller;


use Illuminate\Http\Request;

use App\Modules\MasterData\Requests;
use Illuminate\Support\Facades\Input;


use App\Modules\MasterData\Models\ProductPromotion;
use App\Modules\MasterData\Models\Discount;
use App\Modules\MasterData\Models\ProductBrand;
use App\Modules\MasterData\Models\Category;
use App\Modules\MasterData\Models\DiscountBrand;
use App\Modules\MasterData\Models\DiscountCategory;



use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class DiscountController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */


    public function create(){
        $ModuleTitle = "Manage Discount Information";
        $PageTitle = "Add Discount Information";
        $TableTitle = "Discount Information list";

        $Brand = ProductBrand::where('status',1)
                                ->orderby('name','asc')
                                ->select('id', DB::raw("concat(name,' (',id,') ' ) as name"))
                                ->pluck('name','id')
                                ->all();
//        $Brand[''] = 'Select Brand';
//        asort($Brand);

        $Category = Category::getHierarchyCategory();
//        echo '<pre>';
//        print_r($Category);
//        exit();

        return view("MasterData::discount.create", compact('ModuleTitle','PageTitle','TableTitle','Brand','Category'));

    }

    public function store(Requests\DiscountRequest $request){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = Discount::where('slug',$slug)->count();
        if ($SlugExistsOrNot ==0) {
            $input['slug'] = $slug;

            if ($request->file('discount_image') != '') {
                $avatar = $request->file('discount_image');
                $slug = str_replace("%", "", $slug);
                $file_title = $slug.'.'.$avatar->getClientOriginalExtension();
                $input['discount_image'] = $file_title;
                $path = public_path("backend/image/ProDiscountImage/");
                $target_file =  $path.basename($file_title);
                $file_path = $_FILES['discount_image']['tmp_name'];
                $result = move_uploaded_file($file_path,$target_file);
            }

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // Store promotion data
                if ($Discount_data = Discount::create($input)) {
                    $Discount_data->save();

                    if (isset($input['brand_id']) && !empty($input['brand_id'])) {
                        foreach ($input['brand_id'] as $value) {
                            $model_brand = new DiscountBrand();
                            $model_brand->discount_id = $Discount_data->id;
                            $model_brand->brand_id = $value;
                            $model_brand->save();
                        }
                    }
                    if (isset($input['category_id']) && !empty($input['category_id'])) {
                        foreach ($input['category_id'] as $value) {
                            $model_category = new DiscountCategory();
                            $model_category->discount_id = $Discount_data->id;
                            $model_category->category_id = $value;
                            $model_category->save();
                        }
                    }
                }

                DB::commit();
                Session::flash('message', 'Information added Successfully!');
//                return redirect()->back();
                return redirect()->route('admin.discount.index');
            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }

    }

    public function index(){
        $ModuleTitle = "Manage Discount Information";
        $PageTitle = " Discount Information list";
        $TableTitle = "Discount Information list";

        $all_discount = Discount::where('status',1)->orderby('id','desc')->paginate(10);

        return view("MasterData::discount.index", compact('ModuleTitle','PageTitle','TableTitle','all_discount'));

    }

    public function edit($id){
        $ModuleTitle = "Manage Discount Information";
        $PageTitle = "Update Discount Information";

        $data = Discount::where('status',1)->where('id',$id)->first();

        $DiscountBrand = DiscountBrand::join('ems_product_brand', 'ems_discount_brand.brand_id', '=', 'ems_product_brand.id')
                                        ->where('ems_discount_brand.discount_id',$id)
                                        ->pluck('ems_product_brand.id', 'ems_product_brand.name')
                                        ->all();

        $Brand = ProductBrand::where('status',1)
                                ->orderby('name','asc')
                                ->select('id', DB::raw("concat(name,' (',id,') ' ) as name"))
                                ->pluck('name','id')
                                ->all();

        $Category = Category::getHierarchyCategory();
        $DiscountCategory = DiscountCategory::join('ems_category', 'ems_discount_category.category_id', '=', 'ems_category.id')
                                                ->where('ems_discount_category.discount_id',$id)
                                                ->pluck('ems_category.id', 'ems_category.name')
                                                ->all();

        return view("MasterData::discount.edit", compact('data','ModuleTitle','PageTitle','Brand','Category','DiscountBrand','DiscountCategory'));

    }

    public function update(Requests\DiscountUpdateRequest $request,$id){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = Discount::where('slug',$slug)->count();
        $SlugToId = Discount::where('slug',$slug)->first();
        $DiscountUpdateModel = Discount::where('id',$id)->first();

        if ($SlugExistsOrNot == 0 || ($SlugExistsOrNot == 1 && $id == $SlugToId->id)){
            $input['slug'] = $slug;

            if ($request->file('discount_image') != '') {
                File::delete(public_path().'/backend/image/ProDiscountImage/'.$DiscountUpdateModel->discount_image);
                $avatar = $request->file('discount_image');
                $slug = str_replace("%", "", $slug);
                $file_title = $slug.'.'.$avatar->getClientOriginalExtension();
                $input['discount_image'] = $file_title;
                $path = public_path("backend/image/ProDiscountImage/");
                $target_file =  $path.basename($file_title);
                $file_path = $_FILES['discount_image']['tmp_name'];
                $result = move_uploaded_file($file_path,$target_file);
            }else{
                $input['discount_image'] = $DiscountUpdateModel['discount_image'];
            }

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // update brand data
                $result = $DiscountUpdateModel->update($input);
                $DiscountUpdateModel->save();

                $Model_brand_delete = DiscountBrand::where('discount_id',$id)->delete();

                if (isset($input['brand_id']) && !empty($input['brand_id'])) {
                    foreach ($input['brand_id'] as $value) {
                        $model_brand = new DiscountBrand();
                        $model_brand->discount_id = $id;
                        $model_brand->brand_id = $value;
                        $model_brand->save();
                    }
                }

                $Model_category_delete = DiscountCategory::where('discount_id',$id)->delete();

                if (isset($input['category_id']) && !empty($input['category_id'])){
                    foreach ($input['category_id']  as $value ) {
                        $model_category = new DiscountCategory();
                        $model_category->discount_id = $id;
                        $model_category->category_id = $value;
                        $model_category->save();
                    }
                }


                DB::commit();

                Session::flash('message', 'Information Updated Successfully!');
                return redirect()->route('admin.discount.index');

            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }

        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function delete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {
            $ProductBrand = App\Modules\Product\Models\Product::where('discount_id',$id)->count();
            if ($ProductBrand == 0) {
            $DiscountDeleteModel = Discount::where('id', $id)
                ->select('*')
                ->first();
            File::delete(public_path().'/backend/image/ProDiscountImage/'.$DiscountDeleteModel->discount_image);
            $DiscountDeleteModel->delete();

            DB::commit();
            Session::flash('delete', 'Delete Successfully !');
            }else{
                Session::flash('validate', 'Already use in Product !');
            }
//            return Redirect::back()->withErrors(['message', 'Delete Successfully !']);
            return redirect('admin-discount-index');
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }
}
