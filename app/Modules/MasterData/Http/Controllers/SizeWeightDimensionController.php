<?php

namespace App\Modules\MasterData\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Modules\MasterData\Requests;
use Illuminate\Support\Facades\Input;


use App\Modules\MasterData\Models\SizeWeightDimension;



use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class SizeWeightDimensionController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
        return view("MasterData::welcome");
    }

    public function create(){
        $ModuleTitle = "Size/Weight/Dimension manage";
        $PageTitle = "Add Size/Weight/Dimension";
        $TableTitle = "Size/Weight/Dimension list";

        $all_sizeweightdimension = SizeWeightDimension::where('status','1')->orderby('id','desc')->paginate(10);

        return view("MasterData::sizeweightdimension.create", compact('ModuleTitle','PageTitle','TableTitle','all_sizeweightdimension'));

    }

    public function store(Requests\SizeWeightDimensionRequest $request){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = SizeWeightDimension::where('slug',$slug)->count();
        if ($SlugExistsOrNot ==0) {
            $input['slug'] = $slug;

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // Store sizeunit data
                if ($sizeweight_data = SizeWeightDimension::create($input)) {
                    $sizeweight_data->save();

                }

                DB::commit();
                Session::flash('message', 'Information added Successfully!');
                return redirect()->back();

            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function edit($id){
        $ModuleTitle = "Size/Weight/Dimension manage";
        $PageTitle = "Update Size/Weight/Dimension";
        $TableTitle = "Size/Weight/Dimension list";

        $data = SizeWeightDimension::where('status','1')->where('id',$id)->first();
        $all_sizeweightdimension = SizeWeightDimension::where('status','1')->orderby('id','desc')->paginate(10);

        return view("MasterData::sizeweightdimension.edit", compact('data','ModuleTitle','PageTitle','TableTitle','all_sizeweightdimension'));

    }

    public function update(Requests\SizeWeightDimensionRequest $request,$id){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = SizeWeightDimension::where('slug',$slug)->count();
        $SlugToId = SizeWeightDimension::where('slug',$slug)->first();
        $SizeWeightModel = SizeWeightDimension::where('id',$id)->first();
        if ($SlugExistsOrNot == 0 || ($SlugExistsOrNot == 1 && $id == $SlugToId->id)){
            $input['slug'] = $slug;
            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // update itemunit data
                $result = $SizeWeightModel->update($input);
                $SizeWeightModel->save();

                DB::commit();

                Session::flash('message', 'Information Updated Successfully!');

                if ($input['status'] == 1){
                    return redirect()->route('admin.sizeweightdimension.edit', ['id' => $id]);
                }else{
                    return redirect()->route('admin.sizeweightdimension.create');
                }



            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function delete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {
            $ProductBrand = App\Modules\Product\Models\Product::where('size_weight_dimen_id',$id)->count();
            if ($ProductBrand == 0) {
            $SizeWeightModel = SizeWeightDimension::where('id', $id)
                ->select('*')
                ->first();
            $SizeWeightModel->delete();

            DB::commit();
            Session::flash('delete', 'Delete Successfully !');
//            return Redirect::back()->withErrors(['message', 'Delete Successfully !']);
            }else{
                Session::flash('validate', 'Already use in Product !');
            }
            return redirect('admin-sizeweightdimension-create');
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }
}
