<?php

namespace App\Modules\MasterData\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Modules\MasterData\Requests;
use Illuminate\Support\Facades\Input;


use App\Modules\MasterData\Models\ProductPromotion;



use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class PromotionController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */


    public function create(){
        $ModuleTitle = "Manage Product Promotion";
        $PageTitle = "Add Product Promotion";
        $TableTitle = "Product Promotion list";

        return view("MasterData::promotion.create", compact('ModuleTitle','PageTitle','TableTitle'));

    }

    public function store(Requests\PromotionRequest $request){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = ProductPromotion::where('slug',$slug)->count();
        if ($SlugExistsOrNot ==0) {
            $input['slug'] = $slug;

            if ($request->file('promotion_image') != '') {
                $avatar = $request->file('promotion_image');
                $file_title = $slug.'.'.$avatar->getClientOriginalExtension();
                $input['promotion_image'] = $file_title;
                $path = public_path("backend/image/ProPromotionImage/");
                $target_file =  $path.basename($file_title);
                $file_path = $_FILES['promotion_image']['tmp_name'];
                $result = move_uploaded_file($file_path,$target_file);
            }

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // Store promotion data
                if ($promotion_data = ProductPromotion::create($input)) {
                    $promotion_data->save();

                }

                DB::commit();
                Session::flash('message', 'Information added Successfully!');
//                return redirect()->back();
                return redirect()->route('admin.promotion.index');
            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }

    }

    public function index(){
        $ModuleTitle = "Manage Product Promotion";
        $PageTitle = " Product Promotion list";
        $TableTitle = "Product Promotion list";

        $all_promotion = ProductPromotion::where('status',1)->orderby('id','desc')->paginate(10);

        return view("MasterData::promotion.index", compact('ModuleTitle','PageTitle','TableTitle','all_promotion'));

    }

    public function edit($id){
        $ModuleTitle = "Manage Product Promotion";
        $PageTitle = "Update Product Promotion";

        $data = ProductPromotion::where('status','1')->where('id',$id)->first();

        return view("MasterData::promotion.edit", compact('data','ModuleTitle','PageTitle'));

    }

    public function update(Requests\UpdatePromotionRequest $request,$id){
        $input = $request->all();

        if (!isset($input['feature'])){
            $input['feature'] = null;
        }

        if (!isset($input['tag'])){
            $input['tag'] = null;
        }

        if (!isset($input['promotion'])){
            $input['promotion'] = null;
        }

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = ProductPromotion::where('slug',$slug)->count();
        $SlugToId = ProductPromotion::where('slug',$slug)->first();
        $PromotionUpdateModel = ProductPromotion::where('id',$id)->first();

        if ($SlugExistsOrNot == 0 || ($SlugExistsOrNot == 1 && $id == $SlugToId->id)){
            $input['slug'] = $slug;

            if ($request->file('promotion_image') != '') {
                File::delete(public_path().'/backend/image/ProPromotionImage/'.$PromotionUpdateModel->promotion_image);
                $avatar = $request->file('promotion_image');
                $file_title = $slug.'.'.$avatar->getClientOriginalExtension();
                $input['promotion_image'] = $file_title;
                $path = public_path("backend/image/ProPromotionImage/");
                $target_file =  $path.basename($file_title);
                $file_path = $_FILES['promotion_image']['tmp_name'];
                $result = move_uploaded_file($file_path,$target_file);
            }else{
                $input['promotion_image'] = $PromotionUpdateModel['promotion_image'];
            }

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // update brand data
                $result = $PromotionUpdateModel->update($input);
                $PromotionUpdateModel->save();

                DB::commit();

                Session::flash('message', 'Information Updated Successfully!');
                return redirect()->route('admin.promotion.index');

            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }

        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function delete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {
            $ProductBrand = App\Modules\Product\Models\Product::where('promotion_id',$id)->count();
            $ProductBrandtag = App\Modules\Product\Models\ProductTag::where('tag_id',$id)->count();
            if ($ProductBrand == 0 && $ProductBrandtag == 0) {
            $PromotionDeleteModel = ProductPromotion::where('id', $id)
                ->select('*')
                ->first();
            File::delete(public_path().'/backend/image/ProPromotionImage/'.$PromotionDeleteModel->promotion_image);
            $PromotionDeleteModel->delete();

            DB::commit();
            Session::flash('delete', 'Delete Successfully !');
//            return Redirect::back()->withErrors(['message', 'Delete Successfully !']);
            }else{
                Session::flash('validate', 'Already use in Product !');
            }
            return redirect('admin-promotion-index');
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }
}
