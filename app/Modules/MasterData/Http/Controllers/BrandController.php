<?php

namespace App\Modules\MasterData\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Modules\MasterData\Requests;
use Illuminate\Support\Facades\Input;


use App\Modules\MasterData\Models\ProductBrand;
use App\Modules\MasterData\Models\DiscountBrand;


use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class BrandController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */


    public function create(){
        $ModuleTitle = "Manage Product Brand ";
        $PageTitle = "Add Product Brand";
        $TableTitle = "Product Brand list";

        return view("MasterData::brand.create", compact('ModuleTitle','PageTitle','TableTitle'));
    }

    public function store(Requests\BrandRequest $request){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = ProductBrand::where('slug',$slug)->count();
        if ($SlugExistsOrNot ==0) {
            $input['slug'] = $slug;

            if ($request->file('brand_image') != '') {
                $avatar = $request->file('brand_image');
                $file_title = $slug.'.'.$avatar->getClientOriginalExtension();
                $input['brand_image'] = $file_title;
                $path = public_path("backend/image/ProBrandImage/");
                $target_file =  $path.basename($file_title);
                $file_path = $_FILES['brand_image']['tmp_name'];
                $result = move_uploaded_file($file_path,$target_file);
            }

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // Store brand data
                if ($brand_data = ProductBrand::create($input)) {
                    $brand_data->save();

                }

                DB::commit();
                Session::flash('message', 'Information added Successfully!');
//                return redirect()->back();
                return redirect()->route('admin.brand.index');
            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }

    }



    public function index(){
        $ModuleTitle = "Manage Product Brand ";
        $PageTitle = " Product Brand list";
        $TableTitle = "Product Brand list";

        $all_brand = ProductBrand::where('status',1)->orderby('id','desc')->paginate(10);

        return view("MasterData::brand.index", compact('ModuleTitle','PageTitle','TableTitle','all_brand'));

    }

    public function edit($id){
        $ModuleTitle = "Manage Product Brand";
        $PageTitle = "Update Product Brand";

        $data = ProductBrand::where('status','1')->where('id',$id)->first();

        return view("MasterData::brand.edit", compact('data','ModuleTitle','PageTitle'));

    }

    public function update(Requests\UpdateBrandRequest $request,$id){
        $input = $request->all();

        if (!isset($input['feature'])){
            $input['feature'] = null;
        }

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = ProductBrand::where('slug',$slug)->count();
        $SlugToId = ProductBrand::where('slug',$slug)->first();
        $BrandUpdateModel = ProductBrand::where('id',$id)->first();

        if ($SlugExistsOrNot == 0 || ($SlugExistsOrNot == 1 && $id == $SlugToId->id)){
            $input['slug'] = $slug;

            if ($request->file('brand_image') != '') {
                File::delete(public_path().'/backend/image/ProBrandImage/'.$BrandUpdateModel->brand_image);
                $avatar = $request->file('brand_image');
                $file_title = $slug.'.'.$avatar->getClientOriginalExtension();
                $input['brand_image'] = $file_title;
                $path = public_path("backend/image/ProBrandImage/");
                $target_file =  $path.basename($file_title);
                $file_path = $_FILES['brand_image']['tmp_name'];
                $result = move_uploaded_file($file_path,$target_file);
            }else{
                $input['brand_image'] = $BrandUpdateModel['brand_image'];
            }

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // update brand data
                $result = $BrandUpdateModel->update($input);
                $BrandUpdateModel->save();

                DB::commit();

                Session::flash('message', 'Information Updated Successfully!');
                return redirect()->route('admin.brand.index');

            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }

        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function delete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {
            $DiscountBrandExists = DiscountBrand::where('brand_id',$id)->count();

            if ($DiscountBrandExists == 0) {
                $ProductBrand = App\Modules\Product\Models\Product::where('brand_id',$id)->count();
                if ($ProductBrand == 0) {
                    $BrandDeleteModel = ProductBrand::where('id', $id)
                        ->select('*')
                        ->first();
                    File::delete(public_path() . '/backend/image/ProBrandImage/' . $BrandDeleteModel->brand_image);
                    $BrandDeleteModel->delete();
                    Session::flash('delete', 'Delete Successfully !');
                }else{
                    Session::flash('validate', 'Already use in Product !');
                    return redirect('admin-category-index');
                }
            }else{
                Session::flash('validate', 'Already use in discount !');
            }
            DB::commit();

//            return Redirect::back()->withErrors(['message', 'Delete Successfully !']);
            return redirect('admin-brand-index');
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }
}
