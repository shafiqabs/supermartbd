<?php

namespace App\Modules\MasterData\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Modules\MasterData\Requests;
use Illuminate\Support\Facades\Input;


use App\Modules\MasterData\Models\WarningType;



use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class WarningtypeController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */

    public function create(){
        $ModuleTitle = "Manage Warning Type";
        $PageTitle = "Add Warning Type";
        $TableTitle = "Warning Type list";

        $all_warningtype = WarningType::where('status','1')->orderby('id','desc')->paginate(10);

        return view("MasterData::warningtype.create", compact('ModuleTitle','PageTitle','TableTitle','all_warningtype'));

    }

    public function store(Requests\WarningtypeRequest $request){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = WarningType::where('slug',$slug)->count();
        if ($SlugExistsOrNot ==0) {
            $input['slug'] = $slug;

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // Store warning type data
                if ($warning_data = WarningType::create($input)) {
                    $warning_data->save();

                }

                DB::commit();
                Session::flash('message', 'Information added Successfully!');
                return redirect()->back();

            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function edit($id){
        $ModuleTitle = "Manage Warning Type";
        $PageTitle = "Update Warning Type";
        $TableTitle = "Warning Type list";

        $data = WarningType::where('status','1')->where('id',$id)->first();
        $all_warningtype = WarningType::where('status','1')->orderby('id','desc')->paginate(10);

        return view("MasterData::warningtype.edit", compact('data','ModuleTitle','PageTitle','TableTitle','all_warningtype'));

    }

    public function update(Requests\WarningtypeRequest $request,$id){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = WarningType::where('slug',$slug)->count();
        $SlugToId = WarningType::where('slug',$slug)->first();
        $WarningModel = WarningType::where('id',$id)->first();
        if ($SlugExistsOrNot == 0 || ($SlugExistsOrNot == 1 && $id == $SlugToId->id)){
            $input['slug'] = $slug;
            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // update warning data
                $result = $WarningModel->update($input);
                $WarningModel->save();

                DB::commit();

                Session::flash('message', 'Information Updated Successfully!');

                if ($input['status'] == 1){
                    return redirect()->route('admin.warningtype.edit', ['id' => $id]);
                }else{
                    return redirect()->route('admin.warningtype.create');
                }



            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('vaidate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function delete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {
            $ProductBrand = App\Modules\Product\Models\Product::where('warning_id',$id)->count();
            if ($ProductBrand == 0) {
            $WarningModel = WarningType::where('id', $id)
                ->select('*')
                ->first();
            $WarningModel->delete();

            DB::commit();
            Session::flash('delete', 'Delete Successfully !');
//            return Redirect::back()->withErrors(['message', 'Delete Successfully !']);
            }else{
                Session::flash('validate', 'Already use in Product !');
            }
            return redirect('admin-warningtype-create');
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }
}
