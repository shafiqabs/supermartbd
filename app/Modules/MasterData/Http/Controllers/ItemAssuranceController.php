<?php

namespace App\Modules\MasterData\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Modules\MasterData\Requests;
use Illuminate\Support\Facades\Input;


use App\Modules\MasterData\Models\ItemAssurance;



use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class ItemAssuranceController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */

    public function create(){
        $ModuleTitle = "Manage Item Assurance";
        $PageTitle = "Add Item Assurance";
        $TableTitle = "Item Assurance list";

        $all_ItemAssurance = ItemAssurance::where('status','1')->orderby('id','desc')->paginate(10);

        return view("MasterData::itemassurance.create", compact('ModuleTitle','PageTitle','TableTitle','all_ItemAssurance'));

    }

    public function store(Requests\ItemAssuranceRequest $request){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = ItemAssurance::where('slug',$slug)->count();
        if ($SlugExistsOrNot ==0) {
            $input['slug'] = $slug;

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // Store Item Assurance data
                if ($ItemAssurance_data = ItemAssurance::create($input)) {
                    $ItemAssurance_data->save();

                }

                DB::commit();
                Session::flash('message', 'Information added Successfully!');
                return redirect()->back();

            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function edit($id){
        $ModuleTitle = "Manage Item Assurance";
        $PageTitle = "Update Item Assurance";
        $TableTitle = "Item Assurance list";

        $data = ItemAssurance::where('status','1')->where('id',$id)->first();
        $all_ItemAssurance = ItemAssurance::where('status','1')->orderby('id','desc')->paginate(10);

        return view("MasterData::itemassurance.edit", compact('data','ModuleTitle','PageTitle','TableTitle','all_ItemAssurance'));

    }

    public function update(Requests\ItemAssuranceRequest $request,$id){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = ItemAssurance::where('slug',$slug)->count();
        $SlugToId = ItemAssurance::where('slug',$slug)->first();
        $ItemAssuranceModel = ItemAssurance::where('id',$id)->first();
        if ($SlugExistsOrNot == 0 || ($SlugExistsOrNot == 1 && $id == $SlugToId->id)){
            $input['slug'] = $slug;
            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // update Item Assurance data
                $result = $ItemAssuranceModel->update($input);
                $ItemAssuranceModel->save();

                DB::commit();

                Session::flash('message', 'Information Updated Successfully!');

                if ($input['status'] == 1){
                    return redirect()->route('admin.itemassurance.edit', ['id' => $id]);
                }else{
                    return redirect()->route('admin.itemassurance.create');
                }



            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('vaidate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function delete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {
            $ProductBrand = App\Modules\Product\Models\Product::where('item_assurance_id',$id)->count();
            if ($ProductBrand == 0) {
            $ItemAssuranceModel = ItemAssurance::where('id', $id)
                ->select('*')
                ->first();
            $ItemAssuranceModel->delete();

            DB::commit();
            Session::flash('delete', 'Delete Successfully !');
//            return Redirect::back()->withErrors(['message', 'Delete Successfully !']);
            }else{
                Session::flash('validate', 'Already use in Product !');
            }
            return redirect('admin-itemassurance-create');
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }
}
