<?php

namespace App\Modules\MasterData\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Modules\MasterData\Requests;
use Illuminate\Support\Facades\Input;


use App\Modules\MasterData\Models\Sizeunit;



use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class SizeunitController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
        return view("MasterData::welcome");
    }

    public function create(){
        $ModuleTitle = "Size unit manage";
        $PageTitle = "Add Size unit";
        $TableTitle = "Size unit list";

        $all_sizeunit = Sizeunit::where('status','1')->orderby('id','desc')->paginate(10);

        return view("MasterData::sizeunit.create", compact('ModuleTitle','PageTitle','TableTitle','all_sizeunit'));

    }

    public function store(Requests\SizeunitRequest $request){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = Sizeunit::where('slug',$slug)->count();
        if ($SlugExistsOrNot ==0) {
            $input['slug'] = $slug;

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // Store sizeunit data
                if ($sizeunit_data = Sizeunit::create($input)) {
                    $sizeunit_data->save();

                }

                DB::commit();
                Session::flash('message', 'Information added Successfully!');
                return redirect()->back();

            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function edit($id){
        $ModuleTitle = "Size unit manage";
        $PageTitle = "Update Size unit";
        $TableTitle = "Size unit list";

        $data = Sizeunit::where('status','1')->where('id',$id)->first();
        $all_sizeunit = Sizeunit::where('status','1')->orderby('id','desc')->paginate(10);

        return view("MasterData::sizeunit.edit", compact('data','ModuleTitle','PageTitle','TableTitle','all_sizeunit'));

    }

    public function update(Requests\SizeunitRequest $request,$id){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = Sizeunit::where('slug',$slug)->count();
        $SlugToId = Sizeunit::where('slug',$slug)->first();
        $SizeUnitModel = Sizeunit::where('id',$id)->first();
        if ($SlugExistsOrNot == 0 || ($SlugExistsOrNot == 1 && $id == $SlugToId->id)){
            $input['slug'] = $slug;
            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // update sizeunit data
                $result = $SizeUnitModel->update($input);
                $SizeUnitModel->save();

                DB::commit();

                Session::flash('message', 'Information Updated Successfully!');

                if ($input['status'] == 1){
                    return redirect()->route('admin.sizeunit.edit', ['id' => $id]);
                }else{
                    return redirect()->route('admin.sizeunit.create');
                }



            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function delete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {
            $ProductBrand = App\Modules\Product\Models\Product::where('size_unit_id',$id)->count();
            if ($ProductBrand == 0) {
            $SizeunitModel = Sizeunit::where('id', $id)
                ->select('*')
                ->first();
            $SizeunitModel->delete();

            DB::commit();
            Session::flash('delete', 'Delete Successfully !');
//            return Redirect::back()->withErrors(['message', 'Delete Successfully !']);
            }else{
                Session::flash('validate', 'Already use in Product !');
            }
            return redirect('admin-sizeunit-create');
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }
}
