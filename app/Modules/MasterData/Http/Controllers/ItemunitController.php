<?php

namespace App\Modules\MasterData\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Modules\MasterData\Requests;
use Illuminate\Support\Facades\Input;


use App\Modules\MasterData\Models\ItemUnit;



use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class ItemunitController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */

    public function create(){
        $ModuleTitle = "Item unit manage";
        $PageTitle = "Add Item unit";
        $TableTitle = "Item unit list";

        $all_itemunit = ItemUnit::where('status','1')->orderby('id','desc')->paginate(10);

        return view("MasterData::itemunit.create", compact('ModuleTitle','PageTitle','TableTitle','all_itemunit'));

    }

    public function store(Requests\ItemunitRequest $request){
        $input = $request->all();
        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = ItemUnit::where('slug',$slug)->count();
        if ($SlugExistsOrNot ==0) {
            $input['slug'] = $slug;

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // Store itemunit data
                if ($itemunit_data = ItemUnit::create($input)) {
                    $itemunit_data->save();

                }

                DB::commit();
                Session::flash('message', 'Information added Successfully!');
                return redirect()->back();

            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function edit($id){
        $ModuleTitle = "Item unit manage";
        $PageTitle = "Update Item unit";
        $TableTitle = "Item unit list";

        $data = ItemUnit::where('status','1')->where('id',$id)->first();
        $all_itemunit = ItemUnit::where('status','1')->orderby('id','desc')->paginate(10);

        return view("MasterData::itemunit.edit", compact('data','ModuleTitle','PageTitle','TableTitle','all_itemunit'));

    }

    public function update(Requests\ItemunitRequest $request,$id){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = ItemUnit::where('slug',$slug)->count();
        $SlugToId = ItemUnit::where('slug',$slug)->first();
        $ItemunitModel = ItemUnit::where('id',$id)->first();
        if ($SlugExistsOrNot == 0 || ($SlugExistsOrNot == 1 && $id == $SlugToId->id)){
            $input['slug'] = $slug;
            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // update itemunit data
                $result = $ItemunitModel->update($input);
                $ItemunitModel->save();

                DB::commit();

                Session::flash('message', 'Information Updated Successfully!');

                if ($input['status'] == 1){
                    return redirect()->route('admin.itemunit.edit', ['id' => $id]);
                }else{
                    return redirect()->route('admin.itemunit.create');
                }



            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('vaidate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function delete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {
            $ProductBrand = App\Modules\Product\Models\Product::where('item_unit_id',$id)->count();
            if ($ProductBrand == 0) {
            $ItemunitModel = ItemUnit::where('id', $id)
                ->select('*')
                ->first();
            $ItemunitModel->delete();

            DB::commit();
            Session::flash('delete', 'Delete Successfully !');
//            return Redirect::back()->withErrors(['message', 'Delete Successfully !']);
            }else{
                Session::flash('validate', 'Already use in Product !');
            }
            return redirect('admin-itemunit-create');
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }
}
