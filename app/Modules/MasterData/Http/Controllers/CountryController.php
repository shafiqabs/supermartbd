<?php

namespace App\Modules\MasterData\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Modules\MasterData\Requests;
use Illuminate\Support\Facades\Input;


use App\Modules\MasterData\Models\Country;



use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class CountryController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */

    public function create(){
        $ModuleTitle = "Manage Country Information";
        $PageTitle = "Add Country Information";
        $TableTitle = "Country Information list";

        $all_country = Country::where('status','1')->orderby('id','desc')->paginate(10);

        return view("MasterData::country.create", compact('ModuleTitle','PageTitle','TableTitle','all_country'));

    }

    public function store(Requests\CountryRequest $request){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = Country::where('slug',$slug)->count();

        if ($SlugExistsOrNot ==0) {
            $input['slug'] = $slug;

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // Store sizeunit data
                if ($country_data = Country::create($input)) {
                    $country_data->save();

                }

                DB::commit();
                Session::flash('message', 'Information added Successfully!');
                return redirect()->back();

            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function edit($id){
        $ModuleTitle = "Manage Country Information";
        $PageTitle = "Update Country Information";
        $TableTitle = "Country Information list";

        $data = Country::where('status','1')->where('id',$id)->first();
        $all_country = Country::where('status','1')->orderby('id','desc')->paginate(10);

        return view("MasterData::country.edit", compact('data','ModuleTitle','PageTitle','TableTitle','all_country'));

    }

    public function update(Requests\CountryRequest $request,$id){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = Country::where('slug',$slug)->count();
        $SlugToId = Country::where('slug',$slug)->first();
        $CountryModel = Country::where('id',$id)->first();
        if ($SlugExistsOrNot == 0 || ($SlugExistsOrNot == 1 && $id == $SlugToId->id)){
            $input['slug'] = $slug;
            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // update sizeunit data
                $result = $CountryModel->update($input);
                $CountryModel->save();

                DB::commit();

                Session::flash('message', 'Information Updated Successfully!');

                if ($input['status'] == 1){
                    return redirect()->route('admin.country.edit', ['id' => $id]);
                }else{
                    return redirect()->route('admin.country.create');
                }



            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function delete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {
            $ProductBrand = App\Modules\Product\Models\Product::where('country_id',$id)->count();
            if ($ProductBrand == 0) {
            $CountryModel = Country::where('id', $id)
                ->select('*')
                ->first();
            $CountryModel->delete();

            DB::commit();
            Session::flash('delete', 'Delete Successfully !');
//            return Redirect::back()->withErrors(['message', 'Delete Successfully !']);
            return redirect('admin-country-create');
            }else{
                Session::flash('validate', 'Already use in Product !');
                return redirect('admin-country-create');
            }
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }
}
