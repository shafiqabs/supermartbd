<?php

namespace App\Modules\MasterData\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class CategoryRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name'       => 'required',
            'banner_header'       => 'required|max:20',
            'banner_title'       => 'required|max:25',
            'banner_sub_title'       => 'required|max:16',
            'category_image'=> 'required|image|mimes:jpeg,JPEG,Jpeg,PNG,Png,png,jpg,JPG,Jpg',
        ];

    }

}
