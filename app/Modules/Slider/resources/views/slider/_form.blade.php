<?php
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request as Input;

?>

    <div class="row">
        <div class="col-md-8">
            <div class="from-group">
                {!! Form::label('Key Word', 'Key Word', array('class' => 'form-label')) !!}
                <span style="color: red">*</span>
                <div class="input-group mb-3">
                    {!! Form::text('keyword',Input::old('keyword'),['id'=>'keyword','class' => 'form-control','data-checkify'=>'minlen=3,required,maxlen=20','Placeholder' => 'Enter Key Word','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
                    <span style="color: #ff0000">{!! $errors->first('keyword') !!}</span>
                </div>
            </div>

            <div class="from-group">
                {!! Form::label('Name', 'Name', array('class' => 'form-label')) !!}
                <span style="color: red">*</span>
                <div class="input-group mb-3">
                    {!! Form::text('name',Input::old('name'),['id'=>'name','class' => 'form-control','data-checkify'=>'minlen=3,required,maxlen=50','Placeholder' => 'Enter Name','aria-label' =>'content','aria-describedby'=>'basic-addon2']) !!}
                    <span style="color: #ff0000">{!! $errors->first('name') !!}</span>
                </div>
            </div>


            <div class="from-group">
                {!! Form::label('Short Discription', 'Short Discription', array('class' => 'form-label')) !!}
                <span style="color: red">*</span>
                <div class="input-group mb-3">
                    {!! Form::textarea('discription',Input::old('discription'),['id'=>'discription','class' => 'form-control','data-checkify'=>'minlen=3,required,maxlen=50','aria-label' =>'content','aria-describedby'=>'basic-addon2','rows'=>2]) !!}
                    <span style="color: #ff0000">{!! $errors->first('discription') !!}</span>
                </div>
            </div>
        </div>



        <div class="col-md-4">
            <div class="from-group">
                {!! Form::label('Slider Image', 'Slider Image', array('class' => 'form-label','for'=>'formFile')) !!}
                @if(isset($data))
                @else
                    <span style="color: red">*</span>
                @endif

                <div class="mb-3">
                    @if(isset($data))
                        <input class="form-control" accept="image/*" name="slider_image" type="file" id="file" onchange="loadFile(event)">
                    @else
                        <input required class="form-control" accept="image/*" name="slider_image" type="file" id="file" onchange="loadFile(event)">

                    @endif

                    <div style="text-align: center;padding-top: 5px;">
                        @if($errors->first('slider_image'))
                            <span style="color: #ff0000">{!! $errors->first('slider_image') !!}</span>
                        @else
                            <img id="slider_image" width="150" height="120"/>
                        @endif

                        @if(isset($data) && $data->slider_image !='')
                            <img id="slider_image" src="{{ asset('backend/image/ProSliderImage').'/'.$data->slider_image;}}" width="150" height="120"/>
                        @endif

                    </div>
                </div>
            </div>


            <script>
                var loadFile = function(event) {
                    var image = document.getElementById('slider_image');
                    image.src = URL.createObjectURL(event.target.files[0]);
                };
            </script>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 offset-md-8">
            <div class="from-group">
                <div class="from-group">
                    {!! Form::label('Status', 'Status', array('class' => 'form-label')) !!}
                    <span style="color: red">*</span>
                    <div class="input-group mb-3">
                        {!! Form::select('status',array(1=>'Active',0=>'Inactive'),Input::old('status'),['id'=>'ordering','class' => 'form-control','data-checkify'=>'minlen=1,required,number','Placeholder' => 'Enter ordering number','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('ordering') !!}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="row">

        <div class="col-md-12" style="text-align: right;">
            <div class="from-group">
                <div class="">
                    <button type="reset" class="btn submit-button">Reset</button>
                    <button type="submit" class="btn submit-button">Submit</button>
                </div>
            </div>
        </div>

    </div>
