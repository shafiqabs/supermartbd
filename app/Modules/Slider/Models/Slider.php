<?php

namespace App\Modules\Slider\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App;
use Auth;

class Slider extends Model
{
    use HasFactory;
    protected $table = 'ems_slider';
    protected $fillable = [
        'keyword',
        'slug',
        'name',
        'discription',
        'status',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'slider_image'
    ];



    // TODO :: boot
    // boot() function used to insert logged user_id at 'created_by' & 'updated_by'
    public static function boot(){
        parent::boot();
        static::creating(function($query){
            if(Auth::check()){
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function($query){
            if(Auth::check()){
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}
