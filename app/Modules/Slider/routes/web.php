<?php

use Illuminate\Support\Facades\Route;

//Route::get('slider', 'SliderController@welcome');
Route::group(['module' => 'Slider', 'middleware' => ['web','auth','adminmiddleware']], function() {
    include 'slider.php';
});
