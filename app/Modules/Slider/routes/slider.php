<?php

Route::get('admin-slider-create', [
    'as' => 'admin.slider.create',
    'uses' => 'SliderController@create'
]);

Route::get('admin-slider-index', [
    'as' => 'admin.slider.index',
    'uses' => 'SliderController@index'
]);

Route::post('admin-slider-store',[
    'as' => 'admin.slider.store',
    'uses' => 'SliderController@store'
]);

Route::get('admin-slider-edit/{id}',[
    'as' => 'admin.slider.edit',
    'uses' => 'SliderController@edit'
]);

Route::PATCH('admin-slider-update/{id}',[
    'as' => 'admin.slider.update',
    'uses' => 'SliderController@update'
]);

Route::get('admin-slider-delete/{id}',[
    'as' => 'admin.slider.delete',
    'uses' => 'SliderController@delete'
]);


