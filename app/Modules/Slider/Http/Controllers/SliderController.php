<?php

namespace App\Modules\Slider\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\MasterData\Models\ProductBrand;
use App\Modules\Slider\Models\Slider;
use App\Modules\Vendor\Models\Vendor;
use Illuminate\Http\Request;
use App\Modules\Slider\Requests;

use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class SliderController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {
        return view("Slider::welcome");
    }

    public function index(){
        $ModuleTitle = "Manage Slider Information";
        $PageTitle = " Slider Information list";
        $TableTitle = "Slider Information list";

        $all_slider = Slider::where('status',1)->orderby('id','desc')->paginate(10);

        return view("Slider::slider.index", compact('ModuleTitle','PageTitle','TableTitle','all_slider'));

    }


    public function create(){
        $ModuleTitle = "Manage Slider Information";
        $PageTitle = "Add Slider Information";
        $TableTitle = "Slider Information list";

        return view("Slider::slider.create", compact('ModuleTitle','PageTitle','TableTitle'));

    }


    public function store(Requests\SliderRequest $request){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['keyword']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = Slider::where('slug',$slug)->count();
        if ($SlugExistsOrNot ==0) {
            $input['slug'] = $slug;

            if ($request->file('slider_image') != '') {
                $avatar = $request->file('slider_image');
                $file_title = $slug.'.'.$avatar->getClientOriginalExtension();
                $input['slider_image'] = $file_title;
                $path = public_path("backend/image/ProSliderImage/");
                $target_file =  $path.basename($file_title);
                $file_path = $_FILES['slider_image']['tmp_name'];
                $result = move_uploaded_file($file_path,$target_file);
            }

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // Store brand data
                if ($slider_data = Slider::create($input)) {
                    $slider_data->save();

                }

                DB::commit();
                Session::flash('message', 'Information added Successfully!');
//                return redirect()->back();
                return redirect()->route('admin.slider.index');
            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }

    }



    public function edit($id){
        $ModuleTitle = "Manage Slider Information";
        $PageTitle = "Update Slider Information";

        $data = Slider::where('status','1')->where('id',$id)->first();

        return view("Slider::slider.edit", compact('data','ModuleTitle','PageTitle'));

    }

    public function update(Requests\SliderRequest $request,$id){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['keyword']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);
        $input['slug'] = $slug;

        $SlugExistsOrNot = Slider::where('slug',$slug)->count();
        $SlugToId = Slider::where('slug',$slug)->first();
        $SliderUpdateModel = Slider::where('id',$id)->first();

        if ($SlugExistsOrNot == 0 || ($SlugExistsOrNot == 1 && $id == $SlugToId->id)){
            $input['slug'] = $slug;

            if ($request->file('slider_image') != '') {
                File::delete(public_path().'/backend/image/ProSliderImage/'.$SliderUpdateModel->slider_image);
                $avatar = $request->file('slider_image');
                $file_title = $slug.time().'.'.$avatar->getClientOriginalExtension();
                $input['slider_image'] = $file_title;
                $path = public_path("backend/image/ProSliderImage/");
                $target_file =  $path.basename($file_title);
                $file_path = $_FILES['slider_image']['tmp_name'];
                $result = move_uploaded_file($file_path,$target_file);
            }else{
                $input['slider_image'] = $SliderUpdateModel['slider_image'];
            }

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // update slider data
                $result = $SliderUpdateModel->update($input);
                $SliderUpdateModel->save();

                DB::commit();

                Session::flash('message', 'Information Updated Successfully!');
                return redirect()->route('admin.slider.index');

            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }

        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function delete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {
        $SliderModel = Slider::where('id', $id)
            ->select('*')
            ->first();
        File::delete(public_path() . '/backend/image/ProSliderImage/' . $SliderModel->slider_image);
            $SliderModel->delete();

        DB::commit();
        Session::flash('delete', 'Delete Successfully !');
        //            return Redirect::back()->withErrors(['message', 'Delete Successfully !']);
        return redirect('admin-slider-index');

        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }
}
