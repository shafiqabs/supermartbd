<?php

namespace App\Modules\Slider\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class SliderRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'keyword'       => 'required',
            'name'       => 'required',
            'discription'       => 'required',
            'slider_image'=> 'image|mimes:jpeg,JPEG,Jpeg,PNG,Png,png,jpg,JPG,Jpg',
        ];

    }

}
