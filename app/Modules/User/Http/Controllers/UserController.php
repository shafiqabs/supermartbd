<?php

namespace App\Modules\User\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\MasterData\Models\DiscountBrand;
use App\Modules\MasterData\Models\ProductBrand;
use App\Modules\Vendor\Models\Vendor;
use App\Modules\User\Models\User;
use Illuminate\Http\Request;
use App\Modules\User\Requests;

use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class UserController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $ModuleTitle = "Manage User Information ";
        $PageTitle = "Add User Information";
        $TableTitle = "User Information list";

        return view("User::user.create", compact('ModuleTitle','PageTitle','TableTitle'));

    }

    public function store(Requests\UserRequest $request){
        $input = $request->all();

        $EmailExistsOrNot = User::where('email',$input['email'])->count();

        if ($EmailExistsOrNot == 0) {

            $name = str_replace(' ', '-', $input['name']);
            $name = str_replace("/\s+/", "-", $name);
            $name = str_replace(".", "-", $name);
            $name = strtolower($name);

//            $Password = rand(10,100000).''.$input['mobile'];
            $Password = '123456';
            $input['password'] = password_hash($Password,PASSWORD_DEFAULT) ;

            if ($request->file('user_image') != '') {
                $avatar = $request->file('user_image');
                $file_title = $name.time().'.'.$avatar->getClientOriginalExtension();
                $input['user_image'] = $file_title;
                $path = public_path("backend/image/UserImage/");

                $target_file =  $path.basename($file_title);

                $file_path = $_FILES['user_image']['tmp_name'];
                move_uploaded_file($file_path,$target_file);
            }else{
                $input['user_image'] = 'defaultuser.png';
            }
//            if ($input['type'] == 'Vendor'){
//                if ($request->file('user_image') != '') {
//                    $avatar1 = $request->file('user_image');
//                    $file_title1 = $name.time().'.'.$avatar1->getClientOriginalExtension();
//
//                    $VendorInfo['store_image'] = $file_title1;
//                    $path1 = public_path("backend/image/ProVendorImage/");
//                    $target_file1 =  $path1.basename($file_title1);
////                    echo $target_file;
//                    $file_path1 = $_FILES['user_image']['tmp_name'];
////                    echo $file_path.'<br>';
////                    move_uploaded_file($file_path,$target_file);
//                    if (move_uploaded_file($file_path1,$target_file1)){
//                        echo 'ok';
//                        echo '<br>';
//                    }
//                }
//            }
//    exit();
            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // Store User data
                if ($input['type'] == 'Admin' || $input['type'] == 'Vendor' || $input['type'] == 'Customer' || $input['type'] == 'Moderator' || $input['type'] == 'Courier') {
                    if ($UserData = User::create($input)) {
                        $UserData->save();

                        $details = [
                            'mailpage' => 'UserMail',
                            'title' => 'Supermartbd Login Information',
                            'name' => 'Hi ' . $input['name'],
                            'Email' => $input['email'],
                            'Type' => $input['type'],
                            'Password' => $Password
                        ];

                        \Mail::to($input['email'])->send(new \App\Mail\MailSend($details));
                    }

                    if ($input['type'] == 'Vendor'){
                        $name = str_replace(' ', '-', $input['email']);
                        $name = str_replace("/\s+/", "-", $name);
                        $name = str_replace(".", "-", $name);
                        $name = strtolower($name);

                        $VendorInfo['user_id'] = $UserData->id;
                        $VendorInfo['store_name'] = $input['name'];
                        $VendorInfo['slug'] = $name;
                        $VendorInfo['store_address'] = $input['address'];
                        $VendorInfo['contact_person_name'] = $input['name'];
                        $VendorInfo['mobile'] = $input['mobile'];
                        $VendorInfo['email'] = $input['email'];
                        $VendorInfo['status'] = $input['status'];
                        Vendor::create($VendorInfo);
                    }

                    if ($input['type'] == 'Customer'){
                        $CustomerInfo['user_id'] = $UserData->id;
                        $CustomerInfo['first_name'] = $input['name'];
                        $CustomerInfo['bill_street_address'] = $input['address'];
                        $CustomerInfo['mobile'] = $input['mobile'];
                        $CustomerInfo['email'] = $input['email'];
                        $CustomerInfo['status'] = $input['status'];
                        App\Modules\Customer\Models\Customer::create($CustomerInfo);
                    }

                }

                DB::commit();
                Session::flash('message', 'Information added Successfully!');
//                return redirect()->back();
                return redirect()->route('admin.user.index');
            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Email already exists');
            return redirect()->back()->withInput($input);
        }

    }



    public function index(){
        $ModuleTitle = "Manage User Information ";
        $PageTitle = " User Information list";
        $TableTitle = "User Information list";

        $all_user = User::where('status',1)->orderby('id','desc')->paginate(10);

        return view("User::user.index", compact('ModuleTitle','PageTitle','TableTitle','all_user'));

    }

    public function edit($id){
        $ModuleTitle = "Manage User Information";
        $PageTitle = "Update User Information";

        $data = User::where('status',1)->where('id',$id)->first();

        return view("User::user.edit", compact('data','ModuleTitle','PageTitle'));

    }

    public function update(Requests\UserRequest $request,$id){
        $input = $request->all();

        $name = str_replace(' ', '-', $input['name']);
        $name = str_replace("/\s+/", "-", $name);
        $name = str_replace(".", "-", $name);
        $name = strtolower($name);

        $UserExistsOrNot = User::where('email',$input['email'])->count();
        $SlugToId = User::where('email',$input['email'])->first();
        $UserUpdateModel = User::where('id',$id)->first();

        if ($UserExistsOrNot == 0 || ($UserExistsOrNot == 1 && $id == $SlugToId->id)){

            if ($request->file('user_image') != '') {
                File::delete(public_path().'/backend/image/UserImage/'.$UserUpdateModel->user_image);
                $avatar = $request->file('user_image');
                $file_title = $name.time().'.'.$avatar->getClientOriginalExtension();
                $input['user_image'] = $file_title;
                $path = public_path("backend/image/UserImage/");
                $target_file =  $path.basename($file_title);
                $file_path = $_FILES['user_image']['tmp_name'];
                $result = move_uploaded_file($file_path,$target_file);
            }else{
                $input['user_image'] = $UserUpdateModel['user_image'];
            }

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // update brand data
                $result = $UserUpdateModel->update($input);
                $UserUpdateModel->save();
                if ($result && $UserExistsOrNot == 0){
                    $details = [
                        'mailpage' => 'UserMail',
                        'title' => 'Supermartbd Login Information',
                        'name' => 'Hi ' . $input['name'],
                        'body' => 'Use this '. $input['email'] .' email for login',
                        'Email' => $input['email'],
                        'Type' => $input['type']
                    ];

                    \Mail::to($input['email'])->send(new \App\Mail\MailSend($details));
                }

                DB::commit();

                Session::flash('message', 'Information Updated Successfully!');
                return redirect()->route('admin.user.index');

            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }

        }else{
            Session::flash('validate', 'Email already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function delete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {
            $UserInfo = User::where('id',$id)->first();
            $UserType = $UserInfo->type;

            if ($UserType == 'Vendor'){
                $VendorExistsOrNot = Vendor::where('user_id',$id)->first();
                $VendorId = $VendorExistsOrNot->id;

                if (isset($VendorId) && !empty($VendorId)){
                    $ProductVendor = App\Modules\Product\Models\Product::where('vendor_id',$VendorId)->count();

                    if ($ProductVendor == 0){

                        $VendorModel = Vendor::where('id', $VendorId)
                            ->select('*')
                            ->first();

                        if (isset($VendorModel->store_image) && !empty($VendorModel->store_image)) {
                            File::delete(public_path() . '/backend/image/ProVendorImage/' . $VendorModel->store_image);
                            $VendorModel->delete();
                        }else{
                            $VendorModel->delete();
                        }

                        $UserModel = User::where('id',$id)->first();
                        if (isset($UserModel->user_image) && !empty($UserModel->user_image) && $UserModel->user_image != 'defaultuser.png') {
                            File::delete(public_path() . '/backend/image/UserImage/' . $UserModel->user_image);
                        }
                        $UserModel->delete();
                        DB::commit();

                        Session::flash('validate', 'User remove Successfully');
                        return redirect('admin-user-index');
                    }else{
                        Session::flash('validate', 'This vendor has total '.$ProductVendor.' products.');
                        return redirect('admin-user-index');
                    }
                }else{
                    File::delete(public_path() . '/backend/image/UserImage/' . $UserInfo->user_image);
                    $UserInfo->delete();
                    Session::flash('validate', 'User remove Successfully');
                    return redirect('admin-user-index');
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }

    public function PasswordForm(){
        $ModuleTitle = Auth::user()->type." Password Change";
        $PageTitle = Auth::user()->name." Password Change";
        $TableTitle = "Password Change";

        return view("User::user.passwordchange", compact('ModuleTitle','PageTitle','TableTitle'));

    }

    public function PasswordChange(Request $request){
        $input = $request->all();

        $validated = $request->validate([
            'password' => 'required|min:6',
            'confirm' => 'required|min:6',
        ]);
//        |same:confirm
        if ($input['password'] == $input['confirm']){
            $password = password_hash($input['password'],PASSWORD_DEFAULT) ;

            $UpatePaaword = User::find(Auth::user()->id);
            $UpPass['password'] = $password;
            $UpatePaaword->update($UpPass);
            $UpatePaaword->save();
            Session::flash('message', 'Password change successfully');
            return redirect()->back();
        }else{
            Session::flash('validate', 'Password must be same');
            return redirect()->back()->withInput($input);
        }
    }
}
