<?php
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request as Input;
?>


    <div class="row">
        <div class="col-md-8">

            <div class="from-group">
                {!! Form::label('Name', 'Name', array('class' => 'form-label')) !!}
                <span style="color: red">*</span>
                <div class="input-group mb-3">
                    {!! Form::text('name',Input::old('name'),['id'=>'name','class' => 'form-control','data-checkify'=>'minlen=3,required','Placeholder' => 'Enter User Name','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                    <span style="color: #ff0000">{!! $errors->first('name') !!}</span>
                </div>
            </div>
<div class="row">
            <div class="col-md-6">
            <div class="from-group">
                {!! Form::label('Mobile', 'Mobile', array('class' => 'form-label')) !!}
                <span style="color: red">*</span>
                <div class="input-group mb-3">
                    {!! Form::text('mobile',Input::old('mobile'),['id'=>'mobile','class' => 'form-control','data-checkify'=>'minlen=3,required,number,maxlen=14','Placeholder' => 'Enter Mobile','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                    <span style="color: #ff0000">{!! $errors->first('mobile') !!}</span>
                </div>
            </div>
            </div>

            <div class="col-md-6">
                <div class="from-group">
                    {!! Form::label('Email', 'Email', array('class' => 'form-label')) !!}
                                    <span style="color: red">*</span>
                    <div class="input-group mb-3">
                        {!! Form::text('email',Input::old('email'),['id'=>'email','class' => 'form-control','data-checkify'=>'minlen=6,maxlen=40,email,required','Placeholder' => 'Enter email','aria-label' =>'email','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('email') !!}</span>
                    </div>
                </div>
            </div>
</div>
            <div class="from-group">
                {!! Form::label('Address', 'Address', array('class' => 'form-label')) !!}
                <div class="input-group mb-3">
                    {!! Form::textarea('address',Input::old('address'),['id'=>'address','class' => 'form-control','Placeholder' => 'Enter Address','aria-label' =>'content','aria-describedby'=>'basic-addon2','rows'=>1]) !!}
                    <span style="color: #ff0000">{!! $errors->first('address') !!}</span>
                </div>
            </div>
        </div>



        <div class="col-md-4">
            <div class="from-group">
                {!! Form::label('User Image', 'User Image', array('class' => 'form-label','for'=>'formFile')) !!}
                @if(isset($data))
                @else
{{--                    <span style="color: red">*</span>--}}
                @endif

                <div class="mb-3">
                    @if(isset($data))
                        <input class="form-control" accept="image/*" name="user_image" type="file" id="file" onchange="loadFile(event)">
                    @else
                        <input class="form-control" accept="image/*" name="user_image" type="file" id="file" onchange="loadFile(event)">

                    @endif

                    <div style="text-align: center;padding-top: 5px;">
                        @if($errors->first('user_image'))
                            <span style="color: #ff0000">{!! $errors->first('user_image') !!}</span>
                        @else
                            <img id="user_image" width="150" height="120"/>
                        @endif

                        @if(isset($data) && $data->user_image !='')
                            <img id="user_image" src="{{ asset('backend/image/UserImage').'/'.$data->user_image}}" width="150" height="120"/>
                        @endif

                    </div>
                </div>
            </div>


            <script>
                var loadFile = function(event) {
                    var image = document.getElementById('user_image');
                    image.src = URL.createObjectURL(event.target.files[0]);
                };
            </script>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 offset-md-4">
            <div class="from-group">
                <div class="from-group">
                    {!! Form::label('User Type', 'User Type', array('class' => 'form-label')) !!}
                    <span style="color: red">*</span>
                    <div class="input-group mb-3">
                        <?php
                            $UserType = array();
                            $UserType['Admin'] = 'Admin';
                            $UserType['Vendor'] = 'Vendor';
                            $UserType['Customer'] = 'Customer';
                            $UserType['Moderator'] = 'Moderator';
                            $UserType['Courier'] = 'Courier';
                        ?>
                        {!! Form::select('type',$UserType,Input::old('type'),['id'=>'type','class' => 'form-control','data-checkify'=>'required','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('type') !!}</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="from-group">
                <div class="from-group">
                    {!! Form::label('Status', 'Status', array('class' => 'form-label')) !!}
                    <span style="color: red">*</span>
                    <div class="input-group mb-3">
                        {!! Form::select('status',array(1=>'Active',0=>'Inactive'),Input::old('status'),['id'=>'status','class' => 'form-control','data-checkify'=>'required','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('ordering') !!}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="row">

        <div class="col-md-12" style="text-align: right;">
            <div class="from-group">
                <div class="">
                    <button type="reset" class="btn submit-button">Reset</button>
                    <button type="submit" class="btn submit-button">Submit</button>
                </div>
            </div>
        </div>

    </div>
