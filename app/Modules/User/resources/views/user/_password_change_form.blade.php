<?php
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request as Input;
?>


<div class="row">
    <div class="col-md-6">

        <div class="from-group">
            {!! Form::label('New Password', 'New Password', array('class' => 'form-label')) !!}
            <span style="color: red">*</span>
            <div class="input-group mb-3">
                {!! Form::text('password',Input::old('password'),['id'=>'password','class' => 'form-control','data-checkify'=>'minlen=6,required','Placeholder' => 'New Password','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
            </div>
            <span style="color: #ff0000">{!! $errors->first('password') !!}</span>
        </div>
    </div>

    <div class="col-md-6">

        <div class="from-group">
            {!! Form::label('Confirm Password', 'Confirm Password', array('class' => 'form-label')) !!}
            <span style="color: red">*</span>
            <div class="input-group mb-3">
                {!! Form::text('confirm',Input::old('confirm'),['id'=>'confirm','class' => 'form-control','data-checkify'=>'minlen=6,required','Placeholder' => 'Confirm Password','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
            </div>
            <span style="color: #ff0000">{!! $errors->first('confirm') !!}</span>
        </div>
    </div>
</div>




<div class="row">

    <div class="col-md-12" style="text-align: right;">
        <div class="from-group">
            <div class="">
                <button type="reset" class="btn submit-button">Reset</button>
                <button type="submit" class="btn submit-button">Submit</button>
            </div>
        </div>
    </div>

</div>
