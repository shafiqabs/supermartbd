<?php

namespace App\Modules\User\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class User extends Model
{
    use HasFactory;

    protected $table = 'users';
    protected $fillable = [
        'name',
        'email',
        'mobile',
        'address',
        'password',
        'user_image',
        'type',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'status',
        'resetcode'
    ];

    public function UserVendor(){
        return $this->hasOne('App\Modules\Vendor\Models\Vendor','user_id','id');
    }

    public function UserCustomer(){
        return $this->hasOne('App\Modules\Customer\Models\Customer','user_id','id');
    }



    // TODO :: boot
    // boot() function used to insert logged user_id at 'created_by' & 'updated_by'
    public static function boot(){
        parent::boot();
        static::creating(function($query){
            if(Auth::check()){
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function($query){
            if(Auth::check()){
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}
