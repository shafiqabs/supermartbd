<?php
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request as Input;

?>

    <div class="row">
        <div class="col-md-12">
            <div class="row">


                <div class="col-md-6">
                    <div class="from-group">
                        {!! Form::label('Banner Title', 'Banner Title', array('class' => 'form-label')) !!}
                        <span style="color: red">*</span>
                        <div class="input-group mb-3">
                            {!! Form::text('title',Input::old('title'),['id'=>'title','class' => 'form-control','data-checkify'=>'minlen=3,required','Placeholder' => 'Enter title','aria-label' =>'content','aria-describedby'=>'basic-addon2']) !!}
                        </div>
                        <span style="color: #ff0000">{!! $errors->first('title') !!}</span>
                    </div>
                </div>



                <div class="col-md-6">
                    <div class="from-group">
                        {!! Form::label('Banner Position', 'Banner Position', array('class' => 'form-label')) !!}
                        <span style="color: red">*</span>
                        <div class="input-group mb-3">
                            <?php
                            $BannerPosition = array();
                            $BannerPosition[''] = 'Select Banner Position';
                            $BannerPosition['banner-top'] = 'Banner Top';
//                            $BannerPosition['banner-middle'] = 'Banner Middle';
//                            $BannerPosition['banner-bottom-up'] = 'Banner Bottom Up';
                            $BannerPosition['banner-bottom'] = 'Banner Bottom';
                            ?>
                            {!! Form::select('banner_position',$BannerPosition,Input::old('banner_position'),['id'=>'banner_position','class' => 'form-control form-select js-example-basic-single','data-checkify'=>'required','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
                        </div>
                        <span style="color: #ff0000">{!! $errors->first('banner_position') !!}</span>

                    </div>
                </div>


                <div class="col-md-6">
                    <div class="from-group">
                        {!! Form::label('Frontend Visibility', 'Frontend Visibility', array('class' => 'form-label')) !!}
                        <span style="color: red">*</span>
                        <div class="input-group mb-3">
                            <?php
                            $GroupData = array();
                            $GroupData[''] = 'Select Visibility';
                            $GroupData['Show'] = 'Show';
                            $GroupData['Hide'] = 'Hide';
                            ?>
                            {!! Form::select('banner_visibility',$GroupData,Input::old('banner_visibility'),['id'=>'banner_visibility','class' => 'form-control form-select js-example-basic-single','data-checkify'=>'required','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
                        </div>
                        <span style="color: #ff0000">{!! $errors->first('banner_visibility') !!}</span>

                    </div>
                </div>


{{--            <div class="col-md-6">--}}
{{--            <div class="from-group">--}}
{{--                {!! Form::label('Banner Visibility', 'Banner Visibility', array('class' => 'form-label')) !!}--}}
{{--                <span style="color: red">*</span>--}}
{{--                <div class="input-group mb-3">--}}
{{--                    <?php--}}
{{--                        $GroupData = array();--}}
{{--                    $GroupData[''] = 'Select Visibility';--}}
{{--                    $GroupData['Top Products'] = 'Top Products';--}}
{{--                    $GroupData['New Arrivals'] = 'New Arrivals';--}}
{{--                    $GroupData['Best Seller'] = 'Best Seller';--}}
{{--                    $GroupData['Deals Hot'] = 'Deals Hot';--}}
{{--                    $GroupData['Mega Sale'] = 'Mega Sale';--}}
{{--                    $GroupData['Flash Sale'] = 'Flash Sale';--}}
{{--                    $GroupData['Recommend'] = 'Recommend';--}}
{{--                    ?>--}}
{{--                    {!! Form::select('banner_tag',$GroupData,Input::old('banner_tag'),['id'=>'banner_tag','class' => 'form-control form-select js-example-basic-single','data-checkify'=>'required','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}--}}
{{--                </div>--}}
{{--                <span style="color: #ff0000">{!! $errors->first('banner_tag') !!}</span>--}}
{{--            </div>--}}
{{--            </div>--}}



            <div class="col-md-6">
                <div class="from-group">
                    {!! Form::label('Banner Items', 'Banner Items', array('class' => 'form-label')) !!}
                    <span style="color: red">*</span>
                    <div class="input-group mb-3">
                        <?php
                        $GroupData = array();
                        $GroupData[''] = 'Select Items';
                        $GroupData['2'] = '2';
                        $GroupData['3'] = '3';
                        $GroupData['4'] = '4';
                        ?>
                        {!! Form::select('banner_item',$GroupData,Input::old('banner_item'),['id'=>'banner_item','class' => 'form-control form-select js-example-basic-single','data-checkify'=>'required','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
                    </div>
                    <span style="color: #ff0000">{!! $errors->first('banner_item') !!}</span>
                </div>
            </div>



            </div>

{{--            <div class="row">--}}
{{--                <div class="col-md-6">--}}
{{--                    <div class="from-group">--}}
{{--                        {!! Form::label('Banner For Product', 'Banner For Product', array('class' => 'form-label')) !!}--}}
{{--                        <span style="color: red">*</span>--}}
{{--                        <div class="input-group mb-3">--}}
{{--                            {!! Form::select('product_id',$AllProduct,Input::old('product_id'),['id'=>'product_id','class' => 'form-control form-select js-example-basic-single','data-checkify'=>'required','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}--}}
{{--                        </div>--}}
{{--                        <span style="color: #ff0000">{!! $errors->first('product_id') !!}</span>--}}

{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-md-6">--}}
{{--                    <div class="from-group">--}}
{{--                        {!! Form::label('Banner Image', 'Banner Image', array('class' => 'form-label','for'=>'formFile')) !!}--}}


{{--                        <div class="mb-3">--}}
{{--                            @if(isset($data))--}}
{{--                                <input class="form-control" accept="image/*" name="banner_image" type="file" id="file" onchange="loadFile(event)">--}}
{{--                            @else--}}
{{--                                <input required class="form-control" accept="image/*" name="banner_image" type="file" id="file" onchange="loadFile(event)">--}}

{{--                            @endif--}}

{{--                            <div style="text-align: center;padding-top: 5px;">--}}
{{--                                @if($errors->first('banner_image'))--}}
{{--                                    <span style="color: #ff0000">{!! $errors->first('banner_image') !!}</span>--}}
{{--                                @else--}}
{{--                                    <img id="banner_image" width="150" height="120"/>--}}
{{--                                @endif--}}

{{--                                @if(isset($data) && $data->feature_image !='')--}}
{{--                                    <img id="banner_image" src="{{ asset('backend/image/Banner').'/'.$data->banner_image}}" width="150" height="120"/>--}}
{{--                                @endif--}}

{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}


{{--                    <script>--}}
{{--                        var loadFile = function(event) {--}}
{{--                            var image = document.getElementById('banner_image');--}}
{{--                            image.src = URL.createObjectURL(event.target.files[0]);--}}
{{--                        };--}}
{{--                    </script>--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 offset-md-8">
            <div class="from-group">
                <div class="from-group">
                    {!! Form::label('Status', 'Status', array('class' => 'form-label')) !!}
                    <span style="color: red">*</span>
                    <div class="input-group mb-3">
                        {!! Form::select('status',array(1=>'Active',0=>'Inactive'),Input::old('status'),['id'=>'ordering','class' => 'form-control','data-checkify'=>'minlen=1,required,number','Placeholder' => 'Enter ordering number','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('ordering') !!}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="row">

        <div class="col-md-12" style="text-align: right;">
            <div class="from-group">
                <div class="">
                    <button type="reset" class="btn submit-button">Reset</button>
                    <button type="submit" class="btn submit-button">Submit</button>
                </div>
            </div>
        </div>

    </div>
