<?php
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request as Input;


?>
<tr sl-no="{{$sl_no}}" id="delete_row_{{$sl_no}}">

    <td width="15%">

        <?php
        $BannerType = array();
        $BannerType[''] = 'Select Banner Type';
        $BannerType['Category'] = 'Category';
        $BannerType['Brand'] = 'Brand';
        ?>
        {!! Form::select('banner_type[]',$BannerType,Input::old('banner_type'),['id'=>'banner_type','sl-no'=>$sl_no,'class' => 'form-control form-select js-example-basic-single','data-checkify'=>'required','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
    </td>

    <td width="20%">

        <span style="display: none" class="banner_show_{{$sl_no}}">
        {!! Form::select('brand_id[]',$AllBrand,Input::old('brand_id'),['id'=>'brand_id'.$sl_no,'class' => 'form-control form-select js-example-basic-single','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
    </span>
<span style="display: none" class="category_show_{{$sl_no}}">
        {!! Form::select('category_id[]',$Category,Input::old('category_id'),['id'=>'category_id'.$sl_no,'class' => 'form-control form-select js-example-basic-single','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}

    </span>
  </td>


    <td width="20%">

        {!! Form::text('title_top[]',Input::old('title_top'),['id'=>'title_top','class' => 'form-control ','data-checkify'=>'required','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
    </td>

    <td width="20%">

        {!! Form::text('title[]',Input::old('title'),['id'=>'title','class' => 'form-control','data-checkify'=>'required','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
    </td>

    <td width="20%">

        <div style="position:relative;border: 1px solid #e6e0e0;">
            <a class='btn btn-primary btn-sm font-10' href='javascript:;'>
                Choose File...
                <input name="attach_link[]" type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file_source" size="40"  onchange='$("#upload-file-info{{$sl_no}}").html($(this).val());'>
            </a>
            &nbsp;
            <span style="color: red">{!! $errors->first('attach_link[]') !!}</span>
            <span class='label label-info' id="upload-file-info{{$sl_no}}"></span>
        </div>
    </td>

    <td width="2%" style="color: red">
    <i class="fa fa-trash btn btn-danger" id="delete_click" sl_no={{$sl_no}} aria-hidden="true" style="text-align: right;cursor: pointer;"></i>
    </td>
</tr>
