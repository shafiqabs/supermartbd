<?php
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request as Input;

?>

    <div class="row">
        <div class="col-md-12">
            <div class="row">

                <div class="col-md-6">
                    <div class="from-group">
                        {!! Form::label('Select Banner', 'Select Banner', array('class' => 'form-label')) !!}
                        <span style="color: red">*</span>
                        <div class="input-group mb-3">
                            {!! Form::select('banner_id',$AllBanner,Input::old('banner_id'),['id'=>'banner_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
                        </div>
                        <span style="color: #ff0000">{!! $errors->first('banner_id') !!}</span>

                    </div>
                </div>

            </div>


            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive" style="overflow-x: clip;">
                        <table class="table table-bordered table-striped ">
                            <thead style="border-style: hidden !important;">
                            <tr>
                                <th style="text-align: right;" colspan="6">
                                    <a style="display: none" id="banneritemroute" data-href="{{route('admin.banneritem.moreitems')}}"></a>
                                    <p class="btn btn-primary" sl-no="1" id="BannerMoreItems">+ More Items</p>
                                </th>
                            </tr>
                            </thead>
                            <tbody id="add_more_attach_row">
                            <tr sl-no="1">
                                <td width="15%">
                                    {!! Form::label('Banner Type', 'Banner Type', array('class' => 'form-label')) !!}
                                    <span style="color: red">*</span>
                                        <?php
                                        $BannerType = array();
                                        $BannerType[''] = 'Select Type';
                                        $BannerType['Category'] = 'Category';
                                        $BannerType['Brand'] = 'Brand';
                                        ?>
                                        {!! Form::select('banner_type[]',$BannerType,Input::old('banner_type'),['id'=>'banner_type','sl-no'=>'1','class' => 'form-control ','data-checkify'=>'required','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
                                    <span style="color: red">{!! $errors->first('banner_type') !!}</span>

                                </td>

                                <td width="20%">
                                    {!! Form::label('Banner For', 'Banner For', array('class' => 'form-label')) !!}
                                    <span style="color: red">*</span>
                                    <span style="display: none" class="banner_show_1"><br>
                                        {!! Form::select('brand_id[]',$AllBrand,Input::old('brand_id'),['id'=>'brand_id1','class' => 'form-control ','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
                                    </span>
                                    <span style="display: none" class="category_show_1"><br>
                                        {!! Form::select('category_id[]',$Category,Input::old('category_id'),['id'=>'category_id1','class' => 'form-control','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
                                    </span>
                                 </td>


                                <td width="20%">
                                        {!! Form::label('Tag', 'Tag', array('class' => 'form-label')) !!}
                                        <span style="color: red">*</span>
                                        {!! Form::text('title_top[]',Input::old('title_top'),['id'=>'title_top','class' => 'form-control','data-checkify'=>'required','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
                                    <span style="color: red">{!! $errors->first('title_top') !!}</span>
                                </td>

                                <td width="20%">
                                    {!! Form::label(' Title', ' Title', array('class' => 'form-label')) !!}
                                    <span style="color: red">*</span>
                                    {!! Form::text('title[]',Input::old('title'),['id'=>'title','class' => 'form-control','data-checkify'=>'required','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
                                    <span style="color: red">{!! $errors->first('title') !!}</span>
                                </td>

                                <td width="20%">
                                    {!! Form::label(' Banner Image', ' Banner Image', array('class' => 'form-label')) !!}
                                    <span style="color: red">*</span>

                                    <div style="position:relative;border: 1px solid #e6e0e0;">
                                        <a class='btn btn-primary btn-sm font-10' href='javascript:;'>
                                            Choose File...
                                            <input name="attach_link[]" type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file_source" size="40"  onchange='$("#upload-file-info1").html($(this).val());'>
                                        </a>
                                        &nbsp;
                                        <span style="color: red">{!! $errors->first('attach_link') !!}</span>
                                        <span class='label label-info' id="upload-file-info1"></span>
                                    </div>
                                </td>
                                <td width="2%"></td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>




        </div>
    </div>

    <div class="row">
        <div class="col-md-4 offset-md-8">
            <div class="from-group">
                <div class="from-group">
                    {!! Form::label('Status', 'Status', array('class' => 'form-label')) !!}
                    <span style="color: red">*</span>
                    <div class="input-group mb-3">
                        {!! Form::select('status',array(1=>'Active',0=>'Inactive'),Input::old('status'),['id'=>'status','class' => 'form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('status') !!}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="row">

        <div class="col-md-12" style="text-align: right;">
            <div class="from-group">
                <div class="">
                    <button type="reset" class="btn submit-button">Reset</button>
                    <button type="submit" class="btn submit-button">Submit</button>
                </div>
            </div>
        </div>

    </div>
