@extends(/** @lang text */'backend.layouts.master')

@section('body')
    <div class="dashboard-area">
        <div id="carbon-block" class="">
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">{{$ModuleTitle}}</h1>

                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group me-2">
                            <a style="color: #000;" href="{{route('admin.banneritem.create')}}" title="Add Banner" class="module_button_header">
                                <button type="button" class="btn btn-sm btn-outline-secondary">
                                    <i class="fas fa-plus-circle"></i> Create Banner Items
                                </button>
                            </a>

                            <a style="color: #000;" href="{{route('admin.banner.create')}}" title="Add Banner" class="module_button_header">
                                <button type="button" class="btn btn-sm btn-outline-secondary">
                                    <i class="fas fa-plus-circle"></i> Create New Banner
                                </button>
                            </a>
                        </div>




                    </div>
                </div>
            </main>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">
                            {{$PageTitle}}
                        </div>
                        <div class="card-body">

                            @include('backend.layouts.message')

                            @if(isset($AllBanner) && !empty($AllBanner))
                                <div class="table-responsive">
                                    <table  class="table table-striped table-bordered text-center" id="">
                                        <thead>
                                        <th>SL</th>
                                        <th>Title</th>
                                        <th>Position</th>
                                        <th>Visibility</th>
                                        <th>Items</th>
                                        <th>Action</th>
                                        </thead>

                                        <tbody>
                                        <?php $i=1; ?>
                                        @foreach($AllBanner as $value)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$value->title}}</td>
                                                <td>{{$value->banner_position}}</td>
                                                <td>{{$value->banner_visibility}}</td>
                                                <td>{{$value->banner_item}}</td>

                                                <td>
                                                    <a href="{{route('admin.banner.edit',$value->id)}}" title="Edit Banner"><i class="fas fa-user-edit"></i></a> |
                                                    <a href="{{route('admin.banner.delete',$value->id)}}" title="Delete Banner" onclick="return confirm('Are you sure to Delete?')"><i class="fas fa-trash-alt"></i></a>
                                                    @if(count($value->BannerItems) != 0)
                                                        <a style="color: red;" href="{{route('admin.banneritem.edit',$value->id)}}" title="Edit Banner Items"><i class="far fa-edit"></i></a>
                                                        <a style="color: red;" href="{{route('admin.banneritem.delete',$value->id)}}" title="Delete Banner Items" onclick="return confirm('Are you sure to Delete?')"><i class="fas fa-eraser"></i></a>
                                                    @endif

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class=" justify-content-right">
                                    {{ $AllBanner->links('backend.layouts.pagination') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>


@endsection
