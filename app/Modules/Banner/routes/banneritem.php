<?php

Route::get('admin-banneritem-create', [
    'as' => 'admin.banneritem.create',
    'uses' => 'BannerItemController@create'
]);

Route::get('admin-banneritem-index', [
    'as' => 'admin.banneritem.index',
    'uses' => 'BannerItemController@index'
]);

Route::get('admin-banneritem-moreitems', [
    'as' => 'admin.banneritem.moreitems',
    'uses' => 'BannerItemController@moreitems'
]);

Route::post('admin-banneritem-store',[
    'as' => 'admin.banneritem.store',
    'uses' => 'BannerItemController@store'
]);

Route::get('admin-banneritem-edit/{id}',[
    'as' => 'admin.banneritem.edit',
    'uses' => 'BannerItemController@edit'
]);

Route::PATCH('admin-banneritem-update/{id}',[
    'as' => 'admin.banneritem.update',
    'uses' => 'BannerItemController@update'
]);

Route::get('admin-banneritem-delete/{id}',[
    'as' => 'admin.banneritem.delete',
    'uses' => 'BannerItemController@delete'
]);
