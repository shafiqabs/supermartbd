<?php

use Illuminate\Support\Facades\Route;

//Route::get('banner', 'BannerController@welcome');
Route::group(['module' => 'Banner', 'middleware' => ['web','auth','adminmiddleware']], function() {
    include 'banner.php';
    include 'banneritem.php';
});
