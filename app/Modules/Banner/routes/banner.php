<?php

Route::get('admin-banner-create', [
    'as' => 'admin.banner.create',
    'uses' => 'BannerController@create'
]);

Route::get('admin-banner-index', [
    'as' => 'admin.banner.index',
    'uses' => 'BannerController@index'
]);

Route::post('admin-banner-store',[
    'as' => 'admin.banner.store',
    'uses' => 'BannerController@store'
]);

Route::get('admin-banner-edit/{id}',[
    'as' => 'admin.banner.edit',
    'uses' => 'BannerController@edit'
]);

Route::PATCH('admin-banner-update/{id}',[
    'as' => 'admin.banner.update',
    'uses' => 'BannerController@update'
]);

Route::get('admin-banner-delete/{id}',[
    'as' => 'admin.banner.delete',
    'uses' => 'BannerController@delete'
]);
