<?php

namespace App\Modules\Banner\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Banner\Models\Banner;
use App\Modules\Banner\Models\BannerItems;
use App\Modules\Product\Models\Product;
use App\Modules\MasterData\Models\Category;
use App\Modules\MasterData\Models\ProductBrand;
use App\Modules\Banner\Requests;


use DB;
use Mockery\CountValidator\Exact;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class BannerItemController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $ModuleTitle = "Manage Banner Item Information ";
        $PageTitle = " Banner Item Information list";
        $TableTitle = "Banner Item Information list";

        $AllBanner = Banner::where('status',1)->orderby('id','desc')->paginate(10);

        return view("Banner::banneritem.index", compact('ModuleTitle','PageTitle','TableTitle','AllBanner'));
    }


    public function create(){
        $ModuleTitle = "Manage Banner Item Information ";
        $PageTitle = "Add Banner Item Information";
        $TableTitle = "Banner Item Information list";

        $AllBanner= Banner::where('status','1')->orderby('title','asc')
            ->select('id', DB::raw("concat(title,' ( ',banner_position,', ',banner_visibility,')') as name"))
                                            ->pluck('name','id')
                                            ->all();
        $AllBanner[''] = 'Choose Banner';
        ksort($AllBanner);

        $Category = Category::getHierarchyCategory();
        $Category[''] = 'Choose Category';
        ksort($Category);

        $AllBrand = ProductBrand::where('status','1')->pluck('name','id')->all();
        $AllBrand[''] = 'Choose Brand';
        ksort($AllBrand);

        return view("Banner::banneritem.create", compact('AllBrand','Category','ModuleTitle','PageTitle','TableTitle','AllBanner'));
    }


    public function moreitems(){
        $sl_no = $_GET['sl_no'];


        $Category = Category::getHierarchyCategory();
        $Category[''] = 'Choose Category';
        ksort($Category);

        $AllBrand = ProductBrand::where('status','1')->pluck('name','id')->all();
        $AllBrand[''] = 'Choose Brand';
        ksort($AllBrand);

        $view = \Illuminate\Support\Facades\View::make('Banner::banneritem._more_items_form',compact('sl_no','Category','AllBrand'));
        $response['sl_no'] = $sl_no++;
        $contents = $view->render();
        $response['content'] = $contents;

        return $response;
    }


    public function store(Requests\BannerItemsRequest $request){
        $input = $request->all();

        if (isset($input['banner_type'])){
            foreach ($input['banner_type'] as $key => $value){
                $key1 = $key+1;
                if ($value == 'Category'){
                    if ($input['category_id'][$key] != ''){
                        $validation = true;
                    }else{
                        $validation = false;
                        $message = "Select Banner For (Category) at row ".$key1;
                    }
                }

                if ($value == 'Brand'){
                    if ($input['brand_id'][$key] != ''){
                        $validation = true;
                    }else{
                        $validation = false;
                        $message = "Select Banner For (Brand) at row ".$key1;
                    }
                }
            }
        }

        if ($validation){
            /* Transaction Start Here */
            DB::beginTransaction();
            try {

                $img_sl = 1;
                if ($request->file('attach_link') != '') {
                    foreach($request->file('attach_link') as $index => $value) {
                        $avatar = $request->file('attach_link')[$index];

                        $file_title = 'img'.rand(1,10000).$img_sl.'-'.time().'.'.$avatar->getClientOriginalExtension();
                        $image['attach_link'] = $file_title;
                        $path = public_path("backend/image/BannerImage/");
                        $target_file =  $path.basename($file_title);
                        $file_path = $_FILES['attach_link']['tmp_name'][$index];
                        $result = move_uploaded_file($file_path,$target_file);

                        $image['banner_id'] = $input['banner_id'];
                        $image['brand_id'] = $input['brand_id'][$index];
                        $image['category_id'] = $input['category_id'][$index];
                        $image['banner_type'] = $input['banner_type'][$index];
                        $image['title_top'] = $input['title_top'][$index];
                        $image['title'] = $input['title'][$index];
                        $image['status'] = $input['status'];

                        BannerItems::create($image);
                        $img_sl++;
                    }
                }

                DB::commit();

                Session::flash('message', 'Information Updated Successfully!');
                return redirect()->route('admin.banneritem.index');
            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', $message);
            return redirect()->back()->withInput($input);
        }
    }




    public function edit($id){
        $ModuleTitle = "Manage Banner Items Information";
        $PageTitle = "Update Banner Items Information";

        $data = Banner::where('status','1')->where('id',$id)->first();

        $AllBanner= Banner::where('status','1')->orderby('title','asc')
            ->select('id', DB::raw("concat(title,' ( ',banner_position,', ',banner_visibility,')') as name"))
            ->pluck('name','id')
            ->all();
        $AllBanner[''] = 'Choose Banner';
        ksort($AllBanner);

        $Category = Category::getHierarchyCategory();
        $Category[''] = 'Choose Category';
        ksort($Category);

        $AllBrand = ProductBrand::where('status','1')->pluck('name','id')->all();
        $AllBrand[''] = 'Choose Brand';
        ksort($AllBrand);

        return view("Banner::banneritem.edit", compact('AllBrand','Category','AllBanner','data','ModuleTitle','PageTitle'));
    }


    public function update(Requests\UpdateBannerItemsRequest $request,$id){
        $input = $request->all();

        /* Transaction Start Here */
        DB::beginTransaction();
        try {
            $img_sl = 1;
            foreach ($input['banner_type'] as $key => $value){
                $UpdateModel = BannerItems::where('id',$input['itemsid'][$key])->first();
                if ($value == 'Category'){
                    $UpdateData['category_id'] = $input['category_id'][$key];
                    $UpdateData['brand_id'] = null;
                }else{
                    $UpdateData['brand_id'] = $input['brand_id'][$key];
                    $UpdateData['category_id'] = null;
                }

                $UpdateData['banner_type'] = $input['banner_type'][$key];
                $UpdateData['title_top'] = $input['title_top'][$key];
                $UpdateData['title'] = $input['title'][$key];
                $UpdateData['status'] = $input['status'];

                if ($request->file('attach_link') != '') {
                    File::delete(public_path().'/backend/image/BannerImage/'.$UpdateModel->attach_link);
                    $avatar = $request->file('attach_link')[$key];
                    $file_title = 'img'.rand(1,10000).$img_sl.'-'.time().'.'.$avatar->getClientOriginalExtension();
                    $UpdateData['attach_link'] = $file_title;
                    $path = public_path("backend/image/BannerImage/");
                    $target_file =  $path.basename($file_title);
                    $file_path = $_FILES['attach_link']['tmp_name'][$key];
                    $result = move_uploaded_file($file_path,$target_file);
                }else{
                    $UpdateData['attach_link'] = $UpdateModel['attach_link'];
                }
//                echo '<pre>';
//                print_r($UpdateData);
                $UpdateModel->update($UpdateData);
                $UpdateModel->save();
                $img_sl++;
            }
//            exit();
            DB::commit();

            Session::flash('message', 'Information Updated Successfully!');
//            return redirect()->route('admin.banneritem.index');
            return redirect()->back();
        } catch (\Exception $e) {

            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }



        exit();

//        if(isset($input['banner_type'])){
//            foreach ($input['banner_type'] as $key =>$value){
//                $key1 = $key+1;
//                if ($input['banner_type'][$key] == 'Category') {
////                    echo $input['itemsid'][$key] . ' ' . $input['banner_type'][$key] . ' ' . $input['category_id'][$key];
////                    echo '<br>';
//                    if ($input['category_id'][$key] != ''){
//                        $validation = true;
//                    }else{
//                        $validation = false;
//                        $message = "Select Banner For (Category) at row ".$key1;
//                    }
//                }else{
////                    echo $input['itemsid'][$key] . ' ' . $input['banner_type'][$key] . ' ' . $input['brand_id'][$key];
////                    echo '<br>';
//                    if ($value == 'Brand'){
//                    if ($input['brand_id'][$key] != ''){
//                        $validation = true;
//                    }else{
//                        $validation = false;
//                        $message = "Select Banner For (Brand) at row ".$key1;
//                    }
//                }
//                }
//
//            }
//        }



//        if ($validation){
//            echo '<pre>';
//            print_r($input);
            /* Transaction Start Here */
            DB::beginTransaction();
            try {


//                $img_sl = 1;
//                if ($request->file('attach_link') != '') {
//                    foreach($request->file('attach_link') as $index => $value) {
//                        $avatar = $request->file('attach_link')[$index];
//
//                        $file_title = 'img'.rand(1,10000).$img_sl.'-'.time().'.'.$avatar->getClientOriginalExtension();
//                        $image['attach_link'] = $file_title;
//                        $path = public_path("backend/image/BannerImage/");
//                        $target_file =  $path.basename($file_title);
//                        $file_path = $_FILES['attach_link']['tmp_name'][$index];
//                        $result = move_uploaded_file($file_path,$target_file);
//
//                        $image['banner_id'] = $input['banner_id'];
//                        $image['brand_id'] = $input['brand_id'][$index];
//                        $image['category_id'] = $input['category_id'][$index];
//                        $image['banner_type'] = $input['banner_type'][$index];
//                        $image['title_top'] = $input['title_top'][$index];
//                        $image['title'] = $input['title'][$index];
//                        $image['status'] = $input['status'];
//
//                        BannerItems::create($image);
//                        $img_sl++;
//                    }
//                }

                DB::commit();

                Session::flash('message', 'Information Updated Successfully!');
                return redirect()->route('admin.banneritem.index');
            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
//        }else{
//            Session::flash('validate', $message);
//            return redirect()->back()->withInput($input);
//        }
    }

    public function delete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {

            $DeleteModel = Banner::where('id', $id)
                ->select('*')
                ->first();
            $DeleteModel->delete();
            Session::flash('delete', 'Delete Successfully !');

            DB::commit();
            return redirect('admin-banner-index');
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }
}
