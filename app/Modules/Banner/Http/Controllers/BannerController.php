<?php

namespace App\Modules\Banner\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Banner\Models\Banner;
use App\Modules\Product\Models\Product;
use App\Modules\Banner\Requests;


use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class BannerController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $ModuleTitle = "Manage Banner Information ";
        $PageTitle = " Banner Information list";
        $TableTitle = "Banner Information list";

        $AllBanner = Banner::where('status',1)->orderby('id','desc')->paginate(10);

        return view("Banner::banner.index", compact('ModuleTitle','PageTitle','TableTitle','AllBanner'));
    }


    public function create(){
        $ModuleTitle = "Manage Banner Information ";
        $PageTitle = "Add Banner Information";
        $TableTitle = "Banner Information list";

        return view("Banner::banner.create", compact('ModuleTitle','PageTitle','TableTitle'));
    }


    public function store(Requests\BannerRequest $request){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['title']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);
        $input['slug'] = $slug;

        $BannerExistsOrNot = Banner::where('banner_position',$input['banner_position'])
                                                ->where('banner_visibility','Show')
                                                ->count();
        $SlugExistsOrNot = Banner::where('banner_position',$input['banner_position'])
                                                ->where('slug',$slug)
                                                ->count();

        if ($BannerExistsOrNot == 0 || ($BannerExistsOrNot == 1 && $input['banner_visibility'] == 'Hide')) {
            if ($SlugExistsOrNot == 0) {
                /* Transaction Start Here */
                DB::beginTransaction();
                try {
                    // Store brand data
                    if ($BannerData = Banner::create($input)) {
                        $BannerData->save();
                    }

                    DB::commit();
                    Session::flash('message', 'Information added Successfully');
                    return redirect()->route('admin.banner.index');
                } catch (\Exception $e) {
                    //If there are any exceptions, rollback the transaction`
                    DB::rollback();
                    print($e->getMessage());
                    exit();
                    Session::flash('danger', $e->getMessage());
                }
            }else{
                Session::flash('validate', 'Title already exists to this position ');
                return redirect()->back()->withInput($input);
            }
        }else{
            Session::flash('validate', 'Banner position already exists , frontend visibility hide then try');
            return redirect()->back()->withInput($input);
        }
    }


    public function edit($id){
        $ModuleTitle = "Manage Banner Information";
        $PageTitle = "Update Banner Information";

        $data = Banner::where('status','1')->where('id',$id)->first();

        return view("Banner::banner.edit", compact('data','ModuleTitle','PageTitle'));
    }


    public function update(Requests\BannerRequest $request,$id){
        $input = $request->all();

        $slug = str_replace(' ', '-', $input['title']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);
        $input['slug'] = $slug;

        $BannerExistsOrNot = Banner::where('banner_position',$input['banner_position'])
                                    ->where('banner_visibility','Show')
                                    ->count();

        $BannerToId = Banner::where('banner_position',$input['banner_position'])
                                    ->where('banner_visibility','Show')
                                    ->first();

        if ($BannerExistsOrNot == 0){
            $valdation = true;
        }elseif($BannerExistsOrNot == 1 && $id == $BannerToId->id) {
            $valdation = true;
        }elseif($BannerExistsOrNot == 1 && $id != $BannerToId->id && $input['banner_visibility'] == 'Hide'){
            $valdation = true;
        }else{
            $valdation = false;
            $message = 'Banner position already exists , Select visibility hide then try!';
        }

        if ($valdation){
            $SlugExistsOrNot = Banner::where('banner_position',$input['banner_position'])
                                            ->where('slug',$slug)
                                            ->count();
            $Slugtoid = Banner::where('banner_position',$input['banner_position'])
                                            ->where('slug',$slug)
                                            ->first();

            if ($SlugExistsOrNot == 0 ){
                $valdation = true;
            }elseif($SlugExistsOrNot == 1 && $id == $Slugtoid->id){
                $valdation = true;
            }else{
                $valdation = false;
                $message = 'Title already exists to this position !';
            }
        }

        if ($valdation){
            $UpdateModel = Banner::where('id',$id)->first();
            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // update page data
                $result = $UpdateModel->update($input);
                $UpdateModel->save();

                DB::commit();

                Session::flash('message', 'Information Updated Successfully!');
                return redirect()->route('admin.banner.index');

            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', $message);
            return redirect()->back()->withInput($input);
        }
    }

    public function delete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {

            $DeleteModel = Banner::where('id', $id)
                ->select('*')
                ->first();
            $DeleteModel->delete();
            Session::flash('delete', 'Delete Successfully !');

            DB::commit();
            return redirect('admin-banner-index');
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }
}
