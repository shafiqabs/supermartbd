<?php

namespace App\Modules\Banner\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App;
use Auth;

class Banner extends Model
{
    use HasFactory;
    protected $table = 'ems_banner';
    protected $fillable = [
        'title',
        'slug',
        'banner_position',
        'banner_visibility',
        'banner_item',
        'status',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    public function BannerItems(){
        return $this->hasMany('App\Modules\Banner\Models\BannerItems','banner_id','id');
    }

    public function TopBannerItems(){
        return $this->hasMany('App\Modules\Banner\Models\BannerItems','banner_id','id')->take(3)->inRandomOrder();
    }

    public function BottomBannerItems(){
        return $this->hasMany('App\Modules\Banner\Models\BannerItems','banner_id','id')->take(2)->inRandomOrder();
    }

    // TODO :: boot
    // boot() function used to insert logged user_id at 'created_by' & 'updated_by'
    public static function boot(){
        parent::boot();
        static::creating(function($query){
            if(Auth::check()){
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function($query){
            if(Auth::check()){
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}
