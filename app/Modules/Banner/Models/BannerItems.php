<?php

namespace App\Modules\Banner\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App;
use Auth;

class BannerItems extends Model
{
    use HasFactory;
    protected $table = 'ems_banner_items';
    protected $fillable = [
        'banner_id',
        'brand_id',
        'category_id',
        'banner_type',
        'title_top',
        'title',
        'attach_link',
        'status',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    public function BannerCategorySlug(){
        return $this->belongsTo('App\Modules\MasterData\Models\Category','category_id','id');
    }

//    public function VendorDetails(){
//        return $this->belongsTo('App\Modules\Vendor\Models\Vendor','vendor_id','id');
//    }

    // TODO :: boot
    // boot() function used to insert logged user_id at 'created_by' & 'updated_by'
    public static function boot(){
        parent::boot();
        static::creating(function($query){
            if(Auth::check()){
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function($query){
            if(Auth::check()){
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}
