<?php

namespace App\Modules\Banner\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class BannerRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'banner_position'       => 'required',
            'title'       => 'required',
            'banner_visibility'       => 'required',
            'banner_item' => 'required'
//            'banner_image'       => 'required|image|mimes:jpeg,JPEG,Jpeg,PNG,Png,png,jpg,JPG,Jpg',
        ];

    }

}
