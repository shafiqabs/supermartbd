<?php

namespace App\Modules\Banner\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class UpdateBannerItemsRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'banner_id'       => 'required',
            'banner_type'       => 'required',
//            'brand_id | category_id'       => 'required',
//            'banner_item' => 'required',
            'title_top' => 'required',
            'title' => 'required',
//            'attach_link'       => 'required'
        ];

    }

}
