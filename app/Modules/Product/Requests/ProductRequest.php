<?php

namespace App\Modules\Product\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class ProductRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name'       => 'required',
            'purchaseprice'       => 'required',
            'salesprice'       => 'required',
            'category_id'       => 'required',
            'brand_id'       => 'required',
            'quantity'       => 'required|integer',
            'minquantity'       => 'required|integer',
            'maxquantity'       => 'required|integer',
            'feature_image'=> 'image|mimes:jpeg,JPEG,Jpeg,PNG,Png,png,jpg,JPG,Jpg',
        ];

    }

}
