<?php

namespace App\Modules\Product\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
Use Auth;

class Product extends Model
{
    use HasFactory;

    protected $table = 'ems_product';
    protected $fillable = [
        'vendor_id','name', 'name_bn', 'slug', 'sub_item', 'brand_id', 'category_id', 'item_unit_id', 'size_unit_id', 'size_weight_dimen_id', 'country_id', 'discount_id', 'warning_id', 'item_assurance_id', 'promotion_id', 'quantity', 'minquantity', 'maxquantity', 'salesprice', 'purchaseprice', 'discription', 'feature_image', 'feature_tag', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'
    ];

    public function ProductImage(){
        return $this->hasMany('App\Modules\Product\Models\ProductImage','product_id','id');
    }

    public function ProductAdditionalField(){
        return $this->hasMany('App\Modules\Product\Models\ProductAdditionalField','product_id','id');
    }

    public function ProductCategory(){
        return $this->belongsTo('App\Modules\MasterData\Models\Category','category_id','id');
    }

    public function ProductBrand(){
        return $this->belongsTo('App\Modules\MasterData\Models\ProductBrand','brand_id','id');
    }

    public function ProductVendor(){
        return $this->belongsTo('App\Modules\Vendor\Models\Vendor','vendor_id','id');
    }

//    public function ProductBrand1(){
//        return $this->hasMany('App\Modules\MasterData\Models\ProductBrand','brand_id','id');
//    }



    // TODO :: boot
    // boot() function used to insert logged user_id at 'created_by' & 'updated_by'
    public static function boot(){
        parent::boot();
        static::creating(function($query){
            if(Auth::check()){
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function($query){
            if(Auth::check()){
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}
