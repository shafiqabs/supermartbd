<?php

namespace App\Modules\Product\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App;
use Auth;

class ProductTag extends Model
{
    use HasFactory;

    protected $table = 'ems_product_tags';
    protected $fillable = [
        'product_id', 'tag_id', 'created_at', 'updated_at'
    ];

//    public function ProductPromotionTag(){
//        return $this->belongsToMany('App\Modules\MasterData\Models\ProductPromotion','ems_product_tags','tag_id');
//    }
}
