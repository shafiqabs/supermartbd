<?php

namespace App\Modules\Product\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductFile extends Model
{
    use HasFactory;

    protected $table = 'ems_product';
    protected $guarded = array();

    public function ProductBrand(){
        return $this->belongsTo('App\Modules\MasterData\Models\ProductBrand','brand_id','id');
    }

    public function ProductCategory(){
        return $this->belongsTo('App\Modules\MasterData\Models\Category','category_id','id');
    }

    public function ProductItemUnit(){
        return $this->belongsTo('App\Modules\MasterData\Models\ItemUnit','item_unit_id','id');
    }

    public function ProductSizeUnit(){
        return $this->belongsTo('App\Modules\MasterData\Models\Sizeunit','size_unit_id','id');
    }
}
