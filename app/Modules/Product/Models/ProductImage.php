<?php

namespace App\Modules\Product\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App;
use Auth;

class ProductImage extends Model
{
    use HasFactory;

    protected $table = 'ems_product_image';
    protected $fillable = [
       'product_id','img_level','attach_link','created_at','updated_at'
    ];

}
