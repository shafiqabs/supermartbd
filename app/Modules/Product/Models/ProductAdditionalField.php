<?php

namespace App\Modules\Product\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App;
use Auth;

class ProductAdditionalField extends Model
{
    use HasFactory;

    protected $table = 'ems_product_additionalfield';
    protected $fillable = [
        'product_id', 'level_name', 'level_value', 'created_at', 'updated_at'
    ];

}
