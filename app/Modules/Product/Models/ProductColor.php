<?php

namespace App\Modules\Product\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App;
use Auth;

class ProductColor extends Model
{
    use HasFactory;

    protected $table = 'ems_product_colors';
    protected $fillable = [
        'product_id', 'color_id', 'created_at', 'updated_at'
    ];

//    public function ProductBrand(){
//        return $this->belongsTo('App\Modules\MasterData\Models\ProductBrand','brand_id','id');
//    }
}
