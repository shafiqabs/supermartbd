<?php

namespace App\Modules\Product\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\MasterData\Models\Category;
use App\Modules\MasterData\Models\DiscountBrand;
use App\Modules\MasterData\Models\DiscountCategory;
use Illuminate\Http\Request;
use App\Modules\Product\Requests;
//use GuzzleHttp\Client;
//use Illuminate\Http\Client\Request;
use Illuminate\Support\Facades\Http;

use App\Modules\Product\Models\ProductFile;
use App\Modules\Product\Models\Product;
use App\Modules\Product\Models\ProductColor;
use App\Modules\Product\Models\ProductTag;
use App\Modules\Product\Models\ProductImage;
use App\Modules\Product\Models\ProductAdditionalField;
use App\Exports\FileExportExport;
use App\Imports\FileImportImort;

use App\Modules\MasterData\Models\ItemUnit;
use App\Modules\MasterData\Models\SizeWeightDimension;
use App\Modules\MasterData\Models\Sizeunit;
use App\Modules\MasterData\Models\ProductBrand;
use App\Modules\MasterData\Models\Country;
use App\Modules\MasterData\Models\Color;
use App\Modules\MasterData\Models\Discount;
use App\Modules\MasterData\Models\WarningType;
use App\Modules\MasterData\Models\ItemAssurance;
use App\Modules\MasterData\Models\ProductPromotion;
use App\Modules\Vendor\Models\Vendor;

use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class ProductController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */
    public function filecreate()
    {
        $ModuleTitle = "Manage Product File Import";
        $PageTitle = "Add Product File Import";
        $TableTitle = "Product File Import list";

        return view("Product::productfile.create", compact('ModuleTitle','PageTitle','TableTitle'));
    }

    public function filestore(Request $request){

        \Excel::import(new FileImportImort,$request->import_file);
//        if ($result){
//            print_r($result) ;
//        }
//        echo Session::get('error');

        \Session::flash('message', 'Your file is imported successfully in database.');

        return redirect()->route('admin.product.fileindex');

    }

    public function fileindex(){
        $ModuleTitle = "Manage Product File Import";
        $PageTitle = "list Of Product Information ";
        $TableTitle = "list Of Product Information";

        if (Auth::user()->type == 'Admin') {
            $ProductData = ProductFile::orderby('id', 'desc')->Paginate(10);
            $CountProductData = count($ProductData);
        }

        if (Auth::user()->type == 'Vendor') {
            $ProductData = ProductFile::orderby('id', 'desc')->where('vendor_id',Auth::user()->UserVendor->id)->Paginate(10);
            $CountProductData = count($ProductData);
        }

        return view("Product::productfile.index", compact('CountProductData','ModuleTitle','PageTitle','TableTitle','ProductData'));
    }

    public function filedownload($type)
    {
        return \Excel::download(new FileExportExport, 'ProductFile'.date(" h:i:sa").'-'.date("d-m-Y").'.'.$type);
    }

    public function productdelete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {
            $file_model = ProductFile::where('id', $id)
                ->select('*')
                ->first();
            $file_model->delete();

            DB::commit();
            Session::flash('delete', 'Delete Successfully !');
            return redirect()->route('admin.product.fileindex');
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }


    public function productcreate(){
        $ModuleTitle = "Manage Product Information";
        $PageTitle = "Add New Product";
//        $TableTitle = "list Of Product Information";

        $all_itemunit = ItemUnit::where('status','1')->orderby('name','asc')->pluck('name','id')->all();
        $all_itemunit[''] = 'Choose a Item Unit';
        ksort($all_itemunit);

        $SizeWeightDimension= SizeWeightDimension::where('status','1')->orderby('name','asc')->pluck('name','id')->all();
        $SizeWeightDimension[''] = 'Choose Size/Weight/Dimension';
        ksort($SizeWeightDimension);

        $Sizeunit= Sizeunit::where('status','1')->orderby('name','asc')->pluck('name','id')->all();
        $Sizeunit[''] = 'Choose Size Unit';
        ksort($Sizeunit);

        $Category = Category::getHierarchyCategory();
        $Category[''] = 'Choose Category';
        ksort($Category);

        $ProductBrand = ProductBrand::where('status','1')->pluck('name','id')->all();
        $ProductBrand[''] = 'Choose Brand';
        ksort($ProductBrand);

        $Country = Country::where('status','1')->pluck('name','id')->all();
        $Country[''] = 'Choose Country';
        ksort($Country);

        $Color = Color::where('status','1')->pluck('name','id')->all();

        $Discount= Discount::where('status','1')->pluck('name','id')->all();
        $Discount[''] = 'Choose Discount';
        ksort($Discount);

        $WarningType= WarningType::where('status','1')->pluck('name','id')->all();
        $WarningType[''] = 'Choose Warning Type';
        ksort($WarningType);

        $ItemAssurance= ItemAssurance::where('status','1')->pluck('name','id')->all();
        $ItemAssurance[''] = 'Choose Item Assurance';
        ksort($ItemAssurance);

        $Promotion = ProductPromotion::where('status',1)->where('promotion','yes')->pluck('name','id')->all();
        $Promotion[''] = 'Choose Promotion';
        ksort($Promotion);

        $Tags = ProductPromotion::where('status',1)->where('tag','yes')->pluck('name','id')->all();

        $Vendor = Vendor::where('status',1)->pluck('store_name','id')->all();
        $Vendor[''] = 'Choose Vendor';
        ksort($Vendor);

//        echo '<pre>';
//        print_r($Vendor);
//        exit();



        return view("Product::product.create", compact('Vendor','Tags','Promotion','ItemAssurance','WarningType','Discount','Color','Country','ProductBrand','Category','ModuleTitle','PageTitle','all_itemunit','SizeWeightDimension','Sizeunit'));
    }


    public function productstore(Requests\ProductRequest $request){
        $input = $request->all();

        if (!isset($input['sub_item'])){
            $input['sub_item'] = null;
        }

            $slug = str_replace(' ', '-', $input['name']);
            $slug = str_replace("/\s+/", "-", $slug);
            $slug = str_replace(".", "-", $slug);
            $slug = strtolower($slug);
            $SlugExistsOrNot = ProductFile::where('slug',$slug)->count();

            if ($SlugExistsOrNot == 0) {
                $input['slug'] = $slug;

                if ($request->file('feature_image') != '') {
                    $avatar = $request->file('feature_image');
                    $file_title = $slug.'.'.$avatar->getClientOriginalExtension();
                    $input['feature_image'] = $file_title;
                    $path = public_path("backend/image/ProProductImage/");
                    $target_file =  $path.basename($file_title);
                    $file_path = $_FILES['feature_image']['tmp_name'];
                    $result = move_uploaded_file($file_path,$target_file);
                }

                /* Transaction Start Here */
                DB::beginTransaction();
                try {
                    // Store product data
                    if ($product_data = Product::create($input)) {
                        $product_data->save();
                    }

                    if (isset($input['color_id']) && !empty($input['color_id'])) {
                        foreach ($input['color_id'] as $value) {
                            $Color_Model = new ProductColor();
                            $Color_Model->product_id = $product_data->id;
                            $Color_Model->color_id = $value;
                            $Color_Model->save();
                        }
                    }


                    if (isset($input['tag_id']) && !empty($input['tag_id'])) {
                        foreach ($input['tag_id'] as $value) {
                            $Tag_Model = new ProductTag();
                            $Tag_Model->product_id = $product_data->id;
                            $Tag_Model->tag_id = $value;
                            $Tag_Model->save();
                        }
                    }

                    DB::commit();
                    Session::flash('message', 'Information added Successfully!');
//                return redirect()->back();
                    return redirect()->route('admin.product.fileindex');
                } catch (\Exception $e) {
                    //If there are any exceptions, rollback the transaction`
                    DB::rollback();
                    print($e->getMessage());
                    exit();
                    Session::flash('danger', $e->getMessage());
                }
            }else{
                Session::flash('validate', 'Name already exists');
                return redirect()->back()->withInput($input);
            }

    }


    public function productedit($id)
    {
        $ModuleTitle = "Manage Product Information";
        $PageTitle = "Add New Product";
//        $TableTitle = "list Of Product Information";

        $data = Product::where('id',$id)->first();
//        echo '<pre>';
//        print_r($data);

        $ProductTag = ProductTag::join('ems_promotion', 'ems_product_tags.tag_id', '=', 'ems_promotion.id')
                                                ->where('ems_product_tags.product_id',$id)
                                                ->pluck('ems_promotion.id', 'ems_promotion.name')
                                                ->all();
        $ProductColor = ProductColor::join('ems_color', 'ems_product_colors.color_id', '=', 'ems_color.id')
            ->where('ems_product_colors.product_id',$id)
            ->pluck('ems_color.id', 'ems_color.name')
            ->all();

        $all_itemunit = ItemUnit::where('status', '1')->orderby('name', 'asc')->pluck('name', 'id')->all();
        $all_itemunit[''] = 'Choose a Item Unit';
        ksort($all_itemunit);

        $SizeWeightDimension = SizeWeightDimension::where('status', '1')->orderby('name', 'asc')->pluck('name', 'id')->all();
        $SizeWeightDimension[''] = 'Choose Size/Weight/Dimension';
        ksort($SizeWeightDimension);

        $Sizeunit = Sizeunit::where('status', '1')->orderby('name', 'asc')->pluck('name', 'id')->all();
        $Sizeunit[''] = 'Choose Size Unit';
        ksort($Sizeunit);

        $Category = Category::getHierarchyCategory();
        $Category[''] = 'Choose Category';
        ksort($Category);

        $ProductBrand = ProductBrand::where('status', '1')->pluck('name', 'id')->all();
        $ProductBrand[''] = 'Choose Brand';
        ksort($ProductBrand);

        $Country = Country::where('status', '1')->pluck('name', 'id')->all();
        $Country[''] = 'Choose Country';
        ksort($Country);

        $Color = Color::where('status', '1')->pluck('name', 'id')->all();

        $Discount = Discount::where('status', '1')->pluck('name', 'id')->all();
        $Discount[''] = 'Choose Discount';
        ksort($Discount);

        $WarningType = WarningType::where('status', '1')->pluck('name', 'id')->all();
        $WarningType[''] = 'Choose Warning Type';
        ksort($WarningType);

        $ItemAssurance = ItemAssurance::where('status', '1')->pluck('name', 'id')->all();
        $ItemAssurance[''] = 'Choose Item Assurance';
        ksort($ItemAssurance);

        $Promotion = ProductPromotion::where('status', 1)->where('promotion', 'yes')->pluck('name', 'id')->all();
        $Promotion[''] = 'Choose Promotion';
        ksort($Promotion);

        $Tags = ProductPromotion::where('status', 1)->where('tag', 'yes')->pluck('name', 'id')->all();

        $Vendor = Vendor::where('status',1)->pluck('store_name','id')->all();
        $Vendor[''] = 'Choose Vendor';
        ksort($Vendor);


        return view("Product::product.edit", compact('Vendor','ProductColor','ProductTag','data','Tags', 'Promotion', 'ItemAssurance', 'WarningType', 'Discount', 'Color', 'Country', 'ProductBrand', 'Category', 'ModuleTitle', 'PageTitle', 'all_itemunit', 'SizeWeightDimension', 'Sizeunit'));
    }

    public function productupdate(Requests\ProductRequest $request,$id){
        $input = $request->all();
        if (!isset($input['sub_item'])){
            $input['sub_item'] = null;
        }

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        $SlugExistsOrNot = Product::where('slug',$slug)->count();
        $SlugToId = Product::where('slug',$slug)->first();
        $ProductUpdateModel = Product::where('id',$id)->first();

        if ($SlugExistsOrNot == 0 || ($SlugExistsOrNot == 1 && $id == $SlugToId->id)){
            $input['slug'] = $slug;

            if ($request->file('feature_image') != '') {
                File::delete(public_path().'/backend/image/ProProductImage/'.$ProductUpdateModel->feature_image);
                $avatar = $request->file('feature_image');
                $slug = str_replace("%", "", $slug);
                $file_title = $slug.'.'.$avatar->getClientOriginalExtension();
                $input['feature_image'] = $file_title;
                $path = public_path("backend/image/ProProductImage/");
                $target_file =  $path.basename($file_title);
                $file_path = $_FILES['feature_image']['tmp_name'];
                $result = move_uploaded_file($file_path,$target_file);
            }else{
                $input['feature_image'] = $ProductUpdateModel['feature_image'];
            }

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // update brand data
                $result = $ProductUpdateModel->update($input);
                $ProductUpdateModel->save();

                $Model_tag_delete = ProductTag::where('product_id',$id)->delete();

                if (isset($input['tag_id']) && !empty($input['tag_id'])) {
                    foreach ($input['tag_id'] as $value) {
                        $Tag_Model = new ProductTag();
                        $Tag_Model->product_id = $id;
                        $Tag_Model->tag_id = $value;
                        $Tag_Model->save();
                    }
                }

                $Model_color_delete = ProductColor::where('product_id',$id)->delete();
                if (isset($input['color_id']) && !empty($input['color_id'])) {
                    foreach ($input['color_id'] as $value) {
                        $Color_Model = new ProductColor();
                        $Color_Model->product_id = $id;
                        $Color_Model->color_id = $value;
                        $Color_Model->save();
                    }
                }


                if (isset($input['level_name']) && !empty($input['level_name'])) {
                    foreach ($input['level_name'] as $index => $value) {
                        if ($input['level_name'][$index] !='' || $input['level_value'][$index] !='') {
                            $Additional_Model = new ProductAdditionalField();
                            $Additional_Model->product_id = $id;
                            $Additional_Model->level_name = $input['level_name'][$index];
                            $Additional_Model->level_value = $input['level_value'][$index];
                            $Additional_Model->save();
                        }
                    }
                }


                $img_sl = 1;
                if ($request->file('attach_link') != '') {
                    foreach($request->file('attach_link') as $index => $value) {
                        $avatar = $request->file('attach_link')[$index];

                        $file_title = 'img'.rand(1,10000).$img_sl.'-'.time().'.'.$avatar->getClientOriginalExtension();
                        $image['attach_link'] = $file_title;
                        $path = public_path("backend/image/ProProductImage/More/");
                        $target_file =  $path.basename($file_title);
                        $file_path = $_FILES['attach_link']['tmp_name'][$index];
                        $result = move_uploaded_file($file_path,$target_file);

                        $image['product_id'] = $id;
                        $image['img_level'] = $input['img_level'][$index];

                        ProductImage::create($image);
                        $img_sl++;
                    }
                }

                DB::commit();

                Session::flash('message', 'Information Updated Successfully!');
//                return redirect()->route('admin.product.fileindex');
                return redirect()->back();
            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }

        }else{
            Session::flash('validate', 'Name already exists');
            return redirect()->back()->withInput($input);
        }
    }


    public function deletemoreattach(){
        $sl_no = $_GET['sl_no'];
        $delete_image = ProductImage::where('id',$sl_no)->first();
        File::delete(public_path().'/backend/image/ProProductImage/More/'.$delete_image->attach_link);
        $delete_image->delete();
        $response['ok'] = 'Delete Successfully';
        return $response;
    }


    public function moreattach(){
        $sl_no = $_GET['sl_no'];
        // echo $sl_no;
        $view = \Illuminate\Support\Facades\View::make('Product::product._more_attach_form',compact('sl_no'));
        $response['sl_no'] = $sl_no++;
        $contents = $view->render();
        $response['content'] = $contents;

        return $response;
    }


    public function MoreField(){
        $sl_no = $_GET['sl_no'];
        // echo $sl_no;
        $view = \Illuminate\Support\Facades\View::make('Product::product._more_field_form',compact('sl_no'));
        $response['sl_no'] = $sl_no++;
        $contents = $view->render();
        $response['content'] = $contents;

        return $response;
    }


    public function deleteadditional(){
        $sl_no = $_GET['sl_no'];
        $delete_field = ProductAdditionalField::where('id',$sl_no)->first();
        $delete_field->delete();
        $response['ok'] = 'Delete Successfully';
        return $response;
    }
}
