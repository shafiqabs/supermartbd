<?php
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request as Input;

?>


    <div class="row">
        <div class="col-md-12">

            <div class="from-group">
                {!! Form::label('Product file import', 'Product file import', array('class' => 'form-label','for'=>'formFile')) !!}

                    <span style="color: red">*</span>

                <div class="mb-3">
                    <input required class="form-control" accept="file/*" name="import_file" type="file" id="file">
                </div>
            </div>


        </div>




    </div>






    <div class="row">

        <div class="col-md-12" style="text-align: right;">
            <div class="from-group">
                <div class="">
                    <button type="reset" class="btn submit-button">Reset</button>
                    <button type="submit" class="btn submit-button">Submit</button>
                </div>
            </div>
        </div>

    </div>
