@extends(/** @lang text */'backend.layouts.master')

@section('body')
    <div class="dashboard-area">
        <div id="carbon-block" class="">
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">{{$ModuleTitle}}</h1>

                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group me-2">
                            <a style="color: #000;" href="{{route('admin.product.productcreate')}}" title="Add Product" class="module_button_header">
                                <button type="button" class="btn btn-sm btn-outline-secondary">
                                    <i class="fas fa-plus-circle"></i> New Product
                                </button>
                            </a>

{{--                            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>--}}
{{--                            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>--}}
                        </div>




                        <a style="color: #000;" href="{{route('admin.product.filecreate')}}" title="Add Size Unit" class="module_button_header">
                            <button type="button" class="btn btn-sm btn-outline-secondary">
                                <i class="fas fa-plus-circle"></i> New File
                            </button>
                        </a>


                    </div>
                </div>
            </main>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">
                            {{$PageTitle}}
                        </div>
                        <div class="card-body">

                            @include('backend.layouts.message')


                            @if(isset($ProductData) && $CountProductData > 0)

                                    <div>
                                    <a href="{{ route('admin.product.filedownload', 'xls') }}"><button class="btn btn-secondary btn-sm">Download xls</button></a>
                                    <a href="{{ route('admin.product.filedownload', 'xlsx') }}"><button class="btn btn-secondary btn-sm">Download xlsx</button></a>
                                    <a href="{{ route('admin.product.filedownload', 'csv') }}"><button class="btn btn-secondary btn-sm">Download CSV</button></a>
                                    </div>
                                    <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                        <th>SL</th>
                                        <th>Name</th>
                                        <th>Brand</th>
                                        <th>Category</th>
                                        <th>Size</th>
                                        <th>Unit</th>
                                        <th>Quantity</th>
                                        <th>Min Quantity</th>
                                        <th>Max Quantity</th>
                                        <th>Sales Price</th>
                                        <th>Purchase Price</th>
                                        <th>Image</th>
                                        <th>Action</th>
                                        </thead>

                                        <tbody>
                                        <?php $i=1; ?>
                                        @foreach($ProductData as $value)
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>{{$value->name}}</td>
                                                <td>{{$value->ProductBrand?$value->ProductBrand->name:''}}</td>
                                                <td>{{$value->ProductCategory?$value->ProductCategory->name:''}}</td>
                                                <td>{{$value->ProductSizeUnit?$value->ProductSizeUnit->name:''}}</td>
                                                <td>{{$value->ProductItemUnit?$value->ProductItemUnit->name:''}}</td>
                                                <td>{{$value->quantity}}</td>
                                                <td>{{$value->minquantity}}</td>
                                                <td>{{$value->maxquantity}}</td>
                                                <td>{{$value->salesprice}}</td>
                                                <td>{{$value->purchaseprice}}</td>

                                                <td>
                                                    <div class="tooltipme"> image
                                                        <span class="tooltipmetext">
                                                                <img class="hover_image" src="{{ asset('backend/image/ProProductImage').'/'.$value->feature_image;}}" alt="">

                                                        </span>
                                                    </div>
                                                </td>

                                                <td>
                                                    <a href="{{route('admin.product.productedit',$value->id)}}" title="Edit Product"><i class="fas fa-user-edit"></i></a> |
                                                    <a href="{{route('admin.product.productdelete',$value->id)}}" title="Delete Product" onclick="return confirm('Are you sure to Delete?')"><i class="fas fa-trash-alt"></i></a>

                                                </td>

                                            </tr>

                                        @endforeach

                                        </tbody>

                                    </table>

                                </div>
                                <div class=" justify-content-right">
                                    {{ $ProductData->links('backend.layouts.pagination') }}
                                </div>
                            @endif



                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>


@endsection
