<?php
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request as Input;


?>
<tr sl-no="{{$sl_no}}" id="delete_field_row_{{$sl_no}}">
    <td width="50%">
        {!! Form::text('level_name[]',Input::old('level_name'),['id'=>'level_name','placeholder'=>' Level Name','class'=>'form-control']) !!}
    </td>
    <td width="47%">
        {!! Form::text('level_value[]',Input::old('level_value'),['id'=>'level_value','placeholder'=>' Level Value','class'=>'form-control']) !!}
    </td>
    <td width="3%" style="color: fff">
        <i class="fa fa-trash btn btn-danger" id="delete_field_click" sl_no={{$sl_no}} aria-hidden="true" style="text-align: right;cursor: pointer;"></i>
    </td>

</tr>
