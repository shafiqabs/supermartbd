<?php
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request as Input;


?>
<tr sl-no="{{$sl_no}}" id="delete_row_{{$sl_no}}">
    <td width="70%">
        {!! Form::text('img_level[]',Input::old('img_level'),['id'=>'img_level','placeholder'=>'Image Level','class'=>'form-control']) !!}
    </td>
    <td width="30%">
        <div style="position:relative;border: 1px solid #e6e0e0;">
            <a class='btn btn-primary btn-sm font-10' href='javascript:;'>
                Choose File...
                <input name="attach_link[]" type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file_source" size="40"  onchange='$("#upload-file-info{{$sl_no}}").html($(this).val());'>
            </a>
            &nbsp;
            <span style="color: red">{!! $errors->first('attach_link[]') !!}</span>
            <span class='label label-info' id="upload-file-info{{$sl_no}}"></span>

        </div>

    </td>
    <td width="3%" style="color: red"">
        <i class="fa fa-trash btn btn-danger" id="delete_click" sl_no={{$sl_no}} aria-hidden="true" style="text-align: right;cursor: pointer;"></i>
    </td>

</tr>
