<?php
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request as Input;

?>


    <div class="row">
        <div class=" col-md-5">
            <div class="from-group">
                {!! Form::label('Product Name', 'Product Name', array('class' => 'form-label')) !!}
                <span style="color: red">*</span>
                <div class="input-group mb-3">
                    {!! Form::text('name',Input::old('name'),['id'=>'name','class' => 'form-control','data-checkify'=>'minlen=3,required','Placeholder' => 'Enter Product Name','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                    <span style="color: #ff0000">{!! $errors->first('name') !!}</span>
                </div>
            </div>
        </div>


        <div class="col-md-1">
            <div class="from-group">
                {!! Form::label('', '', array('class' => 'form-label')) !!}
                <div class="form-check">
                    @if(isset($data) && $data->sub_item !='')
                        {!! Form::checkbox('sub_item', 'yes',$data->sub_item,['id'=>'flexCheckDefault','class'=>'form-check-input']) !!} Sub Item
                    @else
                        {!! Form::checkbox('sub_item', 'yes',false,['id'=>'flexCheckDefault','class'=>'form-check-input']) !!}Sub Item
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="from-group">
                {!! Form::label('Item Unit', 'Item Unit', array('class' => 'form-label')) !!}
{{--                <span style="color: red">*</span>--}}
                <div class="input-group mb-3">
                    {!! Form::select('item_unit_id',$all_itemunit,Input::old('item_unit_id'),['id'=>'item_unit_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                    <span style="color: #ff0000">{!! $errors->first('item_unit_id') !!}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="from-group">
                {!! Form::label('Size unit', 'Size unit', array('class' => 'form-label')) !!}
{{--                <span style="color: red">*</span>--}}
                <div class="input-group mb-3">
                    {!! Form::select('size_unit_id',$Sizeunit,Input::old('size_unit_id'),['id'=>'size_unit_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                    <span style="color: #ff0000">{!! $errors->first('size_unit_id') !!}</span>
                </div>
            </div>
        </div>



    </div>

<hr>
<div class="row">
    <div class=" col-md-6">
        <div class="from-group">
            {!! Form::label('Product Name (Bangla)', 'Product Name (Bangla)', array('class' => 'form-label')) !!}
{{--            <span style="color: red">*</span>--}}
            <div class="input-group mb-3">
                {!! Form::text('name_bn',Input::old('name_bn'),['id'=>'name_bn','class' => 'form-control','Placeholder' => 'Enter Product Name Bangla','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                <span style="color: #ff0000">{!! $errors->first('name_bn') !!}</span>
            </div>
        </div>
    </div>


    <div class="col-md-3">
        <div class="from-group">
            {!! Form::label('Purchase Price', 'Purchase Price', array('class' => 'form-label')) !!}
            <span style="color: red">*</span>
            <div class="input-group mb-3">
                {!! Form::text('purchaseprice',Input::old('purchaseprice'),['id'=>'purchaseprice','class' => 'form-control','data-checkify'=>'number,required','Placeholder' => 'Enter Purchase Price','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                <span style="color: #ff0000">{!! $errors->first('purchaseprice') !!}</span>
            </div>
        </div>
    </div>


    <div class="col-md-3">
        <div class="from-group">
            {!! Form::label('Sales Price', 'Sales Price', array('class' => 'form-label')) !!}
            <span style="color: red">*</span>
            <div class="input-group mb-3">
                {!! Form::text('salesprice',Input::old('salesprice'),['id'=>'salesprice','class' => 'form-control','data-checkify'=>'number,required','Placeholder' => 'Enter Purchase Price','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                <span style="color: #ff0000">{!! $errors->first('salesprice') !!}</span>
            </div>
        </div>
    </div>
</div>

<hr>
<div class="row">
        <div class="col-md-2">
            <div class="from-group">
                {!! Form::label('Category', 'Category', array('class' => 'form-label')) !!}
                <span style="color: red">*</span>
                <div class="input-group mb-3">
                    {!! Form::select('category_id',$Category,Input::old('category_id'),['id'=>'category_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2','required'=>'true']) !!}
                    <span style="color: #ff0000">{!! $errors->first('category_id') !!}</span>
                </div>
            </div>
        </div>

    <div class="col-md-2">
        <div class="from-group">
            {!! Form::label('Brand', 'Brand', array('class' => 'form-label')) !!}
            <span style="color: red">*</span>
            <div class="input-group mb-3">
                {!! Form::select('brand_id',$ProductBrand,Input::old('brand_id'),['id'=>'brand_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2','required'=>'true']) !!}
                <span style="color: #ff0000">{!! $errors->first('brand_id') !!}</span>
            </div>
        </div>
    </div>


    <div class="col-md-2">
        <div class="from-group">
            {!! Form::label('Made In', 'Made In', array('class' => 'form-label')) !!}
            <span style="color: red">*</span>
            <div class="input-group mb-3">
                {!! Form::select('country_id',$Country,Input::old('country_id'),['id'=>'country_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2','required'=>'true']) !!}
                <span style="color: #ff0000">{!! $errors->first('country_id') !!}</span>
            </div>
        </div>
    </div>


    <div class="col-md-2">
        <div class="from-group">
            {!! Form::label('Quantity', 'Quantity', array('class' => 'form-label')) !!}
            <span style="color: red">*</span>
            <div class="input-group mb-3">
                {!! Form::text('quantity',Input::old('quantity'),['id'=>'quantity','class' => 'form-control','data-checkify'=>'minlen=1,required,number','Placeholder' => 'Enter Quantity','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                <span style="color: #ff0000">{!! $errors->first('quantity') !!}</span>
            </div>
        </div>
    </div>

    <div class="col-md-2">
        <div class="from-group">
            {!! Form::label('Min Qnt', 'Min Qnt', array('class' => 'form-label')) !!}
            <span style="color: red">*</span>
            <div class="input-group mb-3">
                {!! Form::text('minquantity',Input::old('minquantity'),['id'=>'minquantity','class' => 'form-control','data-checkify'=>'minlen=1,required,number','Placeholder' => 'Enter Min QTY','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                <span style="color: #ff0000">{!! $errors->first('minquantity') !!}</span>
            </div>
        </div>
    </div>

    <div class="col-md-2">
        <div class="from-group">
            {!! Form::label('Max Qnt', 'Max Qnt', array('class' => 'form-label')) !!}
            <span style="color: red">*</span>
            <div class="input-group mb-3">
                {!! Form::text('maxquantity',Input::old('maxquantity'),['id'=>'maxquantity','class' => 'form-control','data-checkify'=>'minlen=1,required,number','Placeholder' => 'Enter Max QTY','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                <span style="color: #ff0000">{!! $errors->first('maxquantity') !!}</span>
            </div>
        </div>
    </div>

</div>


<hr>


<div class="row">
    <div class="col-md-6">
        <div class="from-group">
            {!! Form::label('Colors', 'Colors', array('class' => 'form-label')) !!}

            <div class="input-group mb-3">
                @if(isset($data))
                    {!! Form::select('color_id[]',$Color,Input::old('color_id'),['id'=>'color_id','multiple'=>'multiple','class' => 'form-select js-example-basic-multiple form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                    <span style="color: #ff0000">{!! $errors->first('color_id') !!}</span>
                @else
                    {!! Form::select('color_id[]',$Color,Input::old('color_id'),['id'=>'color_id','multiple'=>'multiple','class' => 'form-select js-example-basic-multiple form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                    <span style="color: #ff0000">{!! $errors->first('color_id') !!}</span>
                @endif
            </div>
        </div>
    </div>

    <div class="col-md-2">
        <div class="from-group">
            {!! Form::label('Discount', 'Discount', array('class' => 'form-label')) !!}
{{--                        <span style="color: red">*</span>--}}
            <div class="input-group mb-3">
                {!! Form::select('discount_id',$Discount,Input::old('discount_id'),['id'=>'discount_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                <span style="color: #ff0000">{!! $errors->first('discount_id') !!}</span>
            </div>
        </div>
    </div>

    <div class="col-md-2">
        <div class="from-group">
            {!! Form::label('Warning Type', 'Warning Type', array('class' => 'form-label')) !!}
{{--                        <span style="color: red">*</span>--}}
            <div class="input-group mb-3">
                {!! Form::select('warning_id',$WarningType,Input::old('warning_id'),['id'=>'warning_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                <span style="color: #ff0000">{!! $errors->first('warning_id') !!}</span>
            </div>
        </div>
    </div>

    <div class="col-md-2">
        <div class="from-group">
            {!! Form::label('Item Assurance', 'Item Assurance', array('class' => 'form-label')) !!}
{{--                        <span style="color: red">*</span>--}}
            <div class="input-group mb-3">
                {!! Form::select('item_assurance_id',$ItemAssurance,Input::old('item_assurance_id'),['id'=>'item_assurance_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                <span style="color: #ff0000">{!! $errors->first('item_assurance_id') !!}</span>
            </div>
        </div>
    </div>

</div>

<hr>


<div class="row">
    <div class=" row col-md-8">
        <div class="col-md-8">
            <div class="from-group">
                {!! Form::label('Tags', 'Tags', array('class' => 'form-label')) !!}

                <div class="input-group mb-3">
                    @if(isset($data))
                        {!! Form::select('tag_id[]',$Tags,['id'=>'tag_id','multiple'=>'multiple','class' => 'form-select js-example-basic-multiple form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('tag_id') !!}</span>
                    @else
                        {!! Form::select('tag_id[]',$Tags,Input::old('tag_id'),['id'=>'tag_id','multiple'=>'multiple','class' => 'form-select js-example-basic-multiple form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('tag_id') !!}</span>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="from-group">
                {!! Form::label('Promotion', 'Promotion', array('class' => 'form-label')) !!}
                {{--            <span style="color: red">*</span>--}}
                <div class="input-group mb-3">
                    {!! Form::select('promotion_id',$Promotion,Input::old('promotion_id'),['id'=>'promotion_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                    <span style="color: #ff0000">{!! $errors->first('promotion_id') !!}</span>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="from-group">
                {!! Form::label('Description', 'Description', array('class' => 'form-label')) !!}

                <div class="input-group mb-3">

                    {!! Form::textarea('discription',Input::old('discription'),['id'=>'discription','rows'=>'2','class' => ' form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                    <span style="color: #ff0000">{!! $errors->first('discription') !!}</span>

                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="from-group">
                {!! Form::label('Feature Tag', 'Feature Tag', array('class' => 'form-label')) !!}
                {{--            <span style="color: red">*</span>--}}
                <?php
                    $FeatureTag = array();
                    $FeatureTag['bestseller'] = 'Best Seller';
                    $FeatureTag['dealshot'] = 'Deals Hot';
                    $FeatureTag['recommend'] = 'Recommend';
                ?>
                <div class="input-group mb-3">
                    {!! Form::select('feature_tag',$FeatureTag,Input::old('feature_tag'),['id'=>'feature_tag','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                    <span style="color: #ff0000">{!! $errors->first('feature_tag') !!}</span>
                </div>
            </div>
        </div>


        <div class="col-md-4">
            <div class="from-group">
                {!! Form::label('Vendor', 'Vendor', array('class' => 'form-label')) !!}
                {{--                <span style="color: red">*</span>--}}
                <div class="input-group mb-3">
                    @if (Auth::user()->type == 'Admin')
                    {!! Form::select('vendor_id',$Vendor,Input::old('vendor_id'),['id'=>'vendor_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                    @endif
                    @if (Auth::user()->type == 'Vendor')
                            <input type="text" name="vendor_name" value="{{Auth::user()->UserVendor->store_name}}" class="form-control" readonly>
                            <input type="hidden" name="vendor_id" value="{{Auth::user()->UserVendor->id}}" class="form-control">
                    @endif
                        <span style="color: #ff0000">{!! $errors->first('vendor_id') !!}</span>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="from-group">
                {!! Form::label('Size/Weight/Dimension', 'Size/Weight/Dimension', array('class' => 'form-label')) !!}
                {{--                <span style="color: red">*</span>--}}
                <div class="input-group mb-3">
                    {!! Form::select('size_weight_dimen_id',$SizeWeightDimension,Input::old('size_weight_dimen_id'),['id'=>'size_weight_dimen_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                    <span style="color: #ff0000">{!! $errors->first('size_weight_dimen_id') !!}</span>
                </div>
            </div>
        </div>
    </div>




    <div class="col-md-4">
        <div class="from-group">
            {!! Form::label('Feature Image', 'Feature Image', array('class' => 'form-label','for'=>'formFile')) !!}


            <div class="mb-3">
                @if(isset($data))
                    <input class="form-control" accept="image/*" name="feature_image" type="file" id="file" onchange="loadFile(event)">
                @else
                    <input required class="form-control" accept="image/*" name="feature_image" type="file" id="file" onchange="loadFile(event)">

                @endif

                <div style="text-align: center;padding-top: 5px;">
                    @if($errors->first('feature_image'))
                        <span style="color: #ff0000">{!! $errors->first('feature_image') !!}</span>
                    @else
                        <img id="feature_image" width="150" height="120"/>
                    @endif

                    @if(isset($data) && $data->feature_image !='')
                        <img id="feature_image" src="{{ asset('backend/image/ProProductImage').'/'.$data->feature_image;}}" width="150" height="120"/>
                    @endif

                </div>
            </div>
        </div>


        <script>
            var loadFile = function(event) {
                var image = document.getElementById('feature_image');
                image.src = URL.createObjectURL(event.target.files[0]);
            };
        </script>
    </div>
</div>


    <div class="row">
        <div class="col-md-4 offset-md-8">
            <div class="from-group">
                <div class="from-group">
                    {!! Form::label('Status', 'Status', array('class' => 'form-label')) !!}
                    <span style="color: red">*</span>
                    <div class="input-group mb-3">
                        {!! Form::select('status',array(1=>'Active',0=>'Inactive'),Input::old('status'),['id'=>'ordering','class' => 'form-control','data-checkify'=>'minlen=1,required,number','Placeholder' => 'Enter ordering number','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('ordering') !!}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="row">

        <div class="col-md-12" style="text-align: right;">
            <div class="from-group">
                <div class="">
                    <button type="reset" class="btn submit-button">Reset</button>
                    <button type="submit" class="btn submit-button">Submit</button>
                </div>
            </div>
        </div>

    </div>
