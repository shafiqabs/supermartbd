@extends(/** @lang text */'backend.layouts.master')

@section('body')
    <div class="dashboard-area">
        <div id="carbon-block" class="">
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">{{$ModuleTitle}}</h1>

                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group me-2">
                            <a style="color: #000;" href="{{route('admin.product.productcreate')}}" title="Add Product" class="module_button_header">
                                <button type="button" class="btn btn-sm btn-outline-secondary">
                                    <i class="fas fa-plus-circle"></i> New Product
                                </button>
                            </a>

                            {{--                            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>--}}
                            {{--                            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>--}}
                        </div>

                        <a style="color: #000;" href="{{route('admin.product.fileindex')}}" title="Product List" class="module_button_header">
                            <button type="button" class="btn btn-sm btn-outline-secondary">
                                <i class="fas fa-th-list"></i> Product List
                            </button>
                        </a>


                    </div>
                </div>
            </main>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">
                            {{$PageTitle}}
                        </div>
                        <div class="card-body">

                            @include('backend.layouts.message')

                            {!! Form::model($data, ['method' => 'PATCH', 'files'=> true, 'route'=> ['admin.product.productupdate', $data->id],"class"=>"", 'id' => 'basic-form']) !!}

                            <?php
                            use Illuminate\Support\Facades\URL;
                            use Illuminate\Support\Facades\Request as Input;
                            ?>


                            <div class="row">
                                <div class=" col-md-5">
                                    <div class="from-group">
                                        {!! Form::label('Product Name', 'Product Name', array('class' => 'form-label')) !!}
                                        <span style="color: red">*</span>
                                        <div class="input-group mb-3">
                                            {!! Form::text('name',Input::old('name'),['id'=>'name','class' => 'form-control','data-checkify'=>'minlen=3,required','Placeholder' => 'Enter Product Name','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                            <span style="color: #ff0000">{!! $errors->first('name') !!}</span>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-1">
                                    <div class="from-group">
                                        {!! Form::label('', '', array('class' => 'form-label')) !!}
                                        <div class="form-check">
                                            @if(isset($data) && $data->sub_item !='')
                                                {!! Form::checkbox('sub_item', 'yes',$data->sub_item,['id'=>'flexCheckDefault','class'=>'form-check-input']) !!} Sub Item
                                            @else
                                                {!! Form::checkbox('sub_item', 'yes',false,['id'=>'flexCheckDefault','class'=>'form-check-input']) !!}Sub Item
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="from-group">
                                        {!! Form::label('Item Unit', 'Item Unit', array('class' => 'form-label')) !!}
                                        {{--                <span style="color: red">*</span>--}}
                                        <div class="input-group mb-3">
                                            {!! Form::select('item_unit_id',$all_itemunit,Input::old('item_unit_id'),['id'=>'item_unit_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                            <span style="color: #ff0000">{!! $errors->first('item_unit_id') !!}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="from-group">
                                        {!! Form::label('Size unit', 'Size unit', array('class' => 'form-label')) !!}
                                        {{--                <span style="color: red">*</span>--}}
                                        <div class="input-group mb-3">
                                            {!! Form::select('size_unit_id',$Sizeunit,Input::old('size_unit_id'),['id'=>'size_unit_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                            <span style="color: #ff0000">{!! $errors->first('size_unit_id') !!}</span>
                                        </div>
                                    </div>
                                </div>



                            </div>

                            <hr>
                            <div class="row">
                                <div class=" col-md-6">
                                    <div class="from-group">
                                        {!! Form::label('Product Name (Bangla)', 'Product Name (Bangla)', array('class' => 'form-label')) !!}
                                        {{--            <span style="color: red">*</span>--}}
                                        <div class="input-group mb-3">
                                            {!! Form::text('name_bn',Input::old('name_bn'),['id'=>'name_bn','class' => 'form-control','Placeholder' => 'Enter Product Name Bangla','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                            <span style="color: #ff0000">{!! $errors->first('name_bn') !!}</span>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="from-group">
                                        {!! Form::label('Purchase Price', 'Purchase Price', array('class' => 'form-label')) !!}
                                        <span style="color: red">*</span>
                                        <div class="input-group mb-3">
                                            {!! Form::text('purchaseprice',Input::old('purchaseprice'),['id'=>'purchaseprice','class' => 'form-control','data-checkify'=>'number,required','Placeholder' => 'Enter Purchase Price','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                            <span style="color: #ff0000">{!! $errors->first('purchaseprice') !!}</span>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="from-group">
                                        {!! Form::label('Sales Price', 'Sales Price', array('class' => 'form-label')) !!}
                                        <span style="color: red">*</span>
                                        <div class="input-group mb-3">
                                            {!! Form::text('salesprice',Input::old('salesprice'),['id'=>'salesprice','class' => 'form-control','data-checkify'=>'number,required','Placeholder' => 'Enter Purchase Price','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                            <span style="color: #ff0000">{!! $errors->first('salesprice') !!}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="from-group">
                                        {!! Form::label('Category', 'Category', array('class' => 'form-label')) !!}
                                        <span style="color: red">*</span>
                                        <div class="input-group mb-3">
                                            {!! Form::select('category_id',$Category,Input::old('category_id'),['id'=>'category_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2','required'=>'true']) !!}
                                            <span style="color: #ff0000">{!! $errors->first('category_id') !!}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="from-group">
                                        {!! Form::label('Brand', 'Brand', array('class' => 'form-label')) !!}
                                        <span style="color: red">*</span>
                                        <div class="input-group mb-3">
                                            {!! Form::select('brand_id',$ProductBrand,Input::old('brand_id'),['id'=>'brand_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2','required'=>'true']) !!}
                                            <span style="color: #ff0000">{!! $errors->first('brand_id') !!}</span>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-2">
                                    <div class="from-group">
                                        {!! Form::label('Made In', 'Made In', array('class' => 'form-label')) !!}
                                        <span style="color: red">*</span>
                                        <div class="input-group mb-3">
                                            {!! Form::select('country_id',$Country,Input::old('country_id'),['id'=>'country_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2','required'=>'true']) !!}
                                            <span style="color: #ff0000">{!! $errors->first('country_id') !!}</span>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-2">
                                    <div class="from-group">
                                        {!! Form::label('Quantity', 'Quantity', array('class' => 'form-label')) !!}
                                        <span style="color: red">*</span>
                                        <div class="input-group mb-3">
                                            {!! Form::text('quantity',Input::old('quantity'),['id'=>'quantity','class' => 'form-control','data-checkify'=>'minlen=1,required,number','Placeholder' => 'Enter Quantity','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                            <span style="color: #ff0000">{!! $errors->first('quantity') !!}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="from-group">
                                        {!! Form::label('Min Qnt', 'Min Qnt', array('class' => 'form-label')) !!}
                                        <span style="color: red">*</span>
                                        <div class="input-group mb-3">
                                            {!! Form::text('minquantity',Input::old('minquantity'),['id'=>'minquantity','class' => 'form-control','data-checkify'=>'minlen=1,required,number','Placeholder' => 'Enter Min QTY','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                            <span style="color: #ff0000">{!! $errors->first('minquantity') !!}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="from-group">
                                        {!! Form::label('Max Qnt', 'Max Qnt', array('class' => 'form-label')) !!}
                                        <span style="color: red">*</span>
                                        <div class="input-group mb-3">
                                            {!! Form::text('maxquantity',Input::old('maxquantity'),['id'=>'maxquantity','class' => 'form-control','data-checkify'=>'minlen=1,required,number','Placeholder' => 'Enter Max QTY','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                            <span style="color: #ff0000">{!! $errors->first('maxquantity') !!}</span>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <hr>


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="from-group">
                                        {!! Form::label('Colors', 'Colors', array('class' => 'form-label')) !!}

                                        <div class="input-group mb-3">

                                                {!! Form::select('color_id[]',$Color,$ProductColor,['id'=>'color_id','multiple'=>'multiple','class' => 'form-select js-example-basic-multiple form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                                <span style="color: #ff0000">{!! $errors->first('color_id') !!}</span>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="from-group">
                                        {!! Form::label('Discount', 'Discount', array('class' => 'form-label')) !!}
                                        {{--                        <span style="color: red">*</span>--}}
                                        <div class="input-group mb-3">
                                            {!! Form::select('discount_id',$Discount,Input::old('discount_id'),['id'=>'discount_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                            <span style="color: #ff0000">{!! $errors->first('discount_id') !!}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="from-group">
                                        {!! Form::label('Warning Type', 'Warning Type', array('class' => 'form-label')) !!}
                                        {{--                        <span style="color: red">*</span>--}}
                                        <div class="input-group mb-3">
                                            {!! Form::select('warning_id',$WarningType,Input::old('warning_id'),['id'=>'warning_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                            <span style="color: #ff0000">{!! $errors->first('warning_id') !!}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="from-group">
                                        {!! Form::label('Item Assurance', 'Item Assurance', array('class' => 'form-label')) !!}
                                        {{--                        <span style="color: red">*</span>--}}
                                        <div class="input-group mb-3">
                                            {!! Form::select('item_assurance_id',$ItemAssurance,Input::old('item_assurance_id'),['id'=>'item_assurance_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                            <span style="color: #ff0000">{!! $errors->first('item_assurance_id') !!}</span>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <hr>


                            <div class="row">
                                <div class=" row col-md-8">
                                    <div class="col-md-8">
                                        <div class="from-group">
                                            {!! Form::label('Tags', 'Tags', array('class' => 'form-label')) !!}

                                            <div class="input-group mb-3">

                                                    {!! Form::select('tag_id[]',$Tags,$ProductTag,['id'=>'tag_id','multiple'=>'multiple','class' => 'form-select js-example-basic-multiple form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                                    <span style="color: #ff0000">{!! $errors->first('tag_id') !!}</span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="from-group">
                                            {!! Form::label('Promotion', 'Promotion', array('class' => 'form-label')) !!}
                                            {{--            <span style="color: red">*</span>--}}
                                            <div class="input-group mb-3">
                                                {!! Form::select('promotion_id',$Promotion,Input::old('promotion_id'),['id'=>'promotion_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                                <span style="color: #ff0000">{!! $errors->first('promotion_id') !!}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="from-group">
                                            {!! Form::label('Description', 'Description', array('class' => 'form-label')) !!}

                                            <div class="input-group mb-3">

                                                {!! Form::textarea('discription',Input::old('discription'),['id'=>'discription','rows'=>'2','class' => ' form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                                <span style="color: #ff0000">{!! $errors->first('discription') !!}</span>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="from-group">
                                            {!! Form::label('Feature Tag', 'Feature Tag', array('class' => 'form-label')) !!}
                                            {{--            <span style="color: red">*</span>--}}
                                            <?php
                                            $FeatureTag = array();
                                            $FeatureTag['bestseller'] = 'Best Seller';
                                            $FeatureTag['dealshot'] = 'Deals Hot';
                                            $FeatureTag['recommend'] = 'Recommend';
                                            ?>
                                            <div class="input-group mb-3">
                                                {!! Form::select('feature_tag',$FeatureTag,Input::old('feature_tag'),['id'=>'feature_tag','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                                <span style="color: #ff0000">{!! $errors->first('feature_tag') !!}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="from-group">
                                            {!! Form::label('Vendor', 'Vendor', array('class' => 'form-label')) !!}
                                            {{--                <span style="color: red">*</span>--}}
                                            <div class="input-group mb-3">
                                                {!! Form::select('vendor_id',$Vendor,Input::old('vendor_id'),['id'=>'vendor_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                                <span style="color: #ff0000">{!! $errors->first('vendor_id') !!}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="from-group">
                                            {!! Form::label('Size/Weight/Dimension', 'Size/Weight/Dimension', array('class' => 'form-label')) !!}
                                            {{--                <span style="color: red">*</span>--}}
                                            <div class="input-group mb-3">
                                                {!! Form::select('size_weight_dimen_id',$SizeWeightDimension,Input::old('size_weight_dimen_id'),['id'=>'size_weight_dimen_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                                <span style="color: #ff0000">{!! $errors->first('size_weight_dimen_id') !!}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>




                                <div class="col-md-4">
                                    <div class="from-group">
                                        {!! Form::label('Feature Image', 'Feature Image', array('class' => 'form-label','for'=>'formFile')) !!}


                                        <div class="mb-3">
                                            @if(isset($data))
                                                <input class="form-control" accept="image/*" name="feature_image" type="file" id="file" onchange="loadFile(event)">
                                            @else
                                                <input required class="form-control" accept="image/*" name="feature_image" type="file" id="file" onchange="loadFile(event)">

                                            @endif

                                            <div style="text-align: center;padding-top: 5px;">
                                                @if($errors->first('feature_image'))
                                                    <span style="color: #ff0000">{!! $errors->first('feature_image') !!}</span>
                                                @else
                                                    <img id="feature_image" width="150" height="120"/>
                                                @endif

                                                @if(isset($data) && $data->feature_image !='')
                                                    <img id="feature_image" src="{{ asset('backend/image/ProProductImage').'/'.$data->feature_image;}}" width="150" height="120"/>
                                                @endif

                                            </div>
                                        </div>
                                    </div>


                                    <script>
                                        var loadFile = function(event) {
                                            var image = document.getElementById('feature_image');
                                            image.src = URL.createObjectURL(event.target.files[0]);
                                        };
                                    </script>
                                </div>
                            </div>

<hr>
<div class="row">
<div class="col-md-12">
    <div class="form-group table-responsive">

        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Additional Field & Value</th>
                <th style="text-align: right;" colspan="2" ">
                    <a style="display: none" id="MoreFieldroute" data-href="{{ route('admin.product.MoreField') }}" ></a>
                    <p class="btn btn-primary" sl-no="1" id="MoreField">+ Add More</p>
                </th>
            </tr>
            </thead>
            <tbody id="add_more_level_row">
            <tr sl-no="1">
                <td width="30%">
                    {!! Form::text('level_name[]',Input::old('level_name'),['id'=>'level_name','placeholder'=>' Level Name','class'=>'form-control']) !!}
                </td>
                <td width="70%" colspan="2">
                    {!! Form::text('level_value[]',Input::old('level_value'),['id'=>'level_value','placeholder'=>' Level Value','class'=>'form-control']) !!}
                </td>

            </tr>
            </tbody>


            @foreach($data->ProductAdditionalField as $value)
            <?php
            $data['level_name'] = $value->level_name;
            $data['level_value'] = $value->level_value;
            ?>

<tr id="hide_additional_field_{{$value->id}}">
    <td width="30%">
        {!! Form::text('level_name[]',Input::old('level_name'),['disabled'=>'true','id'=>'level_name','placeholder'=>' Level Name','class'=>'form-control']) !!}
    </td>
    <td width="70%">
        {!! Form::text('level_value[]',Input::old('level_value'),['disabled'=>'true','id'=>'level_value','placeholder'=>' Level Value','class'=>'form-control']) !!}
    </td>
    <td style="color: red">
        <a style="display: none" id="DeleteAdditionalroute" data-href="{{ route('admin.product.deleteadditional',$value->id) }}" ></a>
        <p sl-no="{{$value->id}}" id="DeleteAdditional" style="cursor: pointer;"><i class="fas fa-trash btn btn-danger"></i></p>
    </td>
</tr>

            @endforeach
        </table>
    </div>
</div>
</div>



<hr>
<div class="row">
<div class="col-md-12">
    <div class="form-group table-responsive">

        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th width="70%">File Attachments</th>
                <th style="text-align: right;" colspan="2" width="30%">
                    <a style="display: none" id="moreattachroute" data-href="{{ route('admin.product.moreattach') }}" ></a>
                    <p class="btn btn-primary" sl-no="1" id="MoreAttachment">+ More Attachment</p>
                </th>
            </tr>
            </thead>
            <tbody id="add_more_attach_row">
            <tr sl-no="1">
                <td width="70%">
                    {!! Form::text('img_level[]',Input::old('img_level'),['id'=>'img_level','placeholder'=>'Image Level','class'=>'form-control']) !!}
                </td>
                <td width="30%" colspan="2">
                    <div style="position:relative;border: 1px solid #e6e0e0;">
                        <a class='btn btn-primary btn-sm font-10' href='javascript:;'>
                            Choose File...
                            <input name="attach_link[]" type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file_source" size="40"  onchange='$("#upload-file-info1").html($(this).val());'>
                        </a>
                        &nbsp;
                        <span style="color: red">{!! $errors->first('attach_link[]') !!}</span>
                        <span class='label label-info' id="upload-file-info1"></span>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="row" style="overflow-x: hidden;">
        @foreach($data->ProductImage as $value)
            <div class="col-md-3">
            <div class="from-group">
                <div class="mb-3">

                    <div style="text-align: center;padding-top: 5px;" id="hide_{{$value->id}}">
                        {{$value->img_level}}
                        @if(isset($value) && $value->attach_link !='')
                            <img id="feature_image" src="{{ asset('backend/image/ProProductImage/More').'/'.$value->attach_link}}" width="150" height="120"/>
                        @endif
                            <a style="display: none" id="Deletemoreattachroute" data-href="{{ route('admin.product.deletemoreattach',$value->id) }}" ></a>
                            <br><p class="btn btn-danger" sl-no="{{$value->id}}" id="DeleteMoreAttachment" style="cursor: pointer;color: #fff;"><i class="fas fa-trash"></i></p>
                    </div>
                </div>
            </div>
            </div>

        @endforeach
        </div>
    </div>
</div>
</div>


                            {{--<div class="row">--}}
{{--    <div class="col-md-6">--}}
{{--        <div class="from-group">--}}
{{--            <div id="filelist">Your browser doesn't have Flash, Silverlight or HTML5 support.</div>--}}
{{--            <br />--}}

{{--            <div id="container">--}}
{{--                <a id="pickfiles" href="javascript:;">[Select files]</a>--}}
{{--                <a id="uploadfiles" href="javascript:;">[Upload files]</a>--}}
{{--            </div>--}}

{{--            <br />--}}
{{--            <pre id="console"></pre>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--</div>--}}


{{--<div class="row">--}}
{{--    <div class="col-md-12" style="background: #2f8884;color: #fff;">--}}
{{--        <h2 style="display: inline-block;text-align: left">abc</h2>--}}
{{--        <h2 style="display: inline-block;text-align: right">abc</h2>--}}
{{--    </div>--}}
{{--    <div class="col-md-6">--}}
{{--        <div class="from-group">--}}

{{--        </div>--}}
{{--    </div>--}}

{{--    <div class="col-md-6">--}}
{{--        <div class="from-group">--}}

{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

                            <div class="row">
                                <div class="col-md-4 offset-md-8">
                                    <div class="from-group">
                                        <div class="from-group">
                                            {!! Form::label('Status', 'Status', array('class' => 'form-label')) !!}
                                            <span style="color: red">*</span>
                                            <div class="input-group mb-3">
                                                {!! Form::select('status',array(1=>'Active',0=>'Inactive'),Input::old('status'),['id'=>'ordering','class' => 'form-control','data-checkify'=>'minlen=1,required,number','Placeholder' => 'Enter ordering number','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                                <span style="color: #ff0000">{!! $errors->first('ordering') !!}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>




                            <div class="row">

                                <div class="col-md-12" style="text-align: right;">
                                    <div class="from-group">
                                        <div class="">
                                            <button type="reset" class="btn submit-button">Reset</button>
                                            <button type="submit" class="btn submit-button">Submit</button>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>





@endsection
