<?php

Route::get('admin-product-filecreate', [
    'as' => 'admin.product.filecreate',
    'uses' => 'ProductController@filecreate'
]);

Route::post('admin-product-filestore', [
    'as' => 'admin.product.filestore',
    'uses' => 'ProductController@filestore'
]);

Route::get('admin-product-fileindex', [
    'as' => 'admin.product.fileindex',
    'uses' => 'ProductController@fileindex'
]);

Route::get('admin-product-filedownload/{type}', [
    'as' => 'admin.product.filedownload',
    'uses' => 'ProductController@filedownload'
]);


Route::get('admin-product-productdelete/{id}', [
    'as' => 'admin.product.productdelete',
    'uses' => 'ProductController@productdelete'
]);

Route::get('admin-product-productcreate', [
    'as' => 'admin.product.productcreate',
    'uses' => 'ProductController@productcreate'
]);

Route::post('admin-product-productstore', [
    'as' => 'admin.product.productstore',
    'uses' => 'ProductController@productstore'
]);

Route::get('admin-product-productedit/{id}', [
    'as' => 'admin.product.productedit',
    'uses' => 'ProductController@productedit'
]);

Route::PATCH('admin-product-productupdate/{id}', [
    'as' => 'admin.product.productupdate',
    'uses' => 'ProductController@productupdate'
]);

Route::get('admin-product-moreattach', [
    'as' => 'admin.product.moreattach',
    'uses' => 'ProductController@moreattach'
]);

Route::get('admin-product-deletemoreattach/{id}', [
    'as' => 'admin.product.deletemoreattach',
    'uses' => 'ProductController@deletemoreattach'
]);

Route::get('admin-product-morefield', [
    'as' => 'admin.product.MoreField',
    'uses' => 'ProductController@MoreField'
]);

Route::get('admin-product-deleteadditional/{id}', [
    'as' => 'admin.product.deleteadditional',
    'uses' => 'ProductController@deleteadditional'
]);








//for api
Route::get('admin-product-brand', [
    'as' => 'admin.product.brand',
    'uses' => 'ProductController@brand'
]);
