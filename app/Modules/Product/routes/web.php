<?php

use Illuminate\Support\Facades\Route;

//Route::get('product', 'ProductController@welcome');
Route::group(['module' => 'Product', 'middleware' => ['web','auth','adminmiddleware']], function() {

    include 'product.php';

});


Route::group(['module' => 'Vendor', 'middleware' => ['web','auth','VendorMiddleware']], function() {
    include 'product.php';
});
