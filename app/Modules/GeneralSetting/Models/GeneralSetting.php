<?php

namespace App\Modules\GeneralSetting\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App;
use Auth;

class GeneralSetting extends Model
{
    use HasFactory;
    protected $table = 'ems_general_setting';
    protected $fillable = [
        'slider_limit',
        'hp_banner_limit',
        'service_limit',
        'hp_brand_limit',
        'hp_category_product_limit',
        'hp_top_weakly_vendor_limit',
        'best_seller_product_limit',
        'deals_hot_product_limit',
        'recommend_product_limit',
        'best_seller_random_order',
        'hot_deals_random_order',
        'recommend_random_order',
        'status',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'company_name',
        'mobile',
        'email','description','facebook','youtube','twitter','instagram'
    ];



    // TODO :: boot
    // boot() function used to insert logged user_id at 'created_by' & 'updated_by'
    public static function boot(){
        parent::boot();
        static::creating(function($query){
            if(Auth::check()){
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function($query){
            if(Auth::check()){
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}
