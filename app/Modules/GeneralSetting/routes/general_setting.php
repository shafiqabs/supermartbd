<?php

Route::get('admin-generalsetting-create', [
    'as' => 'admin.generalsetting.create',
    'uses' => 'GeneralSettingController@create'
]);

Route::get('admin-generalsetting-index', [
    'as' => 'admin.generalsetting.index',
    'uses' => 'GeneralSettingController@index'
]);

Route::post('admin-generalsetting-store',[
    'as' => 'admin.generalsetting.store',
    'uses' => 'GeneralSettingController@store'
]);

Route::get('admin-generalsetting-edit/{id}',[
    'as' => 'admin.generalsetting.edit',
    'uses' => 'GeneralSettingController@edit'
]);

Route::PATCH('admin-generalsetting-update/{id}',[
    'as' => 'admin.generalsetting.update',
    'uses' => 'GeneralSettingController@update'
]);

Route::get('admin-generalsetting-delete/{id}',[
    'as' => 'admin.generalsetting.delete',
    'uses' => 'GeneralSettingController@delete'
]);


