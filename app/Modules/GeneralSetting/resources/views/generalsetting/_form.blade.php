<?php
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request as Input;

?>


    <div class="row">
        <div class="col-md-12">
            <h2 class="seneralsettingsection">Home Page</h2>
            <div class="row">
                <div class="col-md-4">
                    <div class="from-group">
                        {!! Form::label('Slider Limit', 'Slider Limit', array('class' => 'form-label')) !!}
                        <div class="input-group mb-3">
                            {!! Form::text('slider_limit',Input::old('slider_limit'),['id'=>'slider_limit','class' => 'form-control','data-checkify'=>'number','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
                            <span style="color: #ff0000">{!! $errors->first('slider_limit') !!}</span>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="from-group">
                        {!! Form::label('Banner Limit', 'Banner Limit', array('class' => 'form-label')) !!}
                        <div class="input-group mb-3">
                            {!! Form::text('hp_banner_limit',Input::old('hp_banner_limit'),['id'=>'hp_banner_limit','class' => 'form-control','data-checkify'=>'number','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
                            <span style="color: #ff0000">{!! $errors->first('hp_banner_limit') !!}</span>
                        </div>
                    </div>
                </div>


                <div class="col-md-4">
                    <div class="from-group">
                        {!! Form::label('Services Limit', 'Services Limit', array('class' => 'form-label')) !!}
                        <div class="input-group mb-3">
                            {!! Form::text('service_limit',Input::old('service_limit'),['id'=>'service_limit','class' => 'form-control','data-checkify'=>'number','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
                            <span style="color: #ff0000">{!! $errors->first('service_limit') !!}</span>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="from-group">
                        {!! Form::label('Brand Limit', 'Brand Limit', array('class' => 'form-label')) !!}
                        <div class="input-group mb-3">
                            {!! Form::text('hp_brand_limit',Input::old('hp_brand_limit'),['id'=>'hp_brand_limit','class' => 'form-control','data-checkify'=>'number','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
                            <span style="color: #ff0000">{!! $errors->first('hp_brand_limit') !!}</span>
                        </div>
                    </div>
                </div>



                <div class="col-md-4">
                    <div class="from-group">
                        {!! Form::label('Category Product Limit', 'Category Product Limit', array('class' => 'form-label')) !!}
                        <div class="input-group mb-3">
                            {!! Form::text('hp_category_product_limit',Input::old('hp_category_product_limit'),['id'=>'hp_category_product_limit','class' => 'form-control','data-checkify'=>'number','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
                            <span style="color: #ff0000">{!! $errors->first('hp_category_product_limit') !!}</span>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="from-group">
                        {!! Form::label('Top Weakly Vendor Limit', 'Top Weakly Vendor Limit', array('class' => 'form-label')) !!}
                        <div class="input-group mb-3">
                            {!! Form::text('hp_top_weakly_vendor_limit',Input::old('hp_top_weakly_vendor_limit'),['id'=>'hp_top_weakly_vendor_limit','class' => 'form-control','data-checkify'=>'number','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
                            <span style="color: #ff0000">{!! $errors->first('hp_top_weakly_vendor_limit') !!}</span>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h2 class="seneralsettingsection">Best Seller Product</h2>
            <div class="row">
                <div class="col-md-6">
                    <div class="from-group">
                        {!! Form::label('Best Seller Product Limit', 'Best Seller Product Limit', array('class' => 'form-label')) !!}
                        <div class="input-group mb-3">
                            {!! Form::text('best_seller_product_limit',Input::old('best_seller_product_limit'),['id'=>'best_seller_product_limit','class' => 'form-control','data-checkify'=>'number','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
                            <span style="color: #ff0000">{!! $errors->first('best_seller_product_limit') !!}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    {!! Form::label('In Random Order', 'In Random Order', array('class' => 'form-label')) !!}
                    <div class="input-group mb-3">
                        {!! Form::select('best_seller_random_order',array(1=>'Yes',0=>'No'),Input::old('best_seller_random_order'),['id'=>'best_seller_random_order','class' => 'form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('best_seller_random_order') !!}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h2 class="seneralsettingsection">Deals Hot Product</h2>
            <div class="row">
                <div class="col-md-6">
                    <div class="from-group">
                        {!! Form::label('Deals Hot Product Limit', 'Deals Hot Product Limit', array('class' => 'form-label')) !!}

                        <div class="input-group mb-3">
                            {!! Form::text('deals_hot_product_limit',Input::old('deals_hot_product_limit'),['id'=>'deals_hot_product_limit','class' => 'form-control','data-checkify'=>'number','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
                            <span style="color: #ff0000">{!! $errors->first('deals_hot_product_limit') !!}</span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    {!! Form::label('In Random Order', 'In Random Order', array('class' => 'form-label')) !!}
                    <div class="input-group mb-3">
                        {!! Form::select('hot_deals_random_order',array(1=>'Yes',0=>'No'),Input::old('hot_deals_random_order'),['id'=>'hot_deals_random_order','class' => 'form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('hot_deals_random_order') !!}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h2 class="seneralsettingsection">Recommend Product</h2>
            <div class="row">
                <div class="col-md-6">
                    <div class="from-group">
                        {!! Form::label('Recommend Product Limit', 'Recommend Product Limit', array('class' => 'form-label')) !!}
                        <div class="input-group mb-3">
                            {!! Form::text('recommend_product_limit',Input::old('recommend_product_limit'),['id'=>'recommend_product_limit','class' => 'form-control','data-checkify'=>'number','aria-label' =>'keyword','aria-describedby'=>'basic-addon2']) !!}
                            <span style="color: #ff0000">{!! $errors->first('recommend_product_limit') !!}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    {!! Form::label('In Random Order', 'In Random Order', array('class' => 'form-label')) !!}
                    <div class="input-group mb-3">
                        {!! Form::select('recommend_random_order',array(1=>'Yes',0=>'No'),Input::old('recommend_random_order'),['id'=>'recommend_random_order','class' => 'form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('recommend_random_order') !!}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h2 class="seneralsettingsection">Company Information</h2>
            <div class="row">
                <div class="col-md-4">
                    <div class="from-group">
                        {!! Form::label('Company Name', 'Company Name', array('class' => 'form-label')) !!}
                        <div class="input-group mb-3">
                            {!! Form::text('company_name',Input::old('company_name'),['id'=>'company_name','class' => 'form-control']) !!}
                            <span style="color: #ff0000">{!! $errors->first('company_name') !!}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    {!! Form::label('Mobile', 'Mobile', array('class' => 'form-label')) !!}
                    <div class="input-group mb-3">
                        {!! Form::text('mobile',Input::old('mobile'),['id'=>'mobile','class' => 'form-control']) !!}
                        <span style="color: #ff0000">{!! $errors->first('mobile') !!}</span>
                    </div>
                </div>
                <div class="col-md-4">
                    {!! Form::label('Email', 'Email', array('class' => 'form-label')) !!}
                    <div class="input-group mb-3">
                        {!! Form::text('email',Input::old('email'),['id'=>'email','class' => 'form-control']) !!}
                        <span style="color: #ff0000">{!! $errors->first('email') !!}</span>
                    </div>
                </div>
                <div class="col-md-12">
                    {!! Form::label('Company Description', 'Company Description', array('class' => 'form-label')) !!}
                    <div class="input-group mb-3">
                        {!! Form::textarea('description',Input::old('description'),['id'=>'description','class' => 'form-control','rows'=>2]) !!}
                        <span style="color: #ff0000">{!! $errors->first('description') !!}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
    <div class="col-md-12">
        <h2 class="seneralsettingsection">Social Icon</h2>
        <div class="row">
            <div class="col-md-6">
                <div class="from-group">
                    {!! Form::label('Facebook URL (if any)', 'Facebook URL (if any)', array('class' => 'form-label')) !!}
                    <div class="input-group mb-3">
                        {!! Form::text('facebook',Input::old('facebook'),['id'=>'facebook','class' => 'form-control']) !!}
                        <span style="color: #ff0000">{!! $errors->first('facebook') !!}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="from-group">
                    {!! Form::label('Youtube URL (if any)', 'Youtube URL (if any)', array('class' => 'form-label')) !!}
                    <div class="input-group mb-3">
                        {!! Form::text('youtube',Input::old('youtube'),['id'=>'youtube','class' => 'form-control']) !!}
                        <span style="color: #ff0000">{!! $errors->first('youtube') !!}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="from-group">
                    {!! Form::label('Twitter URL (if any)', 'Twitter URL (if any)', array('class' => 'form-label')) !!}
                    <div class="input-group mb-3">
                        {!! Form::text('twitter',Input::old('twitter'),['id'=>'twitter','class' => 'form-control']) !!}
                        <span style="color: #ff0000">{!! $errors->first('twitter') !!}</span>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="from-group">
                    {!! Form::label('Instagram URL (if any)', 'Instagram URL (if any)', array('class' => 'form-label')) !!}
                    <div class="input-group mb-3">
                        {!! Form::text('instagram',Input::old('instagram'),['id'=>'instagram','class' => 'form-control']) !!}
                        <span style="color: #ff0000">{!! $errors->first('instagram') !!}</span>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>


    {{--<div class="row">
        <div class="col-md-4 offset-md-8">
            <div class="from-group">
                <div class="from-group">
                    {!! Form::label('Status', 'Status', array('class' => 'form-label')) !!}
                    <span style="color: red">*</span>
                    <div class="input-group mb-3">
                        {!! Form::select('status',array(1=>'Active',0=>'Inactive'),Input::old('status'),['id'=>'ordering','class' => 'form-control','data-checkify'=>'minlen=1,required,number','Placeholder' => 'Enter ordering number','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                        <span style="color: #ff0000">{!! $errors->first('ordering') !!}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}




    <div class="row">

        <div class="col-md-12" style="text-align: right;">
            <div class="from-group">
                <div class="">
                    <button type="reset" class="btn submit-button">Reset</button>
                    <button type="submit" class="btn submit-button">Submit</button>
                </div>
            </div>
        </div>

    </div>
