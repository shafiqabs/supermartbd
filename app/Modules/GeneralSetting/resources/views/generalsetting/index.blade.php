@extends(/** @lang text */'backend.layouts.master')

@section('body')
    <div class="dashboard-area">
        <div id="carbon-block" class="">
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">{{$ModuleTitle}}</h1>

                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group me-2">

{{--                            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>--}}
{{--                            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>--}}
                        </div>

                        {{--<a style="color: #000;" href="{{route('admin.generalsetting.create')}}" title="Add General Setting" class="module_button_header">
                            <button type="button" class="btn btn-sm btn-outline-secondary">
                                <i class="fas fa-plus-circle"></i> Create New
                            </button>
                        </a>--}}


                    </div>
                </div>
            </main>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">
                            {{$PageTitle}}
                        </div>
                        <div class="card-body">

                            @include('backend.layouts.message')

                            @if(isset($GeneralSetting) && !empty($GeneralSetting))
                                <div class="table-responsive">
                                    <table  class="table table-striped table-bordered text-center" id="">
                                        <thead>
                                        <th>Slider Limit</th>
                                        <th>Banner Limit</th>
                                        <th>Services Limit</th>
                                        <th>Brand Limit</th>
                                        <th>Category Product Limit</th>
                                        <th>Action</th>
                                        </thead>

                                        <tbody>
                                        <?php $i=1; ?>
                                        @foreach($GeneralSetting as $value)
                                            <tr>
                                                <td>{{$value->slider_limit}}</td>
                                                <td>{{$value->hp_banner_limit}}</td>
                                                <td>{{$value->service_limit}}</td>
                                                <td>{{$value->hp_brand_limit}}</td>
                                                <td>{{$value->hp_category_product_limit}}</td>



                                                <td>
                                                    <a href="{{route('admin.generalsetting.edit',$value->id)}}" title="Edit Slider"><i class="fas fa-user-edit"></i></a> |
                                                    <a href="{{route('admin.generalsetting.delete',$value->id)}}" title="Delete Slider" onclick="return confirm('Are you sure to Delete?')"><i class="fas fa-trash-alt"></i></a>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>


@endsection
