<?php

namespace App\Modules\GeneralSetting\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Slider\Models\Slider;
use App\Modules\GeneralSetting\Models\GeneralSetting;
use Illuminate\Http\Request;

use DB;
use Illuminate\Support\Facades\Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class GeneralSettingController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $ModuleTitle = "Manage General Setting";
        $PageTitle = " General Setting list";
        $TableTitle = "General Setting list";

        $GeneralSetting = GeneralSetting::where('status',1)->orderby('id','desc')->get();

        return view("GeneralSetting::generalsetting.index", compact('ModuleTitle','PageTitle','TableTitle','GeneralSetting'));

    }


    public function create(){
        $ModuleTitle = "Manage General Setting";
        $PageTitle = "Add General Setting";
        $TableTitle = "General Setting list";

        return view("GeneralSetting::generalsetting.create", compact('ModuleTitle','PageTitle','TableTitle'));

    }


    public function store(Request $request){
        $input = $request->all();
        DB::beginTransaction();
        try {
            if ($GeneralSettingModel = GeneralSetting::create($input)) {
                $GeneralSettingModel->save();

            }

            DB::commit();
            Session::flash('message', 'Information added Successfully!');
            return redirect()->route('admin.generalsetting.index');
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }

    }



    public function edit($id){
        $ModuleTitle = "Manage General Setting";
        $PageTitle = "Update General Setting";

        $data = GeneralSetting::where('status','1')->where('id',$id)->first();

        return view("GeneralSetting::generalsetting.edit", compact('data','ModuleTitle','PageTitle'));

    }

    public function update(Request $request,$id){
        $input = $request->all();

        $generalSetting = GeneralSetting::find($id);
        if ($generalSetting){
            DB::beginTransaction();
            try {
                $generalSetting->update($input);
                DB::commit();

                Session::flash('message', 'Information Updated Successfully!');
                return redirect()->route('admin.generalsetting.index');

            } catch (\Exception $e) {
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('warning', 'Something went wrong');
            return redirect()->back()->withInput($input);
        }
    }

    public function delete($id){
        /* Transaction Start Here */
        DB::beginTransaction();
        try {
            $SliderModel = Slider::where('id', $id)
                ->select('*')
                ->first();
            File::delete(public_path() . '/backend/image/ProSliderImage/' . $SliderModel->slider_image);
            $SliderModel->delete();

            DB::commit();
            Session::flash('delete', 'Delete Successfully !');
            //            return Redirect::back()->withErrors(['message', 'Delete Successfully !']);
            return redirect('admin-slider-index');

        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }
}
