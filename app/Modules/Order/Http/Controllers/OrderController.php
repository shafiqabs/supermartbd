<?php

namespace App\Modules\Order\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use\App\Models\OrderHead;
use\App\Models\OrderProduct;
use\App\Models\OrderBillingShipping;

class OrderController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $ModuleTitle = "Manage Order information ";
        $PageTitle = " Order information list";
        $TableTitle = "Order information list";

        $OrderHead = OrderHead::orderby('id','desc')->paginate(10);

        return view("Order::order.index", compact('ModuleTitle','PageTitle','TableTitle','OrderHead'));

    }

    public function Orderdetails($id){
        $ModuleTitle = "Manage Order information ";
        $PageTitle = " Order Details View";
        $TableTitle = "Order Details View";

        $OrderHeadDetails = OrderHead::with('OrderProductItem','OrderBillingShipping')->where('id',$id)->first();

        return view("Order::order.orderdetails", compact('ModuleTitle','PageTitle','TableTitle','OrderHeadDetails'));

    }
}
