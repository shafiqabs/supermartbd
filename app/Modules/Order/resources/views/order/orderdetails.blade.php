@extends(/** @lang text */'backend.layouts.master')

@section('body')
    <div class="dashboard-area">
        <div id="carbon-block" class="">
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">{{$ModuleTitle}}</h1>

                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group me-2">

{{--                            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>--}}
{{--                            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>--}}
                        </div>

                        <a style="color: #000;" href="{{route('admin.order.index')}}" title="All Order" class="module_button_header">
                            <button type="button" class="btn btn-sm btn-outline-secondary">
                                <i class="fas fa-th-list"></i> All Order
                            </button>
                        </a>


                    </div>
                </div>
            </main>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">
                            {{$PageTitle}}
                        </div>
                        <div class="card-body">

                            @include('backend.layouts.message')
                        @if(Auth::user()->type == 'Admin')
                            @if(isset($OrderHeadDetails) && !empty($OrderHeadDetails))
                                <div class="table-responsive">
                                    <h5>Order Details </h5>
                                    <table  class="table table-striped table-bordered " id="">
                                        <thead style="background: #800000;color: #fff;">

                                        <th>Order Number</th>
                                        <th>Time Slot</th>
                                        <th>Payment Method</th>
                                        <th>Quantity</th>
                                        <th>Subtotal</th>
                                        <th>Delivery Charge</th>
                                        <th>Total</th>
                                        <th>Status</th>
{{--                                        <th>Time Date</th>--}}
                                        </thead>

                                        <tbody>

                                            <tr>

                                                <td>
                                                    <a href="{{route('admin.order.details',$OrderHeadDetails->id)}}" title="Show Order Details">{{$OrderHeadDetails->order_number}}</a>
                                                </td>
                                                <td>
                                                    @php
                                                    $myArray = explode('-', $OrderHeadDetails->shipping_time_slot);
                                                    @endphp
                                                    {{date('h:i:s a', strtotime($myArray[0]))}}-{{date('h:i:s a', strtotime($myArray[1]))}}
                                                </td>
                                                <td>
                                                    {{$OrderHeadDetails->payment_method}}
                                                </td>
                                                <td>{{$OrderHeadDetails->total_quantity}}</td>
                                                <td>{{$OrderHeadDetails->product_amount}}</td>
                                                <td>{{$OrderHeadDetails->delivery_charge}}</td>
                                                <td>{{$OrderHeadDetails->total_amount}}</td>
                                                <?php
                                                if ($OrderHeadDetails->status == '1'){
                                                    $status = 'Process';
                                                }elseif ($OrderHeadDetails->status == '2'){
                                                    $status = 'Confirm';
                                                }elseif ($OrderHeadDetails->status == '3'){
                                                    $status = 'Pending';
                                                }elseif ($OrderHeadDetails->status == '4'){
                                                    $status = 'Approved';
                                                }else{
                                                    $status = 'Return';
                                                }
                                                ?>
                                                <td>{{$status}}</td>
{{--                                                <td>{{date('h:i:s a m/d/Y', strtotime($OrderHeadDetails->created_at))}}</td>--}}


                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            @endif



                            @if(isset($OrderHeadDetails['OrderProductItem']) && !empty($OrderHeadDetails['OrderProductItem']))
                                <div class="table-responsive" style="overflow-x: clip;">
                                    <h5>Product Details </h5>
                                    <table  class="table table-striped table-bordered ">
                                        <thead style="background: #05618e;color: #fff;">
                                        <th>Name</th>
                                        <th>Image</th>
                                        <th>Quantity</th>
                                        <th>U. Price</th>
                                        <th>Subtotal</th>
                                        </thead>

                                        <tbody>

                                        @foreach($OrderHeadDetails['OrderProductItem'] as $value)
                                        <tr>

                                            <?php
//                                                echo '<pre>';
//                                                print_r($value);
                                            ?>

                                            <td>{{$value->name.' ( '.$value->VendorDetails->store_name .' - '. $value->VendorDetails->mobile.' )' }}</td>

                                            <td>
                                                <div class="tooltipme">Image
                                                    <span class="tooltipmetext">
                                                                <img class="hover_image" src="{{ asset('backend/image/ProProductImage').'/'.$value->ProductDetails->feature_image;}}" alt="">

                                                        </span>
                                                </div>
                                            </td>
                                            <td>{{$value->quantity}}</td>
                                            <td>{{$value->unit_price}}</td>
                                            <td>{{$value->subtotal}}</td>
                                        </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            @endif


                            @if(isset($OrderHeadDetails['OrderBillingShipping']) && !empty($OrderHeadDetails['OrderBillingShipping']))
                                <div class="table-responsive">
                                    <h5>Address Details</h5>
                                    <table  class="table table-striped table-bordered ">
                                        <thead style="background: #04656f;color: #fff;">

                                        <th>Type</th>
                                        <th>Name</th>
                                        <th>Mobile</th>
                                        <th>Email</th>
                                        <th>Address</th>
                                        <th>District</th>
                                        <th>Country</th>

                                        </thead>

                                        <tbody>
                                        @foreach($OrderHeadDetails['OrderBillingShipping'] as $value)
                                            <tr>
                                                <td>{{$value->type}}</td>
                                                <td>{{$value->firstname.' '.$value->lastname}}</td>
                                                <td>{{$value->phone}}</td>
                                                <td>{{$value->email}}</td>
                                                <td>{{$value->address}}</td>
                                                <td>{{$value->district}}</td>
                                                <td>{{$value->country}}</td>



                                            </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        @endif


                            @if(Auth::user()->type == 'Vendor')
                                @if(isset($OrderHeadDetails) && !empty($OrderHeadDetails))
                                    <div class="table-responsive">
                                        <h5>Order Details </h5>
                                        <table  class="table table-striped table-bordered " id="">
                                            <thead style="background: #800000;color: #fff;">

                                            <th>Order Number</th>
                                            <th>Time Slot</th>
                                            <th>Payment Method</th>

                                            <th>Status</th>
                                            {{--                                        <th>Time Date</th>--}}
                                            </thead>

                                            <tbody>

                                            <tr>

                                                <td>
                                                    <a href="{{route('admin.order.details',$OrderHeadDetails->id)}}" title="Show Order Details">{{$OrderHeadDetails->order_number}}</a>
                                                </td>
                                                <td>
                                                    @php
                                                        $myArray = explode('-', $OrderHeadDetails->shipping_time_slot);
                                                    @endphp
                                                    {{date('h:i:s a', strtotime($myArray[0]))}}-{{date('h:i:s a', strtotime($myArray[1]))}}
                                                </td>
                                                <td>
                                                    {{$OrderHeadDetails->payment_method}}
                                                </td>

                                                <?php
                                                if ($OrderHeadDetails->status == '1'){
                                                    $status = 'Process';
                                                }elseif ($OrderHeadDetails->status == '2'){
                                                    $status = 'Confirm';
                                                }elseif ($OrderHeadDetails->status == '3'){
                                                    $status = 'Pending';
                                                }elseif ($OrderHeadDetails->status == '4'){
                                                    $status = 'Approved';
                                                }else{
                                                    $status = 'Return';
                                                }
                                                ?>
                                                <td>{{$status}}</td>
                                                {{--<td>{{date('h:i:s a m/d/Y', strtotime($OrderHeadDetails->created_at))}}</td>--}}


                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                @endif



                                @if(isset($OrderHeadDetails['OrderProductItem']) && !empty($OrderHeadDetails['OrderProductItem']))
                                    <div class="table-responsive" style="overflow-x: clip;">
                                        <h5>Product Details </h5>
                                        <table  class="table table-striped table-bordered ">
                                            <thead style="background: #05618e;color: #fff;">
                                            <th>Name</th>
                                            <th>Image</th>
                                            <th>Quantity</th>
                                            <th>U. Price</th>
                                            <th>Subtotal</th>
                                            </thead>

                                            <tbody>
                                            <?php
                                                $TotalPrice = 0;
                                            ?>
                                            @foreach($OrderHeadDetails['OrderProductItem'] as $value)
                                                @if($value->vendor_id == Auth::user()->UserVendor->id)
                                                <tr>
                                                    <td>{{$value->name}}</td>

                                                    <td>
                                                        <div class="tooltipme">Image
                                                            <span class="tooltipmetext">
                                                                <img class="hover_image" src="{{ asset('backend/image/ProProductImage').'/'.$value->ProductDetails->feature_image;}}" alt="">

                                                        </span>
                                                        </div>
                                                    </td>
                                                    <td>{{$value->quantity}}</td>
                                                    <td>{{$value->unit_price}}</td>
                                                    <td>{{$value->subtotal}}</td>
                                                </tr>
                                                    <?php
                                                    $TotalPrice = $TotalPrice+$value->subtotal
                                                    ?>
                                                @endif
                                            @endforeach

                                            </tbody>
                                            <tr style="background: #e8feffeb;">
                                                <td colspan="4">Total</td>
                                                <td>{{number_format($TotalPrice,2)}}</td>
                                            </tr>
                                        </table>
                                    </div>
                                @endif


                                @if(isset($OrderHeadDetails['OrderBillingShipping']) && !empty($OrderHeadDetails['OrderBillingShipping']))
                                    <div class="table-responsive">
                                        <h5>Address Details</h5>
                                        <table  class="table table-striped table-bordered ">
                                            <thead style="background: #04656f;color: #fff;">

                                            <th>Type</th>
                                            <th>Name</th>
                                            <th>Mobile</th>
                                            <th>Email</th>
                                            <th>Address</th>
                                            <th>District</th>
                                            <th>Country</th>

                                            </thead>

                                            <tbody>
                                            @foreach($OrderHeadDetails['OrderBillingShipping'] as $value)
                                                <tr>
                                                    <td>{{$value->type}}</td>
                                                    <td>{{$value->firstname.' '.$value->lastname}}</td>
                                                    <td>{{$value->phone}}</td>
                                                    <td>{{$value->email}}</td>
                                                    <td>{{$value->address}}</td>
                                                    <td>{{$value->district}}</td>
                                                    <td>{{$value->country}}</td>



                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>


@endsection
