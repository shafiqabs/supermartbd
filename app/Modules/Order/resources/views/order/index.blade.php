@extends(/** @lang text */'backend.layouts.master')

@section('body')
    <div class="dashboard-area">
        <div id="carbon-block" class="">
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">{{$ModuleTitle}}</h1>

                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group me-2">

{{--                            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>--}}
{{--                            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>--}}
                        </div>

{{--                        <a style="color: #000;" href="{{route('admin.brand.create')}}" title="Add Size Unit" class="module_button_header">--}}
{{--                            <button type="button" class="btn btn-sm btn-outline-secondary">--}}
{{--                                <i class="fas fa-plus-circle"></i> Create New--}}
{{--                            </button>--}}
{{--                        </a>--}}


                    </div>
                </div>
            </main>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">


                        <div class="card-header">
                            {{$PageTitle}}
                        </div>
                        <div class="card-body">

                            @include('backend.layouts.message')

                            @if(isset($OrderHead) && !empty($OrderHead))

                                <div class="table-responsive">
                                    <table  class="table table-striped table-bordered text-center" id="">
                                        <thead>
                                        <th>SL</th>
                                        <th>Order Number</th>
                                        <th>Time Slot</th>
                                        @if(Auth::user()->type != 'Vendor')
                                        <th>Payment Method</th>
                                        <th>Quantity</th>
                                        <th>Subtotal</th>
                                        <th>Delivery Charge</th>
                                        <th>Total</th>
                                        @endif
                                        <th>Status</th>
                                        <th>Action</th>
                                        </thead>

                                        <tbody>
                                        <?php $i=1; ?>
                                        @foreach($OrderHead as $value)

                                            @if(Auth::user()->type == 'Admin')
                                            <tr>
                                                <td>{{$i++}}</td>
                                                <td>
                                                    <a href="{{route('admin.order.details',$value->id)}}" title="Show Order Details">{{$value->order_number}}</a>
                                                </td>
                                                <td>
                                                    @php
                                                    $myArray = explode('-', $value->shipping_time_slot);
                                                    @endphp
                                                    {{date('h:i:s a', strtotime($myArray[0]))}}-{{date('h:i:s a', strtotime($myArray[1]))}}
                                                </td>
                                                <td>
                                                    {{$value->payment_method}}
                                                </td>
                                                <td>{{$value->total_quantity}}</td>
                                                <td>{{$value->product_amount}}</td>
                                                <td>{{$value->delivery_charge}}</td>
                                                <td>{{$value->total_amount}}</td>
                                                <?php
                                                if ($value->status == '1'){
                                                    $status = 'Process';
                                                }elseif ($value->status == '2'){
                                                    $status = 'Confirm';
                                                }elseif ($value->status == '3'){
                                                    $status = 'Pending';
                                                }elseif ($value->status == '4'){
                                                    $status = 'Approved';
                                                }else{
                                                    $status = 'Return';
                                                }
                                                ?>
                                                <td>{{$status}}</td>

                                                <td>
                                                    <a href="{{route('admin.order.details',$value->id)}}" title="Show Order Details"><i class="fas fa-eye"></i></a>

                                                </td>
                                            </tr>
                                            @endif

                                            <?php
                                            $VendorId = explode(',',$value->vendor_id);
//                                            echo '<pre>';
//                                            print_r($VendorId);
//                                            $something = array('a' => 'bla', 'b' => 'omg');
//                                            if (in_array(Auth::user()->UserVendor->id, $VendorId)) {
//                                                echo "ase";
//                                            }else{
//                                                echo 'nei';
//                                            }
//                                            echo Auth::user()->UserVendor->id;
//                                            ?>

                                            @if(Auth::user()->type == 'Vendor')
                                                @if(in_array(Auth::user()->UserVendor->id, $VendorId))
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td>
                                                        <a href="{{route('admin.order.details',$value->id)}}" title="Show Order Details">{{$value->order_number}}</a>
                                                    </td>
                                                    <td>
                                                        @php
                                                            $myArray = explode('-', $value->shipping_time_slot);
                                                        @endphp
                                                        {{date('h:i:s a', strtotime($myArray[0]))}}-{{date('h:i:s a', strtotime($myArray[1]))}}
                                                    </td>
                                                    <td>
                                                        {{$value->payment_method}}
                                                    </td>

{{--                                                    <td>{{$value->total_quantity}}</td>--}}
{{--                                                    <td>{{$value->product_amount}}</td>--}}
{{--                                                    <td>{{$value->delivery_charge}}</td>--}}
{{--                                                    <td>{{$value->total_amount}}</td>--}}
                                                    <?php
                                                    if ($value->status == '1'){
                                                        $status = 'Process';
                                                    }elseif ($value->status == '2'){
                                                        $status = 'Confirm';
                                                    }elseif ($value->status == '3'){
                                                        $status = 'Pending';
                                                    }elseif ($value->status == '4'){
                                                        $status = 'Approved';
                                                    }else{
                                                        $status = 'Return';
                                                    }
                                                    ?>
                                                    <td>{{$status}}</td>

                                                    <td>
                                                        <a href="{{route('admin.order.details',$value->id)}}" title="Show Order Details"><i class="fas fa-eye"></i></a>

                                                    </td>
                                                </tr>
                                                @endif
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class=" justify-content-right">
                                    {{ $OrderHead->links('backend.layouts.pagination') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>


@endsection
