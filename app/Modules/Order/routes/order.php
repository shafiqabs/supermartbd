<?php

Route::get('admin-order-index', [
    'as' => 'admin.order.index',
    'uses' => 'OrderController@index'
]);

Route::get('admin-order-details/{id}',[
    'as' => 'admin.order.details',
    'uses' => 'OrderController@Orderdetails'
]);
