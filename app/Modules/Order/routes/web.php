<?php

use Illuminate\Support\Facades\Route;

//Route::get('order', 'OrderController@welcome');
Route::group(['module' => 'Order', 'middleware' => ['web','auth','adminmiddleware']], function() {
    include 'order.php';
});


Route::group(['module' => 'Order', 'middleware' => ['web','auth','VendorMiddleware']], function() {
    include 'order.php';
});
