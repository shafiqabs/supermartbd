<?php

Route::get('admin-customer-create', [
    'as' => 'admin.customer.create',
    'uses' => 'CustomerController@create'
]);

Route::get('admin-customer-findupazala', [
    'as' => 'admin.customer.findupazala',
    'uses' => 'CustomerController@findupazala'
]);

Route::get('admin-customer-index', [
    'as' => 'admin.customer.index',
    'uses' => 'CustomerController@index'
]);

Route::post('admin-customer-store',[
    'as' => 'admin.customer.store',
    'uses' => 'CustomerController@store'
]);

Route::get('admin-customer-edit/{id}',[
    'as' => 'admin.customer.edit',
    'uses' => 'CustomerController@edit'
]);

Route::PATCH('admin-customer-update/{id}',[
    'as' => 'admin.customer.update',
    'uses' => 'CustomerController@update'
]);

Route::get('admin-customer-delete/{id}',[
    'as' => 'admin.customer.delete',
    'uses' => 'CustomerController@delete'
]);

