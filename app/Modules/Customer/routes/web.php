<?php

use Illuminate\Support\Facades\Route;

//Route::get('customer', 'CustomerController@welcome');


Route::group(['module' => 'Customer', 'middleware' => ['web', 'auth','adminmiddleware']], function () {
    include 'customer.php';
});

Route::PATCH('admin-customer-update/{id}',[
    'as' => 'admin.customer.update',
    'uses' => 'CustomerController@update'
]);

Route::get('admin-customer-findupazala', [
    'as' => 'admin.customer.findupazala',
    'uses' => 'CustomerController@findupazala'
]);


//Route::group(['module' => 'Vendor', 'middleware' => ['web', 'auth',]], function () {
//    include 'customer.php';
//});


Route::group(['module' => 'Customer', 'middleware' => ['web', 'auth', 'CustomerMiddleware']], function () {
    include 'webcustomer.php';
});

