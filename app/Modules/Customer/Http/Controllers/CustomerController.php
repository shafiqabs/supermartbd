<?php

namespace App\Modules\Customer\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FrontendController;
use App\Modules\MasterData\Models\Category;
use App\Modules\User\Models\User;
use App\Modules\Vendor\Models\Vendor;
use App\Modules\Customer\Models\Customer;

use Illuminate\Http\Request;
use App\Modules\Customer\Requests;
use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class CustomerController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $ModuleTitle = "Manage Customer Information ";
        $PageTitle = "Add Customer Information";
        $TableTitle = "Customer Information list";

        $AllDistrict = DB::table('districts')->orderby('name','asc')->pluck('name','id')->all();
        $AllDistrict[''] = 'Choose a District';
        ksort($AllDistrict);

        $AllCountry = DB::table('countries')->orderby('country_name','asc')->pluck('country_name','id')->all();
        $AllCountry[''] = 'Choose a Country';
        ksort($AllCountry);

        return view("Customer::customer.create", compact('AllDistrict','AllCountry','ModuleTitle','PageTitle','TableTitle'));

    }

    public function findupazala(){
        $DistrictId = $_GET['id'];
//        echo $DistrictId;
        $GetUpazila = DB::table('upazilas')->where('district_id',$DistrictId)->get();

        if (count($GetUpazila)>0){
            $view = \Illuminate\Support\Facades\View::make('Customer::customer._upazila_option_field',compact('GetUpazila'));
            $response['message'] = 'success';
            $contents = $view->render();
            $response['content'] = $contents;
        }else{
            $response['message'] = 'Not found';
        }
        return $response;
    }

    public function store(Requests\CustomerRequest $request){
        $input = $request->all();
        $input['name'] = $input['first_name'].' '.$input['last_name'];

        $slug = str_replace(' ', '-', $input['name']);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $slug = strtolower($slug);

        if (isset($input['bill_country_id']) && !empty($input['bill_country_id'])){
            $GetCountryName = DB::table('countries')->where('id',$input['bill_country_id'])->first();
            $input['bill_country_name'] = $GetCountryName->country_name;
        }

        if (isset($input['bill_district_id']) && !empty($input['bill_district_id'])){
            $GetDistrictName = DB::table('districts')->where('id',$input['bill_district_id'])->first();
            $input['bill_district_name'] = $GetDistrictName->name;
        }

        if (isset($input['bill_upazila_id']) && !empty($input['bill_upazila_id'])){
            $GetUpazilaName = DB::table('upazilas')->where('id',$input['bill_upazila_id'])->first();
            $input['bill_upazila_name'] = $GetUpazilaName->name;
        }


        if (isset($input['ship_country_id']) && !empty($input['ship_country_id'])){
            $ShipCountryName = DB::table('countries')->where('id',$input['ship_country_id'])->first();
            $input['ship_country_name'] = $ShipCountryName->country_name;
        }

        if (isset($input['ship_district_id']) && !empty($input['ship_district_id'])){
            $ShipDistrictName = DB::table('districts')->where('id',$input['ship_district_id'])->first();
            $input['ship_district_name'] = $ShipDistrictName->name;
        }

        if (isset($input['ship_upazila_id']) && !empty($input['ship_upazila_id'])){
            $ShipUpazilaName = DB::table('upazilas')->where('id',$input['ship_upazila_id'])->first();
            $input['ship_upazila_name'] = $ShipUpazilaName->name;
        }

        $EmailExistsOrNot = User::where('email',$input['email'])->count();
        if ($EmailExistsOrNot == 0 ) {
            $MobileExistsOrNot = Customer::where('mobile',$input['mobile'])->count();
            if ($MobileExistsOrNot == 0 ){
            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // Store customer data
                    $Password = rand(10,100000).''.$input['mobile'];
                    $UserInfo['password'] = password_hash($Password,PASSWORD_DEFAULT) ;
                    $UserInfo['name'] = $input['name'];
                    $UserInfo['mobile'] = $input['mobile'];
                    $UserInfo['email'] = $input['email'];
                    $UserInfo['address'] = $input['bill_street_address'].' , '.$input['bill_upazila_name'].' , '.$input['bill_district_name'].' , '.$input['bill_country_name'];
                    $UserInfo['type'] = 'Customer';
                    $UserInfo['user_image'] = 'defaultuser.png';
                    $UserInfo['status'] = $input['status'];
                    $UserCreate = App\Modules\User\Models\User::create($UserInfo);
                    $input['user_id'] = $UserCreate->id;

                    $details = [
                        'mailpage' => 'UserMail',
                        'title' => 'Supermartbd Login Information',
                        'name' => 'Hi ' . $input['name'],
                        'Email' => $input['email'],
                        'Type' => 'Customer',
                        'Password' => $Password
                    ];

                    \Mail::to($input['email'])->send(new \App\Mail\MailSend($details));

                if ($UserCreate){
                    if ($request->file('customer_image') != '') {
                        $avatar = $request->file('customer_image');
                        $file_title = $slug.time().'.'.$avatar->getClientOriginalExtension();
                        $input['customer_image'] = $file_title;
                        $path = public_path("backend/image/ProCustomerImage/");
                        $target_file =  $path.basename($file_title);
                        $file_path = $_FILES['customer_image']['tmp_name'];
                        move_uploaded_file($file_path,$target_file);
                    }
                    if ($CustomerData = Customer::create($input)) {
                        $CustomerData->save();
                    }
                }


                DB::commit();
                Session::flash('message', 'Information added Successfully!');
                return redirect()->route('admin.customer.index');
            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
            }else{
                Session::flash('validate', 'Mobile already exists');
                return redirect()->back()->withInput($input);
            }
        }else{
            Session::flash('validate', 'Email already exists');
            return redirect()->back()->withInput($input);
        }

    }



    public function index(){
        $ModuleTitle = "Manage Customer Information ";
        $PageTitle = " Customer Information list";
        $TableTitle = "Customer Information list";

        if (isset(Auth::user()->type) && Auth::user()->type == 'Admin'){
            $CustomerData = Customer::where('status',1)->orderby('id','desc')->paginate(10);
        }

        if (isset(Auth::user()->type) && Auth::user()->type == 'Customer'){
            $CustomerData = Customer::where('status',1)->where('user_id',Auth::user()->id)->paginate(10);
        }


        return view("Customer::customer.index", compact('ModuleTitle','PageTitle','TableTitle','CustomerData'));

    }

    public function edit($id){
        $ModuleTitle = "Manage Customer Information";
        $PageTitle = "Update Customer Information";

        $data = Customer::where('status','1')->where('id',$id)->first();

        $AllDistrict = DB::table('districts')->orderby('name','asc')->pluck('name','id')->all();
        $AllDistrict[''] = 'Choose a District';
        ksort($AllDistrict);

        $AllCountry = DB::table('countries')->orderby('country_name','asc')->pluck('country_name','id')->all();
        $AllCountry[''] = 'Choose a Country';
        ksort($AllCountry);

        return view("Customer::customer.edit", compact('AllCountry','AllDistrict','data','ModuleTitle','PageTitle'));

    }

    public function update(Requests\CustomerRequest $request,$id){
        $input = $request->all();

//        echo '<pre>';
//        print_r($input);
//        exit();

        $EmailExistsOrNot = User::where('email',$input['email'])->count();
        $EmailToId = Customer::where('email',$input['email'])->first();
        if ($EmailExistsOrNot == 0 || ($EmailExistsOrNot == 1 && $EmailToId->id == $id )) {
            $MobileExistsOrNot = Customer::where('mobile',$input['mobile'])->count();
            $MobileToId = Customer::where('mobile',$input['mobile'])->first();

            if (($MobileExistsOrNot == 0) || ($MobileExistsOrNot == 1 && $MobileToId->id == $id)){
                if (isset($input['bill_country_id']) && !empty($input['bill_country_id'])){
                    $GetCountryName = DB::table('countries')->where('id',$input['bill_country_id'])->first();
                    $input['bill_country_name'] = $GetCountryName->country_name;
                }

                if ($input['bill_country_id'] == 19){
                    if (isset($input['bill_district_id']) && !empty($input['bill_district_id'])){
                        $GetDistrictName = DB::table('districts')->where('id',$input['bill_district_id'])->first();
                        $input['bill_district_name'] = $GetDistrictName->name;
                    }

                    if (isset($input['bill_upazila_id']) && !empty($input['bill_upazila_id'])){
                        $GetUpazilaName = DB::table('upazilas')->where('id',$input['bill_upazila_id'])->first();
                        $input['bill_upazila_name'] = $GetUpazilaName->name;
                    }
                }else{
                    $input['bill_district_id'] = null;
                    $input['bill_upazila_id'] = null;
                    $input['bill_district_name'] = null;
                    $input['bill_upazila_name'] = null;
                }



                if (isset($input['ship_country_id']) && !empty($input['ship_country_id'])){
                    $ShipCountryName = DB::table('countries')->where('id',$input['ship_country_id'])->first();
                    $input['ship_country_name'] = $ShipCountryName->country_name;
                }

                if ($input['ship_country_id'] == 19) {

                    if (isset($input['ship_district_id']) && !empty($input['ship_district_id'])) {
                        $ShipDistrictName = DB::table('districts')->where('id', $input['ship_district_id'])->first();
                        $input['ship_district_name'] = $ShipDistrictName->name;
                    }

                    if (isset($input['ship_upazila_id']) && !empty($input['ship_upazila_id'])) {
                        $ShipUpazilaName = DB::table('upazilas')->where('id', $input['ship_upazila_id'])->first();
                        $input['ship_upazila_name'] = $ShipUpazilaName->name;
                    }
                }else{
                    $input['ship_district_id'] = null;
                    $input['ship_upazila_id'] = null;
                    $input['ship_district_name'] = null;
                    $input['ship_upazila_name'] = null;
                }



                $CustomerUpdateModel = Customer::where('id',$id)->first();
                $name = $input['first_name'].' '.$input['last_name'];
                $slug = str_replace(' ', '-', $name);
                $slug = str_replace("/\s+/", "-", $slug);
                $slug = str_replace(".", "-", $slug);
                $slug = strtolower($slug);

                if ($request->file('customer_image') != '') {
                    File::delete(public_path().'/backend/image/ProCustomerImage/'.$CustomerUpdateModel->customer_image);
                    $avatar = $request->file('customer_image');
                    $file_title = $slug.time().'.'.$avatar->getClientOriginalExtension();
                    $input['customer_image'] = $file_title;
                    $path = public_path("backend/image/ProCustomerImage/");
                    $target_file =  $path.basename($file_title);
                    $file_path = $_FILES['customer_image']['tmp_name'];
                    $result = move_uploaded_file($file_path,$target_file);
                }else{
                    $input['customer_image'] = $CustomerUpdateModel['customer_image'];
                }

                /* Transaction Start Here */
                DB::beginTransaction();
                try {
                    // update vendor data
                    $result = $CustomerUpdateModel->update($input);
                    $CustomerUpdateModel->save();

                    DB::commit();

                    Session::flash('message', 'Information Updated Successfully!');
                    if ($input['partvalue'] == 'frontend'){
                        return redirect()->back();
                    }else{
                        return redirect()->route('admin.customer.index');
                    }


                } catch (\Exception $e) {
                    //If there are any exceptions, rollback the transaction`
                    DB::rollback();
                    print($e->getMessage());
                    exit();
                    Session::flash('danger', $e->getMessage());
                }

            }else{
                Session::flash('validate', 'Mobile already exists');
                return redirect()->back()->withInput($input);
            }
        }else{
            Session::flash('validate', 'Email already exists');
            return redirect()->back()->withInput($input);
        }
    }

    public function delete($id)
    {
        /* Transaction Start Here */
        DB::beginTransaction();
        try {
            $VendorExistsOrNot = Vendor::where('id', $id)->first();
            $ProductVendor = App\Modules\Product\Models\Product::where('vendor_id', $id)->count();

            if ($ProductVendor == 0) {

                $VendorModel = Vendor::where('id', $id)
                    ->select('*')
                    ->first();

                if (isset($VendorModel->store_image) && !empty($VendorModel->store_image)) {
                    File::delete(public_path() . '/backend/image/ProVendorImage/' . $VendorModel->store_image);
                    $VendorModel->delete();
                } else {
                    $VendorModel->delete();
                }

                $UserModel = User::where('id', $VendorExistsOrNot->user_id)->first();
                if (isset($UserModel->user_image) && !empty($UserModel->user_image) && $UserModel->user_image != 'defaultuser.png') {
                    File::delete(public_path() . '/backend/image/UserImage/' . $UserModel->user_image);
                }
                $UserModel->delete();
                DB::commit();

                Session::flash('validate', 'User remove Successfully');
                return redirect('admin-vendor-index');
            } else {
                Session::flash('validate', 'This vendor has total ' . $ProductVendor . ' products.');
                return redirect('admin-vendor-index');
            }
//            }
            DB::commit();
        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
    }

    public function WebCustomerLogin(){
        $TabHeader = 'Customer Login';

        $response['GroupWiseMyAccountPage'] = (new FrontendController)->GroupWiseGeneralPage('myaccount');
        $response['GroupWiseCustomerServicePage'] = (new FrontendController)->GroupWiseGeneralPage('customerservice');
        $response['GroupWiseCompanyPage'] = (new FrontendController)->GroupWiseGeneralPage('company');
        $response['contact-us'] = (new FrontendController)->SlugWiseGeneralPage('contact-us');
        $response['about-us'] = (new FrontendController)->SlugWiseGeneralPage('about-us');
        $response['career'] = (new FrontendController)->SlugWiseGeneralPage('career');
        $response['WebMenu'] = Category::getWebMenu();
        $response['Category'] = Category::AllCategory();
        $response['general_setting'] = (new FrontendController)->generalSetting();


//
        return view("Customer::dashboard.loginpage",compact('TabHeader','response'));
    }

    public function forgetpassword(){
        $TabHeader = 'Forget Password';
        $response['GroupWiseMyAccountPage'] = (new FrontendController)->GroupWiseGeneralPage('myaccount');
        $response['GroupWiseCustomerServicePage'] = (new FrontendController)->GroupWiseGeneralPage('customerservice');
        $response['GroupWiseCompanyPage'] = (new FrontendController)->GroupWiseGeneralPage('company');
        $response['Category'] = Category::AllCategory();
        $response['contact-us'] = (new FrontendController)->SlugWiseGeneralPage('contact-us');
        $response['about-us'] = (new FrontendController)->SlugWiseGeneralPage('about-us');
        $response['career'] = (new FrontendController)->SlugWiseGeneralPage('career');
        $response['WebMenu'] = Category::getWebMenu();
        $response['general_setting'] = (new FrontendController)->generalSetting();


        return view("Customer::dashboard.forgetpassword",compact('TabHeader','response'));
    }

    public function resetcode(Request $request){
        $TabHeader = 'Forget Password';
        $response['GroupWiseMyAccountPage'] = (new FrontendController)->GroupWiseGeneralPage('myaccount');
        $response['GroupWiseCustomerServicePage'] = (new FrontendController)->GroupWiseGeneralPage('customerservice');
        $response['GroupWiseCompanyPage'] = (new FrontendController)->GroupWiseGeneralPage('company');
        $response['Category'] = Category::AllCategory();
        $response['contact-us'] = (new FrontendController)->SlugWiseGeneralPage('contact-us');
        $response['about-us'] = (new FrontendController)->SlugWiseGeneralPage('about-us');
        $response['career'] = (new FrontendController)->SlugWiseGeneralPage('career');
        $response['WebMenu'] = Category::getWebMenu();
        $response['general_setting'] = (new FrontendController)->generalSetting();


        $input = $request->all();
        if (isset($input['resetcode']) && !empty($input['resetcode'])){
           if (!filter_var($input["emailormobile"], FILTER_VALIDATE_EMAIL)) {
                $SelectUser = App\Modules\User\Models\User::where('resetcode',$input['resetcode'])->where('mobile',$input["emailormobile"])->where('type','Customer')->first();
            }else{
                $SelectUser = App\Modules\User\Models\User::where('resetcode',$input['resetcode'])->where('email',$input["emailormobile"])->where('type','Customer')->first();
            }

           if ($SelectUser === null){

               if (!filter_var($input["emailormobile"], FILTER_VALIDATE_EMAIL)) {
                   $SelectUser = App\Modules\User\Models\User::where('mobile',$input["emailormobile"])->where('type','Customer')->first();
               }else{
                   $SelectUser = App\Modules\User\Models\User::where('email',$input["emailormobile"])->where('type','Customer')->first();
               }
               $msg = 'You enter wrong code';
               return view("Customer::dashboard.forgetpassword", compact('msg','TabHeader', 'SelectUser','response'));
           }else{
               return view("Customer::dashboard.forgetpassword",compact('TabHeader','input','response'));
           }
        }elseif(isset($input['varifycode']) && isset($input['newpassword'])){
            if ($input['newpassword'] == $input['confirmpassword']){
                if (!filter_var($input["emailormobile"], FILTER_VALIDATE_EMAIL)) {
                    $SelectUser = App\Modules\User\Models\User::where('resetcode',$input['varifycode'])->where('mobile',$input["emailormobile"])->where('type','Customer')->first();
                }else{
                    $SelectUser = App\Modules\User\Models\User::where('resetcode',$input['varifycode'])->where('email',$input["emailormobile"])->where('type','Customer')->first();
                }
                if ($SelectUser === null){
                    echo 'not exists';
                }else{
                    $UpdateUser['password'] = password_hash($input['newpassword'],PASSWORD_DEFAULT) ;
                    $UpdateUser['resetcode'] = null;
                    $SelectUser->update($UpdateUser);
                    $SelectUser->save();
                    $msg = 'Password Change Successfully';
                    return view("Customer::dashboard.loginpage",compact('TabHeader','msg','response'));

                }
            }else{
                $input['resetcode'] = $input['varifycode'];
                $msg = 'You enter different password';
                return view("Customer::dashboard.forgetpassword",compact('TabHeader','input','msg','response'));
            }
        }else{
            if (!filter_var($input["emailormobile"], FILTER_VALIDATE_EMAIL)) {
                $SelectUser = App\Modules\User\Models\User::where('mobile',$input["emailormobile"])->where('type','Customer')->first();
            }else{
                $SelectUser = App\Modules\User\Models\User::where('email',$input["emailormobile"])->where('type','Customer')->first();
            }

            if ($SelectUser === null){
                $msg = 'User information not found ';
                return view("Customer::dashboard.forgetpassword",compact('TabHeader','msg','response'));
            }else {
                $resetcode['resetcode'] = rand(1, 1000000);
                $details = [
                    'mailpage' => 'CustomerResetCode',
                    'title' => 'Supermartbd Account Reset Code',
                    'name' => 'Hi ' . $SelectUser->name,
                    'Email' => $SelectUser->email,
                    'Type' => 'Customer',
                    'resetcode' => $resetcode['resetcode']
                ];

                \Mail::to($SelectUser->email)->send(new \App\Mail\MailSend($details));
                $SelectUser->update($resetcode);
                $SelectUser->save;
                return view("Customer::dashboard.forgetpassword", compact('TabHeader', 'SelectUser','response'));
            }
        }

    }
}
