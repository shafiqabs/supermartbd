<?php

namespace App\Modules\Customer\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FrontendController;
use App\Modules\MasterData\Models\Category;
use App\Modules\User\Models\User;
use App\Modules\Vendor\Models\Vendor;
use App\Modules\Customer\Models\Customer;

use Illuminate\Http\Request;
use App\Modules\Customer\Requests;
use DB;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class WebCustomerController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */
    public function MyAccount(){
        $TabHeader = Auth::user()->name.' Account';
        $user_data = \Auth::user();
        if(Auth::check() && $user_data->type != 'Customer'){
            Auth::logout();
        }else{
            $UserInfo = App\Modules\User\Models\User::where('id',Auth::user()->id)->where('type','Customer')->first();
            $CustomerInfo = $UserInfo->UserCustomer;
            $CustomerOrder = App\Models\OrderHead::with('OrderProductItem','OrderBillingShipping')->where('customer_id',$CustomerInfo->id)->get();
//            echo '<pre>';
//            print_r($CustomerOrder);
//            exit();
            $AllDistrict = DB::table('districts')->orderby('name','asc')->pluck('name','id')->all();
            $AllDistrict[''] = 'Choose a District';
            ksort($AllDistrict);

            $AllCountry = DB::table('countries')->orderby('country_name','asc')->pluck('country_name','id')->all();
            $AllCountry[''] = 'Choose a Country';
            ksort($AllCountry);

            $AllUpazala = DB::table('upazilas')->orderby('name','asc')->pluck('name','id')->all();
            $AllUpazala[''] = 'Choose a Upazila';
            ksort($AllUpazala);

            $response['GroupWiseMyAccountPage'] = (new FrontendController)->GroupWiseGeneralPage('myaccount');
            $response['GroupWiseCustomerServicePage'] = (new FrontendController)->GroupWiseGeneralPage('customerservice');
            $response['GroupWiseCompanyPage'] = (new FrontendController)->GroupWiseGeneralPage('company');
            $response['contact-us'] = (new FrontendController)->SlugWiseGeneralPage('contact-us');
            $response['Category'] = Category::AllCategory();
            $response['general_setting'] = (new FrontendController)->generalSetting();



            return view("Customer::dashboard.dashboard",compact('AllUpazala','AllCountry','AllDistrict','CustomerOrder','TabHeader','UserInfo','CustomerInfo','response'));
        }

    }
}
