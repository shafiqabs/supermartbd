<?php

namespace App\Modules\Customer\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Customer extends Model
{
    use HasFactory;
    protected $table = 'ems_customer';
    // protected $dates = ['created_at', 'updated_at'];
    // public $timestamps = true;
    protected $fillable = [
        'user_id', 'first_name', 'last_name', 'mobile', 'email', 'bill_country_id', 'bill_country_name', 'bill_district_id', 'bill_district_name', 'bill_upazila_id', 'bill_upazila_name', 'bill_street_address', 'ship_country_id', 'ship_country_name', 'ship_district_id', 'ship_district_name', 'ship_upazila_id', 'ship_upazila_name', 'ship_street_address', 'customer_image', 'status', 'created_by', 'updated_by', 'created_at', 'updated_at'
    ];



    // TODO :: boot
    // boot() function used to insert logged user_id at 'created_by' & 'updated_by'
    public static function boot(){
        parent::boot();
        static::creating(function($query){
            if(Auth::check()){
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function($query){
            if(Auth::check()){
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}
