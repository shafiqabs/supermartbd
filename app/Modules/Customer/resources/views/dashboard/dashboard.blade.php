

@extends(/** @lang text */'frontend.master.master')

@section('body')




    <!-- Start of Main -->
    <main class="main">
        <!-- Start of Page Header -->
        <div class="page-header">
            <div class="container">
                <h1 class="page-title mb-0">{{Auth::user()->UserCustomer->first_name.' '.Auth::user()->UserCustomer->last_name}} Account</h1>
            </div>
        </div>
        <!-- End of Page Header -->

        <!-- Start of Breadcrumb -->
        <nav class="breadcrumb-nav">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="{{route('web.my.account')}}">Home</a></li>
                    <li>My account</li>
                </ul>
            </div>
        </nav>
        @if($message = Session::get('message'))
            <div class="alert alert-primary" role="alert" style="text-align: center;font-size: 20px;font-weight: bold;">
                <strong>{{$message }}</strong>
            </div>
    @endif
        <!-- End of Breadcrumb -->

        <!-- Start of PageContent -->
        <div class="page-content pt-2">
            <div class="container">
                <div class="tab tab-vertical row gutter-lg">
                    <ul class="nav nav-tabs mb-6" role="tablist">
                        <li class="nav-item">
                            <a href="#account-dashboard" class="nav-link active">Dashboard</a>
                        </li>
                        <li class="nav-item">
                            <a href="#account-orders" class="nav-link">Orders</a>
                        </li>
                        <li class="nav-item">
                            <a href="#account-downloads" class="nav-link">Downloads</a>
                        </li>
                        <li class="nav-item">
                            <a href="#account-addresses" class="nav-link">Addresses</a>
                        </li>
                        <li class="nav-item">
                            <a href="#account-details" class="nav-link">Account details</a>
                        </li>
{{--                        <li class="link-item">--}}
{{--                            <a href="">Wishlist</a>--}}
{{--                        </li>--}}
                        <li class="link-item">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>

                    <div class="tab-content mb-6">
                        <div class="tab-pane active in" id="account-dashboard">
                            <p class="greeting">
                                Hello
                                <span class="text-dark font-weight-bold">{{Auth::user()->UserCustomer->first_name.' '.Auth::user()->UserCustomer->last_name}}</span>
                                (not
                                <span class="text-dark font-weight-bold">{{Auth::user()->UserCustomer->first_name.' '.Auth::user()->UserCustomer->last_name}}</span>?
{{--                                <a href="#" class="text-primary">Log out</a>--}}
                                <a class="text-primary" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                    {{ __('Log out') }}
                                </a>)
                            </p>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>

                            <?php
//                                echo '<pre>';
//                                print_r($CustomerOrder);
                            ?>

                            <p class="mb-4">
                                From your account dashboard you can view your <a href="#account-orders"
                                                                                 class="text-primary link-to-tab">recent orders</a>,
                                manage your <a href="#account-addresses" class="text-primary link-to-tab">shipping
                                    and billing
                                    addresses</a>, and
                                <a href="#account-details" class="text-primary link-to-tab">edit your password and
                                    account details.</a>
                            </p>

                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-4 col-xs-6 mb-4">
                                    <a href="#account-orders" class="link-to-tab">
                                        <div class="icon-box text-center">
                                                <span class="icon-box-icon icon-orders">
                                                    <i class="w-icon-orders"></i>
                                                </span>
                                            <div class="icon-box-content">
                                                <p class="text-uppercase mb-0">{{count($CustomerOrder)}} Orders</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-4 col-xs-6 mb-4">
                                    <a href="#account-downloads" class="link-to-tab">
                                        <div class="icon-box text-center">
                                                <span class="icon-box-icon icon-download">
                                                    <i class="w-icon-download"></i>
                                                </span>
                                            <div class="icon-box-content">
                                                <p class="text-uppercase mb-0">Downloads</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-4 col-xs-6 mb-4">
                                    <a href="#account-addresses" class="link-to-tab">
                                        <div class="icon-box text-center">
                                                <span class="icon-box-icon icon-address">
                                                    <i class="w-icon-map-marker"></i>
                                                </span>
                                            <div class="icon-box-content">
                                                <p class="text-uppercase mb-0">Addresses</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-4 col-xs-6 mb-4">
                                    <a href="#account-details" class="link-to-tab">
                                        <div class="icon-box text-center">
                                                <span class="icon-box-icon icon-account">
                                                    <i class="w-icon-user"></i>
                                                </span>
                                            <div class="icon-box-content">
                                                <p class="text-uppercase mb-0">Account Details</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-4 col-xs-6 mb-4">
                                    <a href="wishlist.html" class="link-to-tab">
                                        <div class="icon-box text-center">
                                                <span class="icon-box-icon icon-wishlist">
                                                    <i class="w-icon-heart"></i>
                                                </span>
                                            <div class="icon-box-content">
                                                <p class="text-uppercase mb-0">Wishlist</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-4 col-xs-6 mb-4">




                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();document.getElementById('logout-form').submit();">

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                        <div class="icon-box text-center">
                                                <span class="icon-box-icon icon-logout">
                                                    <i class="w-icon-logout"></i>
                                                </span>
                                            <div class="icon-box-content">
                                                <p class="text-uppercase mb-0">Logout</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane mb-4" id="account-orders">
                            <div class="icon-box icon-box-side icon-box-light">
                                    <span class="icon-box-icon icon-orders">
                                        <i class="w-icon-orders"></i>
                                    </span>
                                <div class="icon-box-content">
                                    <h4 class="icon-box-title text-capitalize ls-normal mb-0">Orders</h4>
                                </div>
                            </div>

                            <table class="shop-table account-orders-table mb-6">
                                <thead>
                                <tr>
                                    <th class="order-id">Order</th>
                                    <th class="order-date">Date</th>
                                    <th class="order-status">Status</th>
                                    <th class="order-total">Total</th>
                                    <th class="order-actions">Payment Method</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($CustomerOrder as $value)

                                <tr bgcolor="#e6e0e0">
                                    <td class="order-id">{{$value->order_number}}</td>
                                    <td class="order-date">{{date('h:i:s a m/d/Y', strtotime($value->created_at))}}</td>
                                    <td class="order-status">
                                        <?php
                                        if ($value->status == '1'){
                                            $status = 'Process';
                                        }elseif ($value->status == '2'){
                                            $status = 'Confirm';
                                        }elseif ($value->status == '3'){
                                            $status = 'Pending';
                                        }elseif ($value->status == '4'){
                                            $status = 'Approved';
                                        }else{
                                            $status = 'Return';
                                        }
                                        ?>
                                        {{$status}}
                                    </td>
                                    <td class="order-total">
                                        <span class="order-price">{{$value->total_amount}}</span> for
                                        <span class="order-quantity"> {{$value->total_quantity}}</span> item
                                    </td>
                                    <td class="order-action">
                                        {{$value->payment_method}}
                                    </td>
                                </tr>

                                    @foreach($value['OrderProductItem'] as $item)
                                        <tr  style="height: 20px;">
                                            <td>
                                                <img src="{{ asset('backend/image/ProProductImage').'/'.$item->ProductDetails->feature_image}}" alt="">
                                            </td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->quantity}}</td>
                                            <td>{{$item->unit_price}}</td>
                                            <td>{{$item->subtotal}}</td>
                                        </tr>

                                    @endforeach

                                @endforeach
                                </tbody>
                            </table>

                            <a href="" class="btn btn-dark btn-rounded btn-icon-right">Go
                                Shop<i class="w-icon-long-arrow-right"></i></a>
                        </div>

                        <div class="tab-pane" id="account-downloads">
                            <div class="icon-box icon-box-side icon-box-light">
                                    <span class="icon-box-icon icon-downloads mr-2">
                                        <i class="w-icon-download"></i>
                                    </span>
                                <div class="icon-box-content">
                                    <h4 class="icon-box-title ls-normal">Downloads</h4>
                                </div>
                            </div>
                            <p class="mb-4">No downloads available yet.</p>
                            <a href="" class="btn btn-dark btn-rounded btn-icon-right">Go
                                Shop<i class="w-icon-long-arrow-right"></i></a>
                        </div>

                        <div class="tab-pane" id="account-addresses">
                            <div class="icon-box icon-box-side icon-box-light">
                                    <span class="icon-box-icon icon-map-marker">
                                        <i class="w-icon-map-marker"></i>
                                    </span>
                                <div class="icon-box-content">
                                    <h4 class="icon-box-title mb-0 ls-normal">Addresses</h4>
                                </div>
                            </div>
                            <p>The following addresses will be used on the checkout page
                                by default.</p>
                            <div class="row">
                                <div class="col-sm-6 mb-6">
                                    <div class="ecommerce-address billing-address pr-lg-8">
                                        <h4 class="title title-underline ls-25 font-weight-bold">Billing Address</h4>
                                        <address class="mb-4">
                                            <table class="address-table">
                                                <tbody>
                                                <tr>
                                                    <th>Name:</th>
                                                    <td>{{$CustomerInfo->first_name.' '.$CustomerInfo->last_name}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Email:</th>
                                                    <td>{{$CustomerInfo->email}}</td>
                                                </tr>

                                                <tr>
                                                    <th>Address:</th>
                                                    <td>{{$CustomerInfo->bill_street_address}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Upazila:</th>
                                                    <td>{{$CustomerInfo->bill_upazila_name}}</td>
                                                </tr>

                                                <tr>
                                                    <th>District:</th>
                                                    <td>{{$CustomerInfo->bill_district_name}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Country:</th>
                                                    <td>{{$CustomerInfo->bill_country_name}}</td>
                                                </tr>

                                                <tr>
                                                    <th>Phone:</th>
                                                    <td>{{$CustomerInfo->mobile}}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </address>
                                        <a href="#"
                                           class="btn btn-link btn-underline btn-icon-right text-primary">Edit
                                            your billing address<i class="w-icon-long-arrow-right"></i></a>
                                    </div>
                                </div>
                                <div class="col-sm-6 mb-6">
                                    <div class="ecommerce-address shipping-address pr-lg-8">
                                        <h4 class="title title-underline ls-25 font-weight-bold">Shipping Address</h4>
                                        <address class="mb-4">
                                            <table class="address-table">
                                                <tbody>
                                                <tr>
                                                    <th>Name:</th>
                                                    <td>{{$CustomerInfo->first_name.' '.$CustomerInfo->last_name}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Email:</th>
                                                    <td>{{$CustomerInfo->email}}</td>
                                                </tr>

                                                <tr>
                                                    <th>Address:</th>
                                                    <td>{{$CustomerInfo->ship_street_address}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Upazila:</th>
                                                    <td>{{$CustomerInfo->ship_upazila_name}}</td>
                                                </tr>

                                                <tr>
                                                    <th>District:</th>
                                                    <td>{{$CustomerInfo->ship_district_name}}</td>
                                                </tr>
                                                <tr>
                                                    <th>Country:</th>
                                                    <td>{{$CustomerInfo->ship_country_name}}</td>
                                                </tr>

                                                <tr>
                                                    <th>Phone:</th>
                                                    <td>{{$CustomerInfo->mobile}}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </address>
                                        <a href="#"
                                           class="btn btn-link btn-underline btn-icon-right text-primary">Edit your
                                            shipping address<i class="w-icon-long-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="account-details">
                            <div class="icon-box icon-box-side icon-box-light">
                                    <span class="icon-box-icon icon-account mr-2">
                                        <i class="w-icon-user"></i>
                                    </span>
                                <div class="icon-box-content">
                                    <h4 class="icon-box-title mb-0 ls-normal">Account Details</h4>
                                </div>
                            </div>
                            <?php

use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Request as Input;

                            ?>
{{--<form id="" action="{{ route('admin.customer.store') }}" enctype="multipart/form-data" method="POST" files="true">--}}
{!! Form::model($CustomerInfo, ['method' => 'PATCH', 'files'=> true, 'route'=> ['admin.customer.update', $CustomerInfo->id],"class"=>"", 'id' => 'basic-form']) !!}

{{--{!! Form::open(['route' => 'admin.customer.store','enctype'=>'multipart/form-data',  'files'=> true, 'id'=>'basic-form', 'class' => 'form account-details-form']) !!}--}}
<div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="firstname">First name <span style="color: red">*</span></label>
                                            <input type="hidden" name="partvalue" value="frontend">
                                            <input type="text" id="firstname" name="first_name" placeholder="Enter First Name"
                                                   class="form-control form-control-md" value="{{$CustomerInfo->first_name}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="lastname">Last name <span style="color: red">*</span></label>
                                            <input type="text" id="lastname" name="last_name" placeholder="Enter First Name"
                                                   class="form-control form-control-md" value="{{$CustomerInfo->last_name}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="firstname">Mobile</label>
                                            <input type="text" id="firstname" name="mobile" placeholder="Enter Mobile" value="{{$CustomerInfo->mobile}}"
                                                   class="form-control form-control-md" data-checkify="minlen=6,required,maxlen=14,number">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="lastname">Email <span style="color: red">*</span></label>
                                            <input type="text" id="lastname" name="email" placeholder="Enter email" value="{{$CustomerInfo->email}}"
                                                   class="form-control form-control-md">
                                        </div>
                                    </div>
                                </div>

                            <div class="row">
                                <h2 class="" style="font-size: 14px !important;">Bill Address</h2>

                                <div class="col-md-4">
                                    <div class="from-group">
                                        {!! Form::label('Country', 'Country', array('class' => 'form-label')) !!}
                                        {{--                <span style="color: red">*</span>--}}
                                        <div class="input-group mb-3">
                                            <?php
                                            $DefaultCountry = $CustomerInfo->bill_country_id;
                                            $DefaultDistrict = $CustomerInfo->bill_district_id;
                                            $DefaultUpazala = $CustomerInfo->bill_upazila_id;
                                            ?>
                                            {{--                        {!! Form::select('country_id',$AllCountry,Input::old('country_id'),['id'=>'country_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}--}}
                                            {!! Form::select('bill_country_id',$AllCountry,$DefaultCountry,['id'=>'bill_country_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                            {{--                    <span style="color: #ff0000">{!! $errors->first('country_id') !!}</span>--}}
                                        </div>
                                    </div>
                                </div>

                                @if($DefaultCountry == 19)
                                <div class="col-md-4 bill_district_id">
                                    <div class="from-group">
                                        {!! Form::label('District', 'District', array('class' => 'form-label')) !!}
                                        {{--                <span style="color: red">*</span>--}}
                                        <a data-href="{{route('admin.customer.findupazala')}}" id="findupazalaroute"></a>
                                        <div class="input-group mb-3">
                                            {!! Form::select('bill_district_id',$AllDistrict,$DefaultDistrict,['type'=>'bill','id'=>'bill_district_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                            {{--                    <span style="color: #ff0000">{!! $errors->first('country_id') !!}</span>--}}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 bill_upazila_id">
                                    <div class="from-group">
                                        {!! Form::label('Upazila', 'Upazila', array('class' => 'form-label')) !!}
                                        {{--                <span style="color: red">*</span>--}}
                                        <div class="input-group mb-3">
{{--                                            <select name="bill_upazila_id" id="bill_upazila_id" class="form-control form-select js-example-basic-single"></select>--}}
{!! Form::select('bill_upazila_id',$AllUpazala,$DefaultUpazala,['id'=>'bill_upazila_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                            {{--                    <span style="color: #ff0000">{!! $errors->first('country_id') !!}</span>--}}
                                        </div>
                                    </div>
                                </div>
                                @else
                                    <div class="col-md-4 bill_district_id" style="display: none">
                                        <div class="from-group">
                                            {!! Form::label('District', 'District', array('class' => 'form-label')) !!}
                                            {{--                <span style="color: red">*</span>--}}
                                            <a data-href="{{route('admin.customer.findupazala')}}" id="findupazalaroute"></a>
                                            <div class="input-group mb-3">
                                                {!! Form::select('bill_district_id',$AllDistrict,Input::old('district_id'),['type'=>'bill','id'=>'bill_district_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                                {{--                    <span style="color: #ff0000">{!! $errors->first('country_id') !!}</span>--}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 bill_upazila_id" style="display: none">
                                        <div class="from-group">
                                            {!! Form::label('Upazila', 'Upazila', array('class' => 'form-label')) !!}
                                            {{--                <span style="color: red">*</span>--}}
                                            <div class="input-group mb-3">
<select name="bill_upazila_id" id="bill_upazila_id" class="form-control form-select js-example-basic-single"></select>
                                                {{--                    {!! Form::select('bill_upazila_id',$AllDistrict,Input::old('bill_upazila_id'),['id'=>'bill_upazila_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}--}}
                                                {{--                    <span style="color: #ff0000">{!! $errors->first('country_id') !!}</span>--}}
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <div class="col-md-12">
                                    <div class="from-group">
                                        {!! Form::label('Street address', 'Street address', array('class' => 'form-label')) !!}
                                        <div class="input-group mb-3">
                                            {!! Form::textarea('bill_street_address',$CustomerInfo->bill_street_address,['id'=>'bill_street_address','class' => 'form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2','rows'=>1]) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $ShipDefaultCountry = $CustomerInfo->ship_country_id;
                            $ShipDefaultDistrict = $CustomerInfo->ship_district_id;
                            $ShipDefaultUpazala = $CustomerInfo->ship_upazila_id;
                            ?>
                            <div class="row">
                                <h2 class="" style="font-size: 14px !important;">Shipping Address (If another)</h2>
                                <div class="col-md-4">
                                    <div class="from-group">
                                        {!! Form::label('Country', 'Country', array('class' => 'form-label')) !!}
                                        <div class="input-group mb-3">
                                            <?php
                                            $DefaultCountry = 19;
                                            ?>
                                            {!! Form::select('ship_country_id',$AllCountry,$ShipDefaultCountry,['id'=>'ship_country_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                        </div>
                                    </div>
                                </div>

                @if($ShipDefaultCountry == 19)
                    <div class="col-md-4 ship_district_id">
                        <div class="from-group">
                            {!! Form::label('District', 'District', array('class' => 'form-label')) !!}
                            <div class="input-group mb-3">
                                {!! Form::select('ship_district_id',$AllDistrict,$ShipDefaultDistrict,['type'=>'ship','id'=>'ship_district_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4 ship_upazila_id">
                        <div class="from-group">
                            {!! Form::label('Upazila', 'Upazila', array('class' => 'form-label')) !!}
                            <div class="input-group mb-3">
                                {!! Form::select('ship_upazila_id',$AllUpazala,$ShipDefaultUpazala,['id'=>'ship_upazila_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}

                                {{--                                <select name="ship_upazila_id" id="ship_upazila_id" class="form-control form-select js-example-basic-single"></select>--}}
                                {{--                {!! Form::text('ship_upazila_id',Input::old('ship_upazila_id'),['id'=>'ship_upazila_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}--}}
                                        </div>
                                    </div>
                                </div>
                                @else
                                    <div class="col-md-4 ship_district_id" style="display: none;">
                                        <div class="from-group">
                                            {!! Form::label('District', 'District', array('class' => 'form-label')) !!}
                                            <div class="input-group mb-3">
                                                {!! Form::select('ship_district_id',$AllDistrict,$ShipDefaultDistrict,['type'=>'ship','id'=>'ship_district_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 ship_upazila_id" style="display: none;">
                                        <div class="from-group">
                                            {!! Form::label('Upazila', 'Upazila', array('class' => 'form-label')) !!}
                                            <div class="input-group mb-3">
{{--                                                {!! Form::select('bill_upazila_id',$AllUpazala,$ShipDefaultUpazala,['id'=>'bill_upazila_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}--}}

                                                <select name="ship_upazila_id" id="ship_upazila_id" class="form-control form-select js-example-basic-single"></select>
                                                {{--                {!! Form::text('ship_upazila_id',Input::old('ship_upazila_id'),['id'=>'ship_upazila_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}--}}
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <div class="col-md-12">
                                    <div class="from-group">
                                        {!! Form::label('Street address', 'Street address', array('class' => 'form-label')) !!}
                                        <div class="input-group mb-3">
                                            {!! Form::textarea('ship_street_address',$CustomerInfo->ship_street_address,['id'=>'ship_street_address','class' => 'form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2','rows'=>1]) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-md-4 offset-md-4">
                                    <div class="from-group">
                                        {!! Form::label('Image', 'Image', array('class' => 'form-label','for'=>'formFile')) !!}
                                        @if(isset($CustomerInfo))
                                        @else
                                            {{--                    <span style="color: red">*</span>--}}
                                        @endif

                                        <div class="mb-6">
                                            @if(isset($CustomerInfo))
                                                <input class="form-control" accept="image/*" name="customer_image" type="file" id="file" onchange="loadFile(event)">
                                            @else
                                                <input class="form-control" accept="image/*" name="customer_image" type="file" id="file" onchange="loadFile(event)">

                                            @endif

                                            <div style="text-align: center;padding-top: 5px;display: block ruby;">
                                                @if($errors->first('customer_image'))
                                                    <span style="color: #ff0000">{!! $errors->first('customer_image') !!}</span>
                                                @else
                                                    <img id="customer_image" width="150px" height="120px"/>
                                                @endif

                                                @if(isset($CustomerInfo) && $CustomerInfo->customer_image !='')
                                                    <img id="customer_image" src="{{ asset('backend/image/ProCustomerImage').'/'.$CustomerInfo->customer_image}}" width="150px" height="120px"/>
                                                @endif

                                            </div>
                                        </div>
                                    </div>


                                    <script>
                                        var loadFile = function(event) {
                                            var image = document.getElementById('customer_image');
                                            image.src = URL.createObjectURL(event.target.files[0]);
                                        };
                                    </script>
                                </div>

                            </div>





{{--                                <h4 class="title title-password ls-25 font-weight-bold">Password change</h4>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="text-dark" for="cur-password">Current Password leave blank to leave unchanged</label>--}}
{{--                                    <input type="password" class="form-control form-control-md"--}}
{{--                                           id="cur-password" name="cur_password">--}}
{{--                                </div>--}}
{{--                                <div class="form-group">--}}
{{--                                    <label class="text-dark" for="new-password">New Password leave blank to leave unchanged</label>--}}
{{--                                    <input type="password" class="form-control form-control-md"--}}
{{--                                           id="new-password" name="new_password">--}}
{{--                                </div>--}}
{{--                                <div class="form-group mb-10">--}}
{{--                                    <label class="text-dark" for="conf-password">Confirm Password</label>--}}
{{--                                    <input type="password" class="form-control form-control-md"--}}
{{--                                           id="conf-password" name="conf_password">--}}
{{--                                </div>--}}
{{--                            <button type="submit" class="btn submit-button">Submit</button>--}}
                                <button type="submit" class="btn btn-dark btn-rounded btn-sm mb-4">Save Changes</button>

                            {!! Form::close() !!}
{{--</form>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of PageContent -->
    </main>
    <!-- End of Main -->







    <section class="grey-section">
        <div class="container">
            <div class="title-link-wrapper mb-2 appear-animate">
            {{--our blog--}}
            @include('frontend.layouts.our-blog')
            <!-- End of Owl Carousel -->
            {{--our clients--}}
{{--            @include('frontend.layouts.our-clients')--}}
            <!-- End of Brands Wrapper -->

            {{--our news letter--}}
{{--            @include('frontend.layouts.our-newsetter')--}}
            <!--End of Cta Wrapper -->

                {{--out instragram--}}
                {{--@include('frontend.layouts.our-instragram')--}}
            </div>
            </div>
            <!-- End of Container -->
    </section>


    <!-- Start of Footer -->
    <footer class="footer footer-dark appear-animate">
        <div class="container">
            {{--footer top--}}
            @include('frontend.layouts.footer-top')
            @include('frontend.layouts.footer-middle')
            @include('frontend.layouts.footer-bottom')
        </div>
    </footer>
    <!-- End of Footer -->
    <!-- End of Page-wrapper -->

    <!-- Start of Sticky Footer -->
    @include('frontend.layouts.sticky-footer')
    <!-- End of Sticky Footer -->

    <!-- Start of Scroll Top -->
    <a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="fas fa-chevron-up"></i></a>
    <!-- End of Scroll Top -->

    <!-- Start of Mobile Menu -->
    @include('frontend.layouts.mobile-menu')
    <!-- End of Mobile Menu -->

    <!-- Start of Quick View -->
    @include('frontend.layouts.quick-view')
    <!-- End of Quick view -->

@endsection
