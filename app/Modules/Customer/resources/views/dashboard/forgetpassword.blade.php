

@extends(/** @lang text */'frontend.master.master')

@section('body')

    <?php
    $cart_items = [];
    if(Session::has('cart')){
        $cart_items = Session::get('cart');
    }

    $cart_total = [];
    if(Session::has('cart_total')){
        $cart_total = Session::get('cart_total');
    }

    //    echo '<pre>';
    //    print_r($cart_total);
    ?>




    <!-- Start of Main -->
    <main class="main checkout">
        <!-- Start of Breadcrumb -->
        <nav class="breadcrumb-nav">
            <div class="container">
                <ul class="breadcrumb shop-breadcrumb bb-no">
                    <li class="passed"><a href="{{ url('/') }}">Home</a></li>
                    <li class="active"><a href="">Customer Login</a></li>
                </ul>
            </div>
        </nav>
        <!-- End of Breadcrumb -->


        <!-- Start of PageContent -->
        <div class="page-content">
            <div class="container">
                <div class="login-toggle">
                    Returning customer? <a href="#"
                                           class="show-login font-weight-bold text-uppercase text-dark">Forget Password</a>
                </div>



                {!! Form::open(['route' => 'web.customer.resetcode','enctype'=>'multipart/form-data',  'files'=> true, 'id'=>'basic-form', 'class' => 'login-content','style'=>'display:block']) !!}

                <div class="row">
                    @if(isset($msg))
                        <h3 style="color: red">{{$msg}}</h3>
                    @endif

                    @if(isset($SelectUser))
                            <p>Check mail & you got six digit code.</p>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label>Enter Code <span style="color: red">*</span></label>
                            <input type="hidden" class="form-control form-control-md" value="{{$SelectUser->email}}" name="emailormobile" style="margin-bottom: 0px !important;" required>
                            <input type="text" class="form-control form-control-md"  name="resetcode" style="margin-bottom: 0px !important;" required>
                            <span style="color: #ff0000">{!! $errors->first('resetcode') !!}</span>
                        </div>
                    </div><br>
                            <div class="col-xs-6"><br>
                            <button class="btn btn-rounded btn-login">Varify Code</button>
                            </div>
                    @elseif(isset($input) && !empty($input))
                            <div class="col-xs-6">

                                <div class="form-group">
                                    <label> New Password <span style="color: red">*</span></label>
                                    <input type="hidden" name="emailormobile" value="{{$input['emailormobile']}}">
                                    <input type="hidden" name="varifycode" value="{{$input['resetcode']}}">
                                    <input type="text" class="form-control form-control-md" name="newpassword" style="margin-bottom: 0px !important;" required>
                                    <span style="color: #ff0000">{!! $errors->first('newpassword') !!}</span>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label>Confirm Password <span style="color: red">*</span></label>
                                    <input type="text" class="form-control form-control-md"  name="confirmpassword" style="margin-bottom: 0px !important;" required>
                                    <span style="color: #ff0000">{!! $errors->first('newpassword') !!}</span>
                                </div>
                            </div><br>
                            <div class="col-xs-6"><br>
                            <button class="btn btn-rounded btn-login">Set Password</button>
                            </div>
                    @else
                        <p>Enter your mail address or mobile number & get one time password reset code.</p>
                        <div class="col-xs-6">

                            <div class="form-group">
                                <label> Email or Mobile Number <span style="color: red">*</span></label>
                                <input type="text" class="form-control form-control-md" value="@if(old('emailormobile')){{old('emailormobile')}}@endif" name="emailormobile" style="margin-bottom: 0px !important;" required>
                                <span style="color: #ff0000">{!! $errors->first('emailormobile') !!}</span>
                            </div>
                        </div><br>
                            <div class="col-xs-6"><br>
                            <button class="btn btn-rounded btn-login">Send Code</button>
                            </div>
                    @endif
                </div>
<br>

                {!! Form::close() !!}



            </div>
        </div>
        <!-- End of PageContent -->
    </main>
    <!-- End of Main -->







    <section class="grey-section">
        <div class="container">
            <div class="title-link-wrapper mb-2 appear-animate">
            {{--our blog--}}
            @include('frontend.layouts.our-blog')
            <!-- End of Owl Carousel -->
            {{--our clients--}}
            @include('frontend.layouts.our-clients')
            <!-- End of Brands Wrapper -->

            {{--our news letter--}}
            @include('frontend.layouts.our-newsetter')
            <!--End of Cta Wrapper -->

                {{--out instragram--}}
                {{--@include('frontend.layouts.our-instragram')--}}
            </div>
            <!-- End of Container -->
    </section>


    <!-- Start of Footer -->
    <footer class="footer footer-dark appear-animate">
        <div class="container">
            {{--footer top--}}
            @include('frontend.layouts.footer-top')
            @include('frontend.layouts.footer-middle')
            @include('frontend.layouts.footer-bottom')
        </div>
    </footer>
    <!-- End of Footer -->
    </div>
    <!-- End of Page-wrapper -->

    <!-- Start of Sticky Footer -->
    @include('frontend.layouts.sticky-footer')
    <!-- End of Sticky Footer -->

    <!-- Start of Scroll Top -->
    <a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="fas fa-chevron-up"></i></a>
    <!-- End of Scroll Top -->

    <!-- Start of Mobile Menu -->
    @include('frontend.layouts.mobile-menu')
    <!-- End of Mobile Menu -->

    <!-- Start of Quick View -->
    @include('frontend.layouts.quick-view')
    <!-- End of Quick view -->

@endsection
