@extends(/** @lang text */'backend.layouts.master')

@section('body')
    <div class="dashboard-area">
        <div id="carbon-block" class="">
            <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">{{$ModuleTitle}}</h1>

                    <div class="btn-toolbar mb-2 mb-md-0">
                        <div class="btn-group me-2">

{{--                            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>--}}

                            <a style="color: #000;" href="{{route('admin.customer.index')}}" title="Customer List" class="module_button_header">
                                <button type="button" class="btn btn-sm btn-outline-secondary">
                                    <i class="fas fa-th-list"></i> Customer List
                                </button>
                            </a>

                        </div>
                        @if(Auth::user()->type == 'Admin')
                            <a style="color: #000;" href="{{route('admin.customer.create')}}" title="Add Customer" class="module_button_header">
                                <button type="button" class="btn btn-sm btn-outline-secondary">
                                    <i class="fas fa-plus-circle"></i> Create New
                                </button>
                            </a>
                        @endif

                    </div>
                </div>
            </main>

            <div class="row">
                <div class="col-md-12">
                    <div class="card">

                        <div class="card-header">
                            {{$PageTitle}}
                        </div>
                        <div class="card-body">

                            @include('backend.layouts.message')

                            {!! Form::model($data, ['method' => 'PATCH', 'files'=> true, 'route'=> ['admin.customer.update', $data->id],"class"=>"", 'id' => 'basic-form']) !!}

                            <?php
                            use Illuminate\Support\Facades\URL;
                            use Illuminate\Support\Facades\Request as Input;
                            ?>


                            <div class="row">
                                <div class="col-md-3">
                                    <div class="from-group">
                                        {!! Form::label('First Name', 'First Name', array('class' => 'form-label')) !!}
                                        <span style="color: red">*</span>
                                        <div class="input-group mb-3">
                                            {!! Form::text('first_name',Input::old('first_name'),['id'=>'first_name','class' => 'form-control','data-checkify'=>'minlen=3,required','Placeholder' => 'Enter first name','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                            <span style="color: #ff0000">{!! $errors->first('first_name') !!}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="from-group">
                                        {!! Form::label('Last Name', 'Last Name', array('class' => 'form-label')) !!}
                                        <span style="color: red">*</span>
                                        <div class="input-group mb-3">
                                            {!! Form::text('last_name',Input::old('last_name'),['id'=>'last_name','class' => 'form-control','data-checkify'=>'minlen=3,required','Placeholder' => 'Enter last name','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                            <span style="color: #ff0000">{!! $errors->first('last_name') !!}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="from-group">
                                        {!! Form::label('Mobile', 'Mobile', array('class' => 'form-label')) !!}
                                        <span style="color: red">*</span>
                                        <div class="input-group mb-3">
                                            {!! Form::text('mobile',Input::old('mobile'),['id'=>'mobile','class' => 'form-control','data-checkify'=>'minlen=6,required,maxlen=14,number','Placeholder' => 'Enter mobile','aria-label' =>'mobile','aria-describedby'=>'basic-addon2']) !!}
                                            <span style="color: #ff0000">{!! $errors->first('mobile') !!}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="from-group">
                                        {!! Form::label('Email', 'Email', array('class' => 'form-label')) !!}
                                        <span style="color: red">*</span>
                                        <div class="input-group mb-3">
                                            {!! Form::text('email',Input::old('email'),['id'=>'email','class' => 'form-control','data-checkify'=>'minlen=6,maxlen=40,email','Placeholder' => 'Enter email','aria-label' =>'email','aria-describedby'=>'basic-addon2']) !!}
                                            <span style="color: #ff0000">{!! $errors->first('email') !!}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <h2 class="seneralsettingsection" style="font-size: 14px !important;">Bill Address</h2>
                                <div class="col-md-4">
                                    <div class="from-group">
                                        {!! Form::label('Country', 'Country', array('class' => 'form-label')) !!}
                                        {{--                <span style="color: red">*</span>--}}
                                        <div class="input-group mb-3">
                                            <?php
                                            $DefaultCountry = 19;
                                            ?>
                                            {{--                        {!! Form::select('country_id',$AllCountry,Input::old('country_id'),['id'=>'country_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}--}}
                                            {!! Form::select('bill_country_id',$AllCountry,$DefaultCountry,['id'=>'bill_country_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                            {{--                    <span style="color: #ff0000">{!! $errors->first('country_id') !!}</span>--}}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 bill_district_id">
                                    <div class="from-group">
                                        {!! Form::label('District', 'District', array('class' => 'form-label')) !!}
                                        {{--                <span style="color: red">*</span>--}}
                                        <a data-href="{{route('admin.customer.findupazala')}}" id="findupazalaroute"></a>
                                        <div class="input-group mb-3">
                                            {!! Form::select('bill_district_id',$AllDistrict,Input::old('district_id'),['type'=>'bill','id'=>'bill_district_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                            {{--                    <span style="color: #ff0000">{!! $errors->first('country_id') !!}</span>--}}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 bill_upazila_id">
                                    <div class="from-group">
                                        {!! Form::label('Upazila', 'Upazila', array('class' => 'form-label')) !!}
                                        {{--                <span style="color: red">*</span>--}}
                                        <div class="input-group mb-3">
                                            <select name="bill_upazila_id" id="bill_upazila_id" class="form-control form-select js-example-basic-single"></select>
                                            {{--                    {!! Form::select('bill_upazila_id',$AllDistrict,Input::old('bill_upazila_id'),['id'=>'bill_upazila_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}--}}
                                            {{--                    <span style="color: #ff0000">{!! $errors->first('country_id') !!}</span>--}}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="from-group">
                                        {!! Form::label('Street address', 'Street address', array('class' => 'form-label')) !!}
                                        <div class="input-group mb-3">
                                            {!! Form::textarea('bill_street_address',Input::old('bill_street_address'),['id'=>'bill_street_address','class' => 'form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2','rows'=>1]) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <h2 class="seneralsettingsection" style="font-size: 14px !important;">Shipping Address (If another)</h2>
                                <div class="col-md-4">
                                    <div class="from-group">
                                        {!! Form::label('Country', 'Country', array('class' => 'form-label')) !!}
                                        <div class="input-group mb-3">
                                            <?php
                                            $DefaultCountry = 19;
                                            ?>
                                            {!! Form::select('ship_country_id',$AllCountry,$DefaultCountry,['id'=>'ship_country_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 ship_district_id">
                                    <div class="from-group">
                                        {!! Form::label('District', 'District', array('class' => 'form-label')) !!}
                                        <div class="input-group mb-3">
                                            {!! Form::select('ship_district_id',$AllDistrict,Input::old('ship_district_id'),['type'=>'ship','id'=>'ship_district_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 ship_upazila_id">
                                    <div class="from-group">
                                        {!! Form::label('Upazila', 'Upazila', array('class' => 'form-label')) !!}
                                        <div class="input-group mb-3">
                                            <select name="ship_upazila_id" id="ship_upazila_id" class="form-control form-select js-example-basic-single"></select>
                                            {{--                {!! Form::text('ship_upazila_id',Input::old('ship_upazila_id'),['id'=>'ship_upazila_id','class' => 'form-control form-select js-example-basic-single','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}--}}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="from-group">
                                        {!! Form::label('Street address', 'Street address', array('class' => 'form-label')) !!}
                                        <div class="input-group mb-3">
                                            {!! Form::textarea('ship_street_address',Input::old('ship_street_address'),['id'=>'ship_street_address','class' => 'form-control','aria-label' =>'name','aria-describedby'=>'basic-addon2','rows'=>1]) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>






                            <div class="row">
                                <div class="col-md-4 offset-md-4">
                                    <div class="from-group">
                                        {!! Form::label('Image', 'Image', array('class' => 'form-label','for'=>'formFile')) !!}
                                        @if(isset($data))
                                        @else
                                            {{--                    <span style="color: red">*</span>--}}
                                        @endif

                                        <div class="mb-3">
                                            @if(isset($data))
                                                <input class="form-control" accept="image/*" name="customer_image" type="file" id="file" onchange="loadFile(event)">
                                            @else
                                                <input class="form-control" accept="image/*" name="customer_image" type="file" id="file" onchange="loadFile(event)">

                                            @endif

                                            <div style="text-align: center;padding-top: 5px;">
                                                @if($errors->first('customer_image'))
                                                    <span style="color: #ff0000">{!! $errors->first('customer_image') !!}</span>
                                                @else
                                                    <img id="customer_image" width="150" height="120"/>
                                                @endif

                                                @if(isset($data) && $data->customer_image !='')
                                                    <img id="customer_image" src="{{ asset('backend/image/ProCustomerImage').'/'.$data->customer_image}}" width="150" height="120"/>
                                                @endif

                                            </div>
                                        </div>
                                    </div>


                                    <script>
                                        var loadFile = function(event) {
                                            var image = document.getElementById('customer_image');
                                            image.src = URL.createObjectURL(event.target.files[0]);
                                        };
                                    </script>
                                </div>

                                <div class="col-md-4">
                                    <div class="from-group">
                                        <div class="from-group">
                                            {!! Form::label('Status', 'Status', array('class' => 'form-label')) !!}
                                            <span style="color: red">*</span>
                                            <div class="input-group mb-3">
                                                {!! Form::select('status',array(1=>'Active',0=>'Inactive'),Input::old('status'),['id'=>'ordering','class' => 'form-control','data-checkify'=>'minlen=1,required,number','Placeholder' => 'Enter ordering number','aria-label' =>'name','aria-describedby'=>'basic-addon2']) !!}
                                                <span style="color: #ff0000">{!! $errors->first('ordering') !!}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>






                            <div class="row">

                                <div class="col-md-12" style="text-align: right;">
                                    <div class="from-group">
                                        <div class="">
                                            <button type="reset" class="btn submit-button">Reset</button>
                                            <button type="submit" class="btn submit-button">Submit</button>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>


@endsection
