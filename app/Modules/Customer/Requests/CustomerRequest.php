<?php

namespace App\Modules\Customer\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;


class CustomerRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'first_name'       => 'required',
            'last_name'       => 'required',
            'mobile'       => 'required',
            'email'       => 'required',
            'customer_image'=> 'image|mimes:jpeg,JPEG,Jpeg,PNG,Png,png,jpg,JPG,Jpg',
        ];

    }

}
