<?php

namespace App\Http\Controllers;

use App\Http\Controllers\FrontendController;
use App\Modules\MasterData\Models\Category;
use App\Modules\MasterData\Models\ProductBrand;
use App\Models\OrderHead;
use App\Models\OrderProduct;
use App\Models\OrderBillingShipping;
use App\Modules\Product\Models\Product;
use App\Modules\DeliveryTimeSlot\Models\DeliveryTimeSlot;

use App\Modules\Vendor\Models\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\Session;

use DB;
use Image;
use File;
use Storage;
use App;
Use Auth;

class CheckoutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function MyCheckoutStore(){
        $TabHeader = 'Checkout';

        $DeliveryTimeSlot = DeliveryTimeSlot::where('status','1')->orderby('start_time','asc')->get();

        $password =  old('password');
        if (!isset($password)){
            session::forget('error');
        }

        $response['GroupWiseMyAccountPage'] = (new FrontendController)->GroupWiseGeneralPage('myaccount');
        $response['GroupWiseCustomerServicePage'] = (new FrontendController)->GroupWiseGeneralPage('customerservice');
        $response['GroupWiseCompanyPage'] = (new FrontendController)->GroupWiseGeneralPage('company');
        $response['contact-us'] = (new FrontendController)->SlugWiseGeneralPage('contact-us');
        $response['about-us'] = (new FrontendController)->SlugWiseGeneralPage('about-us');
        $response['career'] = (new FrontendController)->SlugWiseGeneralPage('career');
        $response['WebMenu'] = Category::getWebMenu();
        $response['Category'] = Category::AllCategory();
        $response['general_setting'] = (new FrontendController)->generalSetting();

        return view("frontend.page.checkout",compact('TabHeader','DeliveryTimeSlot','response'));
    }


    public function OrderPlace(Request $request){

        $input = $request->all();
//        dd($input);
        $appUrl=env('APP_URL');

        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
        ])->post($appUrl.'/api/create-order',$input);

        $body = $response->getBody()->getContents();
        $InserResponse = json_decode($body);

        if ($InserResponse[0]->success === 'Data Insert'){
            Session::forget('cart');
            Session::forget('cart_total');
            return redirect()->route('web.orderplace.complete',$InserResponse[0]->order_id);
        }else{
            echo $InserResponse[0]->error;
        }
    }


    public function OrderPlaceComplete($id){
        $TabHeader = 'Order Complete';
        $appUrl=env('APP_URL');
        $response1 = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
        ])->get($appUrl.'/api/order-data/'.$id);


        $body = $response1->getBody()->getContents();
        $OrderData = json_decode($body);

        $response['GroupWiseMyAccountPage'] = (new FrontendController)->GroupWiseGeneralPage('myaccount');
        $response['GroupWiseCustomerServicePage'] = (new FrontendController)->GroupWiseGeneralPage('customerservice');
        $response['GroupWiseCompanyPage'] = (new FrontendController)->GroupWiseGeneralPage('company');
        $response['contact-us'] = (new FrontendController)->SlugWiseGeneralPage('contact-us');
        $response['about-us'] = (new FrontendController)->SlugWiseGeneralPage('about-us');
        $response['career'] = (new FrontendController)->SlugWiseGeneralPage('career');
        $response['WebMenu'] = Category::getWebMenu();
        $response['Category'] = Category::AllCategory();
        $response['general_setting'] = (new FrontendController)->generalSetting();


        return view("frontend.page.ordercomplete",compact('TabHeader','OrderData','response'));
    }

    public function userlogin(Request $request){
        $input = $request->all();

        $validator = Validator::make($input, [
            'emailormobile' => 'required',
            'password' => 'required',
        ],[
            'emailormobile.required' => 'The email or mobile field is required',
            'password.required' => 'The password field is required',
        ]);

        if ($validator->fails()) {
            $input['msg'] = 'display: block;';
            $UserData['msg'] = 'display: block;';
            return redirect('web-checkout-store')
                ->withErrors($validator)
                ->withInput($input);
        }else{
            session::forget('error');
            session::forget('msg');
            $user_data = \Auth::user();
            if(Auth::check() && $user_data->type != 'Customer'){
                Auth::logout();
            }

            if (!filter_var($input["emailormobile"], FILTER_VALIDATE_EMAIL)) {
                $SelectUser = App\Modules\User\Models\User::where('mobile',$input["emailormobile"])->where('type','Customer')->first();
            }else{
                $SelectUser = App\Modules\User\Models\User::where('email',$input["emailormobile"])->where('type','Customer')->first();
            }

            if(Auth::check() && $user_data->type == 'Customer'){
                if (($input["emailormobile"] == $user_data->email || $input["emailormobile"] == $user_data->mobile)){
                    $verify = password_verify($input['password'], $user_data->password);
                    if ($verify){
                        $CustomerInfo = App\Modules\Customer\Models\Customer::where('user_id',$SelectUser->id)->first();
                        $message['customerinfo'] = $CustomerInfo;
                        $message['userinfo'] = $SelectUser;
                        $message['msg'] = 'display: block;';
                        Session::put('UserLoginInfo',$message);
                        return redirect()->back()->withInput($input);
                 }else{
                        $message['error'] = 'User Information not match.';
                        $message['msg'] = 'display: block;';
                        Session::put('error',$message);
                        return redirect()->back()->withInput($input);
                    }
                }
            }else {
                if ($SelectUser === null) {
                    $message['error'] = 'User Information not match.';
                    $message['msg'] = 'display: block;';
                    Session::put('error', $message);
                    return redirect()->back()->withInput($input);
                } else {
                    $db_password = $SelectUser->password;
                    $verify = password_verify($input['password'], $db_password);
                    if ($verify) {

                        $attempt = Auth::attempt([
                            'email' => $SelectUser->email,
                            'password' => $input['password'],
                            'type' => 'Customer'
                        ]);

                        if ($attempt) {
                            $CustomerInfo = App\Modules\Customer\Models\Customer::where('user_id', $SelectUser->id)->first();
                            $message['customerinfo'] = $CustomerInfo;
                            $message['userinfo'] = $SelectUser;
                            $message['msg'] = 'display: block;';
                            Session::put('UserLoginInfo', $message);
                            return redirect('web-my-account');
                        }

                    } else {
                        $message['error'] = 'User Information not match.';
                        $message['msg'] = 'display: block;';
                        Session::put('error', $message);
                        return redirect()->back()->withInput($input);
                    }
                }
            }

        }

    }


}
