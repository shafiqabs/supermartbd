<?php

namespace App\Http\Controllers;

use App\Modules\MasterData\Models\Category;
use App\Modules\MasterData\Models\Color;
use App\Modules\MasterData\Models\ProductBrand;
use App\Modules\MasterData\Models\Sizeunit;
use App\Modules\Product\Models\Product;
use App\Modules\User\Models\User;
use App\Modules\Vendor\Models\Vendor;
use App\Modules\Customer\Models\Customer;
use Illuminate\Http\Request;
use App\Support\Collection;
use Illuminate\Support\Facades\Http;

use Illuminate\Support\Facades\Session;
use DB;
use PhpOffice\PhpSpreadsheet\Calculation\Engineering\ErfC;

class FrontendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function HomePage(){

        $TabHeader = 'Home';

        $response['all-brand'] = $this->AllBrand();
        $response['all-slider'] = $this->AllSlider();
        $response['deals-hot'] = $this->AllDealsHot();
        $response['best-seller'] = $this->AllTopSeller();
        $response['vendor-limit'] = $this->AllVendorLimit();
        $response['recommend-product'] = $this->RecommendProduct();

        $response['category-product'] = $this->CustomCategoryProductForHome('software');
        $response['category-product1'] = $this->CustomCategoryProductForHome('consumer-electric');
        $response['category-product2'] = $this->CustomCategoryProductForHome('home-garden-&-kitchen');

        $response['GroupWiseCompanyPage'] = $this->GroupWiseGeneralPage('company');
        $response['GroupWiseMyAccountPage'] = $this->GroupWiseGeneralPage('myaccount');
        $response['GroupWiseCustomerServicePage'] = $this->GroupWiseGeneralPage('customerservice');
        $response['GroupWisesupportPage'] = $this->GroupWiseGeneralPage('support');
        $response['contact-us'] = $this->SlugWiseGeneralPage('contact-us');
        $response['about-us'] = $this->SlugWiseGeneralPage('about-us');
        $response['career'] = $this->SlugWiseGeneralPage('career');
        $response['BannerTop'] = $this->Banner('banner-top');
        $response['BannerBottom'] = $this->Banner('banner-bottom');

        $response['WebMenu'] = Category::getWebMenu();
        $response['Category'] = Category::AllCategory();

        $response['general_setting'] = $this->generalSetting();

        return view("frontend.layouts.welcome",compact('TabHeader','response'));
    }

    public function SingleProductDeails($slug){
        $TabHeader = 'Product Details';

        $response['single-product'] = $this->SingleProduct($slug);

        $category_slug = $response['single-product']->product_category->slug;
        $response['category-product'] = $this->CustomCategoryProductForHome($category_slug);

        $vendor_id = $response['single-product']->vendor_id;
        $vendor_slug = Vendor::where('id',$vendor_id)->select('slug')->first();
        $response['single-vendor-info'] = $this->SingleVendorInfo($vendor_slug->slug);
        $response['vendor-product'] = $this->VendorProduct($vendor_slug->slug);

        $response['GroupWiseMyAccountPage'] = $this->GroupWiseGeneralPage('myaccount');
        $response['GroupWiseCustomerServicePage'] = $this->GroupWiseGeneralPage('customerservice');
        $response['GroupWiseCompanyPage'] = $this->GroupWiseGeneralPage('company');
        $response['GroupWisesupportPage'] = $this->GroupWiseGeneralPage('support');

        $response['contact-us'] = $this->SlugWiseGeneralPage('contact-us');
        $response['about-us'] = $this->SlugWiseGeneralPage('about-us');
        $response['career'] = $this->SlugWiseGeneralPage('career');
        $response['WebMenu'] = Category::getWebMenu();
        $response['Category'] = Category::AllCategory();
        $response['general_setting'] = $this->generalSetting();



        return view("frontend.page.single-product",compact('TabHeader','response'));
    }

    public function BrandWishProduct($slug){
        $TabHeader = 'Brand Product';
        $response['all-brand'] = $this->AllBrand();
        $response['all-category'] = $this->AllCategory();
        $BrandProduct = $this->AllBrandProduct($slug);
//        echo '<pre>';
//        print_r($BrandProduct);
//        exit();
        $response['brand-wish-product'] = (new Collection($BrandProduct))->paginate(12);
        $brand_name = ProductBrand::where('slug',$slug)->first();
        $brand_name = $brand_name->name;

        $response['GroupWiseMyAccountPage'] = $this->GroupWiseGeneralPage('myaccount');
        $response['GroupWiseCustomerServicePage'] = $this->GroupWiseGeneralPage('customerservice');
        $response['GroupWiseCompanyPage'] = $this->GroupWiseGeneralPage('company');
        $response['contact-us'] = $this->SlugWiseGeneralPage('contact-us');
        $response['about-us'] = $this->SlugWiseGeneralPage('about-us');
        $response['career'] = $this->SlugWiseGeneralPage('career');
        $response['WebMenu'] = Category::getWebMenu();
        $response['Category'] = Category::AllCategory();

        $response['size-unit'] = $this->SizeUnit();
        $response['color'] = $this->Color();
        $response['general_setting'] = $this->generalSetting();



        return view("frontend.page.brand-wish-product",compact('TabHeader','response','brand_name'));
//        return view("frontend.page.brand-wish-product",['TabHeader'=>$TabHeader,'response'=>$response,'brand_name'=>$brand_name]);
    }


    public function VendorWishProduct($slug){
        $TabHeader = 'Vendor Product';
        $response['single-vendor-info'] = $this->SingleVendorInfo($slug);
        $response['vendor-product'] = $this->VendorProduct($slug);
        $response['best-seller'] = $this->AllTopSeller();
        $response['deals-hot'] = $this->AllDealsHot();

        $response['GroupWiseMyAccountPage'] = $this->GroupWiseGeneralPage('myaccount');
        $response['GroupWiseCustomerServicePage'] = $this->GroupWiseGeneralPage('customerservice');
        $response['GroupWiseCompanyPage'] = $this->GroupWiseGeneralPage('company');
        $response['contact-us'] = $this->SlugWiseGeneralPage('contact-us');
        $response['about-us'] = $this->SlugWiseGeneralPage('about-us');
        $response['career'] = $this->SlugWiseGeneralPage('career');
        $response['WebMenu'] = Category::getWebMenu();
        $response['Category'] = Category::AllCategory();
        $response['general_setting'] = $this->generalSetting();



        return view("frontend.page.vendor-wish-product",compact('TabHeader','response'));
    }

    public function VendorProduct($slug){
        $appUrl=env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
        ])->get($appUrl.'/api/vendor-wish-product/'.$slug);


        $body = $response->getBody()->getContents();
        $respone = json_decode($body);
        return $respone;
    }

    public function CategoryWishProduct($slug){
        $TabHeader = 'Category Product';
        $response['all-brand'] = $this->AllBrand();
        $response['all-category'] = $this->AllCategory();
//        $response['category-wish-product'] = $this->CustomCategoryProductForHome($slug);
        $CategoryProduct = $this->CategoryWiseProduct($slug);


        $response['category-wish-product'] = (new Collection($CategoryProduct))->paginate(12);
        $response['category-data'] = Category::where('slug',$slug)->first();

        $response['size-unit'] = $this->SizeUnit();
        $response['color'] = $this->Color();


//        echo '<pre>';
//        print_r($response['category-wish-product']);
//        exit();

        $response['GroupWiseMyAccountPage'] = $this->GroupWiseGeneralPage('myaccount');
        $response['GroupWiseCustomerServicePage'] = $this->GroupWiseGeneralPage('customerservice');
        $response['GroupWiseCompanyPage'] = $this->GroupWiseGeneralPage('company');
        $response['contact-us'] = $this->SlugWiseGeneralPage('contact-us');
        $response['about-us'] = $this->SlugWiseGeneralPage('about-us');
        $response['career'] = $this->SlugWiseGeneralPage('career');
        $response['WebMenu'] = Category::getWebMenu();
        $response['Category'] = Category::AllCategory();
        $response['general_setting'] = $this->generalSetting();



        return view("frontend.page.category-wish-product",compact('TabHeader','response'));
    }

    public function ViewGeneralPage($slug){
        $TabHeader = 'General Page View';
        $response['GroupWiseMyAccountPage'] = $this->GroupWiseGeneralPage('myaccount');
        $response['GroupWiseCustomerServicePage'] = $this->GroupWiseGeneralPage('customerservice');
        $response['GroupWiseCompanyPage'] = $this->GroupWiseGeneralPage('company');
        $response['PageContent'] = $this->SlugWiseGeneralPage($slug);
        $response['contact-us'] = $this->SlugWiseGeneralPage('contact-us');
        $response['about-us'] = $this->SlugWiseGeneralPage('about-us');
        $response['career'] = $this->SlugWiseGeneralPage('career');
        $response['WebMenu'] = Category::getWebMenu();
        $response['Category'] = Category::AllCategory();
        $response['general_setting'] = $this->generalSetting();



        return view("frontend.page.general-page-view",compact('TabHeader','response'));
    }

    public function RegisterAccount(){
        $TabHeader = 'Create Account';
        $response['GroupWiseMyAccountPage'] = $this->GroupWiseGeneralPage('myaccount');
        $response['GroupWiseCustomerServicePage'] = $this->GroupWiseGeneralPage('customerservice');
        $response['GroupWiseCompanyPage'] = $this->GroupWiseGeneralPage('company');

        $response['contact-us'] = $this->SlugWiseGeneralPage('contact-us');
        $response['about-us'] = $this->SlugWiseGeneralPage('about-us');
        $response['career'] = $this->SlugWiseGeneralPage('career');
        $response['WebMenu'] = Category::getWebMenu();
        $response['Category'] = Category::AllCategory();
        $response['general_setting'] = $this->generalSetting();



        return view("frontend.page.create-account",compact('TabHeader','response'));
    }

    public function CreateAccount(Request $request){
//        $input = $request->all();
//
//        $path = $request->file('image');
//        $type = pathinfo($path, PATHINFO_EXTENSION);
//        $data = file_get_contents($path);
//        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
//        $input['image'] = $base64;
//
//
//        $appUrl=env('APP_URL');
//
//        $response = Http::withHeaders([
//            'X-API-KEY' => 'supermartbd',
//            'X-API-VALUE' => 'supermartbd@123456',
//            'X-API-SECRET' => 'xquyt567'
//        ])->post($appUrl.'/api/create-customer-account',$input);
//
//        $body = $response->getBody()->getContents();
//        $InserResponse = json_decode($body);
//        echo '<pre>';
//        print_r($InserResponse);

//        if ($InserResponse[0]->success === 'Data Insert'){
//            Session::forget('cart');
//            Session::forget('cart_total');
//            return redirect()->route('web.orderplace.complete',$InserResponse[0]->order_id);
//        }else{
//            echo $InserResponse[0]->error;
//        }

        $input = $request->all();

        $validated = $request->validate([
            'name' => 'required',
            'image' => 'required|image',
            'address' => 'required',
            'email' => 'required|email',
            'mobile' => 'required|min:11|max:11',

        ]);

        $input['type'] = 'Customer';
        $input['status'] = '1';

        $EmailExistsOrNot = User::where('email',$input['email'])->count();

        if ($EmailExistsOrNot == 0) {
            $MobileExistsOrNot = User::where('mobile',$input['mobile'])->count();
        if ($MobileExistsOrNot == 0) {
            $name = str_replace(' ', '-', $input['name']);
            $name = str_replace("/\s+/", "-", $name);
            $name = str_replace(".", "-", $name);
            $name = strtolower($name);

            $Password = rand(10, 100000) . '' . $input['mobile'];
            $input['password'] = password_hash($Password, PASSWORD_DEFAULT);

            if ($request->file('image') != '') {
                $avatar = $request->file('image');
                $file_title = $name . time() . '.' . $avatar->getClientOriginalExtension();
                $input['user_image'] = $file_title;
                $path = public_path("backend/image/UserImage/");

                $target_file = $path . basename($file_title);

                $file_path = $_FILES['image']['tmp_name'];
                move_uploaded_file($file_path, $target_file);
            } else {
                $input['user_image'] = 'defaultuser.png';
            }

            /* Transaction Start Here */
            DB::beginTransaction();
            try {
                // Store User data
                if ($input['type'] == 'Customer') {
                    if ($UserData = User::create($input)) {
                        $UserData->save();

                        if ($input['type'] == 'Customer') {
                            $CustomerInfo['user_id'] = $UserData->id;
                            $CustomerInfo['first_name'] = $input['name'];
                            $CustomerInfo['bill_street_address'] = $input['address'];
                            $CustomerInfo['mobile'] = $input['mobile'];
                            $CustomerInfo['email'] = $input['email'];
                            $CustomerInfo['status'] = $input['status'];
                            Customer::create($CustomerInfo);
                        }

                        $details = [
                            'mailpage' => 'UserMail',
                            'title' => 'Supermartbd Login Information',
                            'name' => 'Hi ' . $input['name'],
                            'Email' => $input['email'],
                            'Type' => $input['type'],
                            'Password' => $Password
                        ];

                        \Mail::to($input['email'])->send(new \App\Mail\MailSend($details));
                    }

                }

                DB::commit();
                Session::flash('validate', 'Information added Successfully & check mail for login');
                echo 'User Added';
                return redirect()->back();
//                return redirect()->route('admin.user.index');
            } catch (\Exception $e) {
                //If there are any exceptions, rollback the transaction`
                DB::rollback();
                print($e->getMessage());
                exit();
                Session::flash('danger', $e->getMessage());
            }
        }else{
            Session::flash('validate', 'Mobile already exists');
            return redirect()->back()->withInput($input);
        }
        }else{
            Session::flash('validate', 'Email already exists');
            return redirect()->back()->withInput($input);
        }
    }


    public function SingleProduct($slug){
        $appUrl=env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
//        ])->get('http://supermartbd.local/api/single-product-details/'.$slug);
        ])->get($appUrl.'/api/single-product-details/'.$slug);


        $body = $response->getBody()->getContents();
        $SingleProduct = json_decode($body);
        return $SingleProduct;
    }


    public function AllBrand(){
        $appUrl = env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
//        ])->get('http://supermartbd.local/api/brand');
        ])->get($appUrl.'/api/brand');

        $body = $response->getBody()->getContents();
        $Brand = json_decode($body);
        return $Brand;
    }


    public  function AllDealsHot(){
        $appUrl=env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
        ])->get($appUrl.'/api/deals-hot');

        $body = $response->getBody()->getContents();
        $DealsHot = json_decode($body);
        return $DealsHot;
    }


    public  function AllTopSeller(){
        $appUrl=env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
        ])->get($appUrl.'/api/best-seller');

        $body = $response->getBody()->getContents();
        $TopSeller = json_decode($body);
        return $TopSeller;
    }

    public  function RecommendProduct(){
        $appUrl=env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
//        ])->get('http://supermartbd.local/api/recommend-product');
        ])->get($appUrl.'/api/recommend-product');

        $body = $response->getBody()->getContents();
        $RecommendProduct = json_decode($body);
        return $RecommendProduct;
    }

    public function GroupWiseGeneralPage($Group){
        $appUrl=env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
        ])->get($appUrl.'/api/group-wise-general-page/'.$Group);

        $body = $response->getBody()->getContents();
        $GroupWisePage = json_decode($body);
        return $GroupWisePage;
    }


    public function SlugWiseGeneralPage($slug){
        $appUrl=env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
        ])->get($appUrl.'/api/slug-wise-general-page/'.$slug);

        $body = $response->getBody()->getContents();
        $GroupWisePage = json_decode($body);
        return $GroupWisePage;
    }

    public function CustomCategoryProductForHome($slug){
        $appUrl=env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
        ])->get($appUrl.'/api/custom-category-product-for-home/'.$slug);

        $body = $response->getBody()->getContents();
        $categoryProduct = json_decode($body);
        return $categoryProduct;
    }

    public function CategoryWiseProduct($slug){
        $appUrl=env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
        ])->get($appUrl.'/api/category-wise-product/'.$slug);

        $body = $response->getBody()->getContents();
        $categoryProduct = json_decode($body);
        return $categoryProduct;
    }




    public function AllBrandProduct($slug){
        $appUrl=env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
//        ])->get('http://supermartbd.local/api/brand-wish-product/'.$slug);
        ])->get($appUrl.'/api/brand-wish-product/'.$slug);

        $body = $response->getBody()->getContents();
        $brandproduct = json_decode($body);
        return $brandproduct;
    }

    public function AllCategory(){
        $appUrl=env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
//        ])->get('http://supermartbd.local/api/category');
        ])->get($appUrl.'/api/category');

        $body = $response->getBody()->getContents();
        $all_category = json_decode($body);
        return $all_category;
    }

    public function AllCategorySubcategory(){
        $appUrl=env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
        ])->get($appUrl.'/api/all-category-subcategory');
//        ])->get('http://supermartbd.local/api/all-category-subcategory');

        $body = $response->getBody()->getContents();
        $Brand = json_decode($body);
        echo $response;
    }

    public function AllProduct(){
        $appUrl = env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
//        ])->get('http://supermartbd.local/api/all-product');
        ])->get($appUrl.'/api/all-product');

        $body = $response->getBody()->getContents();
        $Brand = json_decode($body);
        echo $response;
    }


    public function AllSlider(){
        $appUrl = env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
        ])->get($appUrl.'/api/all-slider');

        $body = $response->getBody()->getContents();
        $response = json_decode($body);
        return $response;
    }

    public function AllVendorLimit(){
        $appUrl=env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
        ])->get($appUrl.'/api/all-vendor-limit');

        $body = $response->getBody()->getContents();
        $response = json_decode($body);
        return $response;
    }

    public function SingleVendorInfo($slug){
        $appUrl=env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
        ])->get($appUrl.'/api/single-vendor-info/'.$slug);

        $body = $response->getBody()->getContents();
        $response = json_decode($body);
        return $response;
    }

    public function Banner($position){
        $appUrl=env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
        ])->get($appUrl.'/api/banner/'.$position);

        $body = $response->getBody()->getContents();
        $response = json_decode($body);
        return $response;
    }

    public function ProductFilter(){
        $TabHeader = 'Product Filter';
        $response['all-brand'] = $this->AllBrand();
        $response['all-category'] = $this->AllCategory();

        $response['GroupWiseMyAccountPage'] = $this->GroupWiseGeneralPage('myaccount');
        $response['GroupWiseCustomerServicePage'] = $this->GroupWiseGeneralPage('customerservice');
        $response['GroupWiseCompanyPage'] = $this->GroupWiseGeneralPage('company');
        $response['contact-us'] = $this->SlugWiseGeneralPage('contact-us');
        $response['about-us'] = $this->SlugWiseGeneralPage('about-us');
        $response['career'] = $this->SlugWiseGeneralPage('career');
        $response['WebMenu'] = Category::getWebMenu();
        $response['Category'] = Category::AllCategory();
        $response['general_setting'] = $this->generalSetting();

        $SearchModel = Product::where('status',1);

        if (isset($_GET['size-unit'])){
            $SizeUnit = $_GET['size-unit'];
        }

        $SizeUnit = explode(',',$SizeUnit);
        $sizeId=[];
        foreach ($SizeUnit as $value){
            if ($value != ''){
                $size = Sizeunit::where('slug',$value)->first();
                $sizeId[]=$size->id;
            }
        }
        if (isset($sizeId) && !empty($sizeId)){
            $SearchModel->where(function($query) use ($sizeId) {
                return $query->whereIn('size_unit_id',$sizeId);
            });
        }

//        echo '<pre>';
//        print_r($sizeId);
//        exit();


//        $TotalProduct = $SearchModel->count();
//        $ProductData = $SearchModel->paginate(12)->appends(request()->query());
//        $ProductData = $SearchModel->get();
//        echo $TotalProduct;
//        echo '<br>';
//        echo '<pre>';
//        print_r($ProductData);
//        exit();

//        if (isset($_GET['color-unit'])){
//            $ColorUnit = $_GET['color-unit'];
//        }
//
//        $ColorUnit = explode(',',$ColorUnit);
//        foreach ($ColorUnit as $value){
//            if ($value != ''){
//                echo 'Color : '.$value;
//                echo '<br>';
//                $color = Color::where('slug',$value)->first();
//                $productId = DB::table('ems_product_colors')->where('color_id',$color->id)->get();
//                $colorId[]=$color->product_id;
//            }
//        }
//        echo '<pre>';
//        print_r($productId);
//        exit();

        if (isset($_GET['price-filter'])){
            $PriceUnit = $_GET['price-filter'];
        }

        $PriceUnit = explode(',',$PriceUnit);
        foreach ($PriceUnit as $value){
            if ($value != ''){
                $priceLimit = explode('-',$value);
                foreach ($priceLimit as $value){
                    $priceValue[]=$value;
                }
            }
        }

        $SearchModel->where(function($query) use ($priceValue) {
            return $query->whereIn('salesprice',$priceValue);
        });

        $TotalProduct = $SearchModel->count();
//        $ProductData = $SearchModel->paginate(12)->appends(request()->query());
//        $ProductData = $SearchModel->get();
        $ProductData = $SearchModel->get();
//        echo $ProductData;
        echo $TotalProduct;
        echo '<br>';
        echo '<pre>';
        print_r($ProductData);
        exit();


//        echo $sizeunit;
    }


    public function ProductSearch(){
        $KeyWord='';
        $CategorySlug='';
        if (isset($_GET['cat'])){
            $CategorySlug = $_GET['cat'];
        }

        if (isset($_GET['key'])){
            $KeyWord = $_GET['key'];
            $KeyWord = trim($KeyWord);
        }


        $TabHeader = 'Product Search';
        $response['all-brand'] = $this->AllBrand();
        $response['all-category'] = $this->AllCategory();

        $response['GroupWiseMyAccountPage'] = $this->GroupWiseGeneralPage('myaccount');
        $response['GroupWiseCustomerServicePage'] = $this->GroupWiseGeneralPage('customerservice');
        $response['GroupWiseCompanyPage'] = $this->GroupWiseGeneralPage('company');
        $response['contact-us'] = $this->SlugWiseGeneralPage('contact-us');
        $response['about-us'] = $this->SlugWiseGeneralPage('about-us');
        $response['career'] = $this->SlugWiseGeneralPage('career');
        $response['WebMenu'] = Category::getWebMenu();
        $response['Category'] = Category::AllCategory();
        $response['general_setting'] = $this->generalSetting();

        if (isset($CategorySlug) && !empty($CategorySlug)) {
            $CategoryId = Category::where('slug', $CategorySlug)->select('id')->first();
            $CategoryId = $CategoryId->id;
        }

        $SearchModel = Product::where('status',1);

        if (((isset($CategoryId) && !empty($CategoryId)) && isset($KeyWord) && !empty($KeyWord))){
             $SearchModel->where('category_id',$CategoryId);
             $SearchModel->orWhere('name', 'LIKE', '%'.$KeyWord.'%');
             $SearchModel->orWhere('slug', 'LIKE', '%'.$KeyWord.'%');
             $SearchModel->orWhere('quantity', 'LIKE', '%'.$KeyWord.'%');
             $SearchModel->orWhere('minquantity', 'LIKE', '%'.$KeyWord.'%');
             $SearchModel->orWhere('maxquantity', 'LIKE', '%'.$KeyWord.'%');
             $SearchModel->orWhere('salesprice', 'LIKE', '%'.$KeyWord.'%');
             $SearchModel->orWhere('purchaseprice', 'LIKE', '%'.$KeyWord.'%');
             $SearchModel->orWhere('discription', 'LIKE', '%'.$KeyWord.'%');
             $SearchModel->orWhere('feature_tag', 'LIKE', '%'.$KeyWord.'%');
        }elseif((isset($CategoryId) && !empty($CategoryId)) && empty($KeyWord)){
             $SearchModel->where('category_id',$CategoryId);
        }elseif (isset($KeyWord) && !empty($KeyWord) && empty($CategoryId)){
             $SearchModel->orWhere('name', 'LIKE', '%'.$KeyWord.'%');
             $SearchModel->orWhere('slug', 'LIKE', '%'.$KeyWord.'%');
             $SearchModel->orWhere('quantity', 'LIKE', '%'.$KeyWord.'%');
             $SearchModel->orWhere('minquantity', 'LIKE', '%'.$KeyWord.'%');
             $SearchModel->orWhere('maxquantity', 'LIKE', '%'.$KeyWord.'%');
             $SearchModel->orWhere('salesprice', 'LIKE', '%'.$KeyWord.'%');
             $SearchModel->orWhere('purchaseprice', 'LIKE', '%'.$KeyWord.'%');
             $SearchModel->orWhere('discription', 'LIKE', '%'.$KeyWord.'%');
             $SearchModel->orWhere('feature_tag', 'LIKE', '%'.$KeyWord.'%');
        }

        $TotalProduct = $SearchModel->count();
        $ProductData = $SearchModel->paginate(12)->appends(request()->query());

        return view("frontend.page.search-index",compact('KeyWord','CategorySlug','TabHeader','TotalProduct','ProductData','response'));

    }

    public function QuickView(){
        $ProductId = $_GET['ProductId'];
        $ProductSlug = $_GET['ProductSlug'];

        $ProductData = $this->SingleProduct($ProductSlug);
//        echo '<pre>';
//        print_r($ProductData);

        $Response['name'] = $ProductData->name;
        $Response['salesprice'] = $ProductData->salesprice;
        $Response['slug'] = $ProductData->slug;
        $i = 1;
        foreach ($ProductData->product_image as $value){
            $Response['product_image_'.$i] = $value->attach_link;
            $i++;
        }
        $Response['product_feature_image'] = $ProductData->feature_image;
        $Response['category'] = $ProductData->product_category->name;
        $Response['category_image'] = $ProductData->product_category->category_image;
        $Response['category_slug'] = $ProductData->product_category->slug;
        $Response['brand'] = $ProductData->product_brand->name;
        $Response['brand_slug'] = $ProductData->product_brand->slug;

//        $Body = \Illuminate\Support\Facades\View::make('frontend.layouts.quick-view',compact('ProductId'));
//        $contents = $Body->render();
        return $Response;
    }


    public function SizeUnit(){
        $appUrl=env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
        ])->get($appUrl.'/api/size-unit/');

        $body = $response->getBody()->getContents();
        $response = json_decode($body);
        return $response;
    }


    public function Color(){
        $appUrl=env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
        ])->get($appUrl.'/api/color/');

        $body = $response->getBody()->getContents();
        $response = json_decode($body);
        return $response;
    }

    public function generalSetting(){
        $appUrl=env('APP_URL');
        $response = Http::withHeaders([
            'X-API-KEY' => 'supermartbd',
            'X-API-VALUE' => 'supermartbd@123456',
            'X-API-SECRET' => 'xquyt567'
        ])->get($appUrl.'/api/general-setting');

        $body = $response->getBody()->getContents();
        $response = json_decode($body);
        return $response;
    }


}
