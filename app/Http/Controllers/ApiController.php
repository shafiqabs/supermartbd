<?php

namespace App\Http\Controllers;


use App\Models\OrderBillingShipping;
use App\Models\OrderHead;
use App\Models\OrderProduct;
use App\Modules\DeliveryTimeSlot\Models\DeliveryTimeSlot;
use App\Modules\GeneralSetting\Models\GeneralSetting;
use App\Modules\MasterData\Models\ProductBrand;
use App\Modules\MasterData\Models\Category;
use App\Modules\Product\Models\ProductFile;
use App\Modules\Product\Models\Product;
use App\Modules\Vendor\Models\Vendor;
use App\Modules\Banner\Models\Banner;
use App\Modules\MasterData\Models\Sizeunit;

//use App\Modules\Banner\Models\BannerItems;


use Illuminate\Http\Request;
use Illuminate\Http\Response;

//use Illuminate\Support\Facades\Session;



use Exception;
use GuzzleHttp\Psr7\Message;

use DB;
use Illuminate\Support\Facades\Validator;
use Session;
use Image;
use File;
use Storage;
use App;
Use Auth;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        global $access;
        $requestDBName = $request->header('x-api-key');
        $requestDBUserName = $request->header('x-api-value');
        $requestDBPass = $request->header('x-api-secret');

        $key = \config('api.key');
        $value = \config('api.value');
        $secret = \config('api.secret');

        if (($requestDBName == $key) && ($requestDBUserName == $value) && ($requestDBPass == $secret)){
            $this->access = 'allow';
        }else{
            $this->access= 'Not allow';
        }
        echo $access;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function BrandGetData(){
        try{
            if ($this->access == 'allow'){
                $all_brand = ProductBrand::all();
                return \response(
                    $all_brand
                );
            }else{
                return \response([
                    'message'=>'Error,Incorrect API Info'
                ]);
            }
        }catch(Exception $ex){
            return \response([
                'message'=>$ex->getMessage()
            ]);
        }
    }

    public function SingleProductDeails($slug){
        try{
            if ($this->access == 'allow'){
                $single_product = Product::with('ProductImage')->with('ProductCategory')->with('ProductBrand')->where('slug',$slug)->first();
                return \response(
                    $single_product
                );
            }else{
                return \response([
                    'message'=>'Error,Incorrect API Info'
                ]);
            }
        }catch(Exception $ex){
            return \response([
                'message'=>$ex->getMessage()
            ]);
        }
    }


    public function Banner($position){
        try{
            if ($this->access == 'allow'){
                if ($position == 'banner-top'){
                    $Banner = Banner::with('TopBannerItems')->where('banner_position',$position)->where('banner_visibility','Show')->first();
                }

                if ($position == 'banner-bottom'){
                    $Banner = Banner::with('BottomBannerItems')->where('banner_position',$position)->where('banner_visibility','Show')->first();
                }

                return \response(
                    $Banner
                );
            }else{
                return \response([
                    'message'=>'Error,Incorrect API Info'
                ]);
            }
        }catch(Exception $ex){
            return \response([
                'message'=>$ex->getMessage()
            ]);
        }
    }

    public function DealsHotData(){
        try{
            if ($this->access == 'allow'){
                $all_dealshot = Product::with('ProductImage','ProductCategory','ProductBrand')->inRandomOrder()->take(20)->where('feature_tag','dealshot')->get();
                return \response(
                    $all_dealshot
                );
            }else{
                return \response([
                    'message'=>'Error,Incorrect API Info'
                ]);
            }
        }catch(Exception $ex){
            return \response([
                'message'=>$ex->getMessage()
            ]);
        }
    }

    public function TopSellerData(){
        try{
            if ($this->access == 'allow'){
                $all_bestseller = Product::with('ProductImage')->inRandomOrder()->take(20)->where('feature_tag','bestseller')->get();
                return \response(
                    $all_bestseller
                );
            }else{
                return \response([
                    'message'=>'Error,Incorrect API Info'
                ]);
            }
        }catch(Exception $ex){
            return \response([
                'message'=>$ex->getMessage()
            ]);
        }
    }


    public function GetRecommendProduct(){
        try{
            if ($this->access == 'allow'){
                $Recommend = Product::with('ProductImage')->inRandomOrder()->take(20)->where('feature_tag','recommend')->get();
                return \response(
                    $Recommend
                );
            }else{
                return \response([
                    'message'=>'Error,Incorrect API Info'
                ]);
            }
        }catch(Exception $ex){
            return \response([
                'message'=>$ex->getMessage()
            ]);
        }
    }


    public function GroupWiseGeneralPage($Group){
        try{
            if ($this->access == 'allow'){
                if ($Group == 'support'){
                    $GroupGeneralPage = App\Modules\GeneralPage\Models\GeneralPage::where('group',$Group)->Orderby('id','DESC')->limit(4)->get();
                }else{
                    $GroupGeneralPage = App\Modules\GeneralPage\Models\GeneralPage::where('group',$Group)->Orderby('id','DESC')->limit(6)->get();
                }

                return \response(
                    $GroupGeneralPage
                );
            }else{
                return \response([
                    'message'=>'Error,Incorrect API Info'
                ]);
            }
        }catch(Exception $ex){
            return \response([
                'message'=>$ex->getMessage()
            ]);
        }
    }


    public function SlugWiseGeneralPage($slug){
        try{
            if ($this->access == 'allow'){

                $GroupGeneralPage = App\Modules\GeneralPage\Models\GeneralPage::where('slug',$slug)->first();

                return \response(
                    $GroupGeneralPage
                );
            }else{
                return \response([
                    'message'=>'Error,Incorrect API Info'
                ]);
            }
        }catch(Exception $ex){
            return \response([
                'message'=>$ex->getMessage()
            ]);
        }
    }



    public function CategoryWiseProduct($slug){
        try{
            if ($this->access == 'allow'){
                $all_categoryProduct = DB::table('ems_product')
                    ->join('ems_product_brand', 'ems_product_brand.id', '=', 'ems_product.brand_id')
                    ->join('ems_category', 'ems_category.id', '=', 'ems_product.category_id')
                    ->where('ems_category.slug', '=', $slug)
                    ->select('ems_product_brand.name as brand_name','ems_product.*','ems_category.name as category_name','ems_category.slug as category_slug')
                    ->get();
//                $all_categoryProduct = Category::with('CategoryProduct')->where('slug',$slug)->first();
                return \response(
                    $all_categoryProduct
                );
            }else{
                return \response([
                    'message'=>'Error,Incorrect API Info'
                ]);
            }
        }catch(Exception $ex){
            return \response([
                'message'=>$ex->getMessage()
            ]);
        }
    }



    public function CustomCategoryProductForHome($slug){
        try{
            if ($this->access == 'allow'){
//                $all_categoryProduct = DB::table('ems_product')
//                    ->join('ems_product_brand', 'ems_product_brand.id', '=', 'ems_product.brand_id')
//                    ->join('ems_category', 'ems_category.id', '=', 'ems_product.category_id')
//                    ->where('ems_category.slug', '=', $slug)
//                    ->select('ems_product_brand.name as brand_name','ems_product.*','ems_category.name as category_name','ems_category.slug as category_slug')
//                    ->get();
                $all_categoryProduct = Category::with('CategoryProduct')->where('slug',$slug)->first();
                return \response(
                    $all_categoryProduct
                );
            }else{
                return \response([
                    'message'=>'Error,Incorrect API Info'
                ]);
            }
        }catch(Exception $ex){
            return \response([
                'message'=>$ex->getMessage()
            ]);
        }
    }

    public function BrandProduct($slug){
        try{
            if ($this->access == 'allow'){
                $Brand_product = DB::table('ems_product')
                    ->join('ems_product_brand', 'ems_product_brand.id', '=', 'ems_product.brand_id')
                    ->join('ems_category', 'ems_category.id', '=', 'ems_product.category_id')
                    ->where('ems_product_brand.slug', '=', $slug)
                    ->select('ems_product_brand.name as brand_name','ems_product.*','ems_category.name as category_name','ems_category.slug as category_slug')
                    ->get();
                return \response(
                    $Brand_product
                );
            }else{
                return \response([
                    'message'=>'Error,Incorrect API Info'
                ]);
            }
        }catch(Exception $ex){
            return \response([
                'message'=>$ex->getMessage()
            ]);
        }
    }

//    public function CategoryProduct1($slug){
//        try{
//            if ($this->access == 'allow'){
//                $all_categoryProduct = Category::with('CategoryProduct')->where('slug',$slug)->first();
//                return \response(
//                    $all_categoryProduct
//                );
//            }else{
//                return \response([
//                    'message'=>'Error,Incorrect DB Info'
//                ]);
//            }
//        }catch(Exception $ex){
//            return \response([
//                'message'=>$ex->getMessage()
//            ]);
//        }
//    }


//    public function CategoryProduct2($slug){
//        try{
//            if ($this->access == 'allow'){
//                $all_categoryProduct = Category::with('CategoryProduct')->where('slug',$slug)->first();
//                return \response(
//                    $all_categoryProduct
//                );
//            }else{
//                return \response([
//                    'message'=>'Error,Incorrect DB Info'
//                ]);
//            }
//        }catch(Exception $ex){
//            return \response([
//                'message'=>$ex->getMessage()
//            ]);
//        }
//    }

    public function CategoryGetData(){
        try{
            if ($this->access == 'allow'){
                $all_category = Category::all();
                return \response(
                    $all_category
                );
            }else{
                return \response([
                    'message'=>'Error,Incorrect API Info'
                ]);
            }
        }catch(Exception $ex){
            return \response([
                'message'=>$ex->getMessage()
            ]);
        }
    }

    public function getHierarchyCategory(){
        try{
            if ($this->access == 'allow'){
                $all_category = Category::getHierarchyCategory();
                return \response(
                    $all_category
                );
            }else{
                return \response([
                    'message'=>'Error,Incorrect API Info'
                ]);
            }
        }catch(Exception $ex){
            return \response([
                'message'=>$ex->getMessage()
            ]);
        }
    }

    public function ProductGetData(){
        try{
            if ($this->access == 'allow'){
                $all_product = ProductFile::get();
                return \response(
                    $all_product
                );
            }else{
                return \response([
                    'message'=>'Error,Incorrect API Info'
                ]);
            }
        }catch(Exception $ex){
            return \response([
                'message'=>$ex->getMessage()
            ]);
        }
    }


    public function SliderGetData(){
        try{
            if ($this->access == 'allow'){
                $all_slider = App\Modules\Slider\Models\Slider::inRandomOrder()->limit(3)->get();
                return \response(
                    $all_slider
                );
            }else{
                return \response([
                    'message'=>'Error,Incorrect API Info'
                ]);
            }
        }catch(Exception $ex){
            return \response([
                'message'=>$ex->getMessage()
            ]);
        }
    }

    public function VendorLimitGetData(){
        try{
            if ($this->access == 'allow'){
                $all_vendor = App\Modules\Vendor\Models\Vendor::with('VendorProduct')->inRandomOrder()->limit(8)->get();
                return \response(
                    $all_vendor
                );
            }else{
                return \response([
                    'message'=>'Error,Incorrect API Info'
                ]);
            }
        }catch(Exception $ex){
            return \response([
                'message'=>$ex->getMessage()
            ]);
        }
    }

    public function SingleVendorGetData($slug){
        try{
            if ($this->access == 'allow'){
                $single_vendor_info = App\Modules\Vendor\Models\Vendor::where('slug',$slug)->first();
                return \response(
                    $single_vendor_info
                );
            }else{
                return \response([
                    'message'=>'Error,Incorrect API Info'
                ]);
            }
        }catch(Exception $ex){
            return \response([
                'message'=>$ex->getMessage()
            ]);
        }
    }

    public function VendorWiseProductGetData($slug){
        try{
            if ($this->access == 'allow'){

//                $single_vendor_product = App\Modules\Vendor\Models\Vendor::with(['VendorProduct' => function ($query) {
//                    $query->paginate(2);
//                }])->where('slug',$slug)->get();


            $single_vendor_product = DB::table('ems_product')
                        ->join('ems_vendor', 'ems_vendor.id', '=', 'ems_product.vendor_id')
                        ->join('ems_category', 'ems_category.id', '=', 'ems_product.category_id')
                        ->where('ems_vendor.slug', '=', $slug)
                        ->select('ems_product.*','ems_category.name as category_name','ems_category.slug as category_slug')
                        ->paginate(20);

                return \response(
                    $single_vendor_product
                );
            }else{
                return \response([
                    'message'=>'Error,Incorrect API Info'
                ]);
            }
        }catch(Exception $ex){
            return \response([
                'message'=>$ex->getMessage()
            ]);
        }
    }


    public function CreateOrder(Request $request){
        if ($this->access == 'allow'){
        $validated = $request->validate([
            'bill_firstname' => 'required|min:3|max:50',
//            'bill_lastname' => 'required|min:3|max:50',
            'bill_phone' => 'required',
            'bill_email' => 'min:3|max:50|email',
            'shipping_time_slot' => 'required',
            'payment_method' => 'required',
            'bill_street_address' => 'required',
        ]);
        $input = $request->all();
//dd($input['product_id']);
            foreach($input['product_id'] as $index => $value) {
                $VendorData = Product::where('id',$input['product_id'][$index])->select('vendor_id')->first();
                $VendorId[] = $VendorData->vendor_id;

                $UserData = App\Modules\Vendor\Models\Vendor::where('id',$VendorData->vendor_id)->select('user_id')->first();
                $UserID[] = $UserData->user_id;

            }

            $VendorId = implode(",",$VendorId);
            $input['vendor_id'] = $VendorId;
            $UserID = implode(",",$UserID);
            $input['user_id'] = $UserID;


        $TimeSlotId = DeliveryTimeSlot::where('time_slot',$input['shipping_time_slot'])->first();
        $input['time_slot_id'] = $TimeSlotId->id;

        $TotalQuantity = 0;
        foreach($input['product_quantity'] as $index => $value) {
            $TotalQuantity = $TotalQuantity+$input['product_quantity'][$index];
        }
        $input['total_quantity'] = $TotalQuantity;

        $input['product_amount'] = $input['total_amount']-$input['delivery_charge'];
        $input['status'] = '1';

        if (isset($input['customer_id']) && $input['customer_id'] !=''){
            $input['customer_id'] = $input['customer_id'];
        }else{
            $input['customer_id'] = null;
        }


        if (isset($input['bill_firstname']) && !empty($input['bill_firstname'])){
            $BillingInfo['type'] = 'billing';
            $BillingInfo['firstname'] = $input['bill_firstname'];
            $BillingInfo['lastname'] = $input['bill_lastname'];
            $BillingInfo['country'] = $input['bill_country'];
            $BillingInfo['district'] = $input['bill_district'];
            $BillingInfo['address'] = $input['bill_street_address'];
            $BillingInfo['phone'] = $input['bill_phone'];
            $BillingInfo['email'] = $input['bill_email'];
            $BillingInfo['customer_id'] = $input['customer_id'];

        }

        if (!empty($input['ship_firstname']) || !empty($input['ship_lastname']) || !empty($input['ship_street_address']) || !empty($input['ship_phone']) || !empty($input['ship_email'])){
            $ShippingInfo['type'] = 'shipping';
            $ShippingInfo['firstname'] = $input['ship_firstname'];
            $ShippingInfo['lastname'] = $input['ship_lastname'];
            $ShippingInfo['country'] = $input['ship_country'];
            $ShippingInfo['district'] = $input['ship_district'];
            $ShippingInfo['address'] = $input['ship_street_address'];
            $ShippingInfo['phone'] = $input['ship_phone'];
            $ShippingInfo['email'] = $input['ship_email'];
            $ShippingInfo['customer_id'] = $input['customer_id'];

        }else{
            $ShippingInfo['type'] = 'shipping';
            $ShippingInfo['firstname'] = $input['bill_firstname'];
            $ShippingInfo['lastname'] = $input['bill_lastname'];
            $ShippingInfo['country'] = $input['bill_country'];
            $ShippingInfo['district'] = $input['bill_district'];
            $ShippingInfo['address'] = $input['bill_street_address'];
            $ShippingInfo['phone'] = $input['bill_phone'];
            $ShippingInfo['email'] = $input['bill_email'];
            $ShippingInfo['customer_id'] = $input['customer_id'];
        }

        DB::beginTransaction();
        try {
            // Store order
            if ($OrderData = OrderHead::create($input)) {
                $OrderData->save();
            }

            $OrderData->order_number = 'SMBD-'.str_replace('-','',date('Y-m-d')).'-'.str_pad($OrderData->id,6,"0",STR_PAD_LEFT);
            $OrderData->save();

            foreach($input['product_id'] as $index => $value) {
                $OrderProduct['product_id'] = $input['product_id'][$index];
                $OrderProduct['order_id'] = $OrderData->id;
                $OrderProduct['quantity'] = $input['product_quantity'][$index];
                $OrderProduct['unit_price'] = $input['product_price'][$index];
                $OrderProduct['subtotal'] = $input['product_quantity'][$index]*$input['product_price'][$index];

                $ProductInfo = Product::where('id',$input['product_id'][$index])->first();
                $OrderProduct['name'] = $ProductInfo->name;
                $OrderProduct['slug'] = $ProductInfo->slug;
                $StoreQuantity = $ProductInfo->quantity;

                $PresentQuantity['quantity'] = $StoreQuantity-$input['product_quantity'][$index];

                $VendorData = Product::where('id',$input['product_id'][$index])->select('vendor_id')->first();
                $OrderProduct['vendor_id'] = $VendorData->vendor_id;


                $UserData = Vendor::where('id',$VendorData->vendor_id)->select('user_id')->first();
                $OrderProduct['user_id'] = $UserData->user_id;

                OrderProduct::create($OrderProduct);
                $ProductInfo->update($PresentQuantity);
                $ProductInfo->save();

            }


            $BillingInfo['order_id'] = $OrderData->id;
            $ShippingInfo['order_id'] = $OrderData->id;
            OrderBillingShipping::create($BillingInfo);
            OrderBillingShipping::create($ShippingInfo);

            DB::commit();

            if (isset($input['bill_email']) && !empty($input['bill_email'])) {
                $OrderData = OrderHead::where('order_number',$OrderData->order_number)->first();
                $details = [
                    'mailpage' => 'OrderMail',
                    'title' => 'MASABA order confirmation mail',
                    'welcome' => 'Hi ' . $input['bill_firstname'] . ' ' . $input['bill_lastname'],
                    'body' => 'Thanks for order , Order Number id is '.$OrderData->order_number,
                    'OrderData' => $OrderData
                ];

                \Mail::to($input['bill_email'])->send(new \App\Mail\MailSend($details));
            }

            if ($OrderData){
                $message['success'] = 'Data Insert';
                $message['order_id'] = $OrderData->order_number;
                return \response([
                    $message
                ]);
            }else{
                $message['error'] = 'Order not submit';
                return \response([
                    $message
                ]);
            }

        } catch (\Exception $e) {
            //If there are any exceptions, rollback the transaction`
            DB::rollback();
            print($e->getMessage());
            exit();
            Session::flash('danger', $e->getMessage());
        }
        }else{
            return \response([
                'message'=>'Error,Incorrect API Info'
            ]);
        }
    }


    public function CustomerAccountCreate(Request $request){
        if ($this->access == 'allow'){
            $input = $request->all();
//            $input['image'] = base64_decode($input['image']);
//            $file = 'invoice.pdf';
//            file_put_contents($file, $decoded);
//            file_put_contents($input, file_get_contents($input['image']));
//            $getImage = $request->file('image');
//            if ($request->file('image') != '') {
//                $avatar = $request->file('image');
//                $file_title = 'rr' . time() . '.' . $avatar->getClientOriginalExtension();
//                $input['user_image'] = $file_title;
//                $path = public_path("backend/image/UserImage/");
//
//                $target_file = $path . basename($file_title);
//
//                $file_path = $_FILES['image']['tmp_name'];
//                move_uploaded_file($file_path, $target_file);
//            } else {
//                $input['user_image'] = 'defaultuser.png';
//            }
            return $input;
//            echo 'ok';
        }else{
            return \response([
                'message'=>'Error,Incorrect API Info'
            ]);
        }
    }



    public function OrderGetData($id){
        try{
            if ($this->access == 'allow'){
                $OrderData = OrderHead::with('OrderProductItem','OrderBillingShipping')->where('order_number',$id)->first();

                return \response(
                    $OrderData
                );
            }else{
                return \response([
                    'message'=>'Error,Incorrect API Info'
                ]);
            }
        }catch(Exception $ex){
            return \response([
                'message'=>$ex->getMessage()
            ]);
        }
    }




    public function CheckoutUserLogin(Request $request){
        if ($this->access == 'allow'){


        }else{
            return \response([
                'message'=>'Error,Incorrect API Info'
            ]);
        }
    }


    public function SizeUnit(){
        if ($this->access == 'allow'){
            $AllSizeUnit = Sizeunit::where('status',1)->inRandomOrder()->take(10)->get();

            return \response(
                $AllSizeUnit
            );
        }else{
            return \response([
                'message'=>'Error,Incorrect API Info'
            ]);
        }
    }

    public function Color(){
        if ($this->access == 'allow'){
            $AllColor = App\Modules\MasterData\Models\Color::where('status',1)->inRandomOrder()->take(10)->get();

            return \response(
                $AllColor
            );
        }else{
            return \response([
                'message'=>'Error,Incorrect API Info'
            ]);
        }
    }

    public function getGeneralSetting(){
        if ($this->access == 'allow'){
            $generalSetting = GeneralSetting::first();

            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode([
                'message' => 'SUCCESS',
                'status' => '404',
                'general_setting' => $generalSetting
            ]));
            $response->setStatusCode(Response::HTTP_OK);
            return $response;

        }else{
            return \response([
                'message'=>'Error,Incorrect API Info'
            ]);
        }
    }



}
