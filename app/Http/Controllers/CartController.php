<?php

namespace App\Http\Controllers;

use App\Modules\MasterData\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Http\Helpers\CartHelper;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\FrontendController;
use App\Modules\Product\Models\Product;


class CartController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */



    public function ProductAdd(){
        $product_id = $_GET['product_id'];
        $product_slug = $_GET['product_slug'];
        $product_image = $_GET['product_image'];
        $product_price = $_GET['product_price'];
        $product_quantity = $_GET['product_quantity'];
        $product_name = $_GET['product_name'];


        $added_items = [];
        $cart = [];
        if(isset($product_quantity)){

            $product = Product::where('id',$product_id)->first();

            if(!empty($product))
            {
                $item['product_id'] = $product_id;
                $item['product_slug'] = $product_slug;
                $item['product_image'] = $product_image;
                $item['product_price'] = $product_price;
                $item['product_quantity'] = $product_quantity;
                $item['product_name'] = $product_name;

                $cart = CartHelper::add_item($item);

            }

            $response = [];
            if(Session::has('cart')){
                $cart_total = number_format(Session::get('cart_total')['total'], 2);
            }else{
                $cart_total = number_format(0,2);
            }

            if(count($cart) > 0)
            {

                $cart_body = \Illuminate\Support\Facades\View::make('frontend.master._cart');
                $contents = $cart_body->render();

                $response['result'] = 'success';
                $response['message'] = 'Product successfully added to cart.';
                $response['total_item'] = count($cart);
                $response['cart_total'] = $cart_total;
                $response['cart_body'] = $contents;

            }else{
                $response['result'] = 'error';
                $response['message'] = 'Product not added to cart.';
            }

            return $response;

        }
    }


    public function ProductRemove(){
        $product_id = $_GET['product_id'];
//        echo $product_id;
        $cart = CartHelper::remove_item($product_id);

        $response = [];
//        $response['result'] = 'success';
//        $response['message'] = 'Product successfully removed from cart.';
//        return $response;
        if(Session::has('cart')){
            $cart_total = number_format(Session::get('cart_total')['total'], 2);
        }else{
            $cart_total = number_format(0,2);
        }


            $cart_body = \Illuminate\Support\Facades\View::make('frontend.master._cart');
            $contents = $cart_body->render();

//            $response['result'] = 'success';
//            $response['message'] = 'Product successfully added to cart.';
            $response['total_item'] = count($cart);
            $response['cart_total'] = $cart_total;
            $response['cart_body'] = $contents;


        return $response;
    }


    public function MyCartView(){
        $TabHeader = 'My Cart';

        $response['GroupWiseMyAccountPage'] = (new FrontendController)->GroupWiseGeneralPage('myaccount');
        $response['GroupWiseCustomerServicePage'] = (new FrontendController)->GroupWiseGeneralPage('customerservice');
        $response['GroupWiseCompanyPage'] = (new FrontendController)->GroupWiseGeneralPage('company');
        $response['contact-us'] = (new FrontendController)->SlugWiseGeneralPage('contact-us');
        $response['about-us'] = (new FrontendController)->SlugWiseGeneralPage('about-us');
        $response['career'] = (new FrontendController)->SlugWiseGeneralPage('career');
        $response['WebMenu'] = Category::getWebMenu();
        $response['Category'] = Category::AllCategory();
        $response['general_setting'] = (new FrontendController)->generalSetting();



        return view("frontend.page.mycart",compact('TabHeader','response'));
    }


    public function MyCartUpdate(){
        $product_id = $_GET['product_id'];
        $product_quantity = $_GET['product_quantity'];

        $items['product_id'] = $product_id;
        $items['product_quantity'] = $product_quantity;

        $cart = CartHelper::update($items);

        if(Session::has('cart')){
            $cart_total = number_format(Session::get('cart_total')['total'], 2);
        }else{
            $cart_total = number_format(0,2);
        }

        $cart_body = \Illuminate\Support\Facades\View::make('frontend.master._cart');
        $contents = $cart_body->render();

        $response['total_item'] = count($cart);
        $response['cart_total'] = $cart_total;
        $response['cart_body'] = $contents;

        return $response;
    }


    public function MyCartClear(){
        Session::forget('cart');
        Session::forget('cart_total');

        $cart_body = \Illuminate\Support\Facades\View::make('frontend.master._cart');
        $contents = $cart_body->render();
        $response['cart_body'] = $contents;

        $response['cart_empty'] = 'Cart Is Empty';

        return $response;
    }



}
