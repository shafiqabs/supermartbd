<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CustomerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next , $guard="web")
    {
        // Check guest user
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('404');
            }
        }


        // Check Vendor / Seller
        if(Auth::user()->type == 'Customer' || Auth::user()->type == 'Admin'){
            return $next($request);
        }else{
//            return redirect()->guest('404');
            return response('Unauthorized Access', 404);
        }

    }
}
