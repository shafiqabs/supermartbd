<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard="web")
    {

        // Check guest user
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized Access', 401);
            } else {
                return redirect()->guest('404');
            }
        }


        // Check Main admin
        if(Auth::user()->type == 'Admin'){
            return $next($request);
        }else{
//            return redirect()->guest('404');
            return response('Unauthorized Access', 404);
        }

    }
}
