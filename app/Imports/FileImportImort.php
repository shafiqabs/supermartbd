<?php

namespace App\Imports;

//use App\Modules\Fileimport\Models\Fileimport;
use App\Modules\Product\Models\ProductFile;
use App\Modules\Product\Models\Product;
use App\Modules\MasterData\Models\Category;
use App\Modules\MasterData\Models\ProductBrand;
use App\Modules\MasterData\Models\Sizeunit;
use App\Modules\MasterData\Models\ItemUnit;
use Maatwebsite\Excel\Concerns\ToModel;

use Auth;
use Session;

class FileImportImort implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {

//    if import file category name exists in category thn get category id or insert category & get insert id
        $slug = str_replace(' ', '-', $row[1]);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $brand_slug = strtolower($slug);

        $SlugExistsOrNotbrand = ProductBrand::where('slug',$brand_slug)->count();

        if ($SlugExistsOrNotbrand == 1){
            $BrandData = ProductBrand::where('slug',$brand_slug)->first();
            $Brand_id = $BrandData->id;
        }else{
            $input['name'] = $row[1];
            $input['slug'] = $brand_slug;
            $input['status'] = 1;
            if ($Brand_store = ProductBrand::create($input)) {
                $Brand_store->save();
                $Brand_id = $Brand_store->id;
            }
        }

//  if import file category name exists in category thn get category id or insert category & get insert id
        $slug = str_replace(' ', '-', $row[2]);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $category_slug = strtolower($slug);

        $SlugExistsOrNot = Category::where('slug',$category_slug)->count();

        if ($SlugExistsOrNot == 1){
            $CategoryData = Category::where('slug',$category_slug)->first();
            $Category_id = $CategoryData->id;
        }else{
            $input['name'] = $row[2];
            $input['slug'] = $category_slug;
            $input['status'] = 1;
            if ($category_store = Category::create($input)) {
                $category_store->save();
                $Category_id = $category_store->id;
            }
        }

//  if import file size name exists  thn get size id or insert size & get insert id
        $slug = str_replace(' ', '-', $row[3]);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $size_slug = strtolower($slug);

        $SlugExistsOrNot = Sizeunit::where('slug',$size_slug)->count();

//        echo $SlugExistsOrNot;
//        exit();

        if ($SlugExistsOrNot == 1){
            $SizeData = Sizeunit::where('slug',$size_slug)->first();
            $size_id = $SizeData->id;
        }else{
            $input['name'] = $row[3];
            $input['slug'] = $size_slug;
            $input['status'] = 1;
            if ($size_store = Sizeunit::create($input)) {
                $size_store->save();
                $size_id = $size_store->id;
            }
        }

//  if import file itemunit name exists  thn get itemunit id or insert itemunit & get insert id
        $slug = str_replace(' ', '-', $row[4]);
        $slug = str_replace("/\s+/", "-", $slug);
        $slug = str_replace(".", "-", $slug);
        $item_slug = strtolower($slug);

        $SlugExistsOrNot = ItemUnit::where('slug',$item_slug)->count();

        if ($SlugExistsOrNot == 1){
            $itemData = ItemUnit::where('slug',$item_slug)->first();
            $item_id = $itemData->id;
        }else{
            $input['name'] = $row[4];
            $input['slug'] = $item_slug;
            $input['status'] = 1;
            if ($item_store = ItemUnit::create($input)) {
                $item_store->save();
                $item_id = $item_store->id;
            }
        }

        //create product slug
        $ProductSlug = str_replace(' ', '-', $row[0]);
        $ProductSlug = str_replace("/\s+/", "-", $ProductSlug);
        $ProductSlug = str_replace(".", "-", $ProductSlug);
        $ProductSlug = strtolower($ProductSlug);
        $ProductSlugExistsOrNot = ProductFile::where('slug',$ProductSlug)->count();

        if (Auth::user()->type == 'Vendor'){
            $vendor_id = Auth::user()->UserVendor->id;
        }else{
            $vendor_id = null;
        }

//        echo $ProductSlugExistsOrNot;
//        exit();

        if ($ProductSlugExistsOrNot == 0) {
            return new ProductFile([
                'name' => $row[0],
                'brand_id' => $Brand_id,
                'category_id' => $Category_id,
                'size_unit_id' => $size_id,
                'item_unit_id' => $item_id,
                'quantity' => $row[5],
                'minquantity' => $row[6],
                'maxquantity' => $row[7],
                'salesprice' => $row[8],
                'purchaseprice' => $row[9],
                'slug' => $ProductSlug,
                'vendor_id' => $vendor_id,
            ]);
        }else{
//            $msg['error'] =  \Session::flash('error', 'Duplicate -'.$row[0]);
//            return $msg['error'];
//            return \Session::flash('error', 'Duplicate -'.$row[0]);

//            return $mes = 'Duplicate row no '.$row[0];
        }
    }
}
