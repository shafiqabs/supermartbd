<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App;
use Auth;

class OrderHead extends Model
{
    use HasFactory;

    protected $table = 'ems_order_head';
    protected $fillable = [
        'order_notes',
        'time_slot_id',
        'time_slot_id',
        'shipping_time_slot',
        'payment_method',
        'product_amount',
        'delivery_charge',
        'total_amount',
        'total_quantity',
        'status',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'vendor_id',
        'customer_id',
        'user_id'
    ];


    public function OrderProductItem(){
        return $this->hasMany('App\Models\OrderProduct','order_id','id');
    }

    public function OrderBillingShipping(){
        return $this->hasMany('App\Models\OrderBillingShipping','order_id','id');
    }

    // TODO :: boot
    // boot() function used to insert logged user_id at 'created_by' & 'updated_by'
    public static function boot(){
        parent::boot();
        static::creating(function($query){
            if(Auth::check()){
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function($query){
            if(Auth::check()){
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}
