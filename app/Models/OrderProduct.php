<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App;
use Auth;

class OrderProduct extends Model
{
    use HasFactory;

    protected $table = 'ems_order_item';
    protected $fillable = [
        'order_id',
        'product_id',
        'name',
        'slug',
        'quantity',
        'unit_price',
        'subtotal',
        'created_at',
        'updated_at',
        'vendor_id',
        'user_id'
    ];

    public function ProductDetails(){
        return $this->belongsTo('App\Modules\Product\Models\Product','product_id','id');
    }

    public function VendorDetails(){
        return $this->belongsTo('App\Modules\Vendor\Models\Vendor','vendor_id','id');
    }



    // TODO :: boot
    // boot() function used to insert logged user_id at 'created_by' & 'updated_by'
    public static function boot(){
        parent::boot();
        static::creating(function($query){
            if(Auth::check()){
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function($query){
            if(Auth::check()){
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}
