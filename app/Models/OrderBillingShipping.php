<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App;
use Auth;

class OrderBillingShipping extends Model
{
    use HasFactory;

    protected $table = 'ems_order_billing_shipping';
    protected $fillable = [
        'order_id',
        'type',
        'firstname',
        'lastname',
        'country',
        'district',
        'address',
        'phone',
        'email',
        'created_at',
        'customer_id',
        'updated_at'
    ];



    // TODO :: boot
    // boot() function used to insert logged user_id at 'created_by' & 'updated_by'
    public static function boot(){
        parent::boot();
        static::creating(function($query){
            if(Auth::check()){
                $query->created_by = Auth::user()->id;
            }
        });
        static::updating(function($query){
            if(Auth::check()){
                $query->updated_by = Auth::user()->id;
            }
        });
    }
}
