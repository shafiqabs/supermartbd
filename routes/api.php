<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Modules\MasterData\Models\ProductBrand;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::POST('api/create-order',[
    'as' => 'api.create-order',
    'uses' => 'App\Http\Controllers\ApiController@CreateOrder',
]);

Route::POST('api/create-customer-account',[
    'as' => 'api.create-customer-account',
    'uses' => 'App\Http\Controllers\ApiController@CustomerAccountCreate',
]);




Route::get('api/order-data/{id}',[
    'as' => 'api.order-data',
    'uses' => 'App\Http\Controllers\ApiController@OrderGetData',
]);


Route::POST('api/checkout-user-login',[
    'as' => 'api/checkout-user-login',
    'uses' => 'App\Http\Controllers\ApiController@CheckoutUserLogin',
]);

Route::get('api/general-setting',[
    'as' => 'api/general-setting',
    'uses' => 'App\Http\Controllers\ApiController@getGeneralSetting',
]);









