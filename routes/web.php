<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', [App\Http\Controllers\FrontendController::class,'HomePage']);


//API ROUTE START
Route::get('api/brand', [App\Http\Controllers\ApiController::class,'BrandGetData']);
Route::get('api/deals-hot', [App\Http\Controllers\ApiController::class,'DealsHotData']);
Route::get('api/best-seller', [App\Http\Controllers\ApiController::class,'TopSellerData']);
Route::get('api/recommend-product', [App\Http\Controllers\ApiController::class,'GetRecommendProduct']);
Route::get('api/custom-category-product-for-home/{slug}', [App\Http\Controllers\ApiController::class,'CustomCategoryProductForHome']);
Route::get('api/category-wise-product/{slug}', [App\Http\Controllers\ApiController::class,'CategoryWiseProduct']);
//Route::get('api/category-product2/{slug}', [App\Http\Controllers\ApiController::class,'CategoryProduct2']);
Route::get('api/category', [App\Http\Controllers\ApiController::class,'CategoryGetData']);
Route::get('api/all-category-subcategory', [App\Http\Controllers\ApiController::class,'getHierarchyCategory']);
Route::get('api/all-product', [App\Http\Controllers\ApiController::class,'ProductGetData']);
Route::get('api/single-product-details/{slug}', [App\Http\Controllers\ApiController::class,'SingleProductDeails']);
Route::get('api/brand-wish-product/{slug}', [App\Http\Controllers\ApiController::class,'BrandProduct']);
Route::get('api/all-slider', [App\Http\Controllers\ApiController::class,'SliderGetData']);
Route::get('api/all-vendor-limit', [App\Http\Controllers\ApiController::class,'VendorLimitGetData']);
Route::get('api/single-vendor-info/{slug}', [App\Http\Controllers\ApiController::class,'SingleVendorGetData']);
Route::get('api/vendor-wish-product/{slug}', [App\Http\Controllers\ApiController::class,'VendorWiseProductGetData']);
Route::get('api/size-unit', [App\Http\Controllers\ApiController::class,'SizeUnit']);
Route::get('api/color', [App\Http\Controllers\ApiController::class,'Color']);
//Route::post('api/web-checkout-orderplace', [App\Http\Controllers\ApiController::class,'OrderPlace']);
//Route::post('api/create-order', [App\Http\Controllers\ApiController::class,'CreateOrder']);

/*Route::post('api/web-checkout-orderplace',[
    'as' => 'api/web.checkout.orderplace',
    'uses' => 'App\Http\Controllers\ApiController@OrderPlace',
]);*/
Route::get('api/group-wise-general-page/{group}', [App\Http\Controllers\ApiController::class,'GroupWiseGeneralPage']);
Route::get('api/slug-wise-general-page/{slug}', [App\Http\Controllers\ApiController::class,'SlugWiseGeneralPage']);
Route::get('api/banner/{position}', [App\Http\Controllers\ApiController::class,'Banner']);

//API ROUTE END

//Frontend Data from Api start
Route::get('/all-brand', [App\Http\Controllers\FrontendController::class,'AllBrand']);
Route::get('/deals-hot', [App\Http\Controllers\FrontendController::class,'AllDealsHot']);
Route::get('/best-seller', [App\Http\Controllers\FrontendController::class,'AllTopSeller']);
Route::get('/recommend-product', [App\Http\Controllers\FrontendController::class,'RecommendProduct']);

Route::get('/category-product', [App\Http\Controllers\FrontendController::class,'AllCategoryProduct']);
//Route::get('/category-product1', [App\Http\Controllers\FrontendController::class,'AllCategoryProduct1']);
//Route::get('/category-product2', [App\Http\Controllers\FrontendController::class,'AllCategoryProduct2']);
Route::get('/all-category', [App\Http\Controllers\FrontendController::class,'AllCategory']);
Route::get('/all-category-subcategory', [App\Http\Controllers\FrontendController::class,'AllCategorySubcategory']);
Route::get('/all-product', [App\Http\Controllers\FrontendController::class,'AllProduct']);
Route::get('/all-slider', [App\Http\Controllers\FrontendController::class,'AllSlider']);
Route::get('/all-vendor-limit', [App\Http\Controllers\FrontendController::class,'AllVendorLimit']);


Route::get('single-product-details/{slug}',[
    'as' => 'single.product.details',
    'uses' => 'App\Http\Controllers\FrontendController@SingleProductDeails',
]);

Route::get('brand-wish-product/{slug}',[
    'as' => 'brand.wish.product',
    'uses' => 'App\Http\Controllers\FrontendController@BrandWishProduct',
]);


Route::get('category-wish-product/{slug}',[
    'as' => 'category.wish.product',
    'uses' => 'App\Http\Controllers\FrontendController@CategoryWishProduct',
]);

Route::get('vendor-wish-product/{slug}',[
    'as' => 'vendor.wish.product',
    'uses' => 'App\Http\Controllers\FrontendController@VendorWishProduct',
]);

Route::get('single-vendor-info/{slug}',[
    'as' => 'single.vendor.info',
    'uses' => 'App\Http\Controllers\FrontendController@SingleVendorInfo',
]);

Route::get('group-wise-general-page/{group}',[
    'as' => 'group.wise.general.page',
    'uses' => 'App\Http\Controllers\FrontendController@GroupWiseGeneralPage',
]);

Route::get('slug-wise-general-page/{slug}',[
    'as' => 'slug.wise.general.page',
    'uses' => 'App\Http\Controllers\FrontendController@SlugWiseGeneralPage',
]);

Route::get('view-general-page/{slug}',[
    'as' => 'view.general.page',
    'uses' => 'App\Http\Controllers\FrontendController@ViewGeneralPage',
]);

Route::get('banner/{position}',[
    'as' => 'banner',
    'uses' => 'App\Http\Controllers\FrontendController@Banner',
]);


Route::get('all-size-unit',[
    'as' => 'all.size.unit',
    'uses' => 'App\Http\Controllers\FrontendController@SizeUnit',
]);

Route::get('color',[
    'as' => 'color',
    'uses' => 'App\Http\Controllers\FrontendController@Color',
]);


//Frontend Data from Api END


//Cart route start
Route::get('web-cart-add',[
    'as' => 'web.cart.add',
    'uses' => 'App\Http\Controllers\CartController@ProductAdd',
]);

Route::get('web-cart-remove',[
    'as' => 'web.cart.remove',
    'uses' => 'App\Http\Controllers\CartController@ProductRemove',
]);


Route::get('web-cart-view',[
    'as' => 'web.cart.view',
    'uses' => 'App\Http\Controllers\CartController@MyCartView',
]);

Route::get('web-cart-update',[
    'as' => 'web.cart.update',
    'uses' => 'App\Http\Controllers\CartController@MyCartUpdate',
]);

Route::get('web-cart-clear',[
    'as' => 'web.cart.clear',
    'uses' => 'App\Http\Controllers\CartController@MyCartClear',
]);

//Cart route end


// Checkout route start
Route::get('web-checkout-store',[
    'as' => 'web.checkout.store',
    'uses' => 'App\Http\Controllers\CheckoutController@MyCheckoutStore',
]);


Route::post('web-checkout-orderplace',[
    'as' => 'web.checkout.orderplace',
    'uses' => 'App\Http\Controllers\CheckoutController@OrderPlace',
]);


Route::get('web-orderplace-complete/{id}',[
    'as' => 'web.orderplace.complete',
    'uses' => 'App\Http\Controllers\CheckoutController@OrderPlaceComplete',
]);

Route::post('web-checkout-userlogin',[
    'as' => 'web.checkout.userlogin',
    'uses' => 'App\Http\Controllers\CheckoutController@userlogin',
]);


// Checkout route end

Auth::routes();
Route::get('/admin-dashboard', [App\Http\Controllers\HomeController::class, 'index'])->name('admin-dashboard');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

include 'api.php';
//Route::POST('api/create-order', [App\Http\Controllers\ApiController::class,'CreateOrder']);
Route::POST('api/create-order',[
    'as' => 'api.create-order',
    'uses' => 'App\Http\Controllers\ApiController@CreateOrder',
]);


Route::get('web-customer-acclogin', [
    'as' => 'web.customer.acclogin',
    'uses' => 'App\Modules\Customer\Http\Controllers\CustomerController@WebCustomerLogin',
]);

Route::get('web-customer-forgetpassword', [
    'as' => 'web.customer.forgetpassword',
    'uses' => 'App\Modules\Customer\Http\Controllers\CustomerController@forgetpassword',
]);


Route::POST('web-customer-resetcode',[
    'as' => 'web.customer.resetcode',
    'uses' => 'App\Modules\Customer\Http\Controllers\CustomerController@resetcode',
]);


Route::get('web-register-account',[
    'as' => 'web.register.account',
    'uses' => 'App\Http\Controllers\FrontendController@RegisterAccount',
]);

Route::POST('web-create-account',[
    'as' => 'web.create.account',
    'uses' => 'App\Http\Controllers\FrontendController@CreateAccount',
]);


Route::get('web-product-search',[
    'as' => 'web.product.search',
    'uses' => 'App\Http\Controllers\FrontendController@ProductSearch',
]);


Route::get('web-product-filter',[
    'as' => 'web.product.filter',
    'uses' => 'App\Http\Controllers\FrontendController@ProductFilter',
]);



Route::get('web-quick-view',[
    'as' => 'web.quick.view',
    'uses' => 'App\Http\Controllers\FrontendController@QuickView',
]);



