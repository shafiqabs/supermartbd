-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 13, 2021 at 12:22 PM
-- Server version: 8.0.26-0ubuntu0.20.04.2
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `supermartbd`
--

-- --------------------------------------------------------

--
-- Table structure for table `ems_category`
--

CREATE TABLE `ems_category` (
  `id` bigint UNSIGNED NOT NULL,
  `category_parent_id` bigint UNSIGNED DEFAULT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `feature` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_image` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_category`
--

INSERT INTO `ems_category` (`id`, `category_parent_id`, `name`, `slug`, `content`, `feature`, `category_image`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 4, 'Air Fresheners', 'air-fresheners', 'Air Fresheners', 'yes', 'Air-Fresheners.jpeg', '1', '1', '1', '2021-08-26 01:22:55', '2021-08-29 06:45:46'),
(2, NULL, 'Antiseptics', 'antiseptics', 'Antiseptics', 'yes', 'Antiseptics.jpeg', '1', '1', '1', '2021-08-26 01:25:51', '2021-08-29 06:45:42'),
(3, NULL, 'Baby & Toddler Food', 'baby-&-toddler-food', 'Baby & Toddler Food', NULL, 'Baby-&-Toddler-Food.jpg', '1', '1', '1', '2021-08-26 01:28:34', '2021-08-29 06:45:37'),
(4, 2, 'Bakery Snacks', 'bakery-snacks', 'Bakery Snacks', 'yes', 'Bakery-Snacks.jpg', '1', '1', '1', '2021-08-26 01:29:24', '2021-08-29 06:45:33'),
(5, 4, 'Baking Needs', 'baking-needs', 'Baking Needs', NULL, 'Baking-Needs.jpg', '1', '1', '1', '2021-08-26 01:30:44', '2021-08-29 06:45:29'),
(6, 1, 'Afghanistan', 'afghanistan', NULL, 'yes', 'Afghanistan.jpeg', '1', '1', '1', '2021-08-26 09:11:07', '2021-08-29 06:45:24'),
(7, NULL, 'Information', 'information', 'Information', 'yes', 'Information.png', '1', '1', '1', '2021-08-26 13:13:21', '2021-08-29 06:44:13'),
(8, 8, 'Category', 'category', NULL, NULL, 'Category.jpeg', '1', '1', '1', '2021-08-26 13:21:15', '2021-08-29 06:43:53'),
(10, 7, 'IT Dept', 'it-dept', 'IT Dept', 'yes', 'it-dept.png', '1', '1', NULL, '2021-08-29 09:01:40', '2021-08-29 09:01:40'),
(11, 7, 'Software', 'software', 'Software', 'yes', 'software.jpg', '1', '1', '1', '2021-08-29 09:02:17', '2021-09-09 10:36:32'),
(12, NULL, 'Baby Accessories', 'baby-accessories', NULL, NULL, NULL, '1', '1', NULL, '2021-09-01 05:55:38', '2021-09-01 05:55:38'),
(13, NULL, 'Food', 'food', NULL, NULL, NULL, '1', '1', NULL, '2021-09-01 05:55:38', '2021-09-01 05:55:38'),
(14, NULL, 'Health Care', 'health-care', NULL, NULL, NULL, '1', '1', NULL, '2021-09-01 05:55:38', '2021-09-01 05:55:38'),
(15, NULL, 'Fresh Vegetables', 'fresh-vegetables', NULL, NULL, NULL, '1', '1', NULL, '2021-09-01 05:55:38', '2021-09-01 05:55:38'),
(20, NULL, 'abc', 'abc', NULL, NULL, NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(21, NULL, 'Consumer Electric', 'consumer-electric', 'Consumer Electric', 'yes', 'consumer-electric.jpg', '1', '1', NULL, '2021-09-09 10:40:59', '2021-09-09 10:40:59'),
(22, NULL, 'Home Garden & Kitchen', 'home-garden-&-kitchen', 'Home Garden & Kitchen', 'yes', 'home-garden-&-kitchen.jpg', '1', '1', NULL, '2021-09-09 11:25:27', '2021-09-09 11:25:27');

-- --------------------------------------------------------

--
-- Table structure for table `ems_color`
--

CREATE TABLE `ems_color` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordering` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_color`
--

INSERT INTO `ems_color` (`id`, `name`, `slug`, `ordering`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, 'Red', 'red', '1', '1', '1', '1', '2021-09-06 06:09:41', '2021-09-06 06:09:57'),
(3, 'Black', 'black', '1', '1', '1', NULL, '2021-09-06 06:09:50', '2021-09-06 06:09:50');

-- --------------------------------------------------------

--
-- Table structure for table `ems_country`
--

CREATE TABLE `ems_country` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordering` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_country`
--

INSERT INTO `ems_country` (`id`, `name`, `slug`, `code`, `ordering`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Afghanistan', 'afghanistan', '004', '1', '1', '1', '1', '2021-08-26 07:18:23', '2021-08-29 06:43:38'),
(2, 'Australia', 'australia', '036', '1', '1', '1', '1', '2021-08-26 07:18:42', '2021-08-29 06:43:35'),
(3, 'Austria', 'austria', '040', '1', '1', '1', '1', '2021-08-26 07:18:58', '2021-08-29 06:43:32'),
(4, 'Bangladesh', 'bangladesh', '050', '1', '1', '1', '1', '2021-08-26 07:19:13', '2021-08-29 06:43:29'),
(5, 'Brazil', 'brazil', '076', '2', '1', '1', '1', '2021-08-26 07:19:30', '2021-08-29 06:43:14'),
(6, 'Canada', 'canada', '124', '2', '1', '1', '1', '2021-08-26 07:19:44', '2021-08-29 06:43:11');

-- --------------------------------------------------------

--
-- Table structure for table `ems_discount`
--

CREATE TABLE `ems_discount` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subitem` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_image` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_discount`
--

INSERT INTO `ems_discount` (`id`, `name`, `slug`, `feature`, `subitem`, `amount`, `type`, `discount_image`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(13, 'September Offer(5)%', 'september-offer(5)%', 'yes', 'yes', '5.00', 'Percentage', 'september-offer(5).jpg', '1', '1', '1', '2021-08-31 10:43:04', '2021-08-31 10:44:18'),
(14, 'September Offer(10)%', 'september-offer(10)%', NULL, 'yes', '10.00', 'Percentage', 'september-offer(10).png', '1', '1', '1', '2021-08-31 10:46:51', '2021-08-31 10:48:15');

-- --------------------------------------------------------

--
-- Table structure for table `ems_discount_brand`
--

CREATE TABLE `ems_discount_brand` (
  `id` bigint UNSIGNED NOT NULL,
  `discount_id` bigint UNSIGNED DEFAULT NULL,
  `brand_id` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_discount_brand`
--

INSERT INTO `ems_discount_brand` (`id`, `discount_id`, `brand_id`, `created_at`, `updated_at`) VALUES
(45, 13, 2, '2021-08-31 10:44:49', '2021-08-31 10:44:49'),
(53, 14, 2, '2021-09-06 04:44:56', '2021-09-06 04:44:56'),
(54, 14, 1, '2021-09-06 04:44:56', '2021-09-06 04:44:56'),
(55, 14, 4, '2021-09-06 04:44:56', '2021-09-06 04:44:56');

-- --------------------------------------------------------

--
-- Table structure for table `ems_discount_category`
--

CREATE TABLE `ems_discount_category` (
  `id` bigint UNSIGNED NOT NULL,
  `discount_id` bigint UNSIGNED DEFAULT NULL,
  `category_id` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_discount_category`
--

INSERT INTO `ems_discount_category` (`id`, `discount_id`, `category_id`, `created_at`, `updated_at`) VALUES
(45, 13, 7, '2021-08-31 10:44:49', '2021-08-31 10:44:49'),
(46, 13, 11, '2021-08-31 10:44:49', '2021-08-31 10:44:49'),
(50, 14, 2, '2021-09-06 04:44:56', '2021-09-06 04:44:56');

-- --------------------------------------------------------

--
-- Table structure for table `ems_itemassurance`
--

CREATE TABLE `ems_itemassurance` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordering` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_itemassurance`
--

INSERT INTO `ems_itemassurance` (`id`, `name`, `slug`, `ordering`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(4, '1 Month', '1-month', '1', '1', '1', NULL, '2021-08-29 07:20:57', '2021-08-29 07:20:57'),
(5, '1 Year', '1-year', '1', '1', '1', NULL, '2021-08-29 07:21:15', '2021-08-29 07:21:15'),
(6, '1 Year 3 Month', '1-year-3-month', '2', '1', '1', NULL, '2021-08-29 07:21:46', '2021-08-29 07:21:46'),
(7, '1 Year 4 Month', '1-year-4-month', '1', '1', '1', NULL, '2021-08-29 07:22:01', '2021-08-29 07:22:01');

-- --------------------------------------------------------

--
-- Table structure for table `ems_itemunit`
--

CREATE TABLE `ems_itemunit` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordering` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_itemunit`
--

INSERT INTO `ems_itemunit` (`id`, `name`, `slug`, `ordering`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Bag', 'bag', '1', '1', '1', '1', '2021-08-26 06:45:09', '2021-08-29 06:41:26'),
(3, 'Can', 'can', '2', '1', '1', '1', '2021-08-26 06:45:33', '2021-08-29 06:41:21'),
(4, 'Carton', 'carton', '1', '1', '1', '1', '2021-08-26 06:45:45', '2021-08-29 06:41:18'),
(5, 'Coil', 'coil', '2', '1', '1', '1', '2021-08-26 06:45:59', '2021-08-29 06:41:15'),
(10, 'pc', 'pc', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(11, 'pcs', 'pcs', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(12, 'ml', 'ml', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(13, 'ltr', 'ltr', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(14, 'gm', 'gm', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16');

-- --------------------------------------------------------

--
-- Table structure for table `ems_product`
--

CREATE TABLE `ems_product` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_bn` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_item` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_id` bigint UNSIGNED DEFAULT NULL,
  `category_id` bigint UNSIGNED DEFAULT NULL,
  `item_unit_id` bigint UNSIGNED DEFAULT NULL,
  `size_unit_id` bigint UNSIGNED DEFAULT NULL,
  `size_weight_dimen_id` bigint UNSIGNED DEFAULT NULL,
  `country_id` bigint UNSIGNED DEFAULT NULL,
  `discount_id` bigint UNSIGNED DEFAULT NULL,
  `warning_id` bigint UNSIGNED DEFAULT NULL,
  `item_assurance_id` bigint UNSIGNED DEFAULT NULL,
  `promotion_id` bigint UNSIGNED DEFAULT NULL,
  `quantity` bigint DEFAULT NULL,
  `minquantity` bigint DEFAULT NULL,
  `maxquantity` bigint DEFAULT NULL,
  `salesprice` decimal(10,2) DEFAULT NULL,
  `purchaseprice` decimal(10,2) DEFAULT NULL,
  `discription` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_tag` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_product`
--

INSERT INTO `ems_product` (`id`, `name`, `name_bn`, `slug`, `sub_item`, `brand_id`, `category_id`, `item_unit_id`, `size_unit_id`, `size_weight_dimen_id`, `country_id`, `discount_id`, `warning_id`, `item_assurance_id`, `promotion_id`, `quantity`, `minquantity`, `maxquantity`, `salesprice`, `purchaseprice`, `discription`, `feature_image`, `feature_tag`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Mini Wireless Earphone', 'Product Name (Bangla)', 'mini-wireless-earphone', 'yes', 2, 3, 4, 2, 2, 5, 13, 4, 6, 1, 500, 30, 5000, '200.00', '100.00', 'Description', 'mini-wireless-earphone.jpg', 'dealshot', '1', '1', '1', '2021-09-06 09:53:50', '2021-09-09 07:32:46'),
(3, 'Product Name', 'Product Name (Bangla)', 'product-name', 'yes', 2, 4, 1, 2, 1, 4, 13, 4, 4, 1, 500, 30, 5000, '200.00', '100.00', 'Description', 'product-name.jpg', 'dealshot', '1', '1', '1', '2021-09-06 10:24:35', '2021-09-13 11:10:32'),
(5, 'Odonil Air Freshener Block Jasmine Mist', NULL, 'odonil-air-freshener-block-jasmine-mist', NULL, 2, 1, 10, 9, NULL, 2, NULL, NULL, NULL, NULL, 1000, 50, 2500, '300.00', '260.00', NULL, 'odonil-air-freshener-block-jasmine-mist.jpeg', 'recommend', '1', NULL, '1', '2021-09-06 13:24:16', '2021-09-09 09:09:32'),
(6, 'ACI Savlon Liquid Antiseptic', NULL, 'aci-savlon-liquid-antiseptic', NULL, 25, 2, 11, 10, NULL, 2, NULL, NULL, NULL, NULL, 1500, 50, 2500, '100.00', '60.00', NULL, 'aci-savlon-liquid-antiseptic.png', 'bestseller', '1', NULL, '1', '2021-09-06 13:24:16', '2021-09-09 09:09:13'),
(7, 'Pur Feeding Bottle', NULL, 'pur-feeding-bottle', NULL, 26, 11, 12, 11, NULL, 3, NULL, NULL, NULL, NULL, 2000, 50, 2500, '120.00', '80.00', NULL, 'pur-feeding-bottle.jpg', 'bestseller', '1', NULL, '1', '2021-09-06 13:24:16', '2021-09-09 10:33:30'),
(8, 'Whiskas Adult Cat Food Tuna', NULL, 'whiskas-adult-cat-food-tuna', NULL, 1, 11, 13, 12, 1, 2, 13, 4, 5, 1, 500, 50, 2500, '300.00', '220.00', NULL, 'whiskas-adult-cat-food-tuna.jpg', 'recommend', '1', NULL, '1', '2021-09-06 13:24:16', '2021-09-09 10:33:12'),
(9, 'Red Cap Sound Marker', 'Product Name (Bangla)', 'red-cap-sound-marker', 'yes', 4, 11, 14, 13, 3, 4, 13, 4, 5, 1, 2000, 50, 2500, '700.00', '550.00', NULL, 'red-cap-sound-marker.jpg', 'dealshot', '1', NULL, '1', '2021-09-06 13:24:16', '2021-09-09 09:54:32'),
(10, 'Badhacopi Pata', NULL, 'badhacopi-pata', NULL, 4, 11, 14, 14, NULL, 4, NULL, NULL, NULL, NULL, 300, 50, 2500, '130.00', '100.00', NULL, 'badhacopi-pata.jpg', 'bestseller', '1', NULL, '1', '2021-09-06 13:24:16', '2021-09-09 10:32:40'),
(12, 'Odonil Air Freshener Block Jasmine Mist1', NULL, 'odonil-air-freshener-block-jasmine-mist1', NULL, 2, 21, 10, 16, 2, 4, NULL, NULL, NULL, NULL, 1000, 50, 2500, '300.00', '260.00', NULL, 'odonil-air-freshener-block-jasmine-mist1.jpg', 'recommend', '1', NULL, '1', '2021-09-09 10:42:31', '2021-09-09 10:59:53'),
(13, 'ACI Savlon Liquid Antiseptic1', NULL, 'aci-savlon-liquid-antiseptic1', NULL, 25, 21, 11, 17, 2, 1, NULL, NULL, NULL, NULL, 1500, 50, 2500, '100.00', '60.00', NULL, 'aci-savlon-liquid-antiseptic1.jpg', 'bestseller', '1', NULL, '1', '2021-09-09 10:42:31', '2021-09-09 10:59:30'),
(14, 'Bluetooth Music Recorder', NULL, 'bluetooth-music-recorder', 'yes', 26, 21, 12, 18, 2, 4, NULL, NULL, NULL, NULL, 2000, 50, 2500, '120.00', '80.00', NULL, 'bluetooth-music-recorder.jpg', 'bestseller', '1', NULL, '1', '2021-09-09 10:42:31', '2021-09-09 10:48:28'),
(15, 'Magenetic Charge Box', NULL, 'magenetic-charge-box', NULL, 1, 21, 13, 19, 1, 2, NULL, NULL, NULL, NULL, 500, 50, 2500, '300.00', '220.00', NULL, 'magenetic-charge-box.jpg', 'bestseller', '1', NULL, '1', '2021-09-09 10:42:31', '2021-09-09 10:47:13'),
(16, 'Bright Light', NULL, 'bright-light', NULL, 4, 21, 14, 20, 2, 2, NULL, NULL, NULL, NULL, 2000, 50, 2500, '700.00', '550.00', NULL, 'bright-light.jpg', 'bestseller', '1', NULL, '1', '2021-09-09 10:42:31', '2021-09-09 10:45:59'),
(17, 'Men\'s Black Watch', NULL, 'men\'s-black-watch', 'yes', 4, 21, 14, 21, 1, 4, 13, 4, 4, 1, 300, 50, 2500, '130.00', '100.00', NULL, 'men\'s-black-watch.jpg', 'recommend', '1', NULL, '1', '2021-09-09 10:42:31', '2021-09-09 10:44:45'),
(18, 'Rotate Crusher', NULL, 'rotate-crusher', NULL, 2, 22, 10, 22, 3, 5, NULL, NULL, NULL, NULL, 1000, 50, 2500, '300.00', '260.00', NULL, 'rotate-crusher.jpg', 'bestseller', '1', NULL, '1', '2021-09-09 11:29:08', '2021-09-09 11:32:33'),
(19, 'Colorful Flavoring Box', NULL, 'colorful-flavoring-box', NULL, 25, 22, 11, 23, NULL, 3, NULL, NULL, NULL, NULL, 1500, 50, 2500, '100.00', '60.00', NULL, 'colorful-flavoring-box.jpg', 'bestseller', '1', NULL, '1', '2021-09-09 11:29:08', '2021-09-09 11:31:18'),
(20, 'Rice Cooker', NULL, 'rice-cooker', NULL, 26, 22, 12, 24, 2, 5, NULL, NULL, NULL, NULL, 2000, 50, 2500, '120.00', '80.00', NULL, 'rice-cooker.jpg', 'recommend', '1', NULL, '1', '2021-09-09 11:29:08', '2021-09-09 11:30:54'),
(21, 'Black Crusher', NULL, 'black-crusher', 'yes', 1, 22, 13, 25, 1, 4, NULL, NULL, NULL, NULL, 500, 50, 2500, '300.00', '220.00', NULL, 'black-crusher.jpg', 'bestseller', '1', NULL, '1', '2021-09-09 11:29:08', '2021-09-09 11:30:08'),
(22, 'Coat Pool  Comfort Jacket', 'Coat Pool  Comfort Jacket', 'coat-pool--comfort-jacket', 'yes', 26, 2, 1, 3, 2, 4, 13, 4, 5, 1, 500, 30, 5000, '300.00', '100.00', NULL, 'coat-pool--comfort-jacket.jpg', 'recommend', '1', '1', NULL, '2021-09-13 10:22:10', '2021-09-13 10:22:10'),
(23, 'Beyond OTP Shirt', 'Beyond OTP Shirt', 'beyond-otp-shirt', 'yes', 1, 11, 4, 4, 3, 4, 13, 4, 4, 1, 500, 30, 5000, '300.00', '100.00', NULL, 'beyond-otp-shirt.jpg', 'recommend', '1', '1', '1', '2021-09-13 10:27:04', '2021-09-13 10:28:55');

-- --------------------------------------------------------

--
-- Table structure for table `ems_product_additionalfield`
--

CREATE TABLE `ems_product_additionalfield` (
  `id` bigint UNSIGNED NOT NULL,
  `product_id` bigint UNSIGNED DEFAULT NULL,
  `level_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level_value` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_product_additionalfield`
--

INSERT INTO `ems_product_additionalfield` (`id`, `product_id`, `level_name`, `level_value`, `created_at`, `updated_at`) VALUES
(4, 3, 'Additional Field 1', 'Additional  Value 1', '2021-09-07 07:26:54', '2021-09-07 07:26:54'),
(6, 3, 'Additional Field 3', 'Additional  Value 3', '2021-09-07 07:26:54', '2021-09-07 07:26:54'),
(16, 10, 'Additional Field 1', 'Additional  Value 1', '2021-09-07 07:51:21', '2021-09-07 07:51:21');

-- --------------------------------------------------------

--
-- Table structure for table `ems_product_brand`
--

CREATE TABLE `ems_product_brand` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_image` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_product_brand`
--

INSERT INTO `ems_product_brand` (`id`, `name`, `slug`, `content`, `feature`, `brand_image`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Fresh', 'fresh', 'Fresh', 'yes', 'fresh.jpg', '1', '1', '1', '2021-08-26 06:52:11', '2021-09-09 07:06:35'),
(2, 'Basudhara', 'basudhara', 'Basudhara', 'yes', 'basudhara.jpg', '1', '1', '1', '2021-08-26 06:52:59', '2021-09-09 07:06:18'),
(4, 'Radhuni', 'radhuni', 'Radhuni', NULL, 'radhuni.jpg', '1', '1', '1', '2021-08-26 07:02:08', '2021-09-09 07:06:01'),
(25, 'ACI', 'aci', NULL, NULL, 'aci.jpg', '1', '1', '1', '2021-09-06 13:24:16', '2021-09-09 07:05:51'),
(26, 'Unilever', 'unilever', NULL, NULL, 'unilever.jpg', '1', '1', '1', '2021-09-06 13:24:16', '2021-09-09 07:05:34'),
(28, 'Jumuna', 'jumuna', 'Jumuna', 'yes', 'jumuna.jpg', '1', '1', NULL, '2021-09-09 07:06:56', '2021-09-09 07:06:56');

-- --------------------------------------------------------

--
-- Table structure for table `ems_product_colors`
--

CREATE TABLE `ems_product_colors` (
  `id` bigint UNSIGNED NOT NULL,
  `product_id` bigint UNSIGNED DEFAULT NULL,
  `color_id` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_product_colors`
--

INSERT INTO `ems_product_colors` (`id`, `product_id`, `color_id`, `created_at`, `updated_at`) VALUES
(76, 1, 2, '2021-09-09 07:32:46', '2021-09-09 07:32:46'),
(77, 1, 3, '2021-09-09 07:32:46', '2021-09-09 07:32:46'),
(80, 9, 2, '2021-09-09 09:54:32', '2021-09-09 09:54:32'),
(81, 9, 3, '2021-09-09 09:54:32', '2021-09-09 09:54:32'),
(83, 17, 3, '2021-09-13 10:14:22', '2021-09-13 10:14:22'),
(86, 22, 2, '2021-09-13 10:22:57', '2021-09-13 10:22:57'),
(87, 22, 3, '2021-09-13 10:22:57', '2021-09-13 10:22:57'),
(92, 23, 2, '2021-09-13 10:28:55', '2021-09-13 10:28:55'),
(93, 23, 3, '2021-09-13 10:28:55', '2021-09-13 10:28:55'),
(94, 3, 2, '2021-09-13 11:10:32', '2021-09-13 11:10:32'),
(95, 3, 3, '2021-09-13 11:10:32', '2021-09-13 11:10:32');

-- --------------------------------------------------------

--
-- Table structure for table `ems_product_image`
--

CREATE TABLE `ems_product_image` (
  `id` bigint UNSIGNED NOT NULL,
  `product_id` bigint UNSIGNED DEFAULT NULL,
  `img_level` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attach_link` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_product_image`
--

INSERT INTO `ems_product_image` (`id`, `product_id`, `img_level`, `attach_link`, `created_at`, `updated_at`) VALUES
(30, 10, 'Product Image', 'img51681-1631001081.png', '2021-09-07 07:51:21', '2021-09-07 07:51:21'),
(31, 9, 'Red Cap Sound Marker 1', 'img72781-1631172499.jpg', '2021-09-09 07:28:19', '2021-09-09 07:28:19'),
(32, 9, 'Red Cap Sound Marker 2', 'img80622-1631172499.jpg', '2021-09-09 07:28:19', '2021-09-09 07:28:19'),
(33, 9, 'Red Cap Sound Marker 3', 'img92673-1631172499.jpg', '2021-09-09 07:28:19', '2021-09-09 07:28:19'),
(34, 9, 'Red Cap Sound Marker 4', 'img27434-1631172499.jpg', '2021-09-09 07:28:19', '2021-09-09 07:28:19'),
(35, 1, 'Mini Wireless Earphone 1', 'img96411-1631172766.jpg', '2021-09-09 07:32:46', '2021-09-09 07:32:46'),
(36, 1, 'Mini Wireless Earphone 2', 'img44212-1631172766.jpg', '2021-09-09 07:32:46', '2021-09-09 07:32:46'),
(37, 1, 'Mini Wireless Earphone 3', 'img20093-1631172766.jpg', '2021-09-09 07:32:46', '2021-09-09 07:32:46'),
(38, 1, 'Mini Wireless Earphone 4', 'img46724-1631172766.jpg', '2021-09-09 07:32:46', '2021-09-09 07:32:46'),
(39, 17, 'Product Image 1', 'img49841-1631528062.jpg', '2021-09-13 10:14:22', '2021-09-13 10:14:22'),
(40, 17, 'Product Image 2', 'img93922-1631528062.jpg', '2021-09-13 10:14:22', '2021-09-13 10:14:22'),
(41, 17, 'Product Image 3', 'img57703-1631528062.jpg', '2021-09-13 10:14:22', '2021-09-13 10:14:22'),
(42, 17, 'Product Image 4', 'img58454-1631528062.jpg', '2021-09-13 10:14:22', '2021-09-13 10:14:22'),
(43, 22, 'Product Image 1', 'img59131-1631528577.jpg', '2021-09-13 10:22:57', '2021-09-13 10:22:57'),
(44, 22, 'Product Image 2', 'img62502-1631528577.jpg', '2021-09-13 10:22:57', '2021-09-13 10:22:57'),
(45, 22, 'Product Image 3', 'img26143-1631528577.jpg', '2021-09-13 10:22:57', '2021-09-13 10:22:57'),
(46, 22, 'Product Image 4', 'img51054-1631528577.jpg', '2021-09-13 10:22:57', '2021-09-13 10:22:57'),
(47, 23, 'Beyond OTP Shirt 1', 'img30941-1631528875.jpg', '2021-09-13 10:27:55', '2021-09-13 10:27:55'),
(48, 23, 'Beyond OTP Shirt 2', 'img27102-1631528875.jpg', '2021-09-13 10:27:55', '2021-09-13 10:27:55'),
(49, 23, 'Beyond OTP Shirt 3', 'img65303-1631528875.jpg', '2021-09-13 10:27:55', '2021-09-13 10:27:55'),
(50, 23, 'Beyond OTP Shirt 4', 'img78194-1631528875.jpg', '2021-09-13 10:27:55', '2021-09-13 10:27:55'),
(51, 8, 'Whiskas Adult Cat Food Tuna 1', 'img40531-1631529266.jpg', '2021-09-13 10:34:26', '2021-09-13 10:34:26'),
(52, 8, 'Whiskas Adult Cat Food Tuna 2', 'img19882-1631529266.jpg', '2021-09-13 10:34:26', '2021-09-13 10:34:26'),
(53, 8, 'Whiskas Adult Cat Food Tuna 3', 'img81373-1631529266.jpg', '2021-09-13 10:34:26', '2021-09-13 10:34:26'),
(54, 8, 'Whiskas Adult Cat Food Tuna 4', 'img18744-1631529266.jpg', '2021-09-13 10:34:26', '2021-09-13 10:34:26'),
(55, 3, 'Product Image 1', 'img62571-1631531432.jpg', '2021-09-13 11:10:32', '2021-09-13 11:10:32'),
(56, 3, 'Product Image 2', 'img42062-1631531432.jpg', '2021-09-13 11:10:32', '2021-09-13 11:10:32'),
(57, 3, 'Product Image 3', 'img42283-1631531432.jpg', '2021-09-13 11:10:32', '2021-09-13 11:10:32'),
(58, 3, 'Product Image 4', 'img21674-1631531432.jpg', '2021-09-13 11:10:32', '2021-09-13 11:10:32');

-- --------------------------------------------------------

--
-- Table structure for table `ems_product_tags`
--

CREATE TABLE `ems_product_tags` (
  `id` bigint UNSIGNED NOT NULL,
  `product_id` bigint UNSIGNED DEFAULT NULL,
  `tag_id` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_product_tags`
--

INSERT INTO `ems_product_tags` (`id`, `product_id`, `tag_id`, `created_at`, `updated_at`) VALUES
(43, 1, 2, '2021-09-09 07:32:46', '2021-09-09 07:32:46'),
(45, 9, 1, '2021-09-09 09:54:32', '2021-09-09 09:54:32'),
(46, 9, 2, '2021-09-09 09:54:32', '2021-09-09 09:54:32'),
(48, 22, 2, '2021-09-13 10:22:57', '2021-09-13 10:22:57'),
(53, 23, 1, '2021-09-13 10:28:55', '2021-09-13 10:28:55'),
(54, 23, 2, '2021-09-13 10:28:55', '2021-09-13 10:28:55'),
(55, 3, 2, '2021-09-13 11:10:32', '2021-09-13 11:10:32');

-- --------------------------------------------------------

--
-- Table structure for table `ems_promotion`
--

CREATE TABLE `ems_promotion` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promotion` varchar(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promotion_image` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_promotion`
--

INSERT INTO `ems_promotion` (`id`, `name`, `slug`, `feature`, `tag`, `promotion`, `promotion_image`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Eid Essential Bazar Promotion', 'eid-essential-bazar-promotion', 'yes', 'yes', 'yes', 'Eid-Essential-Bazar-Promotion.jpeg', '1', '1', '1', '2021-08-26 07:11:48', '2021-08-29 06:42:55'),
(2, 'End December', 'end-december', NULL, 'yes', NULL, 'End-December.jpeg', '1', '1', '1', '2021-08-26 07:14:06', '2021-08-29 06:42:50');

-- --------------------------------------------------------

--
-- Table structure for table `ems_sizeunit`
--

CREATE TABLE `ems_sizeunit` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordering` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_sizeunit`
--

INSERT INTO `ems_sizeunit` (`id`, `name`, `slug`, `ordering`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Bag', 'bag', '1', '1', '1', '1', '2021-08-26 06:47:19', '2021-08-29 06:41:46'),
(2, 'Box', 'box', '1', '1', '1', '1', '2021-08-26 06:47:24', '2021-08-29 06:41:43'),
(3, 'Coil', 'coil', '2', '1', '1', '1', '2021-08-26 06:47:33', '2021-08-29 06:41:39'),
(4, 'Can', 'can', '2', '1', '1', '1', '2021-08-26 06:47:39', '2021-08-29 06:41:37'),
(5, 'Carton', 'carton', '1', '1', '1', '1', '2021-08-26 06:47:44', '2021-08-29 06:41:34'),
(9, 'S', 's', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(10, 'M', 'm', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(11, 'L', 'l', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(12, 'XL', 'xl', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(13, 'XLL', 'xll', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(14, '30', '30', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(15, NULL, '', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(16, 'S', 's', NULL, '1', '1', NULL, '2021-09-09 10:42:31', '2021-09-09 10:42:31'),
(17, 'M', 'm', NULL, '1', '1', NULL, '2021-09-09 10:42:31', '2021-09-09 10:42:31'),
(18, 'L', 'l', NULL, '1', '1', NULL, '2021-09-09 10:42:31', '2021-09-09 10:42:31'),
(19, 'XL', 'xl', NULL, '1', '1', NULL, '2021-09-09 10:42:31', '2021-09-09 10:42:31'),
(20, 'XLL', 'xll', NULL, '1', '1', NULL, '2021-09-09 10:42:31', '2021-09-09 10:42:31'),
(21, '30', '30', NULL, '1', '1', NULL, '2021-09-09 10:42:31', '2021-09-09 10:42:31'),
(22, 'S', 's', NULL, '1', '1', NULL, '2021-09-09 11:29:08', '2021-09-09 11:29:08'),
(23, 'M', 'm', NULL, '1', '1', NULL, '2021-09-09 11:29:08', '2021-09-09 11:29:08'),
(24, 'L', 'l', NULL, '1', '1', NULL, '2021-09-09 11:29:08', '2021-09-09 11:29:08'),
(25, 'XL', 'xl', NULL, '1', '1', NULL, '2021-09-09 11:29:08', '2021-09-09 11:29:08');

-- --------------------------------------------------------

--
-- Table structure for table `ems_sizeweightdimension`
--

CREATE TABLE `ems_sizeweightdimension` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordering` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_sizeweightdimension`
--

INSERT INTO `ems_sizeweightdimension` (`id`, `name`, `slug`, `ordering`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'MM', 'mm', '1', '1', '1', '1', '2021-08-26 06:48:10', '2021-08-29 06:42:11'),
(2, 'Min', 'min', '1', '1', '1', '1', '2021-08-26 06:48:15', '2021-08-29 06:42:06'),
(3, 'Max', 'max', '1', '1', '1', '1', '2021-08-26 06:48:23', '2021-08-29 06:42:03'),
(4, 'XL', 'xl', '2', '1', '1', '1', '2021-08-26 06:48:29', '2021-08-29 06:42:00'),
(5, 'XLL', 'xll', '2', '1', '1', '1', '2021-08-26 06:48:34', '2021-08-29 06:41:57');

-- --------------------------------------------------------

--
-- Table structure for table `ems_warningtype`
--

CREATE TABLE `ems_warningtype` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordering` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_warningtype`
--

INSERT INTO `ems_warningtype` (`id`, `name`, `slug`, `ordering`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(4, 'Warranty', 'warranty', '1', '1', '1', NULL, '2021-08-29 06:36:45', '2021-08-29 06:36:45'),
(5, 'Guarantee', 'guarantee', '1', '1', '1', NULL, '2021-08-29 06:36:59', '2021-08-29 06:36:59');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fileimport`
--

CREATE TABLE `fileimport` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(3, '2021_08_09_063929_create_fileimport_tbl', 2),
(4, '2014_10_12_100000_create_password_resets_table', 3),
(5, '2019_08_19_000000_create_failed_jobs_table', 3),
(6, '2019_12_14_000001_create_personal_access_tokens_table', 3),
(19, '2021_08_23_125700_create_ems_itemunit_table', 4),
(20, '2021_08_23_165337_create_ems_sizeunit_table', 4),
(21, '2021_08_23_173606_create_ems_sizeweightdimension_table', 4),
(22, '2021_08_24_144931_create_ems_product_brand_table', 4),
(23, '2021_08_24_182255_create_ems_promotion_table', 4),
(24, '2021_08_25_114852_create_ems_country_table', 4),
(26, '2021_08_25_152302_create_ems_category_table', 5),
(28, '2021_08_29_122030_create_ems_warningtype_table', 7),
(30, '2021_08_29_130124_create_ems_itemasurance_table', 8),
(31, '2021_08_29_155602_create_ems_discount_table', 9),
(33, '2021_08_29_165805_create_ems_discount_brand_table', 10),
(34, '2021_08_31_103217_create_ems_discount_category_table', 11),
(37, '2021_09_06_110157_create_ems_color_table', 13),
(39, '2021_08_26_162842_create_ems_product_table', 14),
(40, '2021_09_06_161102_create_ems_product_colors_table', 15),
(42, '2021_09_06_161154_create_ems_product_tags_table', 16),
(43, '2021_09_07_111943_create_ems_product_image_table', 17),
(44, '2021_09_07_125356_create_ems_product_additionalfield_table', 18);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `image_link`, `type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Rashedul Raju', 'rbraju3m@gmail.com', NULL, '$2y$10$Y9OFbY3cAmjTkF1Z0RZJi.xXpc/Q.ULemDkCOgXwSFl1/4Tt1jpkK', 'logo.png', 'Admin', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ems_category`
--
ALTER TABLE `ems_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_category_category_parent_id_foreign` (`category_parent_id`);

--
-- Indexes for table `ems_color`
--
ALTER TABLE `ems_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_country`
--
ALTER TABLE `ems_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_discount`
--
ALTER TABLE `ems_discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_discount_brand`
--
ALTER TABLE `ems_discount_brand`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_discount_brand_discount_id_foreign` (`discount_id`),
  ADD KEY `ems_discount_brand_brand_id_foreign` (`brand_id`);

--
-- Indexes for table `ems_discount_category`
--
ALTER TABLE `ems_discount_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_discount_category_discount_id_foreign` (`discount_id`),
  ADD KEY `ems_discount_category_category_id_foreign` (`category_id`);

--
-- Indexes for table `ems_itemassurance`
--
ALTER TABLE `ems_itemassurance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_itemunit`
--
ALTER TABLE `ems_itemunit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_product`
--
ALTER TABLE `ems_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_product_brand_id_foreign` (`brand_id`),
  ADD KEY `ems_product_category_id_foreign` (`category_id`),
  ADD KEY `ems_product_item_unit_id_foreign` (`item_unit_id`),
  ADD KEY `ems_product_size_unit_id_foreign` (`size_unit_id`),
  ADD KEY `ems_product_size_weight_dimen_id_foreign` (`size_weight_dimen_id`),
  ADD KEY `ems_product_country_id_foreign` (`country_id`),
  ADD KEY `ems_product_discount_id_foreign` (`discount_id`),
  ADD KEY `ems_product_warning_id_foreign` (`warning_id`),
  ADD KEY `ems_product_item_assurance_id_foreign` (`item_assurance_id`),
  ADD KEY `ems_product_promotion_id_foreign` (`promotion_id`);

--
-- Indexes for table `ems_product_additionalfield`
--
ALTER TABLE `ems_product_additionalfield`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_product_additionalfield_product_id_foreign` (`product_id`);

--
-- Indexes for table `ems_product_brand`
--
ALTER TABLE `ems_product_brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_product_colors`
--
ALTER TABLE `ems_product_colors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_product_colors_product_id_foreign` (`product_id`),
  ADD KEY `ems_product_colors_color_id_foreign` (`color_id`);

--
-- Indexes for table `ems_product_image`
--
ALTER TABLE `ems_product_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_product_image_product_id_foreign` (`product_id`);

--
-- Indexes for table `ems_product_tags`
--
ALTER TABLE `ems_product_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_product_tags_product_id_foreign` (`product_id`),
  ADD KEY `ems_product_tags_tag_id_foreign` (`tag_id`);

--
-- Indexes for table `ems_promotion`
--
ALTER TABLE `ems_promotion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_sizeunit`
--
ALTER TABLE `ems_sizeunit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_sizeweightdimension`
--
ALTER TABLE `ems_sizeweightdimension`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_warningtype`
--
ALTER TABLE `ems_warningtype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `fileimport`
--
ALTER TABLE `fileimport`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ems_category`
--
ALTER TABLE `ems_category`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `ems_color`
--
ALTER TABLE `ems_color`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ems_country`
--
ALTER TABLE `ems_country`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `ems_discount`
--
ALTER TABLE `ems_discount`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `ems_discount_brand`
--
ALTER TABLE `ems_discount_brand`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `ems_discount_category`
--
ALTER TABLE `ems_discount_category`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `ems_itemassurance`
--
ALTER TABLE `ems_itemassurance`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ems_itemunit`
--
ALTER TABLE `ems_itemunit`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `ems_product`
--
ALTER TABLE `ems_product`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `ems_product_additionalfield`
--
ALTER TABLE `ems_product_additionalfield`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `ems_product_brand`
--
ALTER TABLE `ems_product_brand`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `ems_product_colors`
--
ALTER TABLE `ems_product_colors`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `ems_product_image`
--
ALTER TABLE `ems_product_image`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `ems_product_tags`
--
ALTER TABLE `ems_product_tags`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `ems_promotion`
--
ALTER TABLE `ems_promotion`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ems_sizeunit`
--
ALTER TABLE `ems_sizeunit`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `ems_sizeweightdimension`
--
ALTER TABLE `ems_sizeweightdimension`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ems_warningtype`
--
ALTER TABLE `ems_warningtype`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fileimport`
--
ALTER TABLE `fileimport`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=282;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ems_category`
--
ALTER TABLE `ems_category`
  ADD CONSTRAINT `ems_category_category_parent_id_foreign` FOREIGN KEY (`category_parent_id`) REFERENCES `ems_category` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ems_discount_brand`
--
ALTER TABLE `ems_discount_brand`
  ADD CONSTRAINT `ems_discount_brand_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `ems_product_brand` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_discount_brand_discount_id_foreign` FOREIGN KEY (`discount_id`) REFERENCES `ems_discount` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ems_discount_category`
--
ALTER TABLE `ems_discount_category`
  ADD CONSTRAINT `ems_discount_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `ems_category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_discount_category_discount_id_foreign` FOREIGN KEY (`discount_id`) REFERENCES `ems_discount` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ems_product`
--
ALTER TABLE `ems_product`
  ADD CONSTRAINT `ems_product_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `ems_product_brand` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `ems_category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `ems_country` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_discount_id_foreign` FOREIGN KEY (`discount_id`) REFERENCES `ems_discount` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_item_assurance_id_foreign` FOREIGN KEY (`item_assurance_id`) REFERENCES `ems_itemassurance` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_item_unit_id_foreign` FOREIGN KEY (`item_unit_id`) REFERENCES `ems_itemunit` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_promotion_id_foreign` FOREIGN KEY (`promotion_id`) REFERENCES `ems_promotion` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_size_unit_id_foreign` FOREIGN KEY (`size_unit_id`) REFERENCES `ems_sizeunit` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_size_weight_dimen_id_foreign` FOREIGN KEY (`size_weight_dimen_id`) REFERENCES `ems_sizeweightdimension` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_warning_id_foreign` FOREIGN KEY (`warning_id`) REFERENCES `ems_warningtype` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ems_product_additionalfield`
--
ALTER TABLE `ems_product_additionalfield`
  ADD CONSTRAINT `ems_product_additionalfield_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ems_product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ems_product_colors`
--
ALTER TABLE `ems_product_colors`
  ADD CONSTRAINT `ems_product_colors_color_id_foreign` FOREIGN KEY (`color_id`) REFERENCES `ems_color` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_colors_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ems_product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ems_product_image`
--
ALTER TABLE `ems_product_image`
  ADD CONSTRAINT `ems_product_image_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ems_product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ems_product_tags`
--
ALTER TABLE `ems_product_tags`
  ADD CONSTRAINT `ems_product_tags_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ems_product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_tags_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `ems_promotion` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
