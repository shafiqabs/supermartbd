<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmsDiscountCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ems_discount_category', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('discount_id')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();

            $table->timestamps();

            $table->foreign('discount_id')
                ->references('id')->on('ems_discount')
                ->onDelete('cascade');

            $table->foreign('category_id')
                ->references('id')->on('ems_category')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ems_discount_category');
    }
}
