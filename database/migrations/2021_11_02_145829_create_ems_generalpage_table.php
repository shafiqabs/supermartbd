<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmsGeneralpageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ems_generalpage', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('group',20)->nullable();
            $table->string('title',150)->nullable();
            $table->string('slug',150)->nullable();
            $table->string('content',1500)->nullable();

            $table->enum('status',array('1','0'))->nullable()->comment('active = 1, inactive = 0');

            $table->string('created_by',10)->nullable();
            $table->string('updated_by',10)->nullable();
            $table->timestamps();
            $table->engine= 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ems_generalpage');
    }
}
