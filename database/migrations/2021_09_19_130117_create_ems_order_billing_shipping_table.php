<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmsOrderBillingShippingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ems_order_billing_shipping', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('order_id')->nullable();
            $table->unsignedBigInteger('customer_id')->nullable();

            $table->string('type',10)->nullable();
            $table->string('firstname',30)->nullable();
            $table->string('lastname',30)->nullable();
            $table->string('country',25)->nullable();
            $table->string('district',25)->nullable();
            $table->string('address',50)->nullable();
            $table->string('phone',15)->nullable();
            $table->string('email',50)->nullable();

            $table->string('created_by',10)->nullable();
            $table->string('updated_by',10)->nullable();



            $table->timestamps();

            $table->foreign('order_id')
                ->references('id')->on('ems_order_head')
                ->onDelete('cascade');

            $table->foreign('customer_id')
                ->references('id')->on('ems_customer')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ems_order_billing_shipping');
    }
}
