<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmsDeliveryTimeSlotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ems_delivery_time_slot', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->string('time_slot',20)->nullable();
            $table->integer('ordering')->nullable();
            $table->enum('status',array('1','0'))->nullable()->comment('1 = Actine, 2 = Inactive');

            $table->string('created_by',10)->nullable();
            $table->string('updated_by',10)->nullable();
            $table->timestamps();
            $table->engine= 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ems_delivery_time_slot');
    }
}
