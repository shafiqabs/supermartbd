<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmsGeneralSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ems_general_setting', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('slider_limit')->nullable();
            $table->tinyInteger('hp_banner_limit')->nullable();
            $table->tinyInteger('service_limit')->nullable();
            $table->tinyInteger('hp_brand_limit')->nullable();
            $table->tinyInteger('hp_category_product_limit')->nullable();
            $table->tinyInteger('hp_top_weakly_vendor_limit')->nullable();
            $table->tinyInteger('best_seller_product_limit')->nullable();
            $table->tinyInteger('deals_hot_product_limit')->nullable();
            $table->tinyInteger('recommend_product_limit')->nullable();


            $table->enum('best_seller_random_order',array('1','0'))->nullable()->comment('In Random Order = 1, Not Random Order = 0');
            $table->enum('hot_deals_random_order',array('1','0'))->nullable()->comment('In Random Order = 1, Not Random Order = 0');
            $table->enum('recommend_random_order',array('1','0'))->nullable()->comment('In Random Order = 1, Not Random Order = 0');

            $table->enum('status',array('1','0'))->nullable()->comment('active = 1, inactive = 0');

            $table->string('created_by',10)->nullable();
            $table->string('updated_by',10)->nullable();
            $table->timestamps();
            $table->engine= 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ems_general_setting');
    }
}
