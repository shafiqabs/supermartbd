<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmsProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ems_product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',40)->nullable();
            $table->string('name_bn',50)->nullable();
            $table->string('slug',40)->nullable();
            $table->string('sub_item',3)->nullable();

            $table->unsignedBigInteger('brand_id')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->unsignedBigInteger('item_unit_id')->nullable();
            $table->unsignedBigInteger('size_unit_id')->nullable();
            $table->unsignedBigInteger('size_weight_dimen_id')->nullable();
            $table->unsignedBigInteger('country_id')->nullable();
            $table->unsignedBigInteger('discount_id')->nullable();
            $table->unsignedBigInteger('warning_id')->nullable();
            $table->unsignedBigInteger('item_assurance_id')->nullable();
            $table->unsignedBigInteger('promotion_id')->nullable();
            $table->unsignedBigInteger('vendor_id')->nullable();

            $table->bigInteger('quantity')->nullable();
            $table->bigInteger('minquantity')->nullable();
            $table->bigInteger('maxquantity')->nullable();
            $table->decimal('salesprice',10,2)->nullable();
            $table->decimal('purchaseprice',10,2)->nullable();
            $table->string('discription',500)->nullable();
            $table->string('feature_image',100)->nullable();
            $table->string('feature_tag',30)->nullable();

            $table->enum('status',array('1','0'))->nullable()->comment('active = 1, inactive = 0');

            $table->string('created_by',50)->nullable();
            $table->string('updated_by',50)->nullable();
            $table->timestamps();

            $table->foreign('brand_id')
                ->references('id')->on('ems_product_brand')
                ->onDelete('cascade');

            $table->foreign('category_id')
                ->references('id')->on('ems_category')
                ->onDelete('cascade');

            $table->foreign('item_unit_id')
                ->references('id')->on('ems_itemunit')
                ->onDelete('cascade');

            $table->foreign('size_unit_id')
                ->references('id')->on('ems_sizeunit')
                ->onDelete('cascade');

            $table->foreign('size_weight_dimen_id')
                ->references('id')->on('ems_sizeweightdimension')
                ->onDelete('cascade');

            $table->foreign('country_id')
                ->references('id')->on('ems_country')
                ->onDelete('cascade');

            $table->foreign('discount_id')
                ->references('id')->on('ems_discount')
                ->onDelete('cascade');

            $table->foreign('warning_id')
                ->references('id')->on('ems_warningtype')
                ->onDelete('cascade');

            $table->foreign('item_assurance_id')
                ->references('id')->on('ems_itemassurance')
                ->onDelete('cascade');

            $table->foreign('promotion_id')
                ->references('id')->on('ems_promotion')
                ->onDelete('cascade');

            $table->foreign('vendor_id')
                ->references('id')->on('ems_vendor')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ems_product');
    }
}
