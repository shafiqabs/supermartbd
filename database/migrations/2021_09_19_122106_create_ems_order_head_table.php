<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmsOrderHeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ems_order_head', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_number',30)->nullable();

            $table->unsignedBigInteger('time_slot_id')->nullable();
            $table->string('shipping_time_slot',20)->nullable();

            $table->string('vendor_id',30)->nullable();
            $table->string('user_id',30)->nullable();
            $table->unsignedBigInteger('customer_id')->nullable();


            $table->string('order_notes',50)->nullable();
            $table->string('payment_method',30)->nullable();
            $table->integer('total_quantity')->nullable();

            $table->decimal('product_amount',10,2)->nullable();
            $table->decimal('delivery_charge',10,2)->nullable();
            $table->decimal('total_amount',10,2)->nullable();


            $table->enum('status',array('1','2','3','4','5'))->nullable()->comment('1 = Process, 2 = Confirm, 3 = Pending, 4 = Approved, 5 = Return');

            $table->string('created_by',10)->nullable();
            $table->string('updated_by',10)->nullable();
            $table->timestamps();
            $table->engine= 'InnoDB';

            $table->foreign('time_slot_id')
                ->references('id')->on('ems_delivery_time_slot')
                ->onDelete('cascade');

            $table->foreign('customer_id')
                ->references('id')->on('ems_customer')
                ->onDelete('cascade');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ems_order_head');
    }
}
