<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddedField1IntoGeneralSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ems_general_setting', function (Blueprint $table) {
            $table->longText('description')->nullable();
            $table->string('facebook')->nullable();
            $table->string('youtube')->nullable();
            $table->string('twitter')->nullable();
            $table->string('instagram')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ems_general_setting', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('facebook');
            $table->dropColumn('youtube');
            $table->dropColumn('twitter');
            $table->dropColumn('instagram');
        });
    }
}
