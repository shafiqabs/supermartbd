<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddedFieldIntoCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ems_category', function (Blueprint $table) {
            $table->string('banner_header',20)->nullable();
            $table->string('banner_title',25)->nullable();
            $table->string('banner_sub_title',16)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ems_category', function (Blueprint $table) {
            $table->dropColumn('banner_header');
            $table->dropColumn('banner_title');
            $table->dropColumn('banner_sub_title');
        });
    }
}
