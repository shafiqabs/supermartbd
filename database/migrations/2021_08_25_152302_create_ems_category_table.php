<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmsCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ems_category', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('category_parent_id')->nullable();
            $table->string('name',30)->nullable();
            $table->string('slug',30)->nullable();
            $table->string('content',50)->nullable();
            $table->string('feature',3)->nullable();
            $table->string('category_image',30)->nullable();

            $table->enum('status',array('1','0'))->nullable()->comment('active = 1, inactive = 0');

            $table->string('created_by',10)->nullable();
            $table->string('updated_by',10)->nullable();

            $table->timestamps();
            $table->engine= 'InnoDB';
            $table->foreign('category_parent_id')
                ->references('id')->on('ems_category')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ems_category');
    }
}
