<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmsOrderItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ems_order_item', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('order_id')->nullable();
            $table->unsignedBigInteger('product_id')->nullable();
            $table->unsignedBigInteger('vendor_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();

            $table->string('name',50)->nullable();
            $table->string('slug',50)->nullable();

            $table->bigInteger('quantity')->nullable();
            $table->decimal('unit_price',10,2)->nullable();
            $table->decimal('subtotal',10,2)->nullable();

            $table->string('created_by',10)->nullable();
            $table->string('updated_by',10)->nullable();

            $table->timestamps();

            $table->foreign('order_id')
                ->references('id')->on('ems_order_head')
                ->onDelete('cascade');

            $table->foreign('product_id')
                ->references('id')->on('ems_product')
                ->onDelete('cascade');

            $table->foreign('vendor_id')
                ->references('id')->on('ems_vendor')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ems_order_item');
    }
}
