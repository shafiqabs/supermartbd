-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 03, 2023 at 01:00 PM
-- Server version: 8.0.33-0ubuntu0.20.04.2
-- PHP Version: 7.4.3-4ubuntu2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `supermartbd`
--

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int NOT NULL,
  `phone_code` int NOT NULL,
  `country_code` char(2) NOT NULL,
  `country_name` varchar(80) NOT NULL,
  `continent_code` varchar(2) DEFAULT NULL,
  `continent_name` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `phone_code`, `country_code`, `country_name`, `continent_code`, `continent_name`) VALUES
(1, 93, 'AF', 'Afghanistan', 'AS', 'Asia'),
(2, 358, 'AX', 'Aland Islands', 'EU', 'Europe'),
(3, 355, 'AL', 'Albania', 'EU', 'Europe'),
(4, 213, 'DZ', 'Algeria', 'AF', 'Africa'),
(5, 1684, 'AS', 'American Samoa', 'OC', 'Oceania'),
(6, 376, 'AD', 'Andorra', 'EU', 'Europe'),
(7, 244, 'AO', 'Angola', 'AF', 'Africa'),
(8, 1264, 'AI', 'Anguilla', 'NA', 'North America'),
(9, 672, 'AQ', 'Antarctica', 'AN', 'Antarctica'),
(10, 1268, 'AG', 'Antigua and Barbuda', 'NA', 'North America'),
(11, 54, 'AR', 'Argentina', 'SA', 'South America'),
(12, 374, 'AM', 'Armenia', 'AS', 'Asia'),
(13, 297, 'AW', 'Aruba', 'NA', 'North America'),
(14, 61, 'AU', 'Australia', 'OC', 'Oceania'),
(15, 43, 'AT', 'Austria', 'EU', 'Europe'),
(16, 994, 'AZ', 'Azerbaijan', 'AS', 'Asia'),
(17, 1242, 'BS', 'Bahamas', 'NA', 'North America'),
(18, 973, 'BH', 'Bahrain', 'AS', 'Asia'),
(19, 880, 'BD', 'Bangladesh', 'AS', 'Asia'),
(20, 1246, 'BB', 'Barbados', 'NA', 'North America'),
(21, 375, 'BY', 'Belarus', 'EU', 'Europe'),
(22, 32, 'BE', 'Belgium', 'EU', 'Europe'),
(23, 501, 'BZ', 'Belize', 'NA', 'North America'),
(24, 229, 'BJ', 'Benin', 'AF', 'Africa'),
(25, 1441, 'BM', 'Bermuda', 'NA', 'North America'),
(26, 975, 'BT', 'Bhutan', 'AS', 'Asia'),
(27, 591, 'BO', 'Bolivia', 'SA', 'South America'),
(28, 599, 'BQ', 'Bonaire, Sint Eustatius and Saba', 'NA', 'North America'),
(29, 387, 'BA', 'Bosnia and Herzegovina', 'EU', 'Europe'),
(30, 267, 'BW', 'Botswana', 'AF', 'Africa'),
(31, 55, 'BV', 'Bouvet Island', 'AN', 'Antarctica'),
(32, 55, 'BR', 'Brazil', 'SA', 'South America'),
(33, 246, 'IO', 'British Indian Ocean Territory', 'AS', 'Asia'),
(34, 673, 'BN', 'Brunei Darussalam', 'AS', 'Asia'),
(35, 359, 'BG', 'Bulgaria', 'EU', 'Europe'),
(36, 226, 'BF', 'Burkina Faso', 'AF', 'Africa'),
(37, 257, 'BI', 'Burundi', 'AF', 'Africa'),
(38, 855, 'KH', 'Cambodia', 'AS', 'Asia'),
(39, 237, 'CM', 'Cameroon', 'AF', 'Africa'),
(40, 1, 'CA', 'Canada', 'NA', 'North America'),
(41, 238, 'CV', 'Cape Verde', 'AF', 'Africa'),
(42, 1345, 'KY', 'Cayman Islands', 'NA', 'North America'),
(43, 236, 'CF', 'Central African Republic', 'AF', 'Africa'),
(44, 235, 'TD', 'Chad', 'AF', 'Africa'),
(45, 56, 'CL', 'Chile', 'SA', 'South America'),
(46, 86, 'CN', 'China', 'AS', 'Asia'),
(47, 61, 'CX', 'Christmas Island', 'AS', 'Asia'),
(48, 672, 'CC', 'Cocos (Keeling) Islands', 'AS', 'Asia'),
(49, 57, 'CO', 'Colombia', 'SA', 'South America'),
(50, 269, 'KM', 'Comoros', 'AF', 'Africa'),
(51, 242, 'CG', 'Congo', 'AF', 'Africa'),
(52, 242, 'CD', 'Congo, Democratic Republic of the Congo', 'AF', 'Africa'),
(53, 682, 'CK', 'Cook Islands', 'OC', 'Oceania'),
(54, 506, 'CR', 'Costa Rica', 'NA', 'North America'),
(55, 225, 'CI', 'Cote D\'Ivoire', 'AF', 'Africa'),
(56, 385, 'HR', 'Croatia', 'EU', 'Europe'),
(57, 53, 'CU', 'Cuba', 'NA', 'North America'),
(58, 599, 'CW', 'Curacao', 'NA', 'North America'),
(59, 357, 'CY', 'Cyprus', 'AS', 'Asia'),
(60, 420, 'CZ', 'Czech Republic', 'EU', 'Europe'),
(61, 45, 'DK', 'Denmark', 'EU', 'Europe'),
(62, 253, 'DJ', 'Djibouti', 'AF', 'Africa'),
(63, 1767, 'DM', 'Dominica', 'NA', 'North America'),
(64, 1809, 'DO', 'Dominican Republic', 'NA', 'North America'),
(65, 593, 'EC', 'Ecuador', 'SA', 'South America'),
(66, 20, 'EG', 'Egypt', 'AF', 'Africa'),
(67, 503, 'SV', 'El Salvador', 'NA', 'North America'),
(68, 240, 'GQ', 'Equatorial Guinea', 'AF', 'Africa'),
(69, 291, 'ER', 'Eritrea', 'AF', 'Africa'),
(70, 372, 'EE', 'Estonia', 'EU', 'Europe'),
(71, 251, 'ET', 'Ethiopia', 'AF', 'Africa'),
(72, 500, 'FK', 'Falkland Islands (Malvinas)', 'SA', 'South America'),
(73, 298, 'FO', 'Faroe Islands', 'EU', 'Europe'),
(74, 679, 'FJ', 'Fiji', 'OC', 'Oceania'),
(75, 358, 'FI', 'Finland', 'EU', 'Europe'),
(76, 33, 'FR', 'France', 'EU', 'Europe'),
(77, 594, 'GF', 'French Guiana', 'SA', 'South America'),
(78, 689, 'PF', 'French Polynesia', 'OC', 'Oceania'),
(79, 262, 'TF', 'French Southern Territories', 'AN', 'Antarctica'),
(80, 241, 'GA', 'Gabon', 'AF', 'Africa'),
(81, 220, 'GM', 'Gambia', 'AF', 'Africa'),
(82, 995, 'GE', 'Georgia', 'AS', 'Asia'),
(83, 49, 'DE', 'Germany', 'EU', 'Europe'),
(84, 233, 'GH', 'Ghana', 'AF', 'Africa'),
(85, 350, 'GI', 'Gibraltar', 'EU', 'Europe'),
(86, 30, 'GR', 'Greece', 'EU', 'Europe'),
(87, 299, 'GL', 'Greenland', 'NA', 'North America'),
(88, 1473, 'GD', 'Grenada', 'NA', 'North America'),
(89, 590, 'GP', 'Guadeloupe', 'NA', 'North America'),
(90, 1671, 'GU', 'Guam', 'OC', 'Oceania'),
(91, 502, 'GT', 'Guatemala', 'NA', 'North America'),
(92, 44, 'GG', 'Guernsey', 'EU', 'Europe'),
(93, 224, 'GN', 'Guinea', 'AF', 'Africa'),
(94, 245, 'GW', 'Guinea-Bissau', 'AF', 'Africa'),
(95, 592, 'GY', 'Guyana', 'SA', 'South America'),
(96, 509, 'HT', 'Haiti', 'NA', 'North America'),
(97, 0, 'HM', 'Heard Island and Mcdonald Islands', 'AN', 'Antarctica'),
(98, 39, 'VA', 'Holy See (Vatican City State)', 'EU', 'Europe'),
(99, 504, 'HN', 'Honduras', 'NA', 'North America'),
(100, 852, 'HK', 'Hong Kong', 'AS', 'Asia'),
(101, 36, 'HU', 'Hungary', 'EU', 'Europe'),
(102, 354, 'IS', 'Iceland', 'EU', 'Europe'),
(103, 91, 'IN', 'India', 'AS', 'Asia'),
(104, 62, 'ID', 'Indonesia', 'AS', 'Asia'),
(105, 98, 'IR', 'Iran, Islamic Republic of', 'AS', 'Asia'),
(106, 964, 'IQ', 'Iraq', 'AS', 'Asia'),
(107, 353, 'IE', 'Ireland', 'EU', 'Europe'),
(108, 44, 'IM', 'Isle of Man', 'EU', 'Europe'),
(109, 972, 'IL', 'Israel', 'AS', 'Asia'),
(110, 39, 'IT', 'Italy', 'EU', 'Europe'),
(111, 1876, 'JM', 'Jamaica', 'NA', 'North America'),
(112, 81, 'JP', 'Japan', 'AS', 'Asia'),
(113, 44, 'JE', 'Jersey', 'EU', 'Europe'),
(114, 962, 'JO', 'Jordan', 'AS', 'Asia'),
(115, 7, 'KZ', 'Kazakhstan', 'AS', 'Asia'),
(116, 254, 'KE', 'Kenya', 'AF', 'Africa'),
(117, 686, 'KI', 'Kiribati', 'OC', 'Oceania'),
(118, 850, 'KP', 'Korea, Democratic People\'s Republic of', 'AS', 'Asia'),
(119, 82, 'KR', 'Korea, Republic of', 'AS', 'Asia'),
(120, 381, 'XK', 'Kosovo', 'EU', 'Europe'),
(121, 965, 'KW', 'Kuwait', 'AS', 'Asia'),
(122, 996, 'KG', 'Kyrgyzstan', 'AS', 'Asia'),
(123, 856, 'LA', 'Lao People\'s Democratic Republic', 'AS', 'Asia'),
(124, 371, 'LV', 'Latvia', 'EU', 'Europe'),
(125, 961, 'LB', 'Lebanon', 'AS', 'Asia'),
(126, 266, 'LS', 'Lesotho', 'AF', 'Africa'),
(127, 231, 'LR', 'Liberia', 'AF', 'Africa'),
(128, 218, 'LY', 'Libyan Arab Jamahiriya', 'AF', 'Africa'),
(129, 423, 'LI', 'Liechtenstein', 'EU', 'Europe'),
(130, 370, 'LT', 'Lithuania', 'EU', 'Europe'),
(131, 352, 'LU', 'Luxembourg', 'EU', 'Europe'),
(132, 853, 'MO', 'Macao', 'AS', 'Asia'),
(133, 389, 'MK', 'Macedonia, the Former Yugoslav Republic of', 'EU', 'Europe'),
(134, 261, 'MG', 'Madagascar', 'AF', 'Africa'),
(135, 265, 'MW', 'Malawi', 'AF', 'Africa'),
(136, 60, 'MY', 'Malaysia', 'AS', 'Asia'),
(137, 960, 'MV', 'Maldives', 'AS', 'Asia'),
(138, 223, 'ML', 'Mali', 'AF', 'Africa'),
(139, 356, 'MT', 'Malta', 'EU', 'Europe'),
(140, 692, 'MH', 'Marshall Islands', 'OC', 'Oceania'),
(141, 596, 'MQ', 'Martinique', 'NA', 'North America'),
(142, 222, 'MR', 'Mauritania', 'AF', 'Africa'),
(143, 230, 'MU', 'Mauritius', 'AF', 'Africa'),
(144, 269, 'YT', 'Mayotte', 'AF', 'Africa'),
(145, 52, 'MX', 'Mexico', 'NA', 'North America'),
(146, 691, 'FM', 'Micronesia, Federated States of', 'OC', 'Oceania'),
(147, 373, 'MD', 'Moldova, Republic of', 'EU', 'Europe'),
(148, 377, 'MC', 'Monaco', 'EU', 'Europe'),
(149, 976, 'MN', 'Mongolia', 'AS', 'Asia'),
(150, 382, 'ME', 'Montenegro', 'EU', 'Europe'),
(151, 1664, 'MS', 'Montserrat', 'NA', 'North America'),
(152, 212, 'MA', 'Morocco', 'AF', 'Africa'),
(153, 258, 'MZ', 'Mozambique', 'AF', 'Africa'),
(154, 95, 'MM', 'Myanmar', 'AS', 'Asia'),
(155, 264, 'NA', 'Namibia', 'AF', 'Africa'),
(156, 674, 'NR', 'Nauru', 'OC', 'Oceania'),
(157, 977, 'NP', 'Nepal', 'AS', 'Asia'),
(158, 31, 'NL', 'Netherlands', 'EU', 'Europe'),
(159, 599, 'AN', 'Netherlands Antilles', 'NA', 'North America'),
(160, 687, 'NC', 'New Caledonia', 'OC', 'Oceania'),
(161, 64, 'NZ', 'New Zealand', 'OC', 'Oceania'),
(162, 505, 'NI', 'Nicaragua', 'NA', 'North America'),
(163, 227, 'NE', 'Niger', 'AF', 'Africa'),
(164, 234, 'NG', 'Nigeria', 'AF', 'Africa'),
(165, 683, 'NU', 'Niue', 'OC', 'Oceania'),
(166, 672, 'NF', 'Norfolk Island', 'OC', 'Oceania'),
(167, 1670, 'MP', 'Northern Mariana Islands', 'OC', 'Oceania'),
(168, 47, 'NO', 'Norway', 'EU', 'Europe'),
(169, 968, 'OM', 'Oman', 'AS', 'Asia'),
(170, 92, 'PK', 'Pakistan', 'AS', 'Asia'),
(171, 680, 'PW', 'Palau', 'OC', 'Oceania'),
(172, 970, 'PS', 'Palestinian Territory, Occupied', 'AS', 'Asia'),
(173, 507, 'PA', 'Panama', 'NA', 'North America'),
(174, 675, 'PG', 'Papua New Guinea', 'OC', 'Oceania'),
(175, 595, 'PY', 'Paraguay', 'SA', 'South America'),
(176, 51, 'PE', 'Peru', 'SA', 'South America'),
(177, 63, 'PH', 'Philippines', 'AS', 'Asia'),
(178, 64, 'PN', 'Pitcairn', 'OC', 'Oceania'),
(179, 48, 'PL', 'Poland', 'EU', 'Europe'),
(180, 351, 'PT', 'Portugal', 'EU', 'Europe'),
(181, 1787, 'PR', 'Puerto Rico', 'NA', 'North America'),
(182, 974, 'QA', 'Qatar', 'AS', 'Asia'),
(183, 262, 'RE', 'Reunion', 'AF', 'Africa'),
(184, 40, 'RO', 'Romania', 'EU', 'Europe'),
(185, 70, 'RU', 'Russian Federation', 'AS', 'Asia'),
(186, 250, 'RW', 'Rwanda', 'AF', 'Africa'),
(187, 590, 'BL', 'Saint Barthelemy', 'NA', 'North America'),
(188, 290, 'SH', 'Saint Helena', 'AF', 'Africa'),
(189, 1869, 'KN', 'Saint Kitts and Nevis', 'NA', 'North America'),
(190, 1758, 'LC', 'Saint Lucia', 'NA', 'North America'),
(191, 590, 'MF', 'Saint Martin', 'NA', 'North America'),
(192, 508, 'PM', 'Saint Pierre and Miquelon', 'NA', 'North America'),
(193, 1784, 'VC', 'Saint Vincent and the Grenadines', 'NA', 'North America'),
(194, 684, 'WS', 'Samoa', 'OC', 'Oceania'),
(195, 378, 'SM', 'San Marino', 'EU', 'Europe'),
(196, 239, 'ST', 'Sao Tome and Principe', 'AF', 'Africa'),
(197, 966, 'SA', 'Saudi Arabia', 'AS', 'Asia'),
(198, 221, 'SN', 'Senegal', 'AF', 'Africa'),
(199, 381, 'RS', 'Serbia', 'EU', 'Europe'),
(200, 381, 'CS', 'Serbia and Montenegro', 'EU', 'Europe'),
(201, 248, 'SC', 'Seychelles', 'AF', 'Africa'),
(202, 232, 'SL', 'Sierra Leone', 'AF', 'Africa'),
(203, 65, 'SG', 'Singapore', 'AS', 'Asia'),
(204, 1, 'SX', 'Sint Maarten', 'NA', 'North America'),
(205, 421, 'SK', 'Slovakia', 'EU', 'Europe'),
(206, 386, 'SI', 'Slovenia', 'EU', 'Europe'),
(207, 677, 'SB', 'Solomon Islands', 'OC', 'Oceania'),
(208, 252, 'SO', 'Somalia', 'AF', 'Africa'),
(209, 27, 'ZA', 'South Africa', 'AF', 'Africa'),
(210, 500, 'GS', 'South Georgia and the South Sandwich Islands', 'AN', 'Antarctica'),
(211, 211, 'SS', 'South Sudan', 'AF', 'Africa'),
(212, 34, 'ES', 'Spain', 'EU', 'Europe'),
(213, 94, 'LK', 'Sri Lanka', 'AS', 'Asia'),
(214, 249, 'SD', 'Sudan', 'AF', 'Africa'),
(215, 597, 'SR', 'Suriname', 'SA', 'South America'),
(216, 47, 'SJ', 'Svalbard and Jan Mayen', 'EU', 'Europe'),
(217, 268, 'SZ', 'Swaziland', 'AF', 'Africa'),
(218, 46, 'SE', 'Sweden', 'EU', 'Europe'),
(219, 41, 'CH', 'Switzerland', 'EU', 'Europe'),
(220, 963, 'SY', 'Syrian Arab Republic', 'AS', 'Asia'),
(221, 886, 'TW', 'Taiwan, Province of China', 'AS', 'Asia'),
(222, 992, 'TJ', 'Tajikistan', 'AS', 'Asia'),
(223, 255, 'TZ', 'Tanzania, United Republic of', 'AF', 'Africa'),
(224, 66, 'TH', 'Thailand', 'AS', 'Asia'),
(225, 670, 'TL', 'Timor-Leste', 'AS', 'Asia'),
(226, 228, 'TG', 'Togo', 'AF', 'Africa'),
(227, 690, 'TK', 'Tokelau', 'OC', 'Oceania'),
(228, 676, 'TO', 'Tonga', 'OC', 'Oceania'),
(229, 1868, 'TT', 'Trinidad and Tobago', 'NA', 'North America'),
(230, 216, 'TN', 'Tunisia', 'AF', 'Africa'),
(231, 90, 'TR', 'Turkey', 'AS', 'Asia'),
(232, 7370, 'TM', 'Turkmenistan', 'AS', 'Asia'),
(233, 1649, 'TC', 'Turks and Caicos Islands', 'NA', 'North America'),
(234, 688, 'TV', 'Tuvalu', 'OC', 'Oceania'),
(235, 256, 'UG', 'Uganda', 'AF', 'Africa'),
(236, 380, 'UA', 'Ukraine', 'EU', 'Europe'),
(237, 971, 'AE', 'United Arab Emirates', 'AS', 'Asia'),
(238, 44, 'GB', 'United Kingdom', 'EU', 'Europe'),
(239, 1, 'US', 'United States', 'NA', 'North America'),
(240, 1, 'UM', 'United States Minor Outlying Islands', 'NA', 'North America'),
(241, 598, 'UY', 'Uruguay', 'SA', 'South America'),
(242, 998, 'UZ', 'Uzbekistan', 'AS', 'Asia'),
(243, 678, 'VU', 'Vanuatu', 'OC', 'Oceania'),
(244, 58, 'VE', 'Venezuela', 'SA', 'South America'),
(245, 84, 'VN', 'Viet Nam', 'AS', 'Asia'),
(246, 1284, 'VG', 'Virgin Islands, British', 'NA', 'North America'),
(247, 1340, 'VI', 'Virgin Islands, U.s.', 'NA', 'North America'),
(248, 681, 'WF', 'Wallis and Futuna', 'OC', 'Oceania'),
(249, 212, 'EH', 'Western Sahara', 'AF', 'Africa'),
(250, 967, 'YE', 'Yemen', 'AS', 'Asia'),
(251, 260, 'ZM', 'Zambia', 'AF', 'Africa'),
(252, 263, 'ZW', 'Zimbabwe', 'AF', 'Africa');

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` int UNSIGNED NOT NULL,
  `division_id` int UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL,
  `bn_name` varchar(50) DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lon` double DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `division_id`, `name`, `bn_name`, `lat`, `lon`, `website`, `updated_at`) VALUES
(1, 3, 'Dhaka', 'à¦¢à¦¾à¦•à¦¾', 23.7115253, 90.4111451, 'www.dhaka.gov.bd', '2015-09-13 04:36:20'),
(2, 3, 'Faridpur', 'à¦«à¦°à¦¿à¦¦à¦ªà§à¦°', 23.6070822, 89.8429406, 'www.faridpur.gov.bd', '2015-09-13 04:36:20'),
(3, 3, 'Gazipur', 'à¦—à¦¾à¦œà§€à¦ªà§à¦°', 24.0022858, 90.4264283, 'www.gazipur.gov.bd', '2015-09-13 04:36:20'),
(4, 3, 'Gopalganj', 'à¦—à§‹à¦ªà¦¾à¦²à¦—à¦žà§à¦œ', 23.0050857, 89.8266059, 'www.gopalganj.gov.bd', '2015-09-13 04:36:20'),
(5, 8, 'Jamalpur', 'à¦œà¦¾à¦®à¦¾à¦²à¦ªà§à¦°', 24.937533, 89.937775, 'www.jamalpur.gov.bd', '2016-04-06 10:48:38'),
(6, 3, 'Kishoreganj', 'à¦•à¦¿à¦¶à§‹à¦°à¦—à¦žà§à¦œ', 24.444937, 90.776575, 'www.kishoreganj.gov.bd', '2015-09-13 04:36:20'),
(7, 3, 'Madaripur', 'à¦®à¦¾à¦¦à¦¾à¦°à§€à¦ªà§à¦°', 23.164102, 90.1896805, 'www.madaripur.gov.bd', '2015-09-13 04:36:20'),
(8, 3, 'Manikganj', 'à¦®à¦¾à¦¨à¦¿à¦•à¦—à¦žà§à¦œ', 0, 0, 'www.manikganj.gov.bd', '2015-09-13 04:36:20'),
(9, 3, 'Munshiganj', 'à¦®à§à¦¨à§à¦¸à¦¿à¦—à¦žà§à¦œ', 0, 0, 'www.munshiganj.gov.bd', '2015-09-13 04:36:20'),
(10, 8, 'Mymensingh', 'à¦®à§Ÿà¦®à¦¨à¦¸à¦¿à¦‚', 0, 0, 'www.mymensingh.gov.bd', '2016-04-06 10:49:01'),
(11, 3, 'Narayanganj', 'à¦¨à¦¾à¦°à¦¾à§Ÿà¦¾à¦£à¦—à¦žà§à¦œ', 23.63366, 90.496482, 'www.narayanganj.gov.bd', '2015-09-13 04:36:20'),
(12, 3, 'Narsingdi', 'à¦¨à¦°à¦¸à¦¿à¦‚à¦¦à§€', 23.932233, 90.71541, 'www.narsingdi.gov.bd', '2015-09-13 04:36:20'),
(13, 8, 'Netrokona', 'à¦¨à§‡à¦¤à§à¦°à¦•à§‹à¦¨à¦¾', 24.870955, 90.727887, 'www.netrokona.gov.bd', '2016-04-06 10:46:31'),
(14, 3, 'Rajbari', 'à¦°à¦¾à¦œà¦¬à¦¾à§œà¦¿', 23.7574305, 89.6444665, 'www.rajbari.gov.bd', '2015-09-13 04:36:20'),
(15, 3, 'Shariatpur', 'à¦¶à¦°à§€à§Ÿà¦¤à¦ªà§à¦°', 0, 0, 'www.shariatpur.gov.bd', '2015-09-13 04:36:20'),
(16, 8, 'Sherpur', 'à¦¶à§‡à¦°à¦ªà§à¦°', 25.0204933, 90.0152966, 'www.sherpur.gov.bd', '2016-04-06 10:48:21'),
(17, 3, 'Tangail', 'à¦Ÿà¦¾à¦™à§à¦—à¦¾à¦‡à¦²', 0, 0, 'www.tangail.gov.bd', '2015-09-13 04:36:20'),
(18, 5, 'Bogra', 'à¦¬à¦—à§à§œà¦¾', 24.8465228, 89.377755, 'www.bogra.gov.bd', '2015-09-13 04:36:20'),
(19, 5, 'Joypurhat', 'à¦œà§Ÿà¦ªà§à¦°à¦¹à¦¾à¦Ÿ', 0, 0, 'www.joypurhat.gov.bd', '2015-09-13 04:36:20'),
(20, 5, 'Naogaon', 'à¦¨à¦“à¦—à¦¾à¦', 0, 0, 'www.naogaon.gov.bd', '2015-09-13 04:36:20'),
(21, 5, 'Natore', 'à¦¨à¦¾à¦Ÿà§‹à¦°', 24.420556, 89.000282, 'www.natore.gov.bd', '2015-09-13 04:36:20'),
(22, 5, 'Nawabganj', 'à¦¨à¦¬à¦¾à¦¬à¦—à¦žà§à¦œ', 24.5965034, 88.2775122, 'www.chapainawabganj.gov.bd', '2015-09-13 04:36:20'),
(23, 5, 'Pabna', 'à¦ªà¦¾à¦¬à¦¨à¦¾', 23.998524, 89.233645, 'www.pabna.gov.bd', '2015-09-13 04:36:20'),
(24, 5, 'Rajshahi', 'à¦°à¦¾à¦œà¦¶à¦¾à¦¹à§€', 0, 0, 'www.rajshahi.gov.bd', '2015-09-13 04:36:20'),
(25, 5, 'Sirajgonj', 'à¦¸à¦¿à¦°à¦¾à¦œà¦—à¦žà§à¦œ', 24.4533978, 89.7006815, 'www.sirajganj.gov.bd', '2015-09-13 04:36:20'),
(26, 6, 'Dinajpur', 'à¦¦à¦¿à¦¨à¦¾à¦œà¦ªà§à¦°', 25.6217061, 88.6354504, 'www.dinajpur.gov.bd', '2015-09-13 04:36:20'),
(27, 6, 'Gaibandha', 'à¦—à¦¾à¦‡à¦¬à¦¾à¦¨à§à¦§à¦¾', 25.328751, 89.528088, 'www.gaibandha.gov.bd', '2015-09-13 04:36:20'),
(28, 6, 'Kurigram', 'à¦•à§à§œà¦¿à¦—à§à¦°à¦¾à¦®', 25.805445, 89.636174, 'www.kurigram.gov.bd', '2015-09-13 04:36:20'),
(29, 6, 'Lalmonirhat', 'à¦²à¦¾à¦²à¦®à¦¨à¦¿à¦°à¦¹à¦¾à¦Ÿ', 0, 0, 'www.lalmonirhat.gov.bd', '2015-09-13 04:36:20'),
(30, 6, 'Nilphamari', 'à¦¨à§€à¦²à¦«à¦¾à¦®à¦¾à¦°à§€', 25.931794, 88.856006, 'www.nilphamari.gov.bd', '2015-09-13 04:36:20'),
(31, 6, 'Panchagarh', 'à¦ªà¦žà§à¦šà¦—à§œ', 26.3411, 88.5541606, 'www.panchagarh.gov.bd', '2015-09-13 04:36:20'),
(32, 6, 'Rangpur', 'à¦°à¦‚à¦ªà§à¦°', 25.7558096, 89.244462, 'www.rangpur.gov.bd', '2015-09-13 04:36:20'),
(33, 6, 'Thakurgaon', 'à¦ à¦¾à¦•à§à¦°à¦—à¦¾à¦à¦“', 26.0336945, 88.4616834, 'www.thakurgaon.gov.bd', '2015-09-13 04:36:20'),
(34, 1, 'Barguna', 'à¦¬à¦°à¦—à§à¦¨à¦¾', 0, 0, 'www.barguna.gov.bd', '2015-09-13 04:36:20'),
(35, 1, 'Barisal', 'à¦¬à¦°à¦¿à¦¶à¦¾à¦²', 0, 0, 'www.barisal.gov.bd', '2015-09-13 04:36:20'),
(36, 1, 'Bhola', 'à¦­à§‹à¦²à¦¾', 22.685923, 90.648179, 'www.bhola.gov.bd', '2015-09-13 04:36:20'),
(37, 1, 'Jhalokati', 'à¦à¦¾à¦²à¦•à¦¾à¦ à¦¿', 0, 0, 'www.jhalakathi.gov.bd', '2015-09-13 04:36:20'),
(38, 1, 'Patuakhali', 'à¦ªà¦Ÿà§à§Ÿà¦¾à¦–à¦¾à¦²à§€', 22.3596316, 90.3298712, 'www.patuakhali.gov.bd', '2015-09-13 04:36:20'),
(39, 1, 'Pirojpur', 'à¦ªà¦¿à¦°à§‹à¦œà¦ªà§à¦°', 0, 0, 'www.pirojpur.gov.bd', '2015-09-13 04:36:20'),
(40, 2, 'Bandarban', 'à¦¬à¦¾à¦¨à§à¦¦à¦°à¦¬à¦¾à¦¨', 22.1953275, 92.2183773, 'www.bandarban.gov.bd', '2015-09-13 04:36:20'),
(41, 2, 'Brahmanbaria', 'à¦¬à§à¦°à¦¾à¦¹à§à¦®à¦£à¦¬à¦¾à§œà¦¿à§Ÿà¦¾', 23.9570904, 91.1119286, 'www.brahmanbaria.gov.bd', '2015-09-13 04:36:20'),
(42, 2, 'Chandpur', 'à¦šà¦¾à¦à¦¦à¦ªà§à¦°', 23.2332585, 90.6712912, 'www.chandpur.gov.bd', '2015-09-13 04:36:20'),
(43, 2, 'Chittagong', 'à¦šà¦Ÿà§à¦Ÿà¦—à§à¦°à¦¾à¦®', 22.335109, 91.834073, 'www.chittagong.gov.bd', '2015-09-13 04:36:20'),
(44, 2, 'Comilla', 'à¦•à§à¦®à¦¿à¦²à§à¦²à¦¾', 23.4682747, 91.1788135, 'www.comilla.gov.bd', '2015-09-13 04:36:20'),
(45, 2, 'Cox\'s Bazar', 'à¦•à¦•à§à¦¸ à¦¬à¦¾à¦œà¦¾à¦°', 0, 0, 'www.coxsbazar.gov.bd', '2015-09-13 04:36:20'),
(46, 2, 'Feni', 'à¦«à§‡à¦¨à§€', 23.023231, 91.3840844, 'www.feni.gov.bd', '2015-09-13 04:36:20'),
(47, 2, 'Khagrachari', 'à¦–à¦¾à¦—à§œà¦¾à¦›à§œà¦¿', 23.119285, 91.984663, 'www.khagrachhari.gov.bd', '2015-09-13 04:36:20'),
(48, 2, 'Lakshmipur', 'à¦²à¦•à§à¦·à§à¦®à§€à¦ªà§à¦°', 22.942477, 90.841184, 'www.lakshmipur.gov.bd', '2015-09-13 04:36:20'),
(49, 2, 'Noakhali', 'à¦¨à§‹à§Ÿà¦¾à¦–à¦¾à¦²à§€', 22.869563, 91.099398, 'www.noakhali.gov.bd', '2015-09-13 04:36:20'),
(50, 2, 'Rangamati', 'à¦°à¦¾à¦™à§à¦—à¦¾à¦®à¦¾à¦Ÿà¦¿', 0, 0, 'www.rangamati.gov.bd', '2015-09-13 04:36:20'),
(51, 7, 'Habiganj', 'à¦¹à¦¬à¦¿à¦—à¦žà§à¦œ', 24.374945, 91.41553, 'www.habiganj.gov.bd', '2015-09-13 04:36:20'),
(52, 7, 'Maulvibazar', 'à¦®à§Œà¦²à¦­à§€à¦¬à¦¾à¦œà¦¾à¦°', 24.482934, 91.777417, 'www.moulvibazar.gov.bd', '2015-09-13 04:36:20'),
(53, 7, 'Sunamganj', 'à¦¸à§à¦¨à¦¾à¦®à¦—à¦žà§à¦œ', 25.0658042, 91.3950115, 'www.sunamganj.gov.bd', '2015-09-13 04:36:20'),
(54, 7, 'Sylhet', 'à¦¸à¦¿à¦²à§‡à¦Ÿ', 24.8897956, 91.8697894, 'www.sylhet.gov.bd', '2015-09-13 04:36:20'),
(55, 4, 'Bagerhat', 'à¦¬à¦¾à¦—à§‡à¦°à¦¹à¦¾à¦Ÿ', 22.651568, 89.785938, 'www.bagerhat.gov.bd', '2015-09-13 04:36:20'),
(56, 4, 'Chuadanga', 'à¦šà§à§Ÿà¦¾à¦¡à¦¾à¦™à§à¦—à¦¾', 23.6401961, 88.841841, 'www.chuadanga.gov.bd', '2015-09-13 04:36:20'),
(57, 4, 'Jessore', 'à¦¯à¦¶à§‹à¦°', 23.16643, 89.2081126, 'www.jessore.gov.bd', '2015-09-13 04:36:20'),
(58, 4, 'Jhenaidah', 'à¦à¦¿à¦¨à¦¾à¦‡à¦¦à¦¹', 23.5448176, 89.1539213, 'www.jhenaidah.gov.bd', '2015-09-13 04:36:20'),
(59, 4, 'Khulna', 'à¦–à§à¦²à¦¨à¦¾', 22.815774, 89.568679, 'www.khulna.gov.bd', '2015-09-13 04:36:20'),
(60, 4, 'Kushtia', 'à¦•à§à¦·à§à¦Ÿà¦¿à§Ÿà¦¾', 23.901258, 89.120482, 'www.kushtia.gov.bd', '2015-09-13 04:36:20'),
(61, 4, 'Magura', 'à¦®à¦¾à¦—à§à¦°à¦¾', 23.487337, 89.419956, 'www.magura.gov.bd', '2015-09-13 04:36:20'),
(62, 4, 'Meherpur', 'à¦®à§‡à¦¹à§‡à¦°à¦ªà§à¦°', 23.762213, 88.631821, 'www.meherpur.gov.bd', '2015-09-13 04:36:20'),
(63, 4, 'Narail', 'à¦¨à§œà¦¾à¦‡à¦²', 23.172534, 89.512672, 'www.narail.gov.bd', '2015-09-13 04:36:20'),
(64, 4, 'Satkhira', 'à¦¸à¦¾à¦¤à¦•à§à¦·à§€à¦°à¦¾', 0, 0, 'www.satkhira.gov.bd', '2015-09-13 04:36:20');

-- --------------------------------------------------------

--
-- Table structure for table `ems_banner`
--

CREATE TABLE `ems_banner` (
  `id` bigint UNSIGNED NOT NULL,
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_position` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_visibility` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_item` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_banner`
--

INSERT INTO `ems_banner` (`id`, `title`, `slug`, `banner_position`, `banner_visibility`, `banner_item`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Winter Banner', 'winter-banner', 'banner-top', 'Show', '2', '1', '1', '1', '2021-11-03 11:41:14', '2022-09-11 07:36:17'),
(4, 'Winter Banner', 'winter-banner', 'banner-bottom', 'Show', '2', '1', '1', '1', '2021-11-03 11:56:25', '2022-09-11 07:45:41');

-- --------------------------------------------------------

--
-- Table structure for table `ems_banner_items`
--

CREATE TABLE `ems_banner_items` (
  `id` bigint UNSIGNED NOT NULL,
  `banner_id` bigint UNSIGNED DEFAULT NULL,
  `brand_id` bigint UNSIGNED DEFAULT NULL,
  `category_id` bigint UNSIGNED DEFAULT NULL,
  `banner_type` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_top` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attach_link` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_banner_items`
--

INSERT INTO `ems_banner_items` (`id`, `banner_id`, `brand_id`, `category_id`, `banner_type`, `title_top`, `title`, `attach_link`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(13, 1, NULL, 13, 'Category', 'Food Tag', 'Food Banner', 'img88121-1636275585.jpg', '1', '1', NULL, '2021-11-07 08:59:45', '2021-11-07 08:59:45'),
(14, 1, NULL, NULL, 'Brand', 'Brand Tag', 'Brand Banner', 'img21512-1636275585.jpg', '1', '1', '1', '2021-11-07 08:59:45', '2022-09-11 07:38:31'),
(15, 1, NULL, NULL, 'Category', 'Health', 'Health Banner', 'img42243-1636275585.jpg', '1', '1', '1', '2021-11-07 08:59:45', '2022-09-11 07:38:31'),
(18, 4, NULL, 1, 'Category', 'Category', 'Banner Item', 'img30271-1662882584.jpg', '1', '1', NULL, '2022-09-11 07:49:44', '2022-09-11 07:49:44'),
(19, 4, 2, NULL, 'Brand', 'Brand', 'Item Information', 'img77172-1662882584.jpg', '1', '1', NULL, '2022-09-11 07:49:44', '2022-09-11 07:49:44');

-- --------------------------------------------------------

--
-- Table structure for table `ems_category`
--

CREATE TABLE `ems_category` (
  `id` bigint UNSIGNED NOT NULL,
  `category_parent_id` bigint UNSIGNED DEFAULT NULL,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `feature` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_image` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `banner_header` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_title` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner_sub_title` varchar(16) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_category`
--

INSERT INTO `ems_category` (`id`, `category_parent_id`, `name`, `slug`, `content`, `feature`, `category_image`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `banner_header`, `banner_title`, `banner_sub_title`) VALUES
(1, 4, 'Air Fresheners', 'air-fresheners', 'Air Fresheners', 'yes', 'Air-Fresheners.jpeg', '1', '1', '1', '2021-08-26 01:22:55', '2021-08-29 06:45:46', NULL, NULL, NULL),
(2, NULL, 'Antiseptics', 'antiseptics', 'Antiseptics', 'yes', 'antiseptics.jpg', '1', '1', '1', '2021-08-26 01:25:51', '2021-11-03 14:51:58', NULL, NULL, NULL),
(3, NULL, 'Baby & Toddler Food', 'baby-&-toddler-food', 'Baby & Toddler Food', NULL, 'Baby-&-Toddler-Food.jpg', '1', '1', '1', '2021-08-26 01:28:34', '2021-08-29 06:45:37', NULL, NULL, NULL),
(4, 2, 'Bakery Snacks', 'bakery-snacks', 'Bakery Snacks', 'yes', 'Bakery-Snacks.jpg', '1', '1', '1', '2021-08-26 01:29:24', '2021-08-29 06:45:33', NULL, NULL, NULL),
(5, 4, 'Baking Needs', 'baking-needs', 'Baking Needs', NULL, 'Baking-Needs.jpg', '1', '1', '1', '2021-08-26 01:30:44', '2021-08-29 06:45:29', NULL, NULL, NULL),
(7, NULL, 'Information', 'information', 'Information', 'yes', 'information.jpg', '1', '1', '1', '2021-08-26 13:13:21', '2021-11-03 14:51:30', NULL, NULL, NULL),
(8, 8, 'Category', 'category', NULL, NULL, 'Category.jpeg', '1', '1', '1', '2021-08-26 13:21:15', '2021-08-29 06:43:53', NULL, NULL, NULL),
(11, 7, 'Software', 'software', 'Software as a product (SaaP, also programming product, software product) is a product', 'yes', 'software.jpg', '1', '1', '1', '2021-08-29 09:02:17', '2022-09-12 07:48:45', 'SEASON LIFESTYLE', 'WONDERFUL LONG WEEKEND', 'Women\'s Style'),
(13, NULL, 'Food', 'food', NULL, NULL, NULL, '1', '1', NULL, '2021-09-01 05:55:38', '2021-09-01 05:55:38', NULL, NULL, NULL),
(14, NULL, 'Health Care', 'health-care', NULL, NULL, NULL, '1', '1', NULL, '2021-09-01 05:55:38', '2021-09-01 05:55:38', NULL, NULL, NULL),
(15, NULL, 'Fresh Vegetables', 'fresh-vegetables', NULL, NULL, NULL, '1', '1', NULL, '2021-09-01 05:55:38', '2021-09-01 05:55:38', NULL, NULL, NULL),
(21, NULL, 'Consumer Electric', 'consumer-electric', 'Consumer Electric', 'yes', 'consumer-electric.jpg', '1', '1', NULL, '2021-09-09 10:40:59', '2021-09-09 10:40:59', NULL, NULL, NULL),
(22, 1, 'Home Garden & Kitchen', 'home-garden-&-kitchen', 'Home Garden & Kitchen', 'yes', 'home-garden-&-kitchen.jpg', '1', '1', '1', '2021-09-09 11:25:27', '2022-09-12 08:50:46', 'Home Garden', 'Home Garden Home Garden', 'Home Garden');

-- --------------------------------------------------------

--
-- Table structure for table `ems_color`
--

CREATE TABLE `ems_color` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordering` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_color`
--

INSERT INTO `ems_color` (`id`, `name`, `slug`, `ordering`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, 'Red', 'red', '1', '1', '1', '1', '2021-09-06 06:09:41', '2021-09-06 06:09:57'),
(3, 'Black', 'black', '1', '1', '1', NULL, '2021-09-06 06:09:50', '2021-09-06 06:09:50');

-- --------------------------------------------------------

--
-- Table structure for table `ems_country`
--

CREATE TABLE `ems_country` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordering` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_country`
--

INSERT INTO `ems_country` (`id`, `name`, `slug`, `code`, `ordering`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Afghanistan', 'afghanistan', '004', '1', '1', '1', '1', '2021-08-26 07:18:23', '2021-08-29 06:43:38'),
(2, 'Australia', 'australia', '036', '1', '1', '1', '1', '2021-08-26 07:18:42', '2021-08-29 06:43:35'),
(3, 'Austria', 'austria', '040', '1', '1', '1', '1', '2021-08-26 07:18:58', '2021-08-29 06:43:32'),
(4, 'Bangladesh', 'bangladesh', '050', '1', '1', '1', '1', '2021-08-26 07:19:13', '2021-08-29 06:43:29'),
(5, 'Brazil', 'brazil', '076', '2', '1', '1', '1', '2021-08-26 07:19:30', '2021-08-29 06:43:14');

-- --------------------------------------------------------

--
-- Table structure for table `ems_customer`
--

CREATE TABLE `ems_customer` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED DEFAULT NULL,
  `first_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_country_id` bigint UNSIGNED DEFAULT NULL,
  `bill_country_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_district_id` bigint UNSIGNED DEFAULT NULL,
  `bill_district_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_upazila_id` bigint UNSIGNED DEFAULT NULL,
  `bill_upazila_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bill_street_address` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ship_country_id` bigint UNSIGNED DEFAULT NULL,
  `ship_country_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ship_district_id` bigint UNSIGNED DEFAULT NULL,
  `ship_district_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ship_upazila_id` bigint UNSIGNED DEFAULT NULL,
  `ship_upazila_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ship_street_address` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_image` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_customer`
--

INSERT INTO `ems_customer` (`id`, `user_id`, `first_name`, `last_name`, `mobile`, `email`, `bill_country_id`, `bill_country_name`, `bill_district_id`, `bill_district_name`, `bill_upazila_id`, `bill_upazila_name`, `bill_street_address`, `ship_country_id`, `ship_country_name`, `ship_district_id`, `ship_district_name`, `ship_upazila_id`, `ship_upazila_name`, `ship_street_address`, `customer_image`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, 37, 'Nice Coder', 'Raju', '01987674523', 'nice.coder3m@gmail.com', 19, 'Bangladesh', 1, 'Dhaka', 493, 'Adabor', 'uttara', 19, 'Bangladesh', 3, 'Gazipur', 160, 'Kaliakior', 'address', 'nice-coder-raju1633326324.png', '1', '1', '37', '2021-09-28 12:24:37', '2021-10-04 05:45:24'),
(7, 41, 'Aspire111', 'tss111', '0189054326711', 'rashedulraju3m@gmail.com', 19, 'Bangladesh', 1, 'Dhaka', 494, 'Airport', 'H-10,R-10,S-10,Badda,Dhaka-1230', 19, 'Bangladesh', 3, 'Gazipur', 162, 'Sripur', 'H-10,R-10,S-10,kapasia,Dhaka-1230', 'aspire1-tss1633247223.png', '1', '1', '41', '2021-09-29 05:59:11', '2021-10-03 07:47:51'),
(8, 45, '2021 Men\'s Lifestyle', 'dsdfds', '01987674521', 'rashedul.rightbrainsolution@gmail.com', 19, 'Bangladesh', 12, 'Narsingdi', 231, 'Shibpur ', 'uttara', 19, 'Bangladesh', 2, 'Faridpur', 151, 'Boalmari ', 'sdfdsfsd', '2021-men\'s-lifestyle-dsdfds1636365467.jpg', '1', NULL, '45', '2021-11-08 15:51:54', '2021-11-08 15:57:47'),
(9, 46, 'Salma Sultana', NULL, '01790860543', 'salma.sultana9811@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 'Uttara', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, '2021-11-09 11:09:12', '2021-11-09 11:09:12');

-- --------------------------------------------------------

--
-- Table structure for table `ems_delivery_time_slot`
--

CREATE TABLE `ems_delivery_time_slot` (
  `id` bigint UNSIGNED NOT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `time_slot` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordering` int DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '1 = Actine, 2 = Inactive',
  `created_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_delivery_time_slot`
--

INSERT INTO `ems_delivery_time_slot` (`id`, `start_time`, `end_time`, `time_slot`, `ordering`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, '14:00:00', '19:00:00', '14:00 - 19:00', 1, '1', '1', NULL, '2021-09-19 11:54:43', '2021-09-19 11:54:43'),
(5, '06:00:00', '10:00:00', '06:00 - 10:00', 0, '1', '1', NULL, '2021-10-10 05:08:28', '2021-10-10 05:08:28'),
(6, '10:00:00', '12:00:00', '10:00 - 12:00', 0, '1', '1', NULL, '2021-10-10 05:08:47', '2021-10-10 05:08:47'),
(7, '13:00:00', '14:00:00', '13:00 - 14:00', 0, '1', '1', NULL, '2021-10-10 05:09:20', '2021-10-10 05:09:20');

-- --------------------------------------------------------

--
-- Table structure for table `ems_discount`
--

CREATE TABLE `ems_discount` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subitem` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_image` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_discount`
--

INSERT INTO `ems_discount` (`id`, `name`, `slug`, `feature`, `subitem`, `amount`, `type`, `discount_image`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(13, 'September Offer(5)%', 'september-offer(5)%', 'yes', 'yes', '5.00', 'Percentage', 'september-offer(5).jpg', '1', '1', '1', '2021-08-31 10:43:04', '2021-08-31 10:44:18'),
(14, 'September Offer(10)%', 'september-offer(10)%', NULL, 'yes', '10.00', 'Percentage', 'september-offer(10).png', '1', '1', '1', '2021-08-31 10:46:51', '2021-08-31 10:48:15');

-- --------------------------------------------------------

--
-- Table structure for table `ems_discount_brand`
--

CREATE TABLE `ems_discount_brand` (
  `id` bigint UNSIGNED NOT NULL,
  `discount_id` bigint UNSIGNED DEFAULT NULL,
  `brand_id` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_discount_brand`
--

INSERT INTO `ems_discount_brand` (`id`, `discount_id`, `brand_id`, `created_at`, `updated_at`) VALUES
(45, 13, 2, '2021-08-31 10:44:49', '2021-08-31 10:44:49'),
(63, 14, 25, '2021-11-09 12:19:31', '2021-11-09 12:19:31'),
(64, 14, 2, '2021-11-09 12:19:31', '2021-11-09 12:19:31'),
(65, 14, 1, '2021-11-09 12:19:31', '2021-11-09 12:19:31'),
(66, 14, 28, '2021-11-09 12:19:31', '2021-11-09 12:19:31'),
(67, 14, 4, '2021-11-09 12:19:31', '2021-11-09 12:19:31'),
(68, 14, 26, '2021-11-09 12:19:31', '2021-11-09 12:19:31');

-- --------------------------------------------------------

--
-- Table structure for table `ems_discount_category`
--

CREATE TABLE `ems_discount_category` (
  `id` bigint UNSIGNED NOT NULL,
  `discount_id` bigint UNSIGNED DEFAULT NULL,
  `category_id` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_discount_category`
--

INSERT INTO `ems_discount_category` (`id`, `discount_id`, `category_id`, `created_at`, `updated_at`) VALUES
(45, 13, 7, '2021-08-31 10:44:49', '2021-08-31 10:44:49'),
(46, 13, 11, '2021-08-31 10:44:49', '2021-08-31 10:44:49'),
(52, 14, 2, '2021-11-09 12:19:31', '2021-11-09 12:19:31');

-- --------------------------------------------------------

--
-- Table structure for table `ems_generalpage`
--

CREATE TABLE `ems_generalpage` (
  `id` bigint UNSIGNED NOT NULL,
  `group` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` varchar(5000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_generalpage`
--

INSERT INTO `ems_generalpage` (`id`, `group`, `title`, `slug`, `content`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, 'company', 'About Us', 'about-us', '<h2>What is Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&#39;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', '1', '1', NULL, '2021-11-02 09:49:24', '2021-11-02 09:49:24'),
(4, 'company', 'Career', 'career', '<h2>Where does it come from?</h2>\r\n\r\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>\r\n\r\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from &quot;de Finibus Bonorum et Malorum&quot; by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', '1', '1', NULL, '2021-11-02 09:50:08', '2021-11-02 09:50:08'),
(5, 'myaccount', 'Privacy Policy', 'privacy-policy', '<h3>The standard Lorem Ipsum passage, used since the 1500s</h3>\r\n\r\n<p>&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.&quot;</p>\r\n\r\n<h3>Section 1.10.32 of &quot;de Finibus Bonorum et Malorum&quot;, written by Cicero in 45 BC</h3>\r\n\r\n<p>&quot;Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?&quot;</p>', '1', '1', NULL, '2021-11-02 09:59:16', '2021-11-02 09:59:16'),
(6, 'customerservice', '24 Our Service', '24-our-service', '<h3>1914 translation by H. Rackham</h3>\r\n\r\n<p>&quot;But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?&quot;</p>\r\n\r\n<h3>Section 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot;, written by Cicero in 45 BC</h3>\r\n\r\n<p>&quot;At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.&quot;</p>', '1', '1', NULL, '2021-11-02 10:05:24', '2021-11-02 10:05:24'),
(7, 'support', 'Free Shipping & Returns', 'free-shipping-&-returns', '<p>For all orders over $99</p>', '1', '1', '1', '2021-11-02 10:06:05', '2021-11-02 10:33:36'),
(8, 'support', 'Secure Payment', 'secure-payment', '<p>We ensure secure payment</p>', '1', '1', '1', '2021-11-02 10:18:40', '2021-11-02 10:39:35'),
(9, 'support', 'Money Back Guarantee', 'money-back-guarantee', 'Any back within 30 days', '1', '1', '1', '2021-11-02 10:19:01', '2021-11-02 10:39:12'),
(10, 'support', 'Customer Support', 'customer-support', 'Call or email us 24/7', '1', '1', '1', '2021-11-02 10:19:18', '2021-11-02 10:38:45'),
(12, 'company', 'Team Member', 'team-member', '<p><a href=\"http://139.180.144.205:81/#\">Team Member</a></p>', '1', '1', NULL, '2021-11-02 10:54:20', '2021-11-02 10:54:20'),
(13, 'company', 'Contact Us', 'contact-us', '<section class=\"contact-information-section mb-10\">\r\n                        <div class=\"owl-carousel owl-theme owl-loaded owl-drag\" data-owl-options=\"{\r\n                        \'items\': 4,\r\n                        \'nav\': false,\r\n                        \'dots\': false,\r\n                        \'loop\': false,\r\n                        \'margin\': 20,\r\n                        \'responsive\': {\r\n                            \'0\': {\r\n                                \'items\': 1\r\n                            },\r\n                            \'480\': {\r\n                                \'items\': 2\r\n                            },\r\n                            \'768\': {\r\n                                \'items\': 3\r\n                            },\r\n                            \'992\': {\r\n                                \'items\': 4\r\n                            }\r\n                        }\r\n                    }\">\r\n                            \r\n                            \r\n                            \r\n                            \r\n                        <div class=\"owl-stage-outer\"><div class=\"owl-stage\" style=\"transform: translate3d(0px, 0px, 0px); transition: all 0.7s ease 0s; width: 1218px;\"><div class=\"owl-item active\" style=\"width: 284.5px; margin-right: 20px;\"><div class=\"icon-box text-center icon-box-primary\">\r\n                                <span class=\"icon-box-icon icon-email\">\r\n                                    <i class=\"w-icon-envelop-closed\"></i>\r\n                                </span>\r\n                                <div class=\"icon-box-content\">\r\n                                    <h4 class=\"icon-box-title\">E-mail Address</h4>\r\n                                    <p>mail@example.com</p>\r\n                                </div>\r\n                            </div></div><div class=\"owl-item active\" style=\"width: 284.5px; margin-right: 20px;\"><div class=\"icon-box text-center icon-box-primary\">\r\n                                <span class=\"icon-box-icon icon-headphone\">\r\n                                    <i class=\"w-icon-headphone\"></i>\r\n                                </span>\r\n                                <div class=\"icon-box-content\">\r\n                                    <h4 class=\"icon-box-title\">Phone Number</h4>\r\n                                    <p>(123) 456-7890 / (123) 456-9870</p>\r\n                                </div>\r\n                            </div></div><div class=\"owl-item active\" style=\"width: 284.5px; margin-right: 20px;\"><div class=\"icon-box text-center icon-box-primary\">\r\n                                <span class=\"icon-box-icon icon-map-marker\">\r\n                                    <i class=\"w-icon-map-marker\"></i>\r\n                                </span>\r\n                                <div class=\"icon-box-content\">\r\n                                    <h4 class=\"icon-box-title\">Address</h4>\r\n                                    <p>Lawrence, NY 11345, USA</p>\r\n                                </div>\r\n                            </div></div><div class=\"owl-item active\" style=\"width: 284.5px; margin-right: 20px;\"><div class=\"icon-box text-center icon-box-primary\">\r\n                                <span class=\"icon-box-icon icon-fax\">\r\n                                    <i class=\"w-icon-fax\"></i>\r\n                                </span>\r\n                                <div class=\"icon-box-content\">\r\n                                    <h4 class=\"icon-box-title\">Fax</h4>\r\n                                    <p>1-800-570-7777</p>\r\n                                </div>\r\n                            </div></div></div></div><div class=\"owl-nav disabled\"><button type=\"button\" role=\"presentation\" class=\"owl-prev\"><i class=\"w-icon-angle-left\"></i></button><button type=\"button\" role=\"presentation\" class=\"owl-next\"><i class=\"w-icon-angle-right\"></i></button></div><div class=\"owl-dots disabled\"></div><div class=\"owl-nav disabled\"><button type=\"button\" role=\"presentation\" class=\"owl-prev\"><i class=\"w-icon-angle-left\"></i></button><button type=\"button\" role=\"presentation\" class=\"owl-next\"><i class=\"w-icon-angle-right\"></i></button></div><div class=\"owl-dots disabled\"></div></div>\r\n                    </section>', '1', '1', '1', '2021-11-02 10:54:40', '2021-11-03 11:52:06'),
(14, 'company', 'Affilate', 'affilate', '<p><a href=\"http://139.180.144.205:81/#\">Affilate</a></p>', '1', '1', NULL, '2021-11-02 10:54:54', '2021-11-02 10:54:54'),
(15, 'company', 'Payment Methods', 'payment-methods', '<p>If you&#39;ve never written a company profile before, your first time can be a little intimidating. A company profile&#39;s purpose, after all, is not just to include basic details about the brand. It must also highlight the strengths of your company clearly and confidently. Consider it to be something akin to your company&#39;s resume.</p>', '1', '1', '1', '2021-11-02 10:55:46', '2022-09-11 11:24:18'),
(16, 'myaccount', 'Track My Order', 'track-my-order', '<p><a href=\"http://139.180.144.205:81/web-customer-acclogin#\">Track My Order</a></p>', '1', '1', NULL, '2021-11-02 11:18:46', '2021-11-02 11:18:46'),
(17, 'myaccount', 'View Cart', 'view-cart', '<p><a href=\"http://139.180.144.205:81/cart.html\">View Cart</a></p>', '1', '1', NULL, '2021-11-02 11:19:03', '2021-11-02 11:19:03'),
(18, 'myaccount', 'Sign In', 'sign-in', '<p><a href=\"http://139.180.144.205:81/login.html\">Sign In</a></p>', '1', '1', NULL, '2021-11-02 11:19:17', '2021-11-02 11:19:17'),
(19, 'myaccount', 'Help', 'help', '<p><a href=\"http://139.180.144.205:81/web-customer-acclogin#\">Help</a></p>', '1', '1', NULL, '2021-11-02 11:19:31', '2021-11-02 11:19:31'),
(20, 'myaccount', 'My Wishlist', 'my-wishlist', '<p><a href=\"http://139.180.144.205:81/wishlist.html\">My Wishlist</a></p>', '1', '1', NULL, '2021-11-02 11:20:21', '2021-11-02 11:20:21'),
(21, 'customerservice', 'Term and Conditions', 'term-and-conditions', '<p><a href=\"http://139.180.144.205:81/web-customer-acclogin#\">Term and Conditions</a></p>', '1', '1', NULL, '2021-11-02 11:21:12', '2021-11-02 11:21:12'),
(22, 'customerservice', 'Shipping', 'shipping', '<p><a href=\"http://139.180.144.205:81/web-customer-acclogin#\">Shipping</a></p>', '1', '1', NULL, '2021-11-02 11:21:28', '2021-11-02 11:21:28'),
(23, 'customerservice', 'Support Center', 'support-center', '<p>&nbsp;</p>\r\n\r\n<h3>Safe and reliable repairs</h3>\r\n\r\n<p>At Apple, every product we make is built to last. We design durable, easy-to-use devices with innovative features that customers depend on, all while protecting their privacy and data. Customers should have access to safe, reliable, and secure repairs with genuine Apple parts if they need them.</p>\r\n\r\n<p>Learn more about Apple&rsquo;s approach to expanding access to safe and reliable repairs.</p>\r\n\r\n<p>&nbsp;</p>', '1', '1', '1', '2021-11-02 11:21:41', '2022-09-11 07:58:19'),
(24, 'customerservice', 'Product Returns', 'product-returns', '<p><a href=\"http://139.180.144.205:81/web-customer-acclogin#\">Product Returns</a></p>', '1', '1', NULL, '2021-11-02 11:21:57', '2021-11-02 11:21:57'),
(25, 'customerservice', 'Money-back guarantee!', 'money-back-guarantee!', '<p><a href=\"http://139.180.144.205:81/web-customer-acclogin#\">Money-back guarantee!</a></p>', '1', '1', NULL, '2021-11-02 11:22:09', '2021-11-02 11:22:09');

-- --------------------------------------------------------

--
-- Table structure for table `ems_general_setting`
--

CREATE TABLE `ems_general_setting` (
  `id` bigint UNSIGNED NOT NULL,
  `slider_limit` tinyint DEFAULT NULL,
  `hp_banner_limit` tinyint DEFAULT NULL,
  `service_limit` tinyint DEFAULT NULL,
  `hp_brand_limit` tinyint DEFAULT NULL,
  `hp_category_product_limit` tinyint DEFAULT NULL,
  `hp_top_weakly_vendor_limit` tinyint DEFAULT NULL,
  `best_seller_product_limit` tinyint DEFAULT NULL,
  `deals_hot_product_limit` tinyint DEFAULT NULL,
  `recommend_product_limit` tinyint DEFAULT NULL,
  `best_seller_random_order` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'In Random Order = 1, Not Random Order = 0',
  `hot_deals_random_order` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'In Random Order = 1, Not Random Order = 0',
  `recommend_random_order` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'In Random Order = 1, Not Random Order = 0',
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_general_setting`
--

INSERT INTO `ems_general_setting` (`id`, `slider_limit`, `hp_banner_limit`, `service_limit`, `hp_brand_limit`, `hp_category_product_limit`, `hp_top_weakly_vendor_limit`, `best_seller_product_limit`, `deals_hot_product_limit`, `recommend_product_limit`, `best_seller_random_order`, `hot_deals_random_order`, `recommend_random_order`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`, `company_name`, `mobile`, `email`, `description`, `facebook`, `youtube`, `twitter`, `instagram`) VALUES
(3, 1, 2, 3, 4, 5, 6, 7, 8, 9, '0', '0', '0', '1', '1', '1', '2021-09-23 12:22:16', '2022-09-11 11:04:36', 'Supermart', '01700000000', 'supermartbd@gmail.com', 'If you\'ve never written a company profile before, your first time can be a little intimidating.', 'https://www.facebook.com/', 'https://www.youtube.com/', 'https://www.twitter.com/', 'https://www.instagram.com/');

-- --------------------------------------------------------

--
-- Table structure for table `ems_itemassurance`
--

CREATE TABLE `ems_itemassurance` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordering` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_itemassurance`
--

INSERT INTO `ems_itemassurance` (`id`, `name`, `slug`, `ordering`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(4, '1 Month', '1-month', '1', '1', '1', NULL, '2021-08-29 07:20:57', '2021-08-29 07:20:57'),
(5, '1 Year', '1-year', '1', '1', '1', NULL, '2021-08-29 07:21:15', '2021-08-29 07:21:15'),
(6, '1 Year 3 Month', '1-year-3-month', '2', '1', '1', NULL, '2021-08-29 07:21:46', '2021-08-29 07:21:46');

-- --------------------------------------------------------

--
-- Table structure for table `ems_itemunit`
--

CREATE TABLE `ems_itemunit` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordering` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_itemunit`
--

INSERT INTO `ems_itemunit` (`id`, `name`, `slug`, `ordering`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Bag', 'bag', '1', '1', '1', '1', '2021-08-26 06:45:09', '2021-08-29 06:41:26'),
(3, 'Can', 'can', '2', '1', '1', '1', '2021-08-26 06:45:33', '2021-08-29 06:41:21'),
(4, 'Carton', 'carton', '1', '1', '1', '1', '2021-08-26 06:45:45', '2021-08-29 06:41:18'),
(5, 'Coil', 'coil', '2', '1', '1', '1', '2021-08-26 06:45:59', '2021-08-29 06:41:15'),
(10, 'pc', 'pc', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(11, 'pcs', 'pcs', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(12, 'ml', 'ml', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(13, 'ltr', 'ltr', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(14, 'gm', 'gm', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(16, 'pot', 'pot', '3', '1', '1', NULL, '2021-11-02 13:11:18', '2021-11-02 13:11:18');

-- --------------------------------------------------------

--
-- Table structure for table `ems_order_billing_shipping`
--

CREATE TABLE `ems_order_billing_shipping` (
  `id` bigint UNSIGNED NOT NULL,
  `order_id` bigint UNSIGNED DEFAULT NULL,
  `customer_id` bigint UNSIGNED DEFAULT NULL,
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `firstname` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_order_billing_shipping`
--

INSERT INTO `ems_order_billing_shipping` (`id`, `order_id`, `customer_id`, `type`, `firstname`, `lastname`, `country`, `district`, `address`, `phone`, `email`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(33, 21, NULL, 'billing', 'Rashedul', 'Raju', 'Bangladesh', 'Dhaka', 'H-10,R-10,S-10,Uttara,Dhaka-1230', '01729762344', 'rbraju3m@gmail.com', NULL, NULL, '2021-10-03 09:00:02', '2021-10-03 09:00:02'),
(34, 21, NULL, 'shipping', 'Rashedul', 'Raju', 'Bangladesh', 'Dhaka', 'H-10,R-10,S-10,Uttara,Dhaka-1230', '01729762344', 'rbraju3m@gmail.com', NULL, NULL, '2021-10-03 09:00:02', '2021-10-03 09:00:02'),
(35, 22, 3, 'billing', 'Nice Coder', 'Raju', 'Bangladesh', 'Dhaka', 'uttara', '01987674523', 'nice.coder3m@gmail.com', NULL, NULL, '2021-10-03 09:02:02', '2021-10-03 09:02:02'),
(36, 22, 3, 'shipping', 'Nice Coder', 'Raju', 'Bangladesh', 'Dhaka', 'uttara', '01987674523', 'nice.coder3m@gmail.com', NULL, NULL, '2021-10-03 09:02:02', '2021-10-03 09:02:02'),
(37, 23, NULL, 'billing', 'Md. Test', 'test', 'Bangladesh', 'Dhaka', 'H-10,R-10,S-10,Uttara,Dhaka-1230', '01765789087', 'raju.aspiretss@gmail.com', NULL, NULL, '2021-11-02 10:43:37', '2021-11-02 10:43:37'),
(38, 23, NULL, 'shipping', 'Md. Test', 'test', 'Bangladesh', 'Dhaka', 'H-10,R-10,S-10,Uttara,Dhaka-1230', '01765789087', 'raju.aspiretss@gmail.com', NULL, NULL, '2021-11-02 10:43:37', '2021-11-02 10:43:37'),
(39, 24, NULL, 'billing', 'Md. Test', 'test', 'Bangladesh', 'Dhaka', 'tets', '01729762344', 'rbraju3m@gmail.com', NULL, NULL, '2021-11-08 16:39:15', '2021-11-08 16:39:15'),
(40, 24, NULL, 'shipping', 'Md. Test', 'test', 'Bangladesh', 'Dhaka', 'tets', '01729762344', 'rbraju3m@gmail.com', NULL, NULL, '2021-11-08 16:39:15', '2021-11-08 16:39:15'),
(41, 25, 9, 'billing', 'Salma', 'Sultana', 'Bangladesh', 'Dhaka', 'Uttara', '01790860543', 'salma.sultana9811@gmail.com', NULL, NULL, '2021-11-09 17:44:02', '2021-11-09 17:44:02'),
(42, 25, 9, 'shipping', 'Salma', 'Sultana', 'Bangladesh', 'Dhaka', 'Uttara', '01790860543', 'salma.sultana9811@gmail.com', NULL, NULL, '2021-11-09 17:44:02', '2021-11-09 17:44:02'),
(43, 26, 9, 'billing', 'Salma', 'Sultana', 'Bangladesh', 'Dhaka', 'Uttara', '01790860543', 'salma.sultana9811@gmail.com', NULL, NULL, '2021-11-09 17:48:04', '2021-11-09 17:48:04'),
(44, 26, 9, 'shipping', 'Salma', 'Sultana', 'Bangladesh', 'Dhaka', 'Uttara', '01790860543', 'salma.sultana9811@gmail.com', NULL, NULL, '2021-11-09 17:48:04', '2021-11-09 17:48:04'),
(45, 27, 9, 'billing', 'Salma', 'Sultana', 'Bangladesh', 'Dhaka', 'Uttara', '01790860543', 'salma.sultana9811@gmail.com', NULL, NULL, '2021-11-10 13:51:51', '2021-11-10 13:51:51'),
(46, 27, 9, 'shipping', 'Salma', 'Sultana', 'Bangladesh', 'Dhaka', 'Uttara', '01790860543', 'salma.sultana9811@gmail.com', NULL, NULL, '2021-11-10 13:51:51', '2021-11-10 13:51:51'),
(47, 28, 9, 'billing', 'Salma', 'Sultana', 'Bangladesh', 'Dhaka', 'Uttara', '01790860543', 'salma.sultana9811@gmail.com', NULL, NULL, '2021-11-10 13:51:51', '2021-11-10 13:51:51'),
(48, 28, 9, 'shipping', 'Salma', 'Sultana', 'Bangladesh', 'Dhaka', 'Uttara', '01790860543', 'salma.sultana9811@gmail.com', NULL, NULL, '2021-11-10 13:51:51', '2021-11-10 13:51:51'),
(49, 29, 9, 'billing', 'Salma', 'Sultana', 'Bangladesh', 'Dhaka', 'Uttara', '01790860543', 'salma.sultana9811@gmail.com', NULL, NULL, '2021-11-10 13:51:52', '2021-11-10 13:51:52'),
(50, 29, 9, 'shipping', 'Salma', 'Sultana', 'Bangladesh', 'Dhaka', 'Uttara', '01790860543', 'salma.sultana9811@gmail.com', NULL, NULL, '2021-11-10 13:51:52', '2021-11-10 13:51:52'),
(51, 30, 9, 'billing', 'Salma', 'Sultana', 'Bangladesh', 'Dhaka', 'Uttara', '01790860543', 'salma.sultana9811@gmail.com', NULL, NULL, '2021-11-10 13:51:52', '2021-11-10 13:51:52'),
(52, 30, 9, 'shipping', 'Salma', 'Sultana', 'Bangladesh', 'Dhaka', 'Uttara', '01790860543', 'salma.sultana9811@gmail.com', NULL, NULL, '2021-11-10 13:51:52', '2021-11-10 13:51:52'),
(53, 31, 9, 'billing', 'Salma', 'Sultana', 'Bangladesh', 'Dhaka', 'Uttara', '01790860543', 'salma.sultana9811@gmail.com', NULL, NULL, '2021-11-10 13:51:52', '2021-11-10 13:51:52'),
(54, 31, 9, 'shipping', 'Salma', 'Sultana', 'Bangladesh', 'Dhaka', 'Uttara', '01790860543', 'salma.sultana9811@gmail.com', NULL, NULL, '2021-11-10 13:51:52', '2021-11-10 13:51:52'),
(55, 32, NULL, 'billing', 'Nahid', 'Jaman', 'Bangladesh', 'Dhaka', 'Ggg', '5555654465', 'nahidzzaman21@gmail.com', NULL, NULL, '2022-02-19 10:54:42', '2022-02-19 10:54:42'),
(56, 32, NULL, 'shipping', 'Nahid', 'Jaman', 'Bangladesh', 'Dhaka', 'Ggg', '5555654465', 'nahidzzaman21@gmail.com', NULL, NULL, '2022-02-19 10:54:42', '2022-02-19 10:54:42'),
(57, 33, NULL, 'billing', 'test', 'tesu', 'Bangladesh', 'Dhaka', '11', '01729762344', 'rbraju3m@gmail.com', NULL, NULL, '2022-03-25 01:50:53', '2022-03-25 01:50:53'),
(58, 33, NULL, 'shipping', 'test', 'tesu', 'Bangladesh', 'Dhaka', '11', '01729762344', 'rbraju3m@gmail.com', NULL, NULL, '2022-03-25 01:50:53', '2022-03-25 01:50:53'),
(59, 34, NULL, 'billing', 'test', 'tesu', 'Bangladesh', 'Dhaka', '11', '01729762344', 'rbraju3m@gmail.com', NULL, NULL, '2022-03-25 01:52:41', '2022-03-25 01:52:41'),
(60, 34, NULL, 'shipping', 'test', 'tesu', 'Bangladesh', 'Dhaka', '11', '01729762344', 'rbraju3m@gmail.com', NULL, NULL, '2022-03-25 01:52:41', '2022-03-25 01:52:41'),
(61, 35, NULL, 'billing', 'Moon', 'Sarker', 'Bangladesh', 'Dhaka', 'Test', '01744692526', 'msarkar74388@gmail.com', NULL, NULL, '2022-04-06 20:04:54', '2022-04-06 20:04:54'),
(62, 35, NULL, 'shipping', 'Moon', 'Sarker', 'Bangladesh', 'Dhaka', 'Test', '01744692526', 'msarkar74388@gmail.com', NULL, NULL, '2022-04-06 20:04:54', '2022-04-06 20:04:54'),
(63, 36, NULL, 'billing', 'Moon', 'Sarker', 'Bangladesh', 'Dhaka', 'Test', '01744692526', 'msarkar74388@gmail.com', NULL, NULL, '2022-04-06 20:06:04', '2022-04-06 20:06:04'),
(64, 36, NULL, 'shipping', 'Moon', 'Sarker', 'Bangladesh', 'Dhaka', 'Test', '01744692526', 'msarkar74388@gmail.com', NULL, NULL, '2022-04-06 20:06:04', '2022-04-06 20:06:04'),
(65, 37, NULL, 'billing', 'Moon', 'Sarker', 'Bangladesh', 'Dhaka', 'Test', '01744692526', 'msarkar74388@gmail.com', NULL, NULL, '2022-04-06 20:07:19', '2022-04-06 20:07:19'),
(66, 37, NULL, 'shipping', 'Moon', 'Sarker', 'Bangladesh', 'Dhaka', 'Test', '01744692526', 'msarkar74388@gmail.com', NULL, NULL, '2022-04-06 20:07:19', '2022-04-06 20:07:19'),
(67, 38, NULL, 'billing', 'Moon', 'Sarker', 'Bangladesh', 'Dhaka', 'Test', '01744692526', 'msarkar74388@gmail.com', NULL, NULL, '2022-04-06 20:09:05', '2022-04-06 20:09:05'),
(68, 38, NULL, 'shipping', 'Moon', 'Sarker', 'Bangladesh', 'Dhaka', 'Test', '01744692526', 'msarkar74388@gmail.com', NULL, NULL, '2022-04-06 20:09:05', '2022-04-06 20:09:05'),
(69, 39, NULL, 'billing', 'rrr', 'rrrr', 'Bangladesh', 'Dhaka', 'rr', '01729762344', 'raju.rightbrainsolution@gmail.com', NULL, NULL, '2022-06-22 11:26:28', '2022-06-22 11:26:28'),
(70, 39, NULL, 'shipping', 'rrr', 'rrrr', 'Bangladesh', 'Dhaka', 'rr', '01729762344', 'raju.rightbrainsolution@gmail.com', NULL, NULL, '2022-06-22 11:26:28', '2022-06-22 11:26:28'),
(71, 40, NULL, 'billing', 'rrr', 'rrrr', 'Bangladesh', 'Dhaka', 'hgy', '01729762344', 'raju.rightbrainsolution@gmail.com', NULL, NULL, '2022-08-24 05:16:04', '2022-08-24 05:16:04'),
(72, 40, NULL, 'shipping', 'rrr', 'rrrr', 'Bangladesh', 'Dhaka', 'hgy', '01729762344', 'raju.rightbrainsolution@gmail.com', NULL, NULL, '2022-08-24 05:16:04', '2022-08-24 05:16:04'),
(73, 41, NULL, 'billing', 'Kobir', 'Ahmed', 'Bangladesh', 'Dhaka', '1', '01700000000', 'raju.rightbrainsolution@gmail.com', NULL, NULL, '2022-09-12 04:40:50', '2022-09-12 04:40:50'),
(74, 41, NULL, 'shipping', 'Kobir', 'Ahmed', 'Bangladesh', 'Dhaka', '1', '01700000000', 'raju.rightbrainsolution@gmail.com', NULL, NULL, '2022-09-12 04:40:50', '2022-09-12 04:40:50'),
(75, 42, NULL, 'billing', 'Ahmad Imtiaz', 'Bulbul', 'Bangladesh', 'Dhaka', 'Sector 14 , Uttara , Dhaka', '01700909090', 'raju.rightbrainsolution@gmail.com', NULL, NULL, '2023-01-30 08:01:50', '2023-01-30 08:01:50'),
(76, 42, NULL, 'shipping', 'Ahmad Imtiaz', 'Bulbul', 'Bangladesh', 'Dhaka', 'Sector 14 , Uttara , Dhaka', '01700909090', 'raju.rightbrainsolution@gmail.com', NULL, NULL, '2023-01-30 08:01:50', '2023-01-30 08:01:50'),
(77, 43, NULL, 'billing', 'Kobir', 'Ahmed', 'Bangladesh', 'Dhaka', 'Uttara', '01700000000', 'raju.rightbrainsolution@gmail.com', NULL, NULL, '2023-07-30 05:06:56', '2023-07-30 05:06:56'),
(78, 43, NULL, 'shipping', 'Kobir', 'Ahmed', 'Bangladesh', 'Dhaka', 'Uttara', '01700000000', 'raju.rightbrainsolution@gmail.com', NULL, NULL, '2023-07-30 05:06:56', '2023-07-30 05:06:56');

-- --------------------------------------------------------

--
-- Table structure for table `ems_order_head`
--

CREATE TABLE `ems_order_head` (
  `id` bigint UNSIGNED NOT NULL,
  `order_number` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_slot_id` bigint UNSIGNED DEFAULT NULL,
  `shipping_time_slot` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` bigint UNSIGNED DEFAULT NULL,
  `order_notes` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_method` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_quantity` int DEFAULT NULL,
  `product_amount` decimal(10,2) DEFAULT NULL,
  `delivery_charge` decimal(10,2) DEFAULT NULL,
  `total_amount` decimal(10,2) DEFAULT NULL,
  `status` enum('1','2','3','4','5') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '1 = Process, 2 = Confirm, 3 = Pending, 4 = Approved, 5 = Return',
  `created_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_order_head`
--

INSERT INTO `ems_order_head` (`id`, `order_number`, `time_slot_id`, `shipping_time_slot`, `vendor_id`, `user_id`, `customer_id`, `order_notes`, `payment_method`, `total_quantity`, `product_amount`, `delivery_charge`, `total_amount`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(21, 'SMBD-20211003-000021', 3, '14:00 - 19:00', '4,1,2', '27,24,25', NULL, 'Order notes (optional)', 'Cash on delivery', 5, '1960.00', '60.00', '2020.00', '1', NULL, NULL, '2021-10-03 09:00:02', '2021-10-03 09:00:02'),
(22, 'SMBD-20211003-000022', 3, '14:00 - 19:00', '2,1', '25,24', 3, NULL, 'Cash on delivery', 2, '220.00', '60.00', '280.00', '1', NULL, NULL, '2021-10-03 09:02:02', '2021-10-03 09:02:02'),
(23, 'SMBD-20211102-000023', 5, '06:00 - 10:00', '2,2,4', '25,25,27', NULL, 'very argent', 'Cash on delivery', 3, '1120.00', '60.00', '1180.00', '1', NULL, NULL, '2021-11-02 10:43:37', '2021-11-02 10:43:37'),
(24, 'SMBD-20211108-000024', 5, '06:00 - 10:00', '1,1,3', '24,24,26', NULL, 'very emergency', 'Cash on delivery', 3, '2420.00', '60.00', '2480.00', '1', NULL, NULL, '2021-11-08 16:39:15', '2021-11-08 16:39:15'),
(25, 'SMBD-20211109-000025', 5, '06:00 - 10:00', '2,4', '25,27', 9, NULL, 'Cash on delivery', 3, '600.00', '60.00', '660.00', '1', NULL, NULL, '2021-11-09 17:44:02', '2021-11-09 17:44:02'),
(26, 'SMBD-20211109-000026', 5, '06:00 - 10:00', '2', '25', 9, NULL, 'Cash on delivery', 1, '200.00', '60.00', '260.00', '1', NULL, NULL, '2021-11-09 17:48:04', '2021-11-09 17:48:04'),
(27, 'SMBD-20211110-000027', 5, '06:00 - 10:00', '2', '25', 9, NULL, 'Cash on delivery', 1, '120.00', '60.00', '180.00', '1', NULL, NULL, '2021-11-10 13:51:51', '2021-11-10 13:51:51'),
(28, 'SMBD-20211110-000028', 5, '06:00 - 10:00', '2', '25', 9, NULL, 'Cash on delivery', 1, '120.00', '60.00', '180.00', '1', NULL, NULL, '2021-11-10 13:51:51', '2021-11-10 13:51:51'),
(29, 'SMBD-20211110-000029', 5, '06:00 - 10:00', '2', '25', 9, NULL, 'Cash on delivery', 1, '120.00', '60.00', '180.00', '1', NULL, NULL, '2021-11-10 13:51:52', '2021-11-10 13:51:52'),
(30, 'SMBD-20211110-000030', 5, '06:00 - 10:00', '2', '25', 9, NULL, 'Cash on delivery', 1, '120.00', '60.00', '180.00', '1', NULL, NULL, '2021-11-10 13:51:52', '2021-11-10 13:51:52'),
(31, 'SMBD-20211110-000031', 5, '06:00 - 10:00', '2', '25', 9, NULL, 'Cash on delivery', 1, '120.00', '60.00', '180.00', '1', NULL, NULL, '2021-11-10 13:51:52', '2021-11-10 13:51:52'),
(32, 'SMBD-20220219-000032', 6, '10:00 - 12:00', '4,4', '27,27', NULL, NULL, 'Cash on delivery', 4, '1800.00', '60.00', '1860.00', '1', NULL, NULL, '2022-02-19 10:54:42', '2022-02-19 10:54:42'),
(33, 'SMBD-20220325-000033', 5, '06:00 - 10:00', '4,3,3', '27,26,26', NULL, NULL, 'Cash on delivery', 3, '2830.00', '60.00', '2890.00', '1', NULL, NULL, '2022-03-25 01:50:53', '2022-03-25 01:50:53'),
(34, 'SMBD-20220325-000034', 5, '06:00 - 10:00', '4,3,3', '27,26,26', NULL, NULL, 'Cash on delivery', 3, '2830.00', '60.00', '2890.00', '1', NULL, NULL, '2022-03-25 01:52:41', '2022-03-25 01:52:41'),
(35, 'SMBD-20220406-000035', 6, '10:00 - 12:00', '4', '27', NULL, NULL, 'Cash on delivery', 1, '200.00', '60.00', '260.00', '1', NULL, NULL, '2022-04-06 20:04:54', '2022-04-06 20:04:54'),
(36, 'SMBD-20220406-000036', 6, '10:00 - 12:00', '4', '27', NULL, NULL, 'Cash on delivery', 1, '200.00', '60.00', '260.00', '1', NULL, NULL, '2022-04-06 20:06:04', '2022-04-06 20:06:04'),
(37, 'SMBD-20220406-000037', 6, '10:00 - 12:00', '4', '27', NULL, NULL, 'Cash on delivery', 1, '200.00', '60.00', '260.00', '1', NULL, NULL, '2022-04-06 20:07:19', '2022-04-06 20:07:19'),
(38, 'SMBD-20220406-000038', 7, '13:00 - 14:00', '4', '27', NULL, NULL, 'Cash on delivery', 1, '700.00', '60.00', '760.00', '1', NULL, NULL, '2022-04-06 20:09:05', '2022-04-06 20:09:05'),
(39, 'SMBD-20220622-000039', 6, '10:00 - 12:00', '4,4', '27,27', NULL, NULL, 'Cash on delivery', 3, '1100.00', '60.00', '1160.00', '1', NULL, NULL, '2022-06-22 11:26:28', '2022-06-22 11:26:28'),
(40, 'SMBD-20220824-000040', 6, '10:00 - 12:00', '3', '26', NULL, NULL, 'Cash on delivery', 1, '300.00', '60.00', '360.00', '1', NULL, NULL, '2022-08-24 05:16:04', '2022-08-24 05:16:04'),
(41, 'SMBD-20220912-000041', 6, '10:00 - 12:00', '1', '24', NULL, NULL, 'Cash on delivery', 1, '1500.00', '60.00', '1560.00', '1', NULL, NULL, '2022-09-12 04:40:50', '2022-09-12 04:40:50'),
(42, 'SMBD-20230130-000042', 7, '13:00 - 14:00', '2,4', '25,27', NULL, 'Very Emergency', 'Cash on delivery', 3, '700.00', '60.00', '760.00', '1', NULL, NULL, '2023-01-30 08:01:50', '2023-01-30 08:01:50'),
(43, 'SMBD-20230730-000043', 5, '06:00 - 10:00', '1', '24', NULL, NULL, 'Cash on delivery', 3, '4500.00', '60.00', '4560.00', '1', NULL, NULL, '2023-07-30 05:06:56', '2023-07-30 05:06:56');

-- --------------------------------------------------------

--
-- Table structure for table `ems_order_item`
--

CREATE TABLE `ems_order_item` (
  `id` bigint UNSIGNED NOT NULL,
  `order_id` bigint UNSIGNED DEFAULT NULL,
  `product_id` bigint UNSIGNED DEFAULT NULL,
  `vendor_id` bigint UNSIGNED DEFAULT NULL,
  `user_id` bigint UNSIGNED DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` bigint DEFAULT NULL,
  `unit_price` decimal(10,2) DEFAULT NULL,
  `subtotal` decimal(10,2) DEFAULT NULL,
  `created_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_order_item`
--

INSERT INTO `ems_order_item` (`id`, `order_id`, `product_id`, `vendor_id`, `user_id`, `name`, `slug`, `quantity`, `unit_price`, `subtotal`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(33, 21, 9, 4, 27, 'Red Cap Sound Marker', 'red-cap-sound-marker', 2, '700.00', '1400.00', NULL, NULL, '2021-10-03 09:00:02', '2021-10-03 09:00:02'),
(34, 21, 10, 1, 24, 'Badhacopi Pata', 'badhacopi-pata', 2, '130.00', '260.00', NULL, NULL, '2021-10-03 09:00:02', '2021-10-03 09:00:02'),
(35, 21, 8, 2, 25, 'Whiskas Adult Cat Food Tuna', 'whiskas-adult-cat-food-tuna', 1, '300.00', '300.00', NULL, NULL, '2021-10-03 09:00:02', '2021-10-03 09:00:02'),
(37, 22, 14, 1, 24, 'Bluetooth Music Recorder', 'bluetooth-music-recorder', 1, '120.00', '120.00', NULL, NULL, '2021-10-03 09:02:02', '2021-10-03 09:02:02'),
(38, 23, 7, 2, 25, 'Pur Feeding Bottle', 'pur-feeding-bottle', 1, '120.00', '120.00', NULL, NULL, '2021-11-02 10:43:37', '2021-11-02 10:43:37'),
(39, 23, 8, 2, 25, 'Whiskas Adult Cat Food Tuna', 'whiskas-adult-cat-food-tuna', 1, '300.00', '300.00', NULL, NULL, '2021-11-02 10:43:37', '2021-11-02 10:43:37'),
(40, 23, 16, 4, 27, 'Bright Light', 'bright-light', 1, '700.00', '700.00', NULL, NULL, '2021-11-02 10:43:37', '2021-11-02 10:43:37'),
(41, 24, 12, 1, 24, 'Odonil Air Freshener Block Jasmine Mist1', 'odonil-air-freshener-block-jasmine-mist1', 1, '300.00', '300.00', NULL, NULL, '2021-11-08 16:39:15', '2021-11-08 16:39:15'),
(42, 24, 14, 1, 24, 'Bluetooth Music Recorder', 'bluetooth-music-recorder', 1, '120.00', '120.00', NULL, NULL, '2021-11-08 16:39:15', '2021-11-08 16:39:15'),
(43, 24, 24, 3, 26, 'Credit Card Knife - Black', 'credit-card-knife---black', 1, '2000.00', '2000.00', NULL, NULL, '2021-11-08 16:39:15', '2021-11-08 16:39:15'),
(44, 25, 1, 2, 25, 'Mini Wireless Earphone', 'mini-wireless-earphone', 2, '200.00', '400.00', NULL, NULL, '2021-11-09 17:44:02', '2021-11-09 17:44:02'),
(45, 25, 3, 4, 27, 'Product Name', 'product-name', 1, '200.00', '200.00', NULL, NULL, '2021-11-09 17:44:02', '2021-11-09 17:44:02'),
(46, 26, 1, 2, 25, 'Mini Wireless Earphone', 'mini-wireless-earphone', 1, '200.00', '200.00', NULL, NULL, '2021-11-09 17:48:04', '2021-11-09 17:48:04'),
(47, 27, 7, 2, 25, 'Pur Feeding Bottle', 'pur-feeding-bottle', 1, '120.00', '120.00', NULL, NULL, '2021-11-10 13:51:51', '2021-11-10 13:51:51'),
(48, 28, 7, 2, 25, 'Pur Feeding Bottle', 'pur-feeding-bottle', 1, '120.00', '120.00', NULL, NULL, '2021-11-10 13:51:51', '2021-11-10 13:51:51'),
(49, 29, 7, 2, 25, 'Pur Feeding Bottle', 'pur-feeding-bottle', 1, '120.00', '120.00', NULL, NULL, '2021-11-10 13:51:52', '2021-11-10 13:51:52'),
(50, 30, 7, 2, 25, 'Pur Feeding Bottle', 'pur-feeding-bottle', 1, '120.00', '120.00', NULL, NULL, '2021-11-10 13:51:52', '2021-11-10 13:51:52'),
(51, 31, 7, 2, 25, 'Pur Feeding Bottle', 'pur-feeding-bottle', 1, '120.00', '120.00', NULL, NULL, '2021-11-10 13:51:52', '2021-11-10 13:51:52'),
(52, 32, 3, 4, 27, 'Product Name', 'product-name', 2, '200.00', '400.00', NULL, NULL, '2022-02-19 10:54:42', '2022-02-19 10:54:42'),
(53, 32, 9, 4, 27, 'Red Cap Sound Marker', 'red-cap-sound-marker', 2, '700.00', '1400.00', NULL, NULL, '2022-02-19 10:54:42', '2022-02-19 10:54:42'),
(54, 33, 9, 4, 27, 'Red Cap Sound Marker', 'red-cap-sound-marker', 1, '700.00', '700.00', NULL, NULL, '2022-03-25 01:50:53', '2022-03-25 01:50:53'),
(55, 33, 24, 3, 26, 'Credit Card Knife - Black', 'credit-card-knife---black', 1, '2000.00', '2000.00', NULL, NULL, '2022-03-25 01:50:53', '2022-03-25 01:50:53'),
(56, 33, 17, 3, 26, 'Men\'s Black Watch', 'men\'s-black-watch', 1, '130.00', '130.00', NULL, NULL, '2022-03-25 01:50:53', '2022-03-25 01:50:53'),
(57, 34, 9, 4, 27, 'Red Cap Sound Marker', 'red-cap-sound-marker', 1, '700.00', '700.00', NULL, NULL, '2022-03-25 01:52:41', '2022-03-25 01:52:41'),
(58, 34, 24, 3, 26, 'Credit Card Knife - Black', 'credit-card-knife---black', 1, '2000.00', '2000.00', NULL, NULL, '2022-03-25 01:52:41', '2022-03-25 01:52:41'),
(59, 34, 17, 3, 26, 'Men\'s Black Watch', 'men\'s-black-watch', 1, '130.00', '130.00', NULL, NULL, '2022-03-25 01:52:41', '2022-03-25 01:52:41'),
(60, 35, 3, 4, 27, 'Product Name', 'product-name', 1, '200.00', '200.00', NULL, NULL, '2022-04-06 20:04:54', '2022-04-06 20:04:54'),
(61, 36, 3, 4, 27, 'Product Name', 'product-name', 1, '200.00', '200.00', NULL, NULL, '2022-04-06 20:06:04', '2022-04-06 20:06:04'),
(62, 37, 3, 4, 27, 'Product Name', 'product-name', 1, '200.00', '200.00', NULL, NULL, '2022-04-06 20:07:19', '2022-04-06 20:07:19'),
(63, 38, 9, 4, 27, 'Red Cap Sound Marker', 'red-cap-sound-marker', 1, '700.00', '700.00', NULL, NULL, '2022-04-06 20:09:05', '2022-04-06 20:09:05'),
(64, 39, 3, 4, 27, 'Product Name', 'product-name', 2, '200.00', '400.00', NULL, NULL, '2022-06-22 11:26:28', '2022-06-22 11:26:28'),
(65, 39, 9, 4, 27, 'Red Cap Sound Marker', 'red-cap-sound-marker', 1, '700.00', '700.00', NULL, NULL, '2022-06-22 11:26:28', '2022-06-22 11:26:28'),
(66, 40, 21, 3, 26, 'Black Crusher', 'black-crusher', 1, '300.00', '300.00', NULL, NULL, '2022-08-24 05:16:04', '2022-08-24 05:16:04'),
(67, 41, 48, 1, 24, 'Realme C20A - 5000 Mah Battery', 'realme-c20a---5000-mah-battery', 1, '1500.00', '1500.00', NULL, NULL, '2022-09-12 04:40:50', '2022-09-12 04:40:50'),
(68, 42, 1, 2, 25, 'Mini Wireless Earphone', 'mini-wireless-earphone', 2, '200.00', '400.00', NULL, NULL, '2023-01-30 08:01:50', '2023-01-30 08:01:50'),
(69, 42, 23, 4, 27, 'Beyond OTP Shirt', 'beyond-otp-shirt', 1, '300.00', '300.00', NULL, NULL, '2023-01-30 08:01:50', '2023-01-30 08:01:50'),
(70, 43, 48, 1, 24, 'Realme C20A - 5000 Mah Battery', 'realme-c20a---5000-mah-battery', 3, '1500.00', '4500.00', NULL, NULL, '2023-07-30 05:06:56', '2023-07-30 05:06:56');

-- --------------------------------------------------------

--
-- Table structure for table `ems_product`
--

CREATE TABLE `ems_product` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_bn` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_item` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_id` bigint UNSIGNED DEFAULT NULL,
  `category_id` bigint UNSIGNED DEFAULT NULL,
  `item_unit_id` bigint UNSIGNED DEFAULT NULL,
  `size_unit_id` bigint UNSIGNED DEFAULT NULL,
  `size_weight_dimen_id` bigint UNSIGNED DEFAULT NULL,
  `country_id` bigint UNSIGNED DEFAULT NULL,
  `discount_id` bigint UNSIGNED DEFAULT NULL,
  `warning_id` bigint UNSIGNED DEFAULT NULL,
  `item_assurance_id` bigint UNSIGNED DEFAULT NULL,
  `promotion_id` bigint UNSIGNED DEFAULT NULL,
  `vendor_id` bigint UNSIGNED DEFAULT NULL,
  `quantity` bigint DEFAULT NULL,
  `minquantity` bigint DEFAULT NULL,
  `maxquantity` bigint DEFAULT NULL,
  `salesprice` decimal(10,2) DEFAULT NULL,
  `purchaseprice` decimal(10,2) DEFAULT NULL,
  `discription` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_image` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_tag` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_product`
--

INSERT INTO `ems_product` (`id`, `name`, `name_bn`, `slug`, `sub_item`, `brand_id`, `category_id`, `item_unit_id`, `size_unit_id`, `size_weight_dimen_id`, `country_id`, `discount_id`, `warning_id`, `item_assurance_id`, `promotion_id`, `vendor_id`, `quantity`, `minquantity`, `maxquantity`, `salesprice`, `purchaseprice`, `discription`, `feature_image`, `feature_tag`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Mini Wireless Earphone', 'Product Name (Bangla)', 'mini-wireless-earphone', 'yes', 2, 3, 4, 2, 2, 5, 13, 4, 6, 1, 2, 495, 30, 5000, '200.00', '100.00', 'Description', 'mini-wireless-earphone.jpg', 'dealshot', '1', '1', '1', '2021-09-06 09:53:50', '2023-01-30 08:01:50'),
(3, 'Product Name', 'Product Name (Bangla)', 'product-name', 'yes', 2, 4, 1, 2, 1, 4, 13, 4, 4, 1, 4, 487, 30, 5000, '200.00', '100.00', 'Description', 'product-name.jpg', 'dealshot', '1', '1', '1', '2021-09-06 10:24:35', '2022-06-22 11:26:28'),
(5, 'Odonil Air Freshener Block Jasmine Mist', NULL, 'odonil-air-freshener-block-jasmine-mist', NULL, 2, 1, 10, 9, NULL, 2, NULL, NULL, NULL, NULL, 1, 1000, 50, 2500, '300.00', '260.00', NULL, 'odonil-air-freshener-block-jasmine-mist.jpeg', 'recommend', '1', NULL, '1', '2021-09-06 13:24:16', '2021-09-09 09:09:32'),
(6, 'ACI Savlon Liquid Antiseptic', NULL, 'aci-savlon-liquid-antiseptic', NULL, 25, 2, 11, 10, NULL, 2, NULL, NULL, NULL, NULL, 2, 1500, 50, 2500, '100.00', '60.00', NULL, 'aci-savlon-liquid-antiseptic.png', 'bestseller', '1', NULL, '1', '2021-09-06 13:24:16', '2021-09-09 09:09:13'),
(7, 'Pur Feeding Bottle', NULL, 'pur-feeding-bottle', NULL, 26, 11, 12, 11, NULL, 3, NULL, NULL, NULL, NULL, 2, 1994, 50, 2500, '120.00', '80.00', NULL, 'pur-feeding-bottle.jpg', 'bestseller', '1', NULL, '1', '2021-09-06 13:24:16', '2021-11-10 13:51:52'),
(8, 'Whiskas Adult Cat Food Tuna', NULL, 'whiskas-adult-cat-food-tuna', NULL, 1, 11, 13, 12, 1, 2, 13, 4, 5, 1, 2, 498, 50, 2500, '300.00', '220.00', NULL, 'whiskas-adult-cat-food-tuna.jpg', 'recommend', '1', NULL, '1', '2021-09-06 13:24:16', '2021-11-02 10:43:37'),
(9, 'Red Cap Sound Marker', 'Product Name (Bangla)', 'red-cap-sound-marker', 'yes', 4, 11, 14, 13, 3, 4, 13, 4, 5, 1, 4, 1992, 50, 2500, '700.00', '550.00', NULL, 'red-cap-sound-marker.jpg', 'dealshot', '1', NULL, '1', '2021-09-06 13:24:16', '2022-06-22 11:26:28'),
(10, 'Badhacopi Pata', NULL, 'badhacopi-pata', NULL, 4, 11, 14, 14, NULL, 4, NULL, NULL, NULL, NULL, 1, 298, 50, 2500, '130.00', '100.00', NULL, 'badhacopi-pata.jpg', 'bestseller', '1', NULL, '1', '2021-09-06 13:24:16', '2021-10-03 09:00:02'),
(12, 'Odonil Air Freshener Block Jasmine Mist1', NULL, 'odonil-air-freshener-block-jasmine-mist1', NULL, 2, 21, 10, 16, 2, 4, 14, 4, 6, 1, 1, 999, 50, 2500, '300.00', '260.00', NULL, 'odonil-air-freshener-block-jasmine-mist1.jpg', 'recommend', '1', NULL, '1', '2021-09-09 10:42:31', '2021-11-08 16:39:15'),
(14, 'Bluetooth Music Recorder', NULL, 'bluetooth-music-recorder', 'yes', 26, 21, 12, 18, 2, 4, 13, 5, 4, 1, 1, 1998, 50, 2500, '120.00', '80.00', NULL, 'bluetooth-music-recorder.jpg', 'bestseller', '1', NULL, '1', '2021-09-09 10:42:31', '2021-11-08 16:39:15'),
(15, 'Magenetic Charge Box', NULL, 'magenetic-charge-box', NULL, 1, 21, 13, 19, 1, 2, NULL, NULL, NULL, NULL, 3, 500, 50, 2500, '300.00', '220.00', NULL, 'magenetic-charge-box.jpg', 'bestseller', '1', NULL, '1', '2021-09-09 10:42:31', '2021-09-09 10:47:13'),
(16, 'Bright Light', NULL, 'bright-light', NULL, 4, 21, 14, 20, 2, 2, NULL, NULL, NULL, NULL, 4, 1999, 50, 2500, '700.00', '550.00', NULL, 'bright-light.jpg', 'bestseller', '1', NULL, '1', '2021-09-09 10:42:31', '2021-11-02 10:43:37'),
(17, 'Men\'s Black Watch', NULL, 'men\'s-black-watch', 'yes', 4, 21, 14, 21, 1, 4, 13, 4, 4, 1, 3, 298, 50, 2500, '130.00', '100.00', NULL, 'men\'s-black-watch.jpg', 'recommend', '1', NULL, '1', '2021-09-09 10:42:31', '2022-03-25 01:52:41'),
(18, 'Rotate Crusher', NULL, 'rotate-crusher', 'yes', 2, 22, 10, 22, 3, 5, NULL, NULL, NULL, 1, 2, 1000, 50, 2500, '300.00', '260.00', NULL, 'rotate-crusher.jpg', 'bestseller', '1', NULL, '1', '2021-09-09 11:29:08', '2021-09-27 08:13:25'),
(19, 'Colorful Flavoring Box', NULL, 'colorful-flavoring-box', NULL, 25, 22, 11, 23, 1, 3, NULL, NULL, NULL, 1, 2, 1500, 50, 2500, '100.00', '60.00', NULL, 'colorful-flavoring-box.jpg', 'bestseller', '1', NULL, '1', '2021-09-09 11:29:08', '2021-09-27 08:12:06'),
(20, 'Rice Cooker', NULL, 'rice-cooker', 'yes', 26, 22, 12, 24, 2, 5, NULL, NULL, NULL, 1, 3, 2000, 50, 2500, '120.00', '80.00', NULL, 'rice-cooker.jpg', 'recommend', '1', NULL, '1', '2021-09-09 11:29:08', '2021-09-27 08:10:47'),
(21, 'Black Crusher', NULL, 'black-crusher', 'yes', 1, 22, 13, 25, 1, 4, NULL, NULL, NULL, NULL, 3, 499, 50, 2500, '300.00', '220.00', NULL, 'black-crusher.jpg', 'bestseller', '1', NULL, '1', '2021-09-09 11:29:08', '2022-08-24 05:16:04'),
(22, 'Coat Pool  Comfort Jacket', 'Coat Pool  Comfort Jacket', 'coat-pool--comfort-jacket', 'yes', 26, 2, 1, 3, 2, 4, 13, 4, 5, 1, 1, 500, 30, 5000, '300.00', '100.00', NULL, 'coat-pool--comfort-jacket.jpg', 'recommend', '1', '1', NULL, '2021-09-13 10:22:10', '2021-09-13 10:22:10'),
(23, 'Beyond OTP Shirt', 'Beyond OTP Shirt', 'beyond-otp-shirt', 'yes', 1, 11, 4, 4, 3, 4, 13, 4, 4, 1, 4, 499, 30, 5000, '300.00', '100.00', NULL, 'beyond-otp-shirt.jpg', 'recommend', '1', '1', '1', '2021-09-13 10:27:04', '2023-01-30 08:01:50'),
(24, 'Credit Card Knife - Black', 'Credit Card Knife - Black', 'credit-card-knife---black', 'yes', 2, 14, 1, 2, 3, 4, 13, 5, 5, 1, 3, 47, 10, 500, '2000.00', '1500.00', 'A super light and super sharp utility knife, the same size as a credit card. Just three ingenious folding operations metamorphosis the card into an elegant pocket utility tool. Slimmer and lighter than an ordinary knife, updated features include a stiffer, polypropylene body (living hinges guaranteed for life) and a unique safety lock (cannot open in pocket or drawer and is child proof). The extra long stainless steel surgical blade ensures longer lasting rust free sharpness .', 'credit-card-knife---black.jpg', 'dealshot', '1', '1', '1', '2021-09-21 10:32:23', '2022-03-25 01:52:41'),
(25, 'Hw Cordless Drill Machine - 12v', 'Hw Cordless Drill Machine - 12v', 'hw-cordless-drill-machine---12v', 'yes', 28, 21, 11, 11, 2, 4, 13, 5, 5, 1, 2, 500, 30, 5000, '2500.00', '2000.00', 'A drill or drilling machine is a tool primarily used for making round holes or driving fasteners. It is fitted with a bit, either a drill or driver, depending on application, secured by a chuck. Some powered drills also include a hammer function. Drills vary widely in speed, power, and size.', 'hw-cordless-drill-machine---12v.png', 'bestseller', '1', '25', NULL, '2021-09-28 06:02:17', '2021-09-28 06:02:17'),
(40, 'Rotate Crusher1', NULL, 'rotate-crusher1', NULL, 2, 22, 10, 46, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1000, 50, 2500, '300.00', '260.00', NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-28 06:32:18', '2021-09-28 06:32:18'),
(41, 'Colorful Flavoring Box1', NULL, 'colorful-flavoring-box1', NULL, 25, 22, 11, 47, NULL, NULL, NULL, NULL, NULL, NULL, 2, 1500, 50, 2500, '100.00', '60.00', NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-28 06:32:18', '2021-09-28 06:32:18'),
(42, 'Rice Cooker1', NULL, 'rice-cooker1', NULL, 26, 22, 12, 48, NULL, NULL, NULL, NULL, NULL, NULL, 2, 2000, 50, 2500, '120.00', '80.00', NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-28 06:32:18', '2021-09-28 06:32:18'),
(43, 'Black Crusher1', NULL, 'black-crusher1', NULL, 1, 22, 13, 49, NULL, NULL, NULL, NULL, NULL, NULL, 2, 500, 50, 2500, '300.00', '220.00', NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-28 06:32:18', '2021-09-28 06:32:18'),
(44, 'Rotate Crusher1', NULL, 'rotate-crusher1', NULL, 2, 22, 10, 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1000, 50, 2500, '300.00', '260.00', NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-28 06:33:47', '2021-09-28 06:33:47'),
(45, 'Colorful Flavoring Box1', NULL, 'colorful-flavoring-box1', NULL, 25, 22, 11, 51, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1500, 50, 2500, '100.00', '60.00', NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-28 06:33:47', '2021-09-28 06:33:47'),
(46, 'Rice Cooker1', NULL, 'rice-cooker1', NULL, 26, 22, 12, 52, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2000, 50, 2500, '120.00', '80.00', NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-28 06:33:47', '2021-09-28 06:33:47'),
(47, 'Black Crusher1', NULL, 'black-crusher1', NULL, 1, 22, 13, 53, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 500, 50, 2500, '300.00', '220.00', NULL, NULL, NULL, NULL, NULL, NULL, '2021-09-28 06:33:47', '2021-09-28 06:33:47'),
(48, 'Realme C20A - 5000 Mah Battery', 'Alloy Die cast Pull Back Mini', 'realme-c20a---5000-mah-battery', 'yes', 2, 22, 1, 2, 1, 4, 13, 4, 4, 1, 1, 496, 30, 2000, '1500.00', '1000.00', 'NO RETURN applicable if the seal is broken\r\n5000mAh Massive Battery\r\nProcessor: MediaTek Helio G35 Octa-core 12nm Processor\r\nCPU: Octa-core CPU, 12nm, up to 2.3GHz\r\nGPU: IMG GE8320.\r\nStorage & RAM: RAM: 2GB LPDDR4X ROM: 32GB\r\n16.5cm (6.5\") Large Display', 'realme-c20a---5000-mah-battery.png', 'dealshot', '1', '24', '24', '2021-11-08 16:50:58', '2023-07-30 05:06:56'),
(49, 'Rotate Crusher1', NULL, 'rotate-crusher1', NULL, 2, 22, 10, 54, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1000, 50, 2500, '300.00', '260.00', NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-08 16:55:22', '2021-11-08 16:55:22'),
(50, 'Colorful Flavoring Box1', NULL, 'colorful-flavoring-box1', NULL, 25, 22, 11, 55, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1500, 50, 2500, '100.00', '60.00', NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-08 16:55:22', '2021-11-08 16:55:22'),
(51, 'Rice Cooker1', NULL, 'rice-cooker1', NULL, 26, 22, 12, 56, NULL, NULL, NULL, NULL, NULL, NULL, 1, 2000, 50, 2500, '120.00', '80.00', NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-08 16:55:22', '2021-11-08 16:55:22'),
(52, 'Black Crusher1', NULL, 'black-crusher1', NULL, 1, 22, 13, 57, NULL, NULL, NULL, NULL, NULL, NULL, 1, 500, 50, 2500, '300.00', '220.00', NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-08 16:55:22', '2021-11-08 16:55:22'),
(53, 'Rotate Crusher1', NULL, 'rotate-crusher1', NULL, 2, 22, 10, 58, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1000, 50, 2500, '300.00', '260.00', NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-14 17:04:22', '2021-11-14 17:04:22'),
(54, 'Colorful Flavoring Box1', NULL, 'colorful-flavoring-box1', NULL, 25, 22, 11, 59, NULL, NULL, NULL, NULL, NULL, NULL, 3, 1500, 50, 2500, '100.00', '60.00', NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-14 17:04:22', '2021-11-14 17:04:22'),
(55, 'Rice Cooker1', NULL, 'rice-cooker1', NULL, 26, 22, 12, 60, NULL, NULL, NULL, NULL, NULL, NULL, 3, 2000, 50, 2500, '120.00', '80.00', NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-14 17:04:22', '2021-11-14 17:04:22'),
(56, 'Black Crusher1', NULL, 'black-crusher1', NULL, 1, 22, 13, 61, NULL, NULL, NULL, NULL, NULL, NULL, 3, 500, 50, 2500, '300.00', '220.00', NULL, NULL, NULL, NULL, NULL, NULL, '2021-11-14 17:04:22', '2021-11-14 17:04:22'),
(57, 'abc', NULL, 'abc', NULL, 26, 4, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, 3, 500, 5, 100, '250.00', '190.00', NULL, 'abc.jpg', 'bestseller', '1', '26', NULL, '2021-11-14 17:23:05', '2021-11-14 17:23:05'),
(58, 'DT100 PRO MAX SMART WATCH GPS TRACK', 'DT100 PRO MAX SMART WATCH GPS TRACK', 'dt100-pro-max-smart-watch-gps-track', 'yes', 28, 21, 11, 14, 1, 4, 13, 4, 4, 1, 1, 10, 5, 30, '4000.00', '3000.00', 'DT100 PRO MAX is Wireless Charger DT100 PLUS is Magnetic charging DT100 PRO MAX have AI Voice Assistant, GPS movement track function. DT100 PLUS Don\'t have!All other functions are the same.', 'dt100-pro-max-smart-watch-gps-track.jpg', 'bestseller', '1', '1', NULL, '2022-09-11 08:20:47', '2022-09-11 08:20:47');

-- --------------------------------------------------------

--
-- Table structure for table `ems_product_additionalfield`
--

CREATE TABLE `ems_product_additionalfield` (
  `id` bigint UNSIGNED NOT NULL,
  `product_id` bigint UNSIGNED DEFAULT NULL,
  `level_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level_value` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_product_additionalfield`
--

INSERT INTO `ems_product_additionalfield` (`id`, `product_id`, `level_name`, `level_value`, `created_at`, `updated_at`) VALUES
(4, 3, 'Additional Field 1', 'Additional  Value 1', '2021-09-07 07:26:54', '2021-09-07 07:26:54'),
(6, 3, 'Additional Field 3', 'Additional  Value 3', '2021-09-07 07:26:54', '2021-09-07 07:26:54'),
(16, 10, 'Additional Field 1', 'Additional  Value 1', '2021-09-07 07:51:21', '2021-09-07 07:51:21');

-- --------------------------------------------------------

--
-- Table structure for table `ems_product_brand`
--

CREATE TABLE `ems_product_brand` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand_image` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_product_brand`
--

INSERT INTO `ems_product_brand` (`id`, `name`, `slug`, `content`, `feature`, `brand_image`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Fresh', 'fresh', 'Fresh', 'yes', 'fresh.jpg', '1', '1', '1', '2021-08-26 06:52:11', '2021-09-09 07:06:35'),
(2, 'Basudhara', 'basudhara', 'Basudhara', 'yes', 'basudhara.jpg', '1', '1', '1', '2021-08-26 06:52:59', '2021-09-09 07:06:18'),
(4, 'Radhuni', 'radhuni', 'Radhuni', NULL, 'radhuni.jpg', '1', '1', '1', '2021-08-26 07:02:08', '2021-09-09 07:06:01'),
(25, 'ACI', 'aci', NULL, NULL, 'aci.jpg', '1', '1', '1', '2021-09-06 13:24:16', '2021-09-09 07:05:51'),
(26, 'Unilever', 'unilever', NULL, NULL, 'unilever.jpg', '1', '1', '1', '2021-09-06 13:24:16', '2021-09-09 07:05:34'),
(28, 'Jumuna', 'jumuna', 'Jumuna', 'yes', 'jumuna.jpg', '1', '1', NULL, '2021-09-09 07:06:56', '2021-09-09 07:06:56');

-- --------------------------------------------------------

--
-- Table structure for table `ems_product_colors`
--

CREATE TABLE `ems_product_colors` (
  `id` bigint UNSIGNED NOT NULL,
  `product_id` bigint UNSIGNED DEFAULT NULL,
  `color_id` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_product_colors`
--

INSERT INTO `ems_product_colors` (`id`, `product_id`, `color_id`, `created_at`, `updated_at`) VALUES
(76, 1, 2, '2021-09-09 07:32:46', '2021-09-09 07:32:46'),
(77, 1, 3, '2021-09-09 07:32:46', '2021-09-09 07:32:46'),
(83, 17, 3, '2021-09-13 10:14:22', '2021-09-13 10:14:22'),
(98, 24, 3, '2021-09-27 08:08:00', '2021-09-27 08:08:00'),
(99, 23, 2, '2021-09-27 08:08:12', '2021-09-27 08:08:12'),
(100, 23, 3, '2021-09-27 08:08:12', '2021-09-27 08:08:12'),
(101, 22, 2, '2021-09-27 08:08:24', '2021-09-27 08:08:24'),
(102, 22, 3, '2021-09-27 08:08:24', '2021-09-27 08:08:24'),
(103, 20, 2, '2021-09-27 08:10:47', '2021-09-27 08:10:47'),
(104, 20, 3, '2021-09-27 08:10:47', '2021-09-27 08:10:47'),
(107, 19, 2, '2021-09-27 08:12:06', '2021-09-27 08:12:06'),
(108, 19, 3, '2021-09-27 08:12:06', '2021-09-27 08:12:06'),
(109, 18, 2, '2021-09-27 08:13:25', '2021-09-27 08:13:25'),
(110, 14, 2, '2021-09-27 08:15:17', '2021-09-27 08:15:17'),
(112, 9, 2, '2021-09-27 08:16:09', '2021-09-27 08:16:09'),
(113, 9, 3, '2021-09-27 08:16:09', '2021-09-27 08:16:09'),
(114, 3, 2, '2021-09-27 08:16:42', '2021-09-27 08:16:42'),
(115, 3, 3, '2021-09-27 08:16:42', '2021-09-27 08:16:42'),
(116, 25, 2, '2021-09-28 06:02:17', '2021-09-28 06:02:17'),
(117, 25, 3, '2021-09-28 06:02:17', '2021-09-28 06:02:17'),
(124, 58, 2, '2022-09-11 08:22:16', '2022-09-11 08:22:16'),
(125, 58, 3, '2022-09-11 08:22:16', '2022-09-11 08:22:16'),
(130, 48, 2, '2022-09-12 04:39:01', '2022-09-12 04:39:01'),
(131, 48, 3, '2022-09-12 04:39:01', '2022-09-12 04:39:01');

-- --------------------------------------------------------

--
-- Table structure for table `ems_product_image`
--

CREATE TABLE `ems_product_image` (
  `id` bigint UNSIGNED NOT NULL,
  `product_id` bigint UNSIGNED DEFAULT NULL,
  `img_level` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attach_link` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_product_image`
--

INSERT INTO `ems_product_image` (`id`, `product_id`, `img_level`, `attach_link`, `created_at`, `updated_at`) VALUES
(30, 10, 'Product Image', 'img51681-1631001081.png', '2021-09-07 07:51:21', '2021-09-07 07:51:21'),
(31, 9, 'Red Cap Sound Marker 1', 'img72781-1631172499.jpg', '2021-09-09 07:28:19', '2021-09-09 07:28:19'),
(32, 9, 'Red Cap Sound Marker 2', 'img80622-1631172499.jpg', '2021-09-09 07:28:19', '2021-09-09 07:28:19'),
(33, 9, 'Red Cap Sound Marker 3', 'img92673-1631172499.jpg', '2021-09-09 07:28:19', '2021-09-09 07:28:19'),
(34, 9, 'Red Cap Sound Marker 4', 'img27434-1631172499.jpg', '2021-09-09 07:28:19', '2021-09-09 07:28:19'),
(35, 1, 'Mini Wireless Earphone 1', 'img96411-1631172766.jpg', '2021-09-09 07:32:46', '2021-09-09 07:32:46'),
(36, 1, 'Mini Wireless Earphone 2', 'img44212-1631172766.jpg', '2021-09-09 07:32:46', '2021-09-09 07:32:46'),
(37, 1, 'Mini Wireless Earphone 3', 'img20093-1631172766.jpg', '2021-09-09 07:32:46', '2021-09-09 07:32:46'),
(38, 1, 'Mini Wireless Earphone 4', 'img46724-1631172766.jpg', '2021-09-09 07:32:46', '2021-09-09 07:32:46'),
(39, 17, 'Product Image 1', 'img49841-1631528062.jpg', '2021-09-13 10:14:22', '2021-09-13 10:14:22'),
(40, 17, 'Product Image 2', 'img93922-1631528062.jpg', '2021-09-13 10:14:22', '2021-09-13 10:14:22'),
(41, 17, 'Product Image 3', 'img57703-1631528062.jpg', '2021-09-13 10:14:22', '2021-09-13 10:14:22'),
(42, 17, 'Product Image 4', 'img58454-1631528062.jpg', '2021-09-13 10:14:22', '2021-09-13 10:14:22'),
(43, 22, 'Product Image 1', 'img59131-1631528577.jpg', '2021-09-13 10:22:57', '2021-09-13 10:22:57'),
(44, 22, 'Product Image 2', 'img62502-1631528577.jpg', '2021-09-13 10:22:57', '2021-09-13 10:22:57'),
(45, 22, 'Product Image 3', 'img26143-1631528577.jpg', '2021-09-13 10:22:57', '2021-09-13 10:22:57'),
(46, 22, 'Product Image 4', 'img51054-1631528577.jpg', '2021-09-13 10:22:57', '2021-09-13 10:22:57'),
(47, 23, 'Beyond OTP Shirt 1', 'img30941-1631528875.jpg', '2021-09-13 10:27:55', '2021-09-13 10:27:55'),
(48, 23, 'Beyond OTP Shirt 2', 'img27102-1631528875.jpg', '2021-09-13 10:27:55', '2021-09-13 10:27:55'),
(49, 23, 'Beyond OTP Shirt 3', 'img65303-1631528875.jpg', '2021-09-13 10:27:55', '2021-09-13 10:27:55'),
(50, 23, 'Beyond OTP Shirt 4', 'img78194-1631528875.jpg', '2021-09-13 10:27:55', '2021-09-13 10:27:55'),
(51, 8, 'Whiskas Adult Cat Food Tuna 1', 'img40531-1631529266.jpg', '2021-09-13 10:34:26', '2021-09-13 10:34:26'),
(52, 8, 'Whiskas Adult Cat Food Tuna 2', 'img19882-1631529266.jpg', '2021-09-13 10:34:26', '2021-09-13 10:34:26'),
(53, 8, 'Whiskas Adult Cat Food Tuna 3', 'img81373-1631529266.jpg', '2021-09-13 10:34:26', '2021-09-13 10:34:26'),
(54, 8, 'Whiskas Adult Cat Food Tuna 4', 'img18744-1631529266.jpg', '2021-09-13 10:34:26', '2021-09-13 10:34:26'),
(55, 3, 'Product Image 1', 'img62571-1631531432.jpg', '2021-09-13 11:10:32', '2021-09-13 11:10:32'),
(56, 3, 'Product Image 2', 'img42062-1631531432.jpg', '2021-09-13 11:10:32', '2021-09-13 11:10:32'),
(57, 3, 'Product Image 3', 'img42283-1631531432.jpg', '2021-09-13 11:10:32', '2021-09-13 11:10:32'),
(58, 3, 'Product Image 4', 'img21674-1631531432.jpg', '2021-09-13 11:10:32', '2021-09-13 11:10:32'),
(59, 24, 'image 1', 'img16971-1632220706.jpg', '2021-09-21 10:38:26', '2021-09-21 10:38:26'),
(60, 24, 'image2', 'img88072-1632220706.jpg', '2021-09-21 10:38:26', '2021-09-21 10:38:26'),
(61, 24, 'nife 3', 'img13333-1632220706.jpg', '2021-09-21 10:38:26', '2021-09-21 10:38:26'),
(62, 24, 'nife 4', 'img79334-1632220706.jpg', '2021-09-21 10:38:26', '2021-09-21 10:38:26'),
(63, 21, 'image 1', 'img10261-1632730181.jpg', '2021-09-27 08:09:41', '2021-09-27 08:09:41'),
(64, 21, 'image2', 'img85522-1632730181.jpg', '2021-09-27 08:09:41', '2021-09-27 08:09:41'),
(65, 21, 'image3', 'img26173-1632730181.jpg', '2021-09-27 08:09:41', '2021-09-27 08:09:41'),
(66, 21, 'image 4', 'img24594-1632730181.jpg', '2021-09-27 08:09:41', '2021-09-27 08:09:41'),
(67, 20, NULL, 'img53991-1632730247.jpg', '2021-09-27 08:10:47', '2021-09-27 08:10:47'),
(68, 20, NULL, 'img28642-1632730247.jpg', '2021-09-27 08:10:47', '2021-09-27 08:10:47'),
(69, 20, NULL, 'img26363-1632730247.jpg', '2021-09-27 08:10:47', '2021-09-27 08:10:47'),
(70, 20, NULL, 'img12104-1632730247.jpg', '2021-09-27 08:10:47', '2021-09-27 08:10:47'),
(71, 19, NULL, 'img42491-1632730316.jpg', '2021-09-27 08:11:56', '2021-09-27 08:11:56'),
(72, 19, NULL, 'img94642-1632730316.jpg', '2021-09-27 08:11:56', '2021-09-27 08:11:56'),
(73, 19, NULL, 'img79053-1632730316.jpg', '2021-09-27 08:11:56', '2021-09-27 08:11:56'),
(74, 19, NULL, 'img26724-1632730316.jpg', '2021-09-27 08:11:56', '2021-09-27 08:11:56'),
(75, 18, NULL, 'img74361-1632730405.png', '2021-09-27 08:13:25', '2021-09-27 08:13:25'),
(76, 18, NULL, 'img33942-1632730405.jpg', '2021-09-27 08:13:25', '2021-09-27 08:13:25'),
(77, 18, NULL, 'img13283-1632730405.jpg', '2021-09-27 08:13:25', '2021-09-27 08:13:25'),
(78, 18, NULL, 'img35814-1632730405.jpg', '2021-09-27 08:13:25', '2021-09-27 08:13:25'),
(83, 58, 'image 1', 'img82501-1662884536.jpg', '2022-09-11 08:22:16', '2022-09-11 08:22:16'),
(84, 58, 'image 2', 'img54592-1662884536.jpg', '2022-09-11 08:22:16', '2022-09-11 08:22:16'),
(85, 48, NULL, 'img20711-1662957463.png', '2022-09-12 04:37:43', '2022-09-12 04:37:43'),
(86, 48, NULL, 'img82302-1662957463.jpg', '2022-09-12 04:37:43', '2022-09-12 04:37:43'),
(87, 48, NULL, 'img55471-1662957541.jpg', '2022-09-12 04:39:01', '2022-09-12 04:39:01');

-- --------------------------------------------------------

--
-- Table structure for table `ems_product_tags`
--

CREATE TABLE `ems_product_tags` (
  `id` bigint UNSIGNED NOT NULL,
  `product_id` bigint UNSIGNED DEFAULT NULL,
  `tag_id` bigint UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_product_tags`
--

INSERT INTO `ems_product_tags` (`id`, `product_id`, `tag_id`, `created_at`, `updated_at`) VALUES
(43, 1, 2, '2021-09-09 07:32:46', '2021-09-09 07:32:46'),
(60, 24, 1, '2021-09-27 08:08:00', '2021-09-27 08:08:00'),
(61, 24, 2, '2021-09-27 08:08:00', '2021-09-27 08:08:00'),
(62, 23, 1, '2021-09-27 08:08:12', '2021-09-27 08:08:12'),
(63, 23, 2, '2021-09-27 08:08:12', '2021-09-27 08:08:12'),
(64, 22, 2, '2021-09-27 08:08:24', '2021-09-27 08:08:24'),
(65, 20, 1, '2021-09-27 08:10:47', '2021-09-27 08:10:47'),
(66, 20, 2, '2021-09-27 08:10:47', '2021-09-27 08:10:47'),
(69, 19, 1, '2021-09-27 08:12:06', '2021-09-27 08:12:06'),
(70, 19, 2, '2021-09-27 08:12:06', '2021-09-27 08:12:06'),
(71, 18, 2, '2021-09-27 08:13:25', '2021-09-27 08:13:25'),
(72, 14, 2, '2021-09-27 08:15:17', '2021-09-27 08:15:17'),
(74, 12, 1, '2021-09-27 08:15:56', '2021-09-27 08:15:56'),
(75, 9, 1, '2021-09-27 08:16:09', '2021-09-27 08:16:09'),
(76, 9, 2, '2021-09-27 08:16:09', '2021-09-27 08:16:09'),
(77, 3, 2, '2021-09-27 08:16:42', '2021-09-27 08:16:42'),
(78, 25, 2, '2021-09-28 06:02:17', '2021-09-28 06:02:17'),
(83, 58, 1, '2022-09-11 08:22:16', '2022-09-11 08:22:16'),
(84, 58, 2, '2022-09-11 08:22:16', '2022-09-11 08:22:16'),
(87, 48, 1, '2022-09-12 04:39:01', '2022-09-12 04:39:01');

-- --------------------------------------------------------

--
-- Table structure for table `ems_promotion`
--

CREATE TABLE `ems_promotion` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promotion` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promotion_image` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_promotion`
--

INSERT INTO `ems_promotion` (`id`, `name`, `slug`, `feature`, `tag`, `promotion`, `promotion_image`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Eid Essential Bazar Promotion', 'eid-essential-bazar-promotion', 'yes', 'yes', 'yes', 'Eid-Essential-Bazar-Promotion.jpeg', '1', '1', '1', '2021-08-26 07:11:48', '2021-08-29 06:42:55'),
(2, 'End December', 'end-december', NULL, 'yes', NULL, 'End-December.jpeg', '1', '1', '1', '2021-08-26 07:14:06', '2021-08-29 06:42:50');

-- --------------------------------------------------------

--
-- Table structure for table `ems_sizeunit`
--

CREATE TABLE `ems_sizeunit` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordering` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_sizeunit`
--

INSERT INTO `ems_sizeunit` (`id`, `name`, `slug`, `ordering`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Bag', 'bag', '1', '1', '1', '1', '2021-08-26 06:47:19', '2021-08-29 06:41:46'),
(2, 'Box', 'box', '1', '1', '1', '1', '2021-08-26 06:47:24', '2021-08-29 06:41:43'),
(3, 'Coil', 'coil', '2', '1', '1', '1', '2021-08-26 06:47:33', '2021-08-29 06:41:39'),
(4, 'Can', 'can', '2', '1', '1', '1', '2021-08-26 06:47:39', '2021-08-29 06:41:37'),
(5, 'Carton', 'carton', '1', '1', '1', '1', '2021-08-26 06:47:44', '2021-08-29 06:41:34'),
(9, 'S', 's', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(10, 'M', 'm', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(11, 'L', 'l', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(12, 'XL', 'xl', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(13, 'XLL', 'xll', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(14, '30', '30', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(15, NULL, '', NULL, '1', '1', NULL, '2021-09-06 13:24:16', '2021-09-06 13:24:16'),
(16, 'S', 's', NULL, '1', '1', NULL, '2021-09-09 10:42:31', '2021-09-09 10:42:31'),
(17, 'M', 'm', NULL, '1', '1', NULL, '2021-09-09 10:42:31', '2021-09-09 10:42:31'),
(18, 'L', 'l', NULL, '1', '1', NULL, '2021-09-09 10:42:31', '2021-09-09 10:42:31'),
(19, 'XL', 'xl', NULL, '1', '1', NULL, '2021-09-09 10:42:31', '2021-09-09 10:42:31'),
(20, 'XLL', 'xll', NULL, '1', '1', NULL, '2021-09-09 10:42:31', '2021-09-09 10:42:31'),
(21, '30', '30', NULL, '1', '1', NULL, '2021-09-09 10:42:31', '2021-09-09 10:42:31'),
(22, 'S', 's', NULL, '1', '1', NULL, '2021-09-09 11:29:08', '2021-09-09 11:29:08'),
(23, 'M', 'm', NULL, '1', '1', NULL, '2021-09-09 11:29:08', '2021-09-09 11:29:08'),
(24, 'L', 'l', NULL, '1', '1', NULL, '2021-09-09 11:29:08', '2021-09-09 11:29:08'),
(25, 'XL', 'xl', NULL, '1', '1', NULL, '2021-09-09 11:29:08', '2021-09-09 11:29:08'),
(26, 'S', 's', NULL, '1', '25', NULL, '2021-09-28 06:03:14', '2021-09-28 06:03:14'),
(27, 'M', 'm', NULL, '1', '25', NULL, '2021-09-28 06:03:14', '2021-09-28 06:03:14'),
(28, 'L', 'l', NULL, '1', '25', NULL, '2021-09-28 06:03:14', '2021-09-28 06:03:14'),
(29, 'XL', 'xl', NULL, '1', '25', NULL, '2021-09-28 06:03:14', '2021-09-28 06:03:14'),
(33, 'S', 's', NULL, '1', '25', NULL, '2021-09-28 06:19:27', '2021-09-28 06:19:27'),
(34, 'M', 'm', NULL, '1', '25', NULL, '2021-09-28 06:19:27', '2021-09-28 06:19:27'),
(35, 'L', 'l', NULL, '1', '25', NULL, '2021-09-28 06:19:27', '2021-09-28 06:19:27'),
(36, 'XL', 'xl', NULL, '1', '25', NULL, '2021-09-28 06:19:27', '2021-09-28 06:19:27'),
(46, 'S', 's', NULL, '1', '25', NULL, '2021-09-28 06:32:18', '2021-09-28 06:32:18'),
(47, 'M', 'm', NULL, '1', '25', NULL, '2021-09-28 06:32:18', '2021-09-28 06:32:18'),
(48, 'L', 'l', NULL, '1', '25', NULL, '2021-09-28 06:32:18', '2021-09-28 06:32:18'),
(49, 'XL', 'xl', NULL, '1', '25', NULL, '2021-09-28 06:32:18', '2021-09-28 06:32:18'),
(50, 'S', 's', NULL, '1', '1', NULL, '2021-09-28 06:33:47', '2021-09-28 06:33:47'),
(51, 'M', 'm', NULL, '1', '1', NULL, '2021-09-28 06:33:47', '2021-09-28 06:33:47'),
(52, 'L', 'l', NULL, '1', '1', NULL, '2021-09-28 06:33:47', '2021-09-28 06:33:47'),
(53, 'XL', 'xl', NULL, '1', '1', NULL, '2021-09-28 06:33:47', '2021-09-28 06:33:47'),
(54, 'S', 's', NULL, '1', '24', NULL, '2021-11-08 16:55:22', '2021-11-08 16:55:22'),
(55, 'M', 'm', NULL, '1', '24', NULL, '2021-11-08 16:55:22', '2021-11-08 16:55:22'),
(56, 'L', 'l', NULL, '1', '24', NULL, '2021-11-08 16:55:22', '2021-11-08 16:55:22'),
(57, 'XL', 'xl', NULL, '1', '24', NULL, '2021-11-08 16:55:22', '2021-11-08 16:55:22'),
(58, 'S', 's', NULL, '1', '26', NULL, '2021-11-14 17:04:22', '2021-11-14 17:04:22'),
(59, 'M', 'm', NULL, '1', '26', NULL, '2021-11-14 17:04:22', '2021-11-14 17:04:22'),
(60, 'L', 'l', NULL, '1', '26', NULL, '2021-11-14 17:04:22', '2021-11-14 17:04:22'),
(61, 'XL', 'xl', NULL, '1', '26', NULL, '2021-11-14 17:04:22', '2021-11-14 17:04:22');

-- --------------------------------------------------------

--
-- Table structure for table `ems_sizeweightdimension`
--

CREATE TABLE `ems_sizeweightdimension` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordering` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_sizeweightdimension`
--

INSERT INTO `ems_sizeweightdimension` (`id`, `name`, `slug`, `ordering`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'MM', 'mm', '1', '1', '1', '1', '2021-08-26 06:48:10', '2021-08-29 06:42:11'),
(2, 'Min', 'min', '1', '1', '1', '1', '2021-08-26 06:48:15', '2021-08-29 06:42:06'),
(3, 'Max', 'max', '1', '1', '1', '1', '2021-08-26 06:48:23', '2021-08-29 06:42:03'),
(4, 'XL', 'xl', '2', '1', '1', '1', '2021-08-26 06:48:29', '2021-08-29 06:42:00');

-- --------------------------------------------------------

--
-- Table structure for table `ems_slider`
--

CREATE TABLE `ems_slider` (
  `id` bigint UNSIGNED NOT NULL,
  `keyword` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discription` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_image` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_slider`
--

INSERT INTO `ems_slider` (`id`, `keyword`, `slug`, `name`, `discription`, `slider_image`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(3, 'Mega Sale', 'mega-sale', '2021 Men\'s Lifestyle', 'Get Free Shipping on all orders over $99.00', 'mega-sale.jpg', '1', '1', NULL, '2021-09-21 09:06:39', '2021-09-21 09:06:39'),
(4, 'Best Seller', 'best-seller', 'Kitchen Electronic', 'Get Free Shipping on all orders over', 'best-seller.jpg', '1', '1', NULL, '2021-09-21 09:12:38', '2021-09-21 09:12:38'),
(5, 'New Arrivals', 'new-arrivals', 'Sports Sneakers', 'Get Free Shipping on all orders over', 'new-arrivals.jpg', '1', '1', NULL, '2021-09-21 09:14:11', '2021-09-21 09:14:11'),
(6, 'Deals Hot', 'deals-hot', 'Supermart Deals Hot', 'Supermart Deals Hot', 'deals-hot.png', '1', '1', NULL, '2021-09-21 09:18:06', '2021-09-21 09:18:06'),
(7, 'Best Sellers', 'best-sellers', 'New Lifestyle Collection', 'Up To 10% Discount', 'best-sellers.jpg', '1', '1', NULL, '2021-09-21 09:53:03', '2021-09-21 09:53:03');

-- --------------------------------------------------------

--
-- Table structure for table `ems_vendor`
--

CREATE TABLE `ems_vendor` (
  `id` bigint UNSIGNED NOT NULL,
  `user_id` bigint UNSIGNED DEFAULT NULL,
  `store_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_image` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_vendor`
--

INSERT INTO `ems_vendor` (`id`, `user_id`, `store_name`, `slug`, `store_address`, `contact_person_name`, `mobile`, `email`, `store_image`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 24, 'Salma Collection', 'salma-collection', 'Uttara', 'Salma Sultana', '0176890876', 'salma@rightbrainsolution.com', 'salma-collection1632729153.jpeg', '1', '1', '24', '2021-09-27 07:50:10', '2021-09-27 07:52:33'),
(2, 25, 'Kalio Commerce', 'kalio-commerce', 'Sector 06, Uttara', 'Sohel Rana', '01765432111', 'sohel@rightbrainsolution.com', 'kalio-commerce1632730686.jpg', '1', '1', '1', '2021-09-27 07:54:58', '2021-09-27 08:18:06'),
(3, 26, 'Epixel MLM Software', 'epixel-mlm-software', 'Dhanmondi ,Dhaka', 'Shahib', '01987674523', 'shihab@rightbrainsolution.com', 'epixel-mlm-software1632729697.jpg', '1', '1', '1', '2021-09-27 08:00:47', '2021-09-27 08:01:37'),
(4, 27, 'Quick eSelling', 'quick-eselling', 'PUL Tower', 'RB Raju', '01987674523', 'raju.aspiretss@gmail.com', 'quick-eselling1632729827.jpg', '1', '1', NULL, '2021-09-27 08:03:47', '2021-09-27 08:03:47'),
(5, 43, 'Kalio Commerce11', 'kalio-commerce11', 'fffdfds', 'Kalio Mark11', '019876745231', 'nice.coder13m@gmail.com', 'kalio-commerce111633333886.jpeg', '1', '1', '1', '2021-10-04 07:51:26', '2021-10-04 07:55:41');

-- --------------------------------------------------------

--
-- Table structure for table `ems_warningtype`
--

CREATE TABLE `ems_warningtype` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ordering` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ems_warningtype`
--

INSERT INTO `ems_warningtype` (`id`, `name`, `slug`, `ordering`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(4, 'Warranty', 'warranty', '1', '1', '1', NULL, '2021-08-29 06:36:45', '2021-08-29 06:36:45'),
(5, 'Guarantee', 'guarantee', '2', '1', '1', '1', '2021-08-29 06:36:59', '2021-11-02 13:14:25');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fileimport`
--

CREATE TABLE `fileimport` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2021_08_09_063929_create_fileimport_tbl', 2),
(4, '2014_10_12_100000_create_password_resets_table', 3),
(5, '2019_08_19_000000_create_failed_jobs_table', 3),
(6, '2019_12_14_000001_create_personal_access_tokens_table', 3),
(19, '2021_08_23_125700_create_ems_itemunit_table', 4),
(20, '2021_08_23_165337_create_ems_sizeunit_table', 4),
(21, '2021_08_23_173606_create_ems_sizeweightdimension_table', 4),
(22, '2021_08_24_144931_create_ems_product_brand_table', 4),
(23, '2021_08_24_182255_create_ems_promotion_table', 4),
(24, '2021_08_25_114852_create_ems_country_table', 4),
(26, '2021_08_25_152302_create_ems_category_table', 5),
(28, '2021_08_29_122030_create_ems_warningtype_table', 7),
(30, '2021_08_29_130124_create_ems_itemasurance_table', 8),
(31, '2021_08_29_155602_create_ems_discount_table', 9),
(33, '2021_08_29_165805_create_ems_discount_brand_table', 10),
(34, '2021_08_31_103217_create_ems_discount_category_table', 11),
(37, '2021_09_06_110157_create_ems_color_table', 13),
(39, '2021_08_26_162842_create_ems_product_table', 14),
(40, '2021_09_06_161102_create_ems_product_colors_table', 15),
(42, '2021_09_06_161154_create_ems_product_tags_table', 16),
(43, '2021_09_07_111943_create_ems_product_image_table', 17),
(44, '2021_09_07_125356_create_ems_product_additionalfield_table', 18),
(46, '2021_09_14_121106_create_ems_slider_table', 19),
(55, '2021_09_19_154531_create_ems_delivery_time_slot_table', 20),
(66, '2021_09_23_181016_create_ems_general_setting_table', 24),
(67, '2014_10_12_000000_create_users_table', 25),
(68, '2021_09_21_124909_create_ems_vendor_table', 26),
(75, '2021_09_19_122106_create_ems_order_head_table', 27),
(76, '2021_09_19_124140_create_ems_order_item_table', 27),
(77, '2021_09_19_130117_create_ems_order_billing_shipping_table', 27),
(78, '2021_11_02_145829_create_ems_generalpage_table', 28),
(79, '2021_11_03_164228_create_ems_banner_table', 29),
(80, '2022_09_11_153223_added_field_into_general_setting', 30),
(81, '2022_09_11_164926_added_field_1_into_general_setting', 31),
(82, '2022_09_12_112514_added_field_into_category', 32);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `upazilas`
--

CREATE TABLE `upazilas` (
  `id` int UNSIGNED NOT NULL,
  `district_id` int UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL,
  `bn_name` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Dumping data for table `upazilas`
--

INSERT INTO `upazilas` (`id`, `district_id`, `name`, `bn_name`, `updated_at`) VALUES
(1, 34, 'Amtali', 'à¦†à¦®à¦¤à¦²à§€', '2016-04-06 06:48:39'),
(2, 34, 'Bamna ', 'à¦¬à¦¾à¦®à¦¨à¦¾', '0000-00-00 00:00:00'),
(3, 34, 'Barguna Sadar ', 'à¦¬à¦°à¦—à§à¦¨à¦¾ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(4, 34, 'Betagi ', 'à¦¬à§‡à¦¤à¦¾à¦—à¦¿', '0000-00-00 00:00:00'),
(5, 34, 'Patharghata ', 'à¦ªà¦¾à¦¥à¦°à¦˜à¦¾à¦Ÿà¦¾', '0000-00-00 00:00:00'),
(6, 34, 'Taltali ', 'à¦¤à¦¾à¦²à¦¤à¦²à§€', '0000-00-00 00:00:00'),
(7, 35, 'Muladi ', 'à¦®à§à¦²à¦¾à¦¦à¦¿', '0000-00-00 00:00:00'),
(8, 35, 'Babuganj ', 'à¦¬à¦¾à¦¬à§à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(9, 35, 'Agailjhara ', 'à¦†à¦—à¦¾à¦‡à¦²à¦à¦°à¦¾', '0000-00-00 00:00:00'),
(10, 35, 'Barisal Sadar ', 'à¦¬à¦°à¦¿à¦¶à¦¾à¦² à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(11, 35, 'Bakerganj ', 'à¦¬à¦¾à¦•à§‡à¦°à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(12, 35, 'Banaripara ', 'à¦¬à¦¾à¦¨à¦¾à§œà¦¿à¦ªà¦¾à¦°à¦¾', '0000-00-00 00:00:00'),
(13, 35, 'Gaurnadi ', 'à¦—à§Œà¦°à¦¨à¦¦à§€', '0000-00-00 00:00:00'),
(14, 35, 'Hizla ', 'à¦¹à¦¿à¦œà¦²à¦¾', '0000-00-00 00:00:00'),
(15, 35, 'Mehendiganj ', 'à¦®à§‡à¦¹à§‡à¦¦à¦¿à¦—à¦žà§à¦œ ', '0000-00-00 00:00:00'),
(16, 35, 'Wazirpur ', 'à¦“à§Ÿà¦¾à¦œà¦¿à¦°à¦ªà§à¦°', '0000-00-00 00:00:00'),
(17, 36, 'Bhola Sadar ', 'à¦­à§‹à¦²à¦¾ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(18, 36, 'Burhanuddin ', 'à¦¬à§à¦°à¦¹à¦¾à¦¨à¦‰à¦¦à§à¦¦à¦¿à¦¨', '0000-00-00 00:00:00'),
(19, 36, 'Char Fasson ', 'à¦šà¦° à¦«à§à¦¯à¦¾à¦¶à¦¨', '0000-00-00 00:00:00'),
(20, 36, 'Daulatkhan ', 'à¦¦à§Œà¦²à¦¤à¦–à¦¾à¦¨', '0000-00-00 00:00:00'),
(21, 36, 'Lalmohan ', 'à¦²à¦¾à¦²à¦®à§‹à¦¹à¦¨', '0000-00-00 00:00:00'),
(22, 36, 'Manpura ', 'à¦®à¦¨à¦ªà§à¦°à¦¾', '0000-00-00 00:00:00'),
(23, 36, 'Tazumuddin ', 'à¦¤à¦¾à¦œà§à¦®à§à¦¦à§à¦¦à¦¿à¦¨', '0000-00-00 00:00:00'),
(24, 37, 'Jhalokati Sadar ', 'à¦à¦¾à¦²à¦•à¦¾à¦ à¦¿ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(25, 37, 'Kathalia ', 'à¦•à¦¾à¦à¦ à¦¾à¦²à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(26, 37, 'Nalchity ', 'à¦¨à¦¾à¦²à¦šà¦¿à¦¤à¦¿', '0000-00-00 00:00:00'),
(27, 37, 'Rajapur ', 'à¦°à¦¾à¦œà¦¾à¦ªà§à¦°', '0000-00-00 00:00:00'),
(28, 38, 'Bauphal ', 'à¦¬à¦¾à¦‰à¦«à¦²', '0000-00-00 00:00:00'),
(29, 38, 'Dashmina ', 'à¦¦à¦¶à¦®à¦¿à¦¨à¦¾', '0000-00-00 00:00:00'),
(30, 38, 'Galachipa ', 'à¦—à¦²à¦¾à¦šà¦¿à¦ªà¦¾', '0000-00-00 00:00:00'),
(31, 38, 'Kalapara ', 'à¦•à¦¾à¦²à¦¾à¦ªà¦¾à¦°à¦¾', '0000-00-00 00:00:00'),
(32, 38, 'Mirzaganj ', 'à¦®à¦¿à¦°à§à¦œà¦¾à¦—à¦žà§à¦œ ', '0000-00-00 00:00:00'),
(33, 38, 'Patuakhali Sadar ', 'à¦ªà¦Ÿà§à§Ÿà¦¾à¦–à¦¾à¦²à§€ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(34, 38, 'Dumki ', 'à¦¡à§à¦®à¦•à¦¿', '0000-00-00 00:00:00'),
(35, 38, 'Rangabali ', 'à¦°à¦¾à¦™à§à¦—à¦¾à¦¬à¦¾à¦²à¦¿', '0000-00-00 00:00:00'),
(36, 39, 'Bhandaria', 'à¦­à§à¦¯à¦¾à¦¨à§à¦¡à¦¾à¦°à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(37, 39, 'Kaukhali', 'à¦•à¦¾à¦‰à¦–à¦¾à¦²à¦¿', '0000-00-00 00:00:00'),
(38, 39, 'Mathbaria', 'à¦®à¦¾à¦ à¦¬à¦¾à§œà¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(39, 39, 'Nazirpur', 'à¦¨à¦¾à¦œà¦¿à¦°à¦ªà§à¦°', '0000-00-00 00:00:00'),
(40, 39, 'Nesarabad', 'à¦¨à§‡à¦¸à¦¾à¦°à¦¾à¦¬à¦¾à¦¦', '0000-00-00 00:00:00'),
(41, 39, 'Pirojpur Sadar', 'à¦ªà¦¿à¦°à§‹à¦œà¦ªà§à¦° à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(42, 39, 'Zianagar', 'à¦œà¦¿à§Ÿà¦¾à¦¨à¦—à¦°', '0000-00-00 00:00:00'),
(43, 40, 'Bandarban Sadar', 'à¦¬à¦¾à¦¨à§à¦¦à¦°à¦¬à¦¨ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(44, 40, 'Thanchi', 'à¦¥à¦¾à¦¨à¦šà¦¿', '0000-00-00 00:00:00'),
(45, 40, 'Lama', 'à¦²à¦¾à¦®à¦¾', '0000-00-00 00:00:00'),
(46, 40, 'Naikhongchhari', 'à¦¨à¦¾à¦‡à¦–à¦‚à¦›à§œà¦¿ ', '0000-00-00 00:00:00'),
(47, 40, 'Ali kadam', 'à¦†à¦²à§€ à¦•à¦¦à¦®', '0000-00-00 00:00:00'),
(48, 40, 'Rowangchhari', 'à¦°à¦‰à§Ÿà¦¾à¦‚à¦›à§œà¦¿ ', '0000-00-00 00:00:00'),
(49, 40, 'Ruma', 'à¦°à§à¦®à¦¾', '0000-00-00 00:00:00'),
(50, 41, 'Brahmanbaria Sadar ', 'à¦¬à§à¦°à¦¾à¦¹à§à¦®à¦£à¦¬à¦¾à§œà¦¿à§Ÿà¦¾ à¦¸à¦¦à', '0000-00-00 00:00:00'),
(51, 41, 'Ashuganj ', 'à¦†à¦¶à§à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(52, 41, 'Nasirnagar ', 'à¦¨à¦¾à¦¸à¦¿à¦° à¦¨à¦—à¦°', '0000-00-00 00:00:00'),
(53, 41, 'Nabinagar ', 'à¦¨à¦¬à§€à¦¨à¦—à¦°', '0000-00-00 00:00:00'),
(54, 41, 'Sarail ', 'à¦¸à¦°à¦¾à¦‡à¦²', '0000-00-00 00:00:00'),
(55, 41, 'Shahbazpur Town', 'à¦¶à¦¾à¦¹à¦¬à¦¾à¦œà¦ªà§à¦° à¦Ÿà¦¾à¦‰à¦¨', '0000-00-00 00:00:00'),
(56, 41, 'Kasba ', 'à¦•à¦¸à¦¬à¦¾', '0000-00-00 00:00:00'),
(57, 41, 'Akhaura ', 'à¦†à¦–à¦¾à¦‰à¦°à¦¾', '0000-00-00 00:00:00'),
(58, 41, 'Bancharampur ', 'à¦¬à¦¾à¦žà§à¦›à¦¾à¦°à¦¾à¦®à¦ªà§à¦°', '0000-00-00 00:00:00'),
(59, 41, 'Bijoynagar ', 'à¦¬à¦¿à¦œà§Ÿ à¦¨à¦—à¦°', '0000-00-00 00:00:00'),
(60, 42, 'Chandpur Sadar', 'à¦šà¦¾à¦à¦¦à¦ªà§à¦° à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(61, 42, 'Faridganj', 'à¦«à¦°à¦¿à¦¦à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(62, 42, 'Haimchar', 'à¦¹à¦¾à¦‡à¦®à¦šà¦°', '0000-00-00 00:00:00'),
(63, 42, 'Haziganj', 'à¦¹à¦¾à¦œà§€à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(64, 42, 'Kachua', 'à¦•à¦šà§à§Ÿà¦¾', '0000-00-00 00:00:00'),
(65, 42, 'Matlab Uttar', 'à¦®à¦¤à¦²à¦¬ à¦‰à¦¤à§à¦¤à¦°', '0000-00-00 00:00:00'),
(66, 42, 'Matlab Dakkhin', 'à¦®à¦¤à¦²à¦¬ à¦¦à¦•à§à¦·à¦¿à¦£', '0000-00-00 00:00:00'),
(67, 42, 'Shahrasti', 'à¦¶à¦¾à¦¹à¦°à¦¾à¦¸à§à¦¤à¦¿', '0000-00-00 00:00:00'),
(68, 43, 'Anwara ', 'à¦†à¦¨à§‹à§Ÿà¦¾à¦°à¦¾', '0000-00-00 00:00:00'),
(69, 43, 'Banshkhali ', 'à¦¬à¦¾à¦¶à¦–à¦¾à¦²à¦¿', '0000-00-00 00:00:00'),
(70, 43, 'Boalkhali ', 'à¦¬à§‹à§Ÿà¦¾à¦²à¦–à¦¾à¦²à¦¿', '0000-00-00 00:00:00'),
(71, 43, 'Chandanaish ', 'à¦šà¦¨à§à¦¦à¦¨à¦¾à¦‡à¦¶', '0000-00-00 00:00:00'),
(72, 43, 'Fatikchhari ', 'à¦«à¦Ÿà¦¿à¦•à¦›à§œà¦¿', '0000-00-00 00:00:00'),
(73, 43, 'Hathazari ', 'à¦¹à¦¾à¦ à¦¹à¦¾à¦œà¦¾à¦°à§€', '0000-00-00 00:00:00'),
(74, 43, 'Lohagara ', 'à¦²à§‹à¦¹à¦¾à¦—à¦¾à¦°à¦¾', '0000-00-00 00:00:00'),
(75, 43, 'Mirsharai ', 'à¦®à¦¿à¦°à¦¸à¦°à¦¾à¦‡', '0000-00-00 00:00:00'),
(76, 43, 'Patiya ', 'à¦ªà¦Ÿà¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(77, 43, 'Rangunia ', 'à¦°à¦¾à¦™à§à¦—à§à¦¨à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(78, 43, 'Raozan ', 'à¦°à¦¾à¦‰à¦œà¦¾à¦¨', '0000-00-00 00:00:00'),
(79, 43, 'Sandwip ', 'à¦¸à¦¨à§à¦¦à§à¦¬à§€à¦ª', '0000-00-00 00:00:00'),
(80, 43, 'Satkania ', 'à¦¸à¦¾à¦¤à¦•à¦¾à¦¨à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(81, 43, 'Sitakunda ', 'à¦¸à§€à¦¤à¦¾à¦•à§à¦£à§à¦¡', '0000-00-00 00:00:00'),
(82, 44, 'Barura ', 'à¦¬à§œà§à¦°à¦¾', '0000-00-00 00:00:00'),
(83, 44, 'Brahmanpara ', 'à¦¬à§à¦°à¦¾à¦¹à§à¦®à¦£à¦ªà¦¾à§œà¦¾', '0000-00-00 00:00:00'),
(84, 44, 'Burichong ', 'à¦¬à§à§œà¦¿à¦šà¦‚', '0000-00-00 00:00:00'),
(85, 44, 'Chandina ', 'à¦šà¦¾à¦¨à§à¦¦à¦¿à¦¨à¦¾', '0000-00-00 00:00:00'),
(86, 44, 'Chauddagram ', 'à¦šà§Œà¦¦à§à¦¦à¦—à§à¦°à¦¾à¦®', '0000-00-00 00:00:00'),
(87, 44, 'Daudkandi ', 'à¦¦à¦¾à¦‰à¦¦à¦•à¦¾à¦¨à§à¦¦à¦¿', '0000-00-00 00:00:00'),
(88, 44, 'Debidwar ', 'à¦¦à§‡à¦¬à§€à¦¦à§à¦¬à¦¾à¦°', '0000-00-00 00:00:00'),
(89, 44, 'Homna ', 'à¦¹à§‹à¦®à¦¨à¦¾', '0000-00-00 00:00:00'),
(90, 44, 'Comilla Sadar ', 'à¦•à§à¦®à¦¿à¦²à§à¦²à¦¾ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(91, 44, 'Laksam ', 'à¦²à¦¾à¦•à¦¸à¦¾à¦®', '0000-00-00 00:00:00'),
(92, 44, 'Monohorgonj ', 'à¦®à¦¨à§‹à¦¹à¦°à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(93, 44, 'Meghna ', 'à¦®à§‡à¦˜à¦¨à¦¾', '0000-00-00 00:00:00'),
(94, 44, 'Muradnagar ', 'à¦®à§à¦°à¦¾à¦¦à¦¨à¦—à¦°', '0000-00-00 00:00:00'),
(95, 44, 'Nangalkot ', 'à¦¨à¦¾à¦™à§à¦—à¦¾à¦²à¦•à§‹à¦Ÿ', '0000-00-00 00:00:00'),
(96, 44, 'Comilla Sadar South ', 'à¦•à§à¦®à¦¿à¦²à§à¦²à¦¾ à¦¸à¦¦à¦° à¦¦à¦•à§à¦·à¦¿', '0000-00-00 00:00:00'),
(97, 44, 'Titas ', 'à¦¤à¦¿à¦¤à¦¾à¦¸', '0000-00-00 00:00:00'),
(98, 45, 'Chakaria ', 'à¦šà¦•à¦°à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(99, 45, 'Chakaria ', 'à¦šà¦•à¦°à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(100, 45, 'Cox\'s Bazar Sadar ', 'à¦•à¦•à§à¦¸ à¦¬à¦¾à¦œà¦¾à¦° à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(101, 45, 'Kutubdia ', 'à¦•à§à¦¤à§à¦¬à¦¦à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(102, 45, 'Maheshkhali ', 'à¦®à¦¹à§‡à¦¶à¦–à¦¾à¦²à§€', '0000-00-00 00:00:00'),
(103, 45, 'Ramu ', 'à¦°à¦¾à¦®à§', '0000-00-00 00:00:00'),
(104, 45, 'Teknaf ', 'à¦Ÿà§‡à¦•à¦¨à¦¾à¦«', '0000-00-00 00:00:00'),
(105, 45, 'Ukhia ', 'à¦‰à¦–à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(106, 45, 'Pekua ', 'à¦ªà§‡à¦•à§à§Ÿà¦¾', '0000-00-00 00:00:00'),
(107, 46, 'Feni Sadar', 'à¦«à§‡à¦¨à§€ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(108, 46, 'Chagalnaiya', 'à¦›à¦¾à¦—à¦² à¦¨à¦¾à¦‡à§Ÿà¦¾', '0000-00-00 00:00:00'),
(109, 46, 'Daganbhyan', 'à¦¦à¦¾à¦—à¦¾à¦¨à¦­à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(110, 46, 'Parshuram', 'à¦ªà¦°à¦¶à§à¦°à¦¾à¦®', '0000-00-00 00:00:00'),
(111, 46, 'Fhulgazi', 'à¦«à§à¦²à¦—à¦¾à¦œà¦¿', '0000-00-00 00:00:00'),
(112, 46, 'Sonagazi', 'à¦¸à§‹à¦¨à¦¾à¦—à¦¾à¦œà¦¿', '0000-00-00 00:00:00'),
(113, 47, 'Dighinala ', 'à¦¦à¦¿à¦˜à¦¿à¦¨à¦¾à¦²à¦¾ ', '0000-00-00 00:00:00'),
(114, 47, 'Khagrachhari ', 'à¦–à¦¾à¦—à§œà¦¾à¦›à§œà¦¿', '0000-00-00 00:00:00'),
(115, 47, 'Lakshmichhari ', 'à¦²à¦•à§à¦·à§à¦®à§€à¦›à§œà¦¿', '0000-00-00 00:00:00'),
(116, 47, 'Mahalchhari ', 'à¦®à¦¹à¦²à¦›à§œà¦¿', '0000-00-00 00:00:00'),
(117, 47, 'Manikchhari ', 'à¦®à¦¾à¦¨à¦¿à¦•à¦›à§œà¦¿', '0000-00-00 00:00:00'),
(118, 47, 'Matiranga ', 'à¦®à¦¾à¦Ÿà¦¿à¦°à¦¾à¦™à§à¦—à¦¾', '0000-00-00 00:00:00'),
(119, 47, 'Panchhari ', 'à¦ªà¦¾à¦¨à¦›à§œà¦¿', '0000-00-00 00:00:00'),
(120, 47, 'Ramgarh ', 'à¦°à¦¾à¦®à¦—à§œ', '0000-00-00 00:00:00'),
(121, 48, 'Lakshmipur Sadar ', 'à¦²à¦•à§à¦·à§à¦®à§€à¦ªà§à¦° à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(122, 48, 'Raipur ', 'à¦°à¦¾à§Ÿà¦ªà§à¦°', '0000-00-00 00:00:00'),
(123, 48, 'Ramganj ', 'à¦°à¦¾à¦®à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(124, 48, 'Ramgati ', 'à¦°à¦¾à¦®à¦—à¦¤à¦¿', '0000-00-00 00:00:00'),
(125, 48, 'Komol Nagar ', 'à¦•à¦®à¦² à¦¨à¦—à¦°', '0000-00-00 00:00:00'),
(126, 49, 'Noakhali Sadar ', 'à¦¨à§‹à§Ÿà¦¾à¦–à¦¾à¦²à§€ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(127, 49, 'Begumganj ', 'à¦¬à§‡à¦—à¦®à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(128, 49, 'Chatkhil ', 'à¦šà¦¾à¦Ÿà¦–à¦¿à¦²', '0000-00-00 00:00:00'),
(129, 49, 'Companyganj ', 'à¦•à§‹à¦®à§à¦ªà¦¾à¦¨à§€à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(130, 49, 'Shenbag ', 'à¦¶à§‡à¦¨à¦¬à¦¾à¦—', '0000-00-00 00:00:00'),
(131, 49, 'Hatia ', 'à¦¹à¦¾à¦¤à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(132, 49, 'Kobirhat ', 'à¦•à¦¬à¦¿à¦°à¦¹à¦¾à¦Ÿ ', '0000-00-00 00:00:00'),
(133, 49, 'Sonaimuri ', 'à¦¸à§‹à¦¨à¦¾à¦‡à¦®à§à¦°à¦¿', '0000-00-00 00:00:00'),
(134, 49, 'Suborno Char ', 'à¦¸à§à¦¬à¦°à§à¦£ à¦šà¦° ', '0000-00-00 00:00:00'),
(135, 50, 'Rangamati Sadar ', 'à¦°à¦¾à¦™à§à¦—à¦¾à¦®à¦¾à¦Ÿà¦¿ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(136, 50, 'Belaichhari ', 'à¦¬à§‡à¦²à¦¾à¦‡à¦›à§œà¦¿', '0000-00-00 00:00:00'),
(137, 50, 'Bagaichhari ', 'à¦¬à¦¾à¦˜à¦¾à¦‡à¦›à§œà¦¿', '0000-00-00 00:00:00'),
(138, 50, 'Barkal ', 'à¦¬à¦°à¦•à¦²', '0000-00-00 00:00:00'),
(139, 50, 'Juraichhari ', 'à¦œà§à¦°à¦¾à¦‡à¦›à§œà¦¿', '0000-00-00 00:00:00'),
(140, 50, 'Rajasthali ', 'à¦°à¦¾à¦œà¦¾à¦¸à§à¦¥à¦²à¦¿', '0000-00-00 00:00:00'),
(141, 50, 'Kaptai ', 'à¦•à¦¾à¦ªà§à¦¤à¦¾à¦‡', '0000-00-00 00:00:00'),
(142, 50, 'Langadu ', 'à¦²à¦¾à¦™à§à¦—à¦¾à¦¡à§', '0000-00-00 00:00:00'),
(143, 50, 'Nannerchar ', 'à¦¨à¦¾à¦¨à§à¦¨à§‡à¦°à¦šà¦° ', '0000-00-00 00:00:00'),
(144, 50, 'Kaukhali ', 'à¦•à¦¾à¦‰à¦–à¦¾à¦²à¦¿', '0000-00-00 00:00:00'),
(150, 2, 'Faridpur Sadar ', 'à¦«à¦°à¦¿à¦¦à¦ªà§à¦° à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(151, 2, 'Boalmari ', 'à¦¬à§‹à§Ÿà¦¾à¦²à¦®à¦¾à¦°à§€', '0000-00-00 00:00:00'),
(152, 2, 'Alfadanga ', 'à¦†à¦²à¦«à¦¾à¦¡à¦¾à¦™à§à¦—à¦¾', '0000-00-00 00:00:00'),
(153, 2, 'Madhukhali ', 'à¦®à¦§à§à¦–à¦¾à¦²à¦¿', '0000-00-00 00:00:00'),
(154, 2, 'Bhanga ', 'à¦­à¦¾à¦™à§à¦—à¦¾', '0000-00-00 00:00:00'),
(155, 2, 'Nagarkanda ', 'à¦¨à¦—à¦°à¦•à¦¾à¦¨à§à¦¡', '0000-00-00 00:00:00'),
(156, 2, 'Charbhadrasan ', 'à¦šà¦°à¦­à¦¦à§à¦°à¦¾à¦¸à¦¨ ', '0000-00-00 00:00:00'),
(157, 2, 'Sadarpur ', 'à¦¸à¦¦à¦°à¦ªà§à¦°', '0000-00-00 00:00:00'),
(158, 2, 'Shaltha ', 'à¦¶à¦¾à¦²à¦¥à¦¾', '0000-00-00 00:00:00'),
(159, 3, 'Gazipur Sadar-Joydebpur', 'à¦—à¦¾à¦œà§€à¦ªà§à¦° à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(160, 3, 'Kaliakior', 'à¦•à¦¾à¦²à¦¿à§Ÿà¦¾à¦•à§ˆà¦°', '0000-00-00 00:00:00'),
(161, 3, 'Kapasia', 'à¦•à¦¾à¦ªà¦¾à¦¸à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(162, 3, 'Sripur', 'à¦¶à§à¦°à§€à¦ªà§à¦°', '0000-00-00 00:00:00'),
(163, 3, 'Kaliganj', 'à¦•à¦¾à¦²à§€à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(164, 3, 'Tongi', 'à¦Ÿà¦™à§à¦—à¦¿', '0000-00-00 00:00:00'),
(165, 4, 'Gopalganj Sadar ', 'à¦—à§‹à¦ªà¦¾à¦²à¦—à¦žà§à¦œ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(166, 4, 'Kashiani ', 'à¦•à¦¾à¦¶à¦¿à§Ÿà¦¾à¦¨à¦¿', '0000-00-00 00:00:00'),
(167, 4, 'Kotalipara ', 'à¦•à§‹à¦Ÿà¦¾à¦²à¦¿à¦ªà¦¾à§œà¦¾', '0000-00-00 00:00:00'),
(168, 4, 'Muksudpur ', 'à¦®à§à¦•à¦¸à§à¦¦à¦ªà§à¦°', '0000-00-00 00:00:00'),
(169, 4, 'Tungipara ', 'à¦Ÿà§à¦™à§à¦—à¦¿à¦ªà¦¾à§œà¦¾', '0000-00-00 00:00:00'),
(170, 5, 'Dewanganj ', 'à¦¦à§‡à¦“à§Ÿà¦¾à¦¨à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(171, 5, 'Baksiganj ', 'à¦¬à¦•à¦¸à¦¿à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(172, 5, 'Islampur ', 'à¦‡à¦¸à¦²à¦¾à¦®à¦ªà§à¦°', '0000-00-00 00:00:00'),
(173, 5, 'Jamalpur Sadar ', 'à¦œà¦¾à¦®à¦¾à¦²à¦ªà§à¦° à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(174, 5, 'Madarganj ', 'à¦®à¦¾à¦¦à¦¾à¦°à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(175, 5, 'Melandaha ', 'à¦®à§‡à¦²à¦¾à¦¨à¦¦à¦¾à¦¹à¦¾', '0000-00-00 00:00:00'),
(176, 5, 'Sarishabari ', 'à¦¸à¦°à¦¿à¦·à¦¾à¦¬à¦¾à§œà¦¿ ', '0000-00-00 00:00:00'),
(177, 5, 'Narundi Police I.C', 'à¦¨à¦¾à¦°à§à¦¨à§à¦¦à¦¿', '0000-00-00 00:00:00'),
(178, 6, 'Astagram ', 'à¦…à¦·à§à¦Ÿà¦—à§à¦°à¦¾à¦®', '0000-00-00 00:00:00'),
(179, 6, 'Bajitpur ', 'à¦¬à¦¾à¦œà¦¿à¦¤à¦ªà§à¦°', '0000-00-00 00:00:00'),
(180, 6, 'Bhairab ', 'à¦­à§ˆà¦°à¦¬', '0000-00-00 00:00:00'),
(181, 6, 'Hossainpur ', 'à¦¹à§‹à¦¸à§‡à¦¨à¦ªà§à¦° ', '0000-00-00 00:00:00'),
(182, 6, 'Itna ', 'à¦‡à¦Ÿà¦¨à¦¾', '0000-00-00 00:00:00'),
(183, 6, 'Karimganj ', 'à¦•à¦°à¦¿à¦®à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(184, 6, 'Katiadi ', 'à¦•à¦¤à¦¿à§Ÿà¦¾à¦¦à¦¿', '0000-00-00 00:00:00'),
(185, 6, 'Kishoreganj Sadar ', 'à¦•à¦¿à¦¶à§‹à¦°à¦—à¦žà§à¦œ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(186, 6, 'Kuliarchar ', 'à¦•à§à¦²à¦¿à§Ÿà¦¾à¦°à¦šà¦°', '0000-00-00 00:00:00'),
(187, 6, 'Mithamain ', 'à¦®à¦¿à¦ à¦¾à¦®à¦¾à¦‡à¦¨', '0000-00-00 00:00:00'),
(188, 6, 'Nikli ', 'à¦¨à¦¿à¦•à¦²à¦¿', '0000-00-00 00:00:00'),
(189, 6, 'Pakundia ', 'à¦ªà¦¾à¦•à§à¦¨à§à¦¡à¦¾', '0000-00-00 00:00:00'),
(190, 6, 'Tarail ', 'à¦¤à¦¾à§œà¦¾à¦‡à¦²', '0000-00-00 00:00:00'),
(191, 7, 'Madaripur Sadar', 'à¦®à¦¾à¦¦à¦¾à¦°à§€à¦ªà§à¦° à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(192, 7, 'Kalkini', 'à¦•à¦¾à¦²à¦•à¦¿à¦¨à¦¿', '0000-00-00 00:00:00'),
(193, 7, 'Rajoir', 'à¦°à¦¾à¦œà¦‡à¦°', '0000-00-00 00:00:00'),
(194, 7, 'Shibchar', 'à¦¶à¦¿à¦¬à¦šà¦°', '0000-00-00 00:00:00'),
(195, 8, 'Manikganj Sadar ', 'à¦®à¦¾à¦¨à¦¿à¦•à¦—à¦žà§à¦œ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(196, 8, 'Singair ', 'à¦¸à¦¿à¦™à§à¦—à¦¾à¦‡à¦°', '0000-00-00 00:00:00'),
(197, 8, 'Shibalaya ', 'à¦¶à¦¿à¦¬à¦¾à¦²à§Ÿ', '0000-00-00 00:00:00'),
(198, 8, 'Saturia ', 'à¦¸à¦¾à¦ à§à¦°à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(199, 8, 'Harirampur ', 'à¦¹à¦°à¦¿à¦°à¦¾à¦®à¦ªà§à¦°', '0000-00-00 00:00:00'),
(200, 8, 'Ghior ', 'à¦˜à¦¿à¦“à¦°', '0000-00-00 00:00:00'),
(201, 8, 'Daulatpur ', 'à¦¦à§Œà¦²à¦¤à¦ªà§à¦°', '0000-00-00 00:00:00'),
(202, 9, 'Lohajang ', 'à¦²à§‹à¦¹à¦¾à¦œà¦‚', '0000-00-00 00:00:00'),
(203, 9, 'Sreenagar ', 'à¦¶à§à¦°à§€à¦¨à¦—à¦°', '0000-00-00 00:00:00'),
(204, 9, 'Munshiganj Sadar ', 'à¦®à§à¦¨à§à¦¸à¦¿à¦—à¦žà§à¦œ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(205, 9, 'Sirajdikhan ', 'à¦¸à¦¿à¦°à¦¾à¦œà¦¦à¦¿à¦–à¦¾à¦¨', '0000-00-00 00:00:00'),
(206, 9, 'Tongibari ', 'à¦Ÿà¦™à§à¦—à¦¿à¦¬à¦¾à§œà¦¿', '0000-00-00 00:00:00'),
(207, 9, 'Gazaria ', 'à¦—à¦œà¦¾à¦°à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(208, 10, 'Bhaluka', 'à¦­à¦¾à¦²à§à¦•à¦¾', '0000-00-00 00:00:00'),
(209, 10, 'Trishal', 'à¦¤à§à¦°à¦¿à¦¶à¦¾à¦²', '0000-00-00 00:00:00'),
(210, 10, 'Haluaghat', 'à¦¹à¦¾à¦²à§à§Ÿà¦¾à¦˜à¦¾à¦Ÿ', '0000-00-00 00:00:00'),
(211, 10, 'Muktagachha', 'à¦®à§à¦•à§à¦¤à¦¾à¦—à¦¾à¦›à¦¾', '0000-00-00 00:00:00'),
(212, 10, 'Dhobaura', 'à¦§à¦¬à¦¾à¦°à§à§Ÿà¦¾', '0000-00-00 00:00:00'),
(213, 10, 'Fulbaria', 'à¦«à§à¦²à¦¬à¦¾à§œà¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(214, 10, 'Gaffargaon', 'à¦—à¦«à¦°à¦—à¦¾à¦à¦“', '0000-00-00 00:00:00'),
(215, 10, 'Gauripur', 'à¦—à§Œà¦°à¦¿à¦ªà§à¦°', '0000-00-00 00:00:00'),
(216, 10, 'Ishwarganj', 'à¦ˆà¦¶à§à¦¬à¦°à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(217, 10, 'Mymensingh Sadar', 'à¦®à§Ÿà¦®à¦¨à¦¸à¦¿à¦‚ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(218, 10, 'Nandail', 'à¦¨à¦¨à§à¦¦à¦¾à¦‡à¦²', '0000-00-00 00:00:00'),
(219, 10, 'Phulpur', 'à¦«à§à¦²à¦ªà§à¦°', '0000-00-00 00:00:00'),
(220, 11, 'Araihazar ', 'à¦†à§œà¦¾à¦‡à¦¹à¦¾à¦œà¦¾à¦°', '0000-00-00 00:00:00'),
(221, 11, 'Sonargaon ', 'à¦¸à§‹à¦¨à¦¾à¦°à¦—à¦¾à¦à¦“', '0000-00-00 00:00:00'),
(222, 11, 'Bandar', 'à¦¬à¦¾à¦¨à§à¦¦à¦¾à¦°', '0000-00-00 00:00:00'),
(223, 11, 'Naryanganj Sadar ', 'à¦¨à¦¾à¦°à¦¾à§Ÿà¦¾à¦¨à¦—à¦žà§à¦œ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(224, 11, 'Rupganj ', 'à¦°à§‚à¦ªà¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(225, 11, 'Siddirgonj ', 'à¦¸à¦¿à¦¦à§à¦§à¦¿à¦°à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(226, 12, 'Belabo ', 'à¦¬à§‡à¦²à¦¾à¦¬à§‹', '0000-00-00 00:00:00'),
(227, 12, 'Monohardi ', 'à¦®à¦¨à§‹à¦¹à¦°à¦¦à¦¿', '0000-00-00 00:00:00'),
(228, 12, 'Narsingdi Sadar ', 'à¦¨à¦°à¦¸à¦¿à¦‚à¦¦à§€ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(229, 12, 'Palash ', 'à¦ªà¦²à¦¾à¦¶', '0000-00-00 00:00:00'),
(230, 12, 'Raipura , Narsingdi', 'à¦°à¦¾à§Ÿà¦ªà§à¦°', '0000-00-00 00:00:00'),
(231, 12, 'Shibpur ', 'à¦¶à¦¿à¦¬à¦ªà§à¦°', '0000-00-00 00:00:00'),
(232, 13, 'Kendua Upazilla', 'à¦•à§‡à¦¨à§à¦¦à§à§Ÿà¦¾', '0000-00-00 00:00:00'),
(233, 13, 'Atpara Upazilla', 'à¦†à¦Ÿà¦ªà¦¾à§œà¦¾', '0000-00-00 00:00:00'),
(234, 13, 'Barhatta Upazilla', 'à¦¬à¦°à¦¹à¦¾à¦Ÿà§à¦Ÿà¦¾', '0000-00-00 00:00:00'),
(235, 13, 'Durgapur Upazilla', 'à¦¦à§à¦°à§à¦—à¦¾à¦ªà§à¦°', '0000-00-00 00:00:00'),
(236, 13, 'Kalmakanda Upazilla', 'à¦•à¦²à¦®à¦¾à¦•à¦¾à¦¨à§à¦¦à¦¾', '0000-00-00 00:00:00'),
(237, 13, 'Madan Upazilla', 'à¦®à¦¦à¦¨', '0000-00-00 00:00:00'),
(238, 13, 'Mohanganj Upazilla', 'à¦®à§‹à¦¹à¦¨à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(239, 13, 'Netrakona-S Upazilla', 'à¦¨à§‡à¦¤à§à¦°à¦•à§‹à¦¨à¦¾ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(240, 13, 'Purbadhala Upazilla', 'à¦ªà§‚à¦°à§à¦¬à¦§à¦²à¦¾', '0000-00-00 00:00:00'),
(241, 13, 'Khaliajuri Upazilla', 'à¦–à¦¾à¦²à¦¿à§Ÿà¦¾à¦œà§à¦°à¦¿', '0000-00-00 00:00:00'),
(242, 14, 'Baliakandi ', 'à¦¬à¦¾à¦²à¦¿à§Ÿà¦¾à¦•à¦¾à¦¨à§à¦¦à¦¿', '0000-00-00 00:00:00'),
(243, 14, 'Goalandaghat ', 'à¦—à§‹à§Ÿà¦¾à¦²à¦¨à§à¦¦ à¦˜à¦¾à¦Ÿ', '0000-00-00 00:00:00'),
(244, 14, 'Pangsha ', 'à¦ªà¦¾à¦‚à¦¶à¦¾', '0000-00-00 00:00:00'),
(245, 14, 'Kalukhali ', 'à¦•à¦¾à¦²à§à¦–à¦¾à¦²à¦¿', '0000-00-00 00:00:00'),
(246, 14, 'Rajbari Sadar ', 'à¦°à¦¾à¦œà¦¬à¦¾à§œà¦¿ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(247, 15, 'Shariatpur Sadar -Palong', 'à¦¶à¦°à§€à§Ÿà¦¤à¦ªà§à¦° à¦¸à¦¦à¦° ', '0000-00-00 00:00:00'),
(248, 15, 'Damudya ', 'à¦¦à¦¾à¦®à§à¦¦à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(249, 15, 'Naria ', 'à¦¨à§œà¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(250, 15, 'Jajira ', 'à¦œà¦¾à¦œà¦¿à¦°à¦¾', '0000-00-00 00:00:00'),
(251, 15, 'Bhedarganj ', 'à¦­à§‡à¦¦à¦¾à¦°à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(252, 15, 'Gosairhat ', 'à¦—à§‹à¦¸à¦¾à¦‡à¦° à¦¹à¦¾à¦Ÿ ', '0000-00-00 00:00:00'),
(253, 16, 'Jhenaigati ', 'à¦à¦¿à¦¨à¦¾à¦‡à¦—à¦¾à¦¤à¦¿', '0000-00-00 00:00:00'),
(254, 16, 'Nakla ', 'à¦¨à¦¾à¦•à¦²à¦¾', '0000-00-00 00:00:00'),
(255, 16, 'Nalitabari ', 'à¦¨à¦¾à¦²à¦¿à¦¤à¦¾à¦¬à¦¾à§œà¦¿', '0000-00-00 00:00:00'),
(256, 16, 'Sherpur Sadar ', 'à¦¶à§‡à¦°à¦ªà§à¦° à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(257, 16, 'Sreebardi ', 'à¦¶à§à¦°à§€à¦¬à¦°à¦¦à¦¿', '0000-00-00 00:00:00'),
(258, 17, 'Tangail Sadar ', 'à¦Ÿà¦¾à¦™à§à¦—à¦¾à¦‡à¦² à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(259, 17, 'Sakhipur ', 'à¦¸à¦–à¦¿à¦ªà§à¦°', '0000-00-00 00:00:00'),
(260, 17, 'Basail ', 'à¦¬à¦¸à¦¾à¦‡à¦²', '0000-00-00 00:00:00'),
(261, 17, 'Madhupur ', 'à¦®à¦§à§à¦ªà§à¦°', '0000-00-00 00:00:00'),
(262, 17, 'Ghatail ', 'à¦˜à¦¾à¦Ÿà¦¾à¦‡à¦²', '0000-00-00 00:00:00'),
(263, 17, 'Kalihati ', 'à¦•à¦¾à¦²à¦¿à¦¹à¦¾à¦¤à¦¿', '0000-00-00 00:00:00'),
(264, 17, 'Nagarpur ', 'à¦¨à¦—à¦°à¦ªà§à¦°', '0000-00-00 00:00:00'),
(265, 17, 'Mirzapur ', 'à¦®à¦¿à¦°à§à¦œà¦¾à¦ªà§à¦°', '0000-00-00 00:00:00'),
(266, 17, 'Gopalpur ', 'à¦—à§‹à¦ªà¦¾à¦²à¦ªà§à¦°', '0000-00-00 00:00:00'),
(267, 17, 'Delduar ', 'à¦¦à§‡à¦²à¦¦à§à§Ÿà¦¾à¦°', '0000-00-00 00:00:00'),
(268, 17, 'Bhuapur ', 'à¦­à§à§Ÿà¦¾à¦ªà§à¦°', '0000-00-00 00:00:00'),
(269, 17, 'Dhanbari ', 'à¦§à¦¾à¦¨à¦¬à¦¾à§œà¦¿', '0000-00-00 00:00:00'),
(270, 55, 'Bagerhat Sadar ', 'à¦¬à¦¾à¦—à§‡à¦°à¦¹à¦¾à¦Ÿ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(271, 55, 'Chitalmari ', 'à¦šà¦¿à¦¤à¦²à¦®à¦¾à§œà¦¿', '0000-00-00 00:00:00'),
(272, 55, 'Fakirhat ', 'à¦«à¦•à¦¿à¦°à¦¹à¦¾à¦Ÿ', '0000-00-00 00:00:00'),
(273, 55, 'Kachua ', 'à¦•à¦šà§à§Ÿà¦¾', '0000-00-00 00:00:00'),
(274, 55, 'Mollahat ', 'à¦®à§‹à¦²à§à¦²à¦¾à¦¹à¦¾à¦Ÿ ', '0000-00-00 00:00:00'),
(275, 55, 'Mongla ', 'à¦®à¦‚à¦²à¦¾', '0000-00-00 00:00:00'),
(276, 55, 'Morrelganj ', 'à¦®à¦°à§‡à¦²à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(277, 55, 'Rampal ', 'à¦°à¦¾à¦®à¦ªà¦¾à¦²', '0000-00-00 00:00:00'),
(278, 55, 'Sarankhola ', 'à¦¸à§à¦®à¦°à¦£à¦–à§‹à¦²à¦¾', '0000-00-00 00:00:00'),
(279, 56, 'Damurhuda ', 'à¦¦à¦¾à¦®à§à¦°à¦¹à§à¦¦à¦¾', '0000-00-00 00:00:00'),
(280, 56, 'Chuadanga-S ', 'à¦šà§à§Ÿà¦¾à¦¡à¦¾à¦™à§à¦—à¦¾ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(281, 56, 'Jibannagar ', 'à¦œà§€à¦¬à¦¨ à¦¨à¦—à¦° ', '0000-00-00 00:00:00'),
(282, 56, 'Alamdanga ', 'à¦†à¦²à¦®à¦¡à¦¾à¦™à§à¦—à¦¾', '0000-00-00 00:00:00'),
(283, 57, 'Abhaynagar ', 'à¦…à¦­à§Ÿà¦¨à¦—à¦°', '0000-00-00 00:00:00'),
(284, 57, 'Keshabpur ', 'à¦•à§‡à¦¶à¦¬à¦ªà§à¦°', '0000-00-00 00:00:00'),
(285, 57, 'Bagherpara ', 'à¦¬à¦¾à¦˜à§‡à¦° à¦ªà¦¾à§œà¦¾ ', '0000-00-00 00:00:00'),
(286, 57, 'Jessore Sadar ', 'à¦¯à¦¶à§‹à¦° à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(287, 57, 'Chaugachha ', 'à¦šà§Œà¦—à¦¾à¦›à¦¾', '0000-00-00 00:00:00'),
(288, 57, 'Manirampur ', 'à¦®à¦¨à¦¿à¦°à¦¾à¦®à¦ªà§à¦° ', '0000-00-00 00:00:00'),
(289, 57, 'Jhikargachha ', 'à¦à¦¿à¦•à¦°à¦—à¦¾à¦›à¦¾', '0000-00-00 00:00:00'),
(290, 57, 'Sharsha ', 'à¦¸à¦¾à¦°à¦¶à¦¾', '0000-00-00 00:00:00'),
(291, 58, 'Jhenaidah Sadar ', 'à¦à¦¿à¦¨à¦¾à¦‡à¦¦à¦¹ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(292, 58, 'Maheshpur ', 'à¦®à¦¹à§‡à¦¶à¦ªà§à¦°', '0000-00-00 00:00:00'),
(293, 58, 'Kaliganj ', 'à¦•à¦¾à¦²à§€à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(294, 58, 'Kotchandpur ', 'à¦•à§‹à¦Ÿ à¦šà¦¾à¦à¦¦à¦ªà§à¦° ', '0000-00-00 00:00:00'),
(295, 58, 'Shailkupa ', 'à¦¶à§ˆà¦²à¦•à§à¦ªà¦¾', '0000-00-00 00:00:00'),
(296, 58, 'Harinakunda ', 'à¦¹à¦¾à§œà¦¿à¦¨à¦¾à¦•à§à¦¨à§à¦¦à¦¾', '0000-00-00 00:00:00'),
(297, 59, 'Terokhada ', 'à¦¤à§‡à¦°à§‹à¦–à¦¾à¦¦à¦¾', '0000-00-00 00:00:00'),
(298, 59, 'Batiaghata ', 'à¦¬à¦¾à¦Ÿà¦¿à§Ÿà¦¾à¦˜à¦¾à¦Ÿà¦¾ ', '0000-00-00 00:00:00'),
(299, 59, 'Dacope ', 'à¦¡à¦¾à¦•à¦ªà§‡', '0000-00-00 00:00:00'),
(300, 59, 'Dumuria ', 'à¦¡à§à¦®à§à¦°à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(301, 59, 'Dighalia ', 'à¦¦à¦¿à¦˜à¦²à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(302, 59, 'Koyra ', 'à¦•à§Ÿà§œà¦¾', '0000-00-00 00:00:00'),
(303, 59, 'Paikgachha ', 'à¦ªà¦¾à¦‡à¦•à¦—à¦¾à¦›à¦¾', '0000-00-00 00:00:00'),
(304, 59, 'Phultala ', 'à¦«à§à¦²à¦¤à¦²à¦¾', '0000-00-00 00:00:00'),
(305, 59, 'Rupsa ', 'à¦°à§‚à¦ªà¦¸à¦¾', '0000-00-00 00:00:00'),
(306, 60, 'Kushtia Sadar', 'à¦•à§à¦·à§à¦Ÿà¦¿à§Ÿà¦¾ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(307, 60, 'Kumarkhali', 'à¦•à§à¦®à¦¾à¦°à¦–à¦¾à¦²à¦¿', '0000-00-00 00:00:00'),
(308, 60, 'Daulatpur', 'à¦¦à§Œà¦²à¦¤à¦ªà§à¦°', '0000-00-00 00:00:00'),
(309, 60, 'Mirpur', 'à¦®à¦¿à¦°à¦ªà§à¦°', '0000-00-00 00:00:00'),
(310, 60, 'Bheramara', 'à¦­à§‡à¦°à¦¾à¦®à¦¾à¦°à¦¾', '0000-00-00 00:00:00'),
(311, 60, 'Khoksa', 'à¦–à§‹à¦•à¦¸à¦¾', '0000-00-00 00:00:00'),
(312, 61, 'Magura Sadar ', 'à¦®à¦¾à¦—à§à¦°à¦¾ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(313, 61, 'Mohammadpur ', 'à¦®à§‹à¦¹à¦¾à¦®à§à¦®à¦¾à¦¦à¦ªà§à¦°', '0000-00-00 00:00:00'),
(314, 61, 'Shalikha ', 'à¦¶à¦¾à¦²à¦¿à¦–à¦¾', '0000-00-00 00:00:00'),
(315, 61, 'Sreepur ', 'à¦¶à§à¦°à§€à¦ªà§à¦°', '0000-00-00 00:00:00'),
(316, 62, 'angni ', 'à¦†à¦‚à¦¨à¦¿', '0000-00-00 00:00:00'),
(317, 62, 'Mujib Nagar ', 'à¦®à§à¦œà¦¿à¦¬ à¦¨à¦—à¦°', '0000-00-00 00:00:00'),
(318, 62, 'Meherpur-S ', 'à¦®à§‡à¦¹à§‡à¦°à¦ªà§à¦° à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(319, 63, 'Narail-S Upazilla', 'à¦¨à§œà¦¾à¦‡à¦² à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(320, 63, 'Lohagara Upazilla', 'à¦²à§‹à¦¹à¦¾à¦—à¦¾à§œà¦¾', '0000-00-00 00:00:00'),
(321, 63, 'Kalia Upazilla', 'à¦•à¦¾à¦²à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(322, 64, 'Satkhira Sadar ', 'à¦¸à¦¾à¦¤à¦•à§à¦·à§€à¦°à¦¾ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(323, 64, 'Assasuni ', 'à¦†à¦¸à¦¸à¦¾à¦¶à§à¦¨à¦¿ ', '0000-00-00 00:00:00'),
(324, 64, 'Debhata ', 'à¦¦à§‡à¦­à¦¾à¦Ÿà¦¾', '0000-00-00 00:00:00'),
(325, 64, 'Tala ', 'à¦¤à¦¾à¦²à¦¾', '0000-00-00 00:00:00'),
(326, 64, 'Kalaroa ', 'à¦•à¦²à¦°à§‹à§Ÿà¦¾', '0000-00-00 00:00:00'),
(327, 64, 'Kaliganj ', 'à¦•à¦¾à¦²à§€à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(328, 64, 'Shyamnagar ', 'à¦¶à§à¦¯à¦¾à¦®à¦¨à¦—à¦°', '0000-00-00 00:00:00'),
(329, 18, 'Adamdighi', 'à¦†à¦¦à¦®à¦¦à¦¿à¦˜à§€', '0000-00-00 00:00:00'),
(330, 18, 'Bogra Sadar', 'à¦¬à¦—à§à§œà¦¾ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(331, 18, 'Sherpur', 'à¦¶à§‡à¦°à¦ªà§à¦°', '0000-00-00 00:00:00'),
(332, 18, 'Dhunat', 'à¦§à§à¦¨à¦Ÿ', '0000-00-00 00:00:00'),
(333, 18, 'Dhupchanchia', 'à¦¦à§à¦ªà¦šà¦¾à¦šà¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(334, 18, 'Gabtali', 'à¦—à¦¾à¦¬à¦¤à¦²à¦¿', '0000-00-00 00:00:00'),
(335, 18, 'Kahaloo', 'à¦•à¦¾à¦¹à¦¾à¦²à§', '0000-00-00 00:00:00'),
(336, 18, 'Nandigram', 'à¦¨à¦¨à§à¦¦à¦¿à¦—à§à¦°à¦¾à¦®', '0000-00-00 00:00:00'),
(337, 18, 'Sahajanpur', 'à¦¶à¦¾à¦¹à¦œà¦¾à¦¹à¦¾à¦¨à¦ªà§à¦°', '0000-00-00 00:00:00'),
(338, 18, 'Sariakandi', 'à¦¸à¦¾à¦°à¦¿à§Ÿà¦¾à¦•à¦¾à¦¨à§à¦¦à¦¿', '0000-00-00 00:00:00'),
(339, 18, 'Shibganj', 'à¦¶à¦¿à¦¬à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(340, 18, 'Sonatala', 'à¦¸à§‹à¦¨à¦¾à¦¤à¦²à¦¾', '0000-00-00 00:00:00'),
(341, 19, 'Joypurhat S', 'à¦œà§Ÿà¦ªà§à¦°à¦¹à¦¾à¦Ÿ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(342, 19, 'Akkelpur', 'à¦†à¦•à§à¦•à§‡à¦²à¦ªà§à¦°', '0000-00-00 00:00:00'),
(343, 19, 'Kalai', 'à¦•à¦¾à¦²à¦¾à¦‡', '0000-00-00 00:00:00'),
(344, 19, 'Khetlal', 'à¦–à§‡à¦¤à¦²à¦¾à¦²', '0000-00-00 00:00:00'),
(345, 19, 'Panchbibi', 'à¦ªà¦¾à¦à¦šà¦¬à¦¿à¦¬à¦¿', '0000-00-00 00:00:00'),
(346, 20, 'Naogaon Sadar ', 'à¦¨à¦“à¦—à¦¾à¦ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(347, 20, 'Mohadevpur ', 'à¦®à¦¹à¦¾à¦¦à§‡à¦¬à¦ªà§à¦°', '0000-00-00 00:00:00'),
(348, 20, 'Manda ', 'à¦®à¦¾à¦¨à§à¦¦à¦¾', '0000-00-00 00:00:00'),
(349, 20, 'Niamatpur ', 'à¦¨à¦¿à§Ÿà¦¾à¦®à¦¤à¦ªà§à¦°', '0000-00-00 00:00:00'),
(350, 20, 'Atrai ', 'à¦†à¦¤à§à¦°à¦¾à¦‡', '0000-00-00 00:00:00'),
(351, 20, 'Raninagar ', 'à¦°à¦¾à¦£à§€à¦¨à¦—à¦°', '0000-00-00 00:00:00'),
(352, 20, 'Patnitala ', 'à¦ªà¦¤à§à¦¨à§€à¦¤à¦²à¦¾', '0000-00-00 00:00:00'),
(353, 20, 'Dhamoirhat ', 'à¦§à¦¾à¦®à¦‡à¦°à¦¹à¦¾à¦Ÿ ', '0000-00-00 00:00:00'),
(354, 20, 'Sapahar ', 'à¦¸à¦¾à¦ªà¦¾à¦¹à¦¾à¦°', '0000-00-00 00:00:00'),
(355, 20, 'Porsha ', 'à¦ªà§‹à¦°à¦¶à¦¾', '0000-00-00 00:00:00'),
(356, 20, 'Badalgachhi ', 'à¦¬à¦¦à¦²à¦—à¦¾à¦›à¦¿', '0000-00-00 00:00:00'),
(357, 21, 'Natore Sadar ', 'à¦¨à¦¾à¦Ÿà§‹à¦° à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(358, 21, 'Baraigram ', 'à¦¬à§œà¦¾à¦‡à¦—à§à¦°à¦¾à¦®', '0000-00-00 00:00:00'),
(359, 21, 'Bagatipara ', 'à¦¬à¦¾à¦—à¦¾à¦¤à¦¿à¦ªà¦¾à§œà¦¾', '0000-00-00 00:00:00'),
(360, 21, 'Lalpur ', 'à¦²à¦¾à¦²à¦ªà§à¦°', '0000-00-00 00:00:00'),
(361, 21, 'Natore Sadar ', 'à¦¨à¦¾à¦Ÿà§‹à¦° à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(362, 21, 'Baraigram ', 'à¦¬à§œà¦¾à¦‡ à¦—à§à¦°à¦¾à¦®', '0000-00-00 00:00:00'),
(363, 22, 'Bholahat ', 'à¦­à§‹à¦²à¦¾à¦¹à¦¾à¦Ÿ', '0000-00-00 00:00:00'),
(364, 22, 'Gomastapur ', 'à¦—à§‹à¦®à¦¸à§à¦¤à¦¾à¦ªà§à¦°', '0000-00-00 00:00:00'),
(365, 22, 'Nachole ', 'à¦¨à¦¾à¦šà§‹à¦²', '0000-00-00 00:00:00'),
(366, 22, 'Nawabganj Sadar ', 'à¦¨à¦¬à¦¾à¦¬à¦—à¦žà§à¦œ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(367, 22, 'Shibganj ', 'à¦¶à¦¿à¦¬à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(368, 23, 'Atgharia ', 'à¦†à¦Ÿà¦˜à¦°à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(369, 23, 'Bera ', 'à¦¬à§‡à§œà¦¾', '0000-00-00 00:00:00'),
(370, 23, 'Bhangura ', 'à¦­à¦¾à¦™à§à¦—à§à¦°à¦¾', '0000-00-00 00:00:00'),
(371, 23, 'Chatmohar ', 'à¦šà¦¾à¦Ÿà¦®à§‹à¦¹à¦°', '0000-00-00 00:00:00'),
(372, 23, 'Faridpur ', 'à¦«à¦°à¦¿à¦¦à¦ªà§à¦°', '0000-00-00 00:00:00'),
(373, 23, 'Ishwardi ', 'à¦ˆà¦¶à§à¦¬à¦°à¦¦à§€', '0000-00-00 00:00:00'),
(374, 23, 'Pabna Sadar ', 'à¦ªà¦¾à¦¬à¦¨à¦¾ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(375, 23, 'Santhia ', 'à¦¸à¦¾à¦¥à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(376, 23, 'Sujanagar ', 'à¦¸à§à¦œà¦¾à¦¨à¦—à¦°', '0000-00-00 00:00:00'),
(377, 24, 'Bagha', 'à¦¬à¦¾à¦˜à¦¾', '0000-00-00 00:00:00'),
(378, 24, 'Bagmara', 'à¦¬à¦¾à¦—à¦®à¦¾à¦°à¦¾', '0000-00-00 00:00:00'),
(379, 24, 'Charghat', 'à¦šà¦¾à¦°à¦˜à¦¾à¦Ÿ', '0000-00-00 00:00:00'),
(380, 24, 'Durgapur', 'à¦¦à§à¦°à§à¦—à¦¾à¦ªà§à¦°', '0000-00-00 00:00:00'),
(381, 24, 'Godagari', 'à¦—à§‹à¦¦à¦¾à¦—à¦¾à¦°à¦¿', '0000-00-00 00:00:00'),
(382, 24, 'Mohanpur', 'à¦®à§‹à¦¹à¦¨à¦ªà§à¦°', '0000-00-00 00:00:00'),
(383, 24, 'Paba', 'à¦ªà¦¬à¦¾', '0000-00-00 00:00:00'),
(384, 24, 'Puthia', 'à¦ªà§à¦ à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(385, 24, 'Tanore', 'à¦¤à¦¾à¦¨à§‹à¦°', '0000-00-00 00:00:00'),
(386, 25, 'Sirajganj Sadar ', 'à¦¸à¦¿à¦°à¦¾à¦œà¦—à¦žà§à¦œ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(387, 25, 'Belkuchi ', 'à¦¬à§‡à¦²à¦•à§à¦šà¦¿', '0000-00-00 00:00:00'),
(388, 25, 'Chauhali ', 'à¦šà§Œà¦¹à¦¾à¦²à¦¿', '0000-00-00 00:00:00'),
(389, 25, 'Kamarkhanda ', 'à¦•à¦¾à¦®à¦¾à¦°à¦–à¦¾à¦¨à§à¦¦à¦¾', '0000-00-00 00:00:00'),
(390, 25, 'Kazipur ', 'à¦•à¦¾à¦œà§€à¦ªà§à¦°', '0000-00-00 00:00:00'),
(391, 25, 'Raiganj ', 'à¦°à¦¾à§Ÿà¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(392, 25, 'Shahjadpur ', 'à¦¶à¦¾à¦¹à¦œà¦¾à¦¦à¦ªà§à¦°', '0000-00-00 00:00:00'),
(393, 25, 'Tarash ', 'à¦¤à¦¾à¦°à¦¾à¦¶', '0000-00-00 00:00:00'),
(394, 25, 'Ullahpara ', 'à¦‰à¦²à§à¦²à¦¾à¦ªà¦¾à§œà¦¾', '0000-00-00 00:00:00'),
(395, 26, 'Birampur ', 'à¦¬à¦¿à¦°à¦¾à¦®à¦ªà§à¦°', '0000-00-00 00:00:00'),
(396, 26, 'Birganj', 'à¦¬à§€à¦°à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(397, 26, 'Biral ', 'à¦¬à¦¿à§œà¦¾à¦²', '0000-00-00 00:00:00'),
(398, 26, 'Bochaganj ', 'à¦¬à§‹à¦šà¦¾à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(399, 26, 'Chirirbandar ', 'à¦šà¦¿à¦°à¦¿à¦°à¦¬à¦¨à§à¦¦à¦°', '0000-00-00 00:00:00'),
(400, 26, 'Phulbari ', 'à¦«à§à¦²à¦¬à¦¾à§œà¦¿', '0000-00-00 00:00:00'),
(401, 26, 'Ghoraghat ', 'à¦˜à§‹à§œà¦¾à¦˜à¦¾à¦Ÿ', '0000-00-00 00:00:00'),
(402, 26, 'Hakimpur ', 'à¦¹à¦¾à¦•à¦¿à¦®à¦ªà§à¦°', '0000-00-00 00:00:00'),
(403, 26, 'Kaharole ', 'à¦•à¦¾à¦¹à¦¾à¦°à§‹à¦²', '0000-00-00 00:00:00'),
(404, 26, 'Khansama ', 'à¦–à¦¾à¦¨à¦¸à¦¾à¦®à¦¾', '0000-00-00 00:00:00'),
(405, 26, 'Dinajpur Sadar ', 'à¦¦à¦¿à¦¨à¦¾à¦œà¦ªà§à¦° à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(406, 26, 'Nawabganj', 'à¦¨à¦¬à¦¾à¦¬à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(407, 26, 'Parbatipur ', 'à¦ªà¦¾à¦°à§à¦¬à¦¤à§€à¦ªà§à¦°', '0000-00-00 00:00:00'),
(408, 27, 'Fulchhari', 'à¦«à§à¦²à¦›à§œà¦¿', '0000-00-00 00:00:00'),
(409, 27, 'Gaibandha sadar', 'à¦—à¦¾à¦‡à¦¬à¦¾à¦¨à§à¦§à¦¾ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(410, 27, 'Gobindaganj', 'à¦—à§‹à¦¬à¦¿à¦¨à§à¦¦à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(411, 27, 'Palashbari', 'à¦ªà¦²à¦¾à¦¶à¦¬à¦¾à§œà§€', '0000-00-00 00:00:00'),
(412, 27, 'Sadullapur', 'à¦¸à¦¾à¦¦à§à¦²à§à¦¯à¦¾à¦ªà§à¦°', '0000-00-00 00:00:00'),
(413, 27, 'Saghata', 'à¦¸à¦¾à¦˜à¦¾à¦Ÿà¦¾', '0000-00-00 00:00:00'),
(414, 27, 'Sundarganj', 'à¦¸à§à¦¨à§à¦¦à¦°à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(415, 28, 'Kurigram Sadar', 'à¦•à§à§œà¦¿à¦—à§à¦°à¦¾à¦® à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(416, 28, 'Nageshwari', 'à¦¨à¦¾à¦—à§‡à¦¶à§à¦¬à¦°à§€', '0000-00-00 00:00:00'),
(417, 28, 'Bhurungamari', 'à¦­à§à¦°à§à¦™à§à¦—à¦¾à¦®à¦¾à¦°à¦¿', '0000-00-00 00:00:00'),
(418, 28, 'Phulbari', 'à¦«à§à¦²à¦¬à¦¾à§œà¦¿', '0000-00-00 00:00:00'),
(419, 28, 'Rajarhat', 'à¦°à¦¾à¦œà¦¾à¦°à¦¹à¦¾à¦Ÿ', '0000-00-00 00:00:00'),
(420, 28, 'Ulipur', 'à¦‰à¦²à¦¿à¦ªà§à¦°', '0000-00-00 00:00:00'),
(421, 28, 'Chilmari', 'à¦šà¦¿à¦²à¦®à¦¾à¦°à¦¿', '0000-00-00 00:00:00'),
(422, 28, 'Rowmari', 'à¦°à¦‰à¦®à¦¾à¦°à¦¿', '0000-00-00 00:00:00'),
(423, 28, 'Char Rajibpur', 'à¦šà¦° à¦°à¦¾à¦œà¦¿à¦¬à¦ªà§à¦°', '0000-00-00 00:00:00'),
(424, 29, 'Lalmanirhat Sadar', 'à¦²à¦¾à¦²à¦®à¦¨à¦¿à¦°à¦¹à¦¾à¦Ÿ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(425, 29, 'Aditmari', 'à¦†à¦¦à¦¿à¦¤à¦®à¦¾à¦°à¦¿', '0000-00-00 00:00:00'),
(426, 29, 'Kaliganj', 'à¦•à¦¾à¦²à§€à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(427, 29, 'Hatibandha', 'à¦¹à¦¾à¦¤à¦¿à¦¬à¦¾à¦¨à§à¦§à¦¾', '0000-00-00 00:00:00'),
(428, 29, 'Patgram', 'à¦ªà¦¾à¦Ÿà¦—à§à¦°à¦¾à¦®', '0000-00-00 00:00:00'),
(429, 30, 'Nilphamari Sadar', 'à¦¨à§€à¦²à¦«à¦¾à¦®à¦¾à¦°à§€ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(430, 30, 'Saidpur', 'à¦¸à§ˆà§Ÿà¦¦à¦ªà§à¦°', '0000-00-00 00:00:00'),
(431, 30, 'Jaldhaka', 'à¦œà¦²à¦¢à¦¾à¦•à¦¾', '0000-00-00 00:00:00'),
(432, 30, 'Kishoreganj', 'à¦•à¦¿à¦¶à§‹à¦°à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(433, 30, 'Domar', 'à¦¡à§‹à¦®à¦¾à¦°', '0000-00-00 00:00:00'),
(434, 30, 'Dimla', 'à¦¡à¦¿à¦®à¦²à¦¾', '0000-00-00 00:00:00'),
(435, 31, 'Panchagarh Sadar', 'à¦ªà¦žà§à¦šà¦—à§œ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(436, 31, 'Debiganj', 'à¦¦à§‡à¦¬à§€à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(437, 31, 'Boda', 'à¦¬à§‹à¦¦à¦¾', '0000-00-00 00:00:00'),
(438, 31, 'Atwari', 'à¦†à¦Ÿà§‹à§Ÿà¦¾à¦°à¦¿', '0000-00-00 00:00:00'),
(439, 31, 'Tetulia', 'à¦¤à§‡à¦¤à§à¦²à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(440, 32, 'Badarganj', 'à¦¬à¦¦à¦°à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(441, 32, 'Mithapukur', 'à¦®à¦¿à¦ à¦¾à¦ªà§à¦•à§à¦°', '0000-00-00 00:00:00'),
(442, 32, 'Gangachara', 'à¦—à¦™à§à¦—à¦¾à¦šà¦°à¦¾', '0000-00-00 00:00:00'),
(443, 32, 'Kaunia', 'à¦•à¦¾à¦‰à¦¨à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(444, 32, 'Rangpur Sadar', 'à¦°à¦‚à¦ªà§à¦° à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(445, 32, 'Pirgachha', 'à¦ªà§€à¦°à¦—à¦¾à¦›à¦¾', '0000-00-00 00:00:00'),
(446, 32, 'Pirganj', 'à¦ªà§€à¦°à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(447, 32, 'Taraganj', 'à¦¤à¦¾à¦°à¦¾à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(448, 33, 'Thakurgaon Sadar ', 'à¦ à¦¾à¦•à§à¦°à¦—à¦¾à¦à¦“ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(449, 33, 'Pirganj ', 'à¦ªà§€à¦°à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(450, 33, 'Baliadangi ', 'à¦¬à¦¾à¦²à¦¿à§Ÿà¦¾à¦¡à¦¾à¦™à§à¦—à¦¿', '0000-00-00 00:00:00'),
(451, 33, 'Haripur ', 'à¦¹à¦°à¦¿à¦ªà§à¦°', '0000-00-00 00:00:00'),
(452, 33, 'Ranisankail ', 'à¦°à¦¾à¦£à§€à¦¸à¦‚à¦•à¦‡à¦²', '0000-00-00 00:00:00'),
(453, 51, 'Ajmiriganj', 'à¦†à¦œà¦®à¦¿à¦°à¦¿à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(454, 51, 'Baniachang', 'à¦¬à¦¾à¦¨à¦¿à§Ÿà¦¾à¦šà¦‚', '0000-00-00 00:00:00'),
(455, 51, 'Bahubal', 'à¦¬à¦¾à¦¹à§à¦¬à¦²', '0000-00-00 00:00:00'),
(456, 51, 'Chunarughat', 'à¦šà§à¦¨à¦¾à¦°à§à¦˜à¦¾à¦Ÿ', '0000-00-00 00:00:00'),
(457, 51, 'Habiganj Sadar', 'à¦¹à¦¬à¦¿à¦—à¦žà§à¦œ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(458, 51, 'Lakhai', 'à¦²à¦¾à¦•à§à¦·à¦¾à¦‡', '0000-00-00 00:00:00'),
(459, 51, 'Madhabpur', 'à¦®à¦¾à¦§à¦¬à¦ªà§à¦°', '0000-00-00 00:00:00'),
(460, 51, 'Nabiganj', 'à¦¨à¦¬à§€à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(461, 51, 'Shaistagonj ', 'à¦¶à¦¾à§Ÿà§‡à¦¸à§à¦¤à¦¾à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(462, 52, 'Moulvibazar Sadar', 'à¦®à§Œà¦²à¦­à§€à¦¬à¦¾à¦œà¦¾à¦°', '0000-00-00 00:00:00'),
(463, 52, 'Barlekha', 'à¦¬à§œà¦²à§‡à¦–à¦¾', '0000-00-00 00:00:00'),
(464, 52, 'Juri', 'à¦œà§à§œà¦¿', '0000-00-00 00:00:00'),
(465, 52, 'Kamalganj', 'à¦•à¦¾à¦®à¦¾à¦²à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(466, 52, 'Kulaura', 'à¦•à§à¦²à¦¾à¦‰à¦°à¦¾', '0000-00-00 00:00:00'),
(467, 52, 'Rajnagar', 'à¦°à¦¾à¦œà¦¨à¦—à¦°', '0000-00-00 00:00:00'),
(468, 52, 'Sreemangal', 'à¦¶à§à¦°à§€à¦®à¦™à§à¦—à¦²', '0000-00-00 00:00:00'),
(469, 53, 'Bishwamvarpur', 'à¦¬à¦¿à¦¸à¦¶à¦®à§à¦­à¦¾à¦°à¦ªà§à¦°', '0000-00-00 00:00:00'),
(470, 53, 'Chhatak', 'à¦›à¦¾à¦¤à¦•', '0000-00-00 00:00:00'),
(471, 53, 'Derai', 'à¦¦à§‡à§œà¦¾à¦‡', '0000-00-00 00:00:00'),
(472, 53, 'Dharampasha', 'à¦§à¦°à¦®à¦ªà¦¾à¦¶à¦¾', '0000-00-00 00:00:00'),
(473, 53, 'Dowarabazar', 'à¦¦à§‹à§Ÿà¦¾à¦°à¦¾à¦¬à¦¾à¦œà¦¾à¦°', '0000-00-00 00:00:00'),
(474, 53, 'Jagannathpur', 'à¦œà¦—à¦¨à§à¦¨à¦¾à¦¥à¦ªà§à¦°', '0000-00-00 00:00:00'),
(475, 53, 'Jamalganj', 'à¦œà¦¾à¦®à¦¾à¦²à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(476, 53, 'Sulla', 'à¦¸à§à¦²à§à¦²à¦¾', '0000-00-00 00:00:00'),
(477, 53, 'Sunamganj Sadar', 'à¦¸à§à¦¨à¦¾à¦®à¦—à¦žà§à¦œ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(478, 53, 'Shanthiganj', 'à¦¶à¦¾à¦¨à§à¦¤à¦¿à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(479, 53, 'Tahirpur', 'à¦¤à¦¾à¦¹à¦¿à¦°à¦ªà§à¦°', '0000-00-00 00:00:00'),
(480, 54, 'Sylhet Sadar', 'à¦¸à¦¿à¦²à§‡à¦Ÿ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(481, 54, 'Beanibazar', 'à¦¬à§‡à§Ÿà¦¾à¦¨à¦¿à¦¬à¦¾à¦œà¦¾à¦°', '0000-00-00 00:00:00'),
(482, 54, 'Bishwanath', 'à¦¬à¦¿à¦¶à§à¦¬à¦¨à¦¾à¦¥', '0000-00-00 00:00:00'),
(483, 54, 'Dakshin Surma ', 'à¦¦à¦•à§à¦·à¦¿à¦£ à¦¸à§à¦°à¦®à¦¾', '0000-00-00 00:00:00'),
(484, 54, 'Balaganj', 'à¦¬à¦¾à¦²à¦¾à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(485, 54, 'Companiganj', 'à¦•à§‹à¦®à§à¦ªà¦¾à¦¨à¦¿à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(486, 54, 'Fenchuganj', 'à¦«à§‡à¦žà§à¦šà§à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(487, 54, 'Golapganj', 'à¦—à§‹à¦²à¦¾à¦ªà¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(488, 54, 'Gowainghat', 'à¦—à§‹à§Ÿà¦¾à¦‡à¦¨à¦˜à¦¾à¦Ÿ', '0000-00-00 00:00:00'),
(489, 54, 'Jaintiapur', 'à¦œà§Ÿà¦¨à§à¦¤à¦ªà§à¦°', '0000-00-00 00:00:00'),
(490, 54, 'Kanaighat', 'à¦•à¦¾à¦¨à¦¾à¦‡à¦˜à¦¾à¦Ÿ', '0000-00-00 00:00:00'),
(491, 54, 'Zakiganj', 'à¦œà¦¾à¦•à¦¿à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(492, 54, 'Nobigonj', 'à¦¨à¦¬à§€à¦—à¦žà§à¦œ', '0000-00-00 00:00:00'),
(493, 1, 'Adabor', NULL, '0000-00-00 00:00:00'),
(494, 1, 'Airport', NULL, '0000-00-00 00:00:00'),
(495, 1, 'Badda', NULL, '0000-00-00 00:00:00'),
(496, 1, 'Banani', NULL, '0000-00-00 00:00:00'),
(497, 1, 'Bangshal', NULL, '0000-00-00 00:00:00'),
(498, 1, 'Bhashantek', NULL, '0000-00-00 00:00:00'),
(499, 1, 'Cantonment', NULL, '0000-00-00 00:00:00'),
(500, 1, 'Chackbazar', NULL, '0000-00-00 00:00:00'),
(501, 1, 'Darussalam', NULL, '0000-00-00 00:00:00'),
(502, 1, 'Daskhinkhan', NULL, '0000-00-00 00:00:00'),
(503, 1, 'Demra', NULL, '0000-00-00 00:00:00'),
(504, 1, 'Dhamrai', NULL, '0000-00-00 00:00:00'),
(505, 1, 'Dhanmondi', NULL, '0000-00-00 00:00:00'),
(506, 1, 'Dohar', NULL, '0000-00-00 00:00:00'),
(507, 1, 'Gandaria', NULL, '0000-00-00 00:00:00'),
(508, 1, 'Gulshan', NULL, '0000-00-00 00:00:00'),
(509, 1, 'Hazaribag', NULL, '0000-00-00 00:00:00'),
(510, 1, 'Jatrabari', NULL, '0000-00-00 00:00:00'),
(511, 1, 'Kafrul', NULL, '0000-00-00 00:00:00'),
(512, 1, 'Kalabagan', NULL, '0000-00-00 00:00:00'),
(513, 1, 'Kamrangirchar', NULL, '0000-00-00 00:00:00'),
(514, 1, 'Keraniganj', NULL, '0000-00-00 00:00:00'),
(515, 1, 'Khilgaon', NULL, '0000-00-00 00:00:00'),
(516, 1, 'Khilkhet', NULL, '0000-00-00 00:00:00'),
(517, 1, 'Kotwali', NULL, '0000-00-00 00:00:00'),
(518, 1, 'Lalbag', NULL, '0000-00-00 00:00:00'),
(519, 1, 'Mirpur Model', NULL, '0000-00-00 00:00:00'),
(520, 1, 'Mohammadpur', NULL, '0000-00-00 00:00:00'),
(521, 1, 'Motijheel', NULL, '0000-00-00 00:00:00'),
(522, 1, 'Mugda', NULL, '0000-00-00 00:00:00'),
(523, 1, 'Nawabganj', NULL, '0000-00-00 00:00:00'),
(524, 1, 'New Market', NULL, '0000-00-00 00:00:00'),
(525, 1, 'Pallabi', NULL, '0000-00-00 00:00:00'),
(526, 1, 'Paltan', NULL, '0000-00-00 00:00:00'),
(527, 1, 'Ramna', NULL, '0000-00-00 00:00:00'),
(528, 1, 'Rampura', NULL, '0000-00-00 00:00:00'),
(529, 1, 'Rupnagar', NULL, '0000-00-00 00:00:00'),
(530, 1, 'Sabujbag', NULL, '0000-00-00 00:00:00'),
(531, 1, 'Savar', NULL, '0000-00-00 00:00:00'),
(532, 1, 'Shah Ali', NULL, '0000-00-00 00:00:00'),
(533, 1, 'Shahbag', NULL, '0000-00-00 00:00:00'),
(534, 1, 'Shahjahanpur', NULL, '0000-00-00 00:00:00'),
(535, 1, 'Sherebanglanagar', NULL, '0000-00-00 00:00:00'),
(536, 1, 'Shyampur', NULL, '0000-00-00 00:00:00'),
(537, 1, 'Sutrapur', NULL, '0000-00-00 00:00:00'),
(538, 1, 'Tejgaon', NULL, '0000-00-00 00:00:00'),
(539, 1, 'Tejgaon I/A', NULL, '0000-00-00 00:00:00'),
(540, 1, 'Turag', NULL, '0000-00-00 00:00:00'),
(541, 1, 'Uttara', NULL, '0000-00-00 00:00:00'),
(542, 1, 'Uttara West', NULL, '0000-00-00 00:00:00'),
(543, 1, 'Uttarkhan', NULL, '0000-00-00 00:00:00'),
(544, 1, 'Vatara', NULL, '0000-00-00 00:00:00'),
(545, 1, 'Wari', NULL, '0000-00-00 00:00:00'),
(546, 1, 'Others', NULL, '0000-00-00 00:00:00'),
(547, 35, 'Airport', 'à¦à§Ÿà¦¾à¦°à¦ªà§‹à¦°à§à¦Ÿ', '0000-00-00 00:00:00'),
(548, 35, 'Kawnia', 'à¦•à¦¾à¦‰à¦¨à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(549, 35, 'Bondor', 'à¦¬à¦¨à§à¦¦à¦°', '0000-00-00 00:00:00'),
(550, 35, 'Others', 'à¦…à¦¨à§à¦¯à¦¾à¦¨à§à¦¯', '0000-00-00 00:00:00'),
(551, 24, 'Boalia', 'à¦¬à§‹à¦¯à¦¼à¦¾à¦²à¦¿à¦¯à¦¼à¦¾', '0000-00-00 00:00:00'),
(552, 24, 'Motihar', 'à¦®à¦¤à¦¿à¦¹à¦¾à¦°', '0000-00-00 00:00:00'),
(553, 24, 'Shahmokhdum', 'à¦¶à¦¾à¦¹à§ à¦®à¦•à¦–à¦¦à§à¦® ', '0000-00-00 00:00:00'),
(554, 24, 'Rajpara', 'à¦°à¦¾à¦œà¦ªà¦¾à¦°à¦¾ ', '0000-00-00 00:00:00'),
(555, 24, 'Others', 'à¦…à¦¨à§à¦¯à¦¾à¦¨à§à¦¯', '0000-00-00 00:00:00'),
(556, 43, 'Akborsha', 'Akborsha', '0000-00-00 00:00:00'),
(557, 43, 'Baijid bostami', 'à¦¬à¦¾à¦‡à¦œà¦¿à¦¦ à¦¬à§‹à¦¸à§à¦¤à¦¾à¦®à§€', '0000-00-00 00:00:00'),
(558, 43, 'Bakolia', 'à¦¬à¦¾à¦•à§‹à¦²à¦¿à§Ÿà¦¾', '0000-00-00 00:00:00'),
(559, 43, 'Bandar', 'à¦¬à¦¨à§à¦¦à¦°', '0000-00-00 00:00:00'),
(560, 43, 'Chandgaon', 'à¦šà¦¾à¦à¦¦à¦—à¦¾à¦“', '0000-00-00 00:00:00'),
(561, 43, 'Chokbazar', 'à¦šà¦•à¦¬à¦¾à¦œà¦¾à¦°', '0000-00-00 00:00:00'),
(562, 43, 'Doublemooring', 'à¦¡à¦¾à¦¬à¦² à¦®à§à¦°à¦¿à¦‚', '0000-00-00 00:00:00'),
(563, 43, 'EPZ', 'à¦‡à¦ªà¦¿à¦œà§‡à¦¡', '0000-00-00 00:00:00'),
(564, 43, 'Hali Shohor', 'à¦¹à¦²à§€ à¦¶à¦¹à¦°', '0000-00-00 00:00:00'),
(565, 43, 'Kornafuli', 'à¦•à¦°à§à¦£à¦«à§à¦²à¦¿', '0000-00-00 00:00:00'),
(566, 43, 'Kotwali', 'à¦•à§‹à¦¤à§‹à¦¯à¦¼à¦¾à¦²à§€', '0000-00-00 00:00:00'),
(567, 43, 'Kulshi', 'à¦•à§à¦²à¦¶à¦¿', '0000-00-00 00:00:00'),
(568, 43, 'Pahartali', 'à¦ªà¦¾à¦¹à¦¾à¦¡à¦¼à¦¤à¦²à§€', '0000-00-00 00:00:00'),
(569, 43, 'Panchlaish', 'à¦ªà¦¾à¦à¦šà¦²à¦¾à¦‡à¦¶', '0000-00-00 00:00:00'),
(570, 43, 'Potenga', 'à¦ªà¦¤à§‡à¦™à§à¦—à¦¾', '0000-00-00 00:00:00'),
(571, 43, 'Shodhorgat', 'à¦¸à¦¦à¦°à¦˜à¦¾à¦Ÿ', '0000-00-00 00:00:00'),
(572, 43, 'Others', 'à¦…à¦¨à§à¦¯à¦¾à¦¨à§à¦¯', '0000-00-00 00:00:00'),
(573, 44, 'Others', 'à¦…à¦¨à§à¦¯à¦¾à¦¨à§à¦¯', '0000-00-00 00:00:00'),
(574, 59, 'Aranghata', 'à¦†à¦¡à¦¼à¦¾à¦‚à¦˜à¦¾à¦Ÿà¦¾', '0000-00-00 00:00:00'),
(575, 59, 'Daulatpur', 'à¦¦à§Œà¦²à¦¤à¦ªà§à¦°', '0000-00-00 00:00:00'),
(576, 59, 'Harintana', 'à¦¹à¦¾à¦°à¦¿à¦¨à§à¦¤à¦¾à¦¨à¦¾ ', '0000-00-00 00:00:00'),
(577, 59, 'Horintana', 'à¦¹à¦°à¦¿à¦£à¦¤à¦¾à¦¨à¦¾ ', '0000-00-00 00:00:00'),
(578, 59, 'Khalishpur', 'à¦–à¦¾à¦²à¦¿à¦¶à¦ªà§à¦°', '0000-00-00 00:00:00'),
(579, 59, 'Khanjahan Ali', 'à¦–à¦¾à¦¨à¦œà¦¾à¦¹à¦¾à¦¨ à¦†à¦²à§€', '0000-00-00 00:00:00'),
(580, 59, 'Khulna Sadar', 'à¦–à§à¦²à¦¨à¦¾ à¦¸à¦¦à¦°', '0000-00-00 00:00:00'),
(581, 59, 'Labanchora', 'à¦²à¦¾à¦¬à¦¾à¦¨à¦›à§‹à¦°à¦¾', '0000-00-00 00:00:00'),
(582, 59, 'Sonadanga', 'à¦¸à§‹à¦¨à¦¾à¦¡à¦¾à¦™à§à¦—à¦¾', '0000-00-00 00:00:00'),
(583, 59, 'Others', 'à¦…à¦¨à§à¦¯à¦¾à¦¨à§à¦¯', '0000-00-00 00:00:00'),
(584, 2, 'Others', 'à¦…à¦¨à§à¦¯à¦¾à¦¨à§à¦¯', '0000-00-00 00:00:00'),
(585, 4, 'Others', 'à¦…à¦¨à§à¦¯à¦¾à¦¨à§à¦¯', '0000-00-00 00:00:00'),
(586, 5, 'Others', 'à¦…à¦¨à§à¦¯à¦¾à¦¨à§à¦¯', '0000-00-00 00:00:00'),
(587, 54, 'Airport', 'à¦¬à¦¿à¦®à¦¾à¦¨à¦¬à¦¨à§à¦¦à¦°', '0000-00-00 00:00:00'),
(588, 54, 'Hazrat Shah Paran', 'à¦¹à¦¯à¦°à¦¤ à¦¶à¦¾à¦¹ à¦ªà¦°à¦¾à¦£', '0000-00-00 00:00:00'),
(589, 54, 'Jalalabad', 'à¦œà¦¾à¦²à¦¾à¦²à¦¾à¦¬à¦¾à¦¦', '0000-00-00 00:00:00'),
(590, 54, 'Kowtali', 'à¦•à§‹à¦¤à§‹à¦¯à¦¼à¦¾à¦²à§€', '0000-00-00 00:00:00'),
(591, 54, 'Moglabazar', 'à¦®à§‹à¦—à¦²à¦¾à¦¬à¦¾à¦œà¦¾à¦°', '0000-00-00 00:00:00'),
(592, 54, 'Osmani Nagar', 'à¦“à¦¸à¦®à¦¾à¦¨à§€ à¦¨à¦—à¦°', '0000-00-00 00:00:00'),
(593, 54, 'South Surma', 'à¦¦à¦•à§à¦·à¦¿à¦£ à¦¸à§à¦°à¦®à¦¾', '0000-00-00 00:00:00'),
(594, 54, 'Others', 'à¦…à¦¨à§à¦¯à¦¾à¦¨à§à¦¯', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(14) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('Admin','Vendor','Customer','Moderator','Courier') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `resetcode` int DEFAULT NULL,
  `user_image` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('1','0') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'active = 1, inactive = 0',
  `created_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_by` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile`, `address`, `password`, `type`, `resetcode`, `user_image`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Raju', 'rbraju3m@gmail.com', '01729762344', 'Uttara', '$2y$10$iSa4qHnQQKkDS.s0lcFnXuLV4qaPgOGmAf/FtilTLmLOGfySNyEU.', 'Admin', NULL, NULL, '1', NULL, '1', NULL, '2021-11-08 18:35:38'),
(24, 'Salma Collection', 'salma@rightbrainsolution.com', '0176890876', 'Uttara', '$2y$10$iSa4qHnQQKkDS.s0lcFnXuLV4qaPgOGmAf/FtilTLmLOGfySNyEU.', 'Vendor', NULL, 'salma-collection1632729006.jpg', '1', '1', '24', '2021-09-27 07:50:06', '2021-11-08 18:36:06'),
(25, 'Sohel Rana', 'sohel@rightbrainsolution.com', '01765432111', 'Sector 06, Uttara', '$2y$10$iSa4qHnQQKkDS.s0lcFnXuLV4qaPgOGmAf/FtilTLmLOGfySNyEU.', 'Vendor', NULL, 'defaultuser.png', '1', '1', NULL, '2021-09-27 07:54:54', '2021-09-27 07:54:54'),
(26, 'Shahib', 'shihab@rightbrainsolution.com', '01987674523', 'Dhanmondi ,Dhaka', '$2y$10$iSa4qHnQQKkDS.s0lcFnXuLV4qaPgOGmAf/FtilTLmLOGfySNyEU.', 'Vendor', NULL, 'shahib1632729644.jpg', '1', '1', NULL, '2021-09-27 08:00:44', '2021-09-27 08:00:44'),
(27, 'RB Raju', 'raju.aspiretss@gmail.com', '0198767452', 'PUL Tower', '$2y$10$iSa4qHnQQKkDS.s0lcFnXuLV4qaPgOGmAf/FtilTLmLOGfySNyEU.', 'Vendor', NULL, 'defaultuser.png', '1', '1', NULL, '2021-09-27 08:03:43', '2021-09-27 08:03:43'),
(37, 'Nice Coder', 'nice.coder3m@gmail.com', '01987674529', 'uttara', '$2y$10$iSa4qHnQQKkDS.s0lcFnXuLV4qaPgOGmAf/FtilTLmLOGfySNyEU.', 'Customer', 892148, 'nice-coder1632831871.jpg', '1', '1', NULL, '2021-09-28 12:24:31', '2022-03-25 02:03:48'),
(41, 'Aspire tss', 'rashedulraju3m@gmail.com', '01890543267', 'Street address , Boalmari  , Faridpur , Bangladesh', '$2y$10$iSa4qHnQQKkDS.s0lcFnXuLV4qaPgOGmAf/FtilTLmLOGfySNyEU.', 'Customer', 467489, 'defaultuser.png', '1', '1', NULL, '2021-09-29 05:59:06', '2022-06-22 10:19:45'),
(43, 'Kalio Mark11', 'nice.coder13m@gmail.com', '019876745231', 'fffdfds', '$2y$10$iSa4qHnQQKkDS.s0lcFnXuLV4qaPgOGmAf/FtilTLmLOGfySNyEU.', 'Vendor', NULL, 'defaultuser.png', '1', '1', '1', '2021-10-04 07:51:20', '2021-10-04 08:00:49'),
(45, '2021 Men\'s Lifestyle', 'rashedul.rightbrainsolution@gmail.com', '01987674521', 'uttara', '$2y$10$iSa4qHnQQKkDS.s0lcFnXuLV4qaPgOGmAf/FtilTLmLOGfySNyEU.', 'Customer', NULL, '2021-men\'s-lifestyle1636365114.png', '1', NULL, NULL, '2021-11-08 15:51:54', '2021-11-08 15:51:54'),
(46, 'Salma Sultana', 'salma.sultana9811@gmail.com', '01790860543', 'Uttara', '$2y$10$iSa4qHnQQKkDS.s0lcFnXuLV4qaPgOGmAf/FtilTLmLOGfySNyEU.', 'Customer', NULL, 'salma-sultana1636434552.jpg', '1', NULL, '46', '2021-11-09 11:09:12', '2021-11-14 15:24:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_banner`
--
ALTER TABLE `ems_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_banner_items`
--
ALTER TABLE `ems_banner_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_banner_items_banner_id_foreign` (`banner_id`),
  ADD KEY `ems_banner_items_brand_id_foreign` (`brand_id`),
  ADD KEY `ems_banner_items_category_id_foreign` (`category_id`);

--
-- Indexes for table `ems_category`
--
ALTER TABLE `ems_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_category_category_parent_id_foreign` (`category_parent_id`);

--
-- Indexes for table `ems_color`
--
ALTER TABLE `ems_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_country`
--
ALTER TABLE `ems_country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_customer`
--
ALTER TABLE `ems_customer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_customer_user_id_foreign` (`user_id`),
  ADD KEY `bill_country_id` (`bill_country_id`),
  ADD KEY `bill_district_id` (`bill_district_id`),
  ADD KEY `bill_upazila_id` (`bill_upazila_id`),
  ADD KEY `ship_country_id` (`ship_country_id`),
  ADD KEY `ship_district_id` (`ship_district_id`),
  ADD KEY `ship_upazila_id` (`ship_upazila_id`);

--
-- Indexes for table `ems_delivery_time_slot`
--
ALTER TABLE `ems_delivery_time_slot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_discount`
--
ALTER TABLE `ems_discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_discount_brand`
--
ALTER TABLE `ems_discount_brand`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_discount_brand_discount_id_foreign` (`discount_id`),
  ADD KEY `ems_discount_brand_brand_id_foreign` (`brand_id`);

--
-- Indexes for table `ems_discount_category`
--
ALTER TABLE `ems_discount_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_discount_category_discount_id_foreign` (`discount_id`),
  ADD KEY `ems_discount_category_category_id_foreign` (`category_id`);

--
-- Indexes for table `ems_generalpage`
--
ALTER TABLE `ems_generalpage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_general_setting`
--
ALTER TABLE `ems_general_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_itemassurance`
--
ALTER TABLE `ems_itemassurance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_itemunit`
--
ALTER TABLE `ems_itemunit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_order_billing_shipping`
--
ALTER TABLE `ems_order_billing_shipping`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_order_billing_shipping_order_id_foreign` (`order_id`),
  ADD KEY `ems_order_billing_shipping_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `ems_order_head`
--
ALTER TABLE `ems_order_head`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_order_head_time_slot_id_foreign` (`time_slot_id`),
  ADD KEY `ems_order_head_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `ems_order_item`
--
ALTER TABLE `ems_order_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_order_item_order_id_foreign` (`order_id`),
  ADD KEY `ems_order_item_product_id_foreign` (`product_id`),
  ADD KEY `ems_order_item_vendor_id_foreign` (`vendor_id`),
  ADD KEY `ems_order_item_user_id_foreign` (`user_id`);

--
-- Indexes for table `ems_product`
--
ALTER TABLE `ems_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_product_brand_id_foreign` (`brand_id`),
  ADD KEY `ems_product_category_id_foreign` (`category_id`),
  ADD KEY `ems_product_item_unit_id_foreign` (`item_unit_id`),
  ADD KEY `ems_product_size_unit_id_foreign` (`size_unit_id`),
  ADD KEY `ems_product_size_weight_dimen_id_foreign` (`size_weight_dimen_id`),
  ADD KEY `ems_product_country_id_foreign` (`country_id`),
  ADD KEY `ems_product_discount_id_foreign` (`discount_id`),
  ADD KEY `ems_product_warning_id_foreign` (`warning_id`),
  ADD KEY `ems_product_item_assurance_id_foreign` (`item_assurance_id`),
  ADD KEY `ems_product_promotion_id_foreign` (`promotion_id`),
  ADD KEY `vendor_id` (`vendor_id`);

--
-- Indexes for table `ems_product_additionalfield`
--
ALTER TABLE `ems_product_additionalfield`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_product_additionalfield_product_id_foreign` (`product_id`);

--
-- Indexes for table `ems_product_brand`
--
ALTER TABLE `ems_product_brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_product_colors`
--
ALTER TABLE `ems_product_colors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_product_colors_product_id_foreign` (`product_id`),
  ADD KEY `ems_product_colors_color_id_foreign` (`color_id`);

--
-- Indexes for table `ems_product_image`
--
ALTER TABLE `ems_product_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_product_image_product_id_foreign` (`product_id`);

--
-- Indexes for table `ems_product_tags`
--
ALTER TABLE `ems_product_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_product_tags_product_id_foreign` (`product_id`),
  ADD KEY `ems_product_tags_tag_id_foreign` (`tag_id`);

--
-- Indexes for table `ems_promotion`
--
ALTER TABLE `ems_promotion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_sizeunit`
--
ALTER TABLE `ems_sizeunit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_sizeweightdimension`
--
ALTER TABLE `ems_sizeweightdimension`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_slider`
--
ALTER TABLE `ems_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ems_vendor`
--
ALTER TABLE `ems_vendor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ems_vendor_user_id_foreign` (`user_id`);

--
-- Indexes for table `ems_warningtype`
--
ALTER TABLE `ems_warningtype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `fileimport`
--
ALTER TABLE `fileimport`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `upazilas`
--
ALTER TABLE `upazilas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `mobile` (`mobile`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=253;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `ems_banner`
--
ALTER TABLE `ems_banner`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ems_banner_items`
--
ALTER TABLE `ems_banner_items`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `ems_category`
--
ALTER TABLE `ems_category`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `ems_color`
--
ALTER TABLE `ems_color`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ems_country`
--
ALTER TABLE `ems_country`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ems_customer`
--
ALTER TABLE `ems_customer`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `ems_delivery_time_slot`
--
ALTER TABLE `ems_delivery_time_slot`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ems_discount`
--
ALTER TABLE `ems_discount`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `ems_discount_brand`
--
ALTER TABLE `ems_discount_brand`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `ems_discount_category`
--
ALTER TABLE `ems_discount_category`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `ems_generalpage`
--
ALTER TABLE `ems_generalpage`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `ems_general_setting`
--
ALTER TABLE `ems_general_setting`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ems_itemassurance`
--
ALTER TABLE `ems_itemassurance`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ems_itemunit`
--
ALTER TABLE `ems_itemunit`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `ems_order_billing_shipping`
--
ALTER TABLE `ems_order_billing_shipping`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT for table `ems_order_head`
--
ALTER TABLE `ems_order_head`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `ems_order_item`
--
ALTER TABLE `ems_order_item`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `ems_product`
--
ALTER TABLE `ems_product`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `ems_product_additionalfield`
--
ALTER TABLE `ems_product_additionalfield`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `ems_product_brand`
--
ALTER TABLE `ems_product_brand`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `ems_product_colors`
--
ALTER TABLE `ems_product_colors`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `ems_product_image`
--
ALTER TABLE `ems_product_image`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `ems_product_tags`
--
ALTER TABLE `ems_product_tags`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `ems_promotion`
--
ALTER TABLE `ems_promotion`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ems_sizeunit`
--
ALTER TABLE `ems_sizeunit`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `ems_sizeweightdimension`
--
ALTER TABLE `ems_sizeweightdimension`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ems_slider`
--
ALTER TABLE `ems_slider`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ems_vendor`
--
ALTER TABLE `ems_vendor`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ems_warningtype`
--
ALTER TABLE `ems_warningtype`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fileimport`
--
ALTER TABLE `fileimport`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `upazilas`
--
ALTER TABLE `upazilas`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=595;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ems_banner_items`
--
ALTER TABLE `ems_banner_items`
  ADD CONSTRAINT `ems_banner_items_banner_id_foreign` FOREIGN KEY (`banner_id`) REFERENCES `ems_banner` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_banner_items_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `ems_product_brand` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_banner_items_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `ems_category` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ems_category`
--
ALTER TABLE `ems_category`
  ADD CONSTRAINT `ems_category_category_parent_id_foreign` FOREIGN KEY (`category_parent_id`) REFERENCES `ems_category` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ems_customer`
--
ALTER TABLE `ems_customer`
  ADD CONSTRAINT `ems_customer_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ems_discount_brand`
--
ALTER TABLE `ems_discount_brand`
  ADD CONSTRAINT `ems_discount_brand_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `ems_product_brand` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_discount_brand_discount_id_foreign` FOREIGN KEY (`discount_id`) REFERENCES `ems_discount` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ems_discount_category`
--
ALTER TABLE `ems_discount_category`
  ADD CONSTRAINT `ems_discount_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `ems_category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_discount_category_discount_id_foreign` FOREIGN KEY (`discount_id`) REFERENCES `ems_discount` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ems_order_billing_shipping`
--
ALTER TABLE `ems_order_billing_shipping`
  ADD CONSTRAINT `ems_order_billing_shipping_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `ems_customer` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_order_billing_shipping_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `ems_order_head` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ems_order_head`
--
ALTER TABLE `ems_order_head`
  ADD CONSTRAINT `ems_order_head_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `ems_customer` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_order_head_time_slot_id_foreign` FOREIGN KEY (`time_slot_id`) REFERENCES `ems_delivery_time_slot` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ems_order_item`
--
ALTER TABLE `ems_order_item`
  ADD CONSTRAINT `ems_order_item_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `ems_order_head` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_order_item_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ems_product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_order_item_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_order_item_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `ems_vendor` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ems_product`
--
ALTER TABLE `ems_product`
  ADD CONSTRAINT `ems_product_brand_id_foreign` FOREIGN KEY (`brand_id`) REFERENCES `ems_product_brand` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `ems_category` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `ems_country` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_discount_id_foreign` FOREIGN KEY (`discount_id`) REFERENCES `ems_discount` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_item_assurance_id_foreign` FOREIGN KEY (`item_assurance_id`) REFERENCES `ems_itemassurance` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_item_unit_id_foreign` FOREIGN KEY (`item_unit_id`) REFERENCES `ems_itemunit` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_promotion_id_foreign` FOREIGN KEY (`promotion_id`) REFERENCES `ems_promotion` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_size_unit_id_foreign` FOREIGN KEY (`size_unit_id`) REFERENCES `ems_sizeunit` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_size_weight_dimen_id_foreign` FOREIGN KEY (`size_weight_dimen_id`) REFERENCES `ems_sizeweightdimension` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_warning_id_foreign` FOREIGN KEY (`warning_id`) REFERENCES `ems_warningtype` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ems_product_additionalfield`
--
ALTER TABLE `ems_product_additionalfield`
  ADD CONSTRAINT `ems_product_additionalfield_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ems_product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ems_product_colors`
--
ALTER TABLE `ems_product_colors`
  ADD CONSTRAINT `ems_product_colors_color_id_foreign` FOREIGN KEY (`color_id`) REFERENCES `ems_color` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_colors_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ems_product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ems_product_image`
--
ALTER TABLE `ems_product_image`
  ADD CONSTRAINT `ems_product_image_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ems_product` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ems_product_tags`
--
ALTER TABLE `ems_product_tags`
  ADD CONSTRAINT `ems_product_tags_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `ems_product` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ems_product_tags_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `ems_promotion` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ems_vendor`
--
ALTER TABLE `ems_vendor`
  ADD CONSTRAINT `ems_vendor_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
