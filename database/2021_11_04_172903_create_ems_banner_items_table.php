<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmsBannerItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ems_banner_items', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('banner_id')->nullable();
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();

            $table->string('banner_type',40)->nullable();
            $table->string('title_top',40)->nullable();
            $table->string('title',100)->nullable();
            $table->string('attach_link',50)->nullable();

            $table->enum('status',array('1','0'))->nullable()->comment('active = 1, inactive = 0');

            $table->string('created_by',10)->nullable();
            $table->string('updated_by',10)->nullable();
            $table->timestamps();
            $table->engine= 'InnoDB';

            $table->foreign('banner_id')
                ->references('id')->on('ems_banner')
                ->onDelete('cascade');

            $table->foreign('brand_id')
                ->references('id')->on('ems_product_brand')
                ->onDelete('cascade');

            $table->foreign('category_id')
                ->references('id')->on('ems_category')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ems_banner_items');
    }
}
